<?php

define('ENVIRONMENT', isset($_SERVER['CI_ENV']) ? $_SERVER['CI_ENV'] : 'development');


    $system_path = 'system';
    $application_folder = 'apps/default';
    $view_folder = '';


    // Set the current directory correctly for CLI requests
    if (defined('STDIN'))
    {
        chdir(dirname(__FILE__));
    }

    if (($_temp = realpath($system_path)) !== FALSE)
    {
        $system_path = $_temp.DIRECTORY_SEPARATOR;
    }
    else
    {
        // Ensure there's a trailing slash
        $system_path = strtr(
            rtrim($system_path, '/\\'),
            '/\\',
            DIRECTORY_SEPARATOR.DIRECTORY_SEPARATOR
        ).DIRECTORY_SEPARATOR;
    }

    // Is the system path correct?
    if ( ! is_dir($system_path))
    {
        header('HTTP/1.1 503 Service Unavailable.', TRUE, 503);
        echo 'Your system folder path does not appear to be set correctly. Please open the following file and correct this: '.pathinfo(__FILE__, PATHINFO_BASENAME);
        exit(3); // EXIT_CONFIG
    }

/*
 * -------------------------------------------------------------------
 *  Now that we know the path, set the main path constants
 * -------------------------------------------------------------------
 */
    // The name of THIS file
    define('SELF', pathinfo(__FILE__, PATHINFO_BASENAME));

    // Path to the system directory
    define('BASEPATH', $system_path);

    // Path to the front controller (this file) directory
    define('FCPATH', dirname(__FILE__).DIRECTORY_SEPARATOR);

    // Name of the "system" directory
    define('SYSDIR', basename(BASEPATH));

    // The path to the "application" directory
    if (is_dir($application_folder))
    {
        if (($_temp = realpath($application_folder)) !== FALSE)
        {
            $application_folder = $_temp;
        }
        else
        {
            $application_folder = strtr(
                rtrim($application_folder, '/\\'),
                '/\\',
                DIRECTORY_SEPARATOR.DIRECTORY_SEPARATOR
            );
        }
    }
    elseif (is_dir(BASEPATH.$application_folder.DIRECTORY_SEPARATOR))
    {
        $application_folder = BASEPATH.strtr(
            trim($application_folder, '/\\'),
            '/\\',
            DIRECTORY_SEPARATOR.DIRECTORY_SEPARATOR
        );
    }
    else
    {
        header('HTTP/1.1 503 Service Unavailable.', TRUE, 503);
        echo 'Your application folder path does not appear to be set correctly. Please open the following file and correct this: '.SELF;
        exit(3); // EXIT_CONFIG
    }

    define('APPPATH', $application_folder.DIRECTORY_SEPARATOR);

    // The path to the "views" directory
    if ( ! isset($view_folder[0]) && is_dir(APPPATH.'views'.DIRECTORY_SEPARATOR))
    {
        $view_folder = APPPATH.'views';
    }
    elseif (is_dir($view_folder))
    {
        if (($_temp = realpath($view_folder)) !== FALSE)
        {
            $view_folder = $_temp;
        }
        else
        {
            $view_folder = strtr(
                rtrim($view_folder, '/\\'),
                '/\\',
                DIRECTORY_SEPARATOR.DIRECTORY_SEPARATOR
            );
        }
    }
    elseif (is_dir(APPPATH.$view_folder.DIRECTORY_SEPARATOR))
    {
        $view_folder = APPPATH.strtr(
            trim($view_folder, '/\\'),
            '/\\',
            DIRECTORY_SEPARATOR.DIRECTORY_SEPARATOR
        );
    }
    else
    {
        header('HTTP/1.1 503 Service Unavailable.', TRUE, 503);
        echo 'Your view folder path does not appear to be set correctly. Please open the following file and correct this: '.SELF;
        exit(3); // EXIT_CONFIG
    }

    define('VIEWPATH', $view_folder.DIRECTORY_SEPARATOR);

    // set Timezone
    if( ! ini_get('date.timezone') )
    {
       date_default_timezone_set('Asia/Manila');
    } 

error_reporting(-1);
ini_set('display_errors', 1);

switch (ENVIRONMENT)
{

    case 'testing':
    case 'production':
        require_once(APPPATH."config".DIRECTORY_SEPARATOR."production".DIRECTORY_SEPARATOR."config.php");
        require_once(APPPATH."config".DIRECTORY_SEPARATOR."production".DIRECTORY_SEPARATOR."database.php");
    break;

    case 'production2':
        require_once(APPPATH."config".DIRECTORY_SEPARATOR."production2".DIRECTORY_SEPARATOR."config.php");
        require_once(APPPATH."config".DIRECTORY_SEPARATOR."production2".DIRECTORY_SEPARATOR."database.php");
    break;

    default:
        require_once(APPPATH."config".DIRECTORY_SEPARATOR."config.php");
        require_once(APPPATH."config".DIRECTORY_SEPARATOR."database.php");
    break;

}

$dbconn = $db[$active_group];

// set Timezone
if( ! ini_get('date.timezone') )
{
   date_default_timezone_set('Asia/Manila');
} 

function compress($filename, $dir, $files){ 
    $zip = new ZipArchive();

    if ($zip->open($filename, ZipArchive::CREATE)!==TRUE) {
        return false;
    }

    foreach($files as $file) {
        $zip->addFile($file, str_replace($dir . '\\', '', $file));
    }

    $zip->close();

    return true;
} 

function table_sql($link, $table) {
    $return='';

    $result = mysqli_query($link,'SELECT * FROM '.$table);
    $num_fields = mysqli_num_fields($result);

    $return.= 'DROP TABLE '.$table.';';
    $row2 = mysqli_fetch_row(mysqli_query($link,'SHOW CREATE TABLE '.$table));
    $return.= "\n\n".$row2[1].";\n\n";

    $attrs = array();
    $attrs_query = mysqli_query($link,'SHOW COLUMNS FROM '.$table);
    while($attr = mysqli_fetch_row($attrs_query)) {
        $attrs[] = $attr;    
    }

    for ($i = 0; $i < $num_fields; $i++) 
    {
        while($row = mysqli_fetch_row($result))
        {
            $return .= 'INSERT INTO '.$table.' VALUES(';
            for($j=0; $j<$num_fields; $j++) 
            {
                $row[$j] = addslashes($row[$j]);
                $row[$j] = str_replace("\n","\\n",$row[$j]);
                if (isset($row[$j])) { 
                    if( $row[$j]=="" ) { 
                        if( $attrs[$j][2] == 'YES' ) {
                            $return .= 'NULL'; 
                        } else {
                            $return .= '""'; 
                        }
                    } else {
                        $return .= '"'.$row[$j].'"'; 
                    }
                } else {
                    if( $attrs[$j][2] == 'YES' ) {
                        $return .= 'NULL'; 
                    } else {
                        $return .= '""'; 
                    }
                }
                if ($j<($num_fields-1)) { 
                    $return .= ','; 
                }
            }
            $return .= ");\n";
        }
    }

    $return.= "\n";
    return $return;
}

function php_backup_tables_single($dbconn, $dir = "backups")
{
    $link = mysqli_connect($dbconn['hostname'],$dbconn['username'],$dbconn['password']);
    mysqli_select_db($link, $dbconn['database']);
    mysqli_query($link,"SET NAMES 'utf8'");


    $tables = array();
    $result = mysqli_query($link,'SHOW TABLES');
    while($row = mysqli_fetch_row($result))
    {
        $tables[] = $row[0];
    }

    $return='';
    $filename = $dir . DIRECTORY_SEPARATOR . $dbconn['database'] . "-" . date('Y-m-d-H-i-s') . '.sql';
    $handle = fopen($filename,'w+');

    foreach($tables as $table)
    {
        $return .= table_sql($link, $table);        
    }
    
    fwrite($handle,$return);
    fclose($handle);

    compress($dir . DIRECTORY_SEPARATOR . $dbconn['database'] . "-" . date('Y-m-d-H-i-s') . "-php-single.sql.zip", $dir, array($filename) );
    unlink($filename);

}

function php_backup_tables_multiple($dbconn, $dir="backups")
{
    $dir = "backups";
    $link = mysqli_connect($dbconn['hostname'],$dbconn['username'],$dbconn['password']);
    mysqli_select_db($link, $dbconn['database']);
    mysqli_query($link,"SET NAMES 'utf8'");


	$tables = array();
	$result = mysqli_query($link,'SHOW TABLES');
	while($row = mysqli_fetch_row($result))
	{
		$tables[] = $row[0];
	}

    $files = array();
    foreach($tables as $table)
    {
        $filename = $dir . DIRECTORY_SEPARATOR . $table . "-" . date('Y-m-d-H-i-s') . '.sql';
        $files[] = $filename;
        $handle = fopen($filename,'w+');
    
        $return = table_sql($link, $table);

        fwrite($handle,$return);
        fclose($handle);
    }
    compress($dir . DIRECTORY_SEPARATOR . $dbconn['database'] . "-" . date('Y-m-d-H-i-s') . "-php-multiple.sql.zip", $dir, $files );
    foreach($files as $source) {
        unlink($source);
    }

}

function win_backup_mysqldump_single($dbconn, $dir="backups")
{    
    $filename = $dbconn['database'] . "-" . date('Y-m-d-H-i-s') . '-win-single.sql';
    
    $cmd = 'D:\wamp\bin\mysql\mysql5.6.17\bin\mysqldump.exe ';
    $cmd .= ' -u ' . $dbconn['username'];
    $cmd .= ' --password="' . $dbconn['password'] . '"';
    $cmd .= ' ' . $dbconn['database'];
    $cmd .= ' > ' . $dir . DIRECTORY_SEPARATOR . $filename;

    exec($cmd);
    compress($dir . DIRECTORY_SEPARATOR . $filename . ".zip", $dir, array( $dir . DIRECTORY_SEPARATOR . $filename ) );
    unlink($dir . DIRECTORY_SEPARATOR . $filename);
}

function win_backup_mysqldump_multiple($dbconn, $dir="backups")
{    
    
    $link = mysqli_connect($dbconn['hostname'],$dbconn['username'],$dbconn['password']);
    mysqli_select_db($link, $dbconn['database']);
    mysqli_query($link,"SET NAMES 'utf8'");


    $tables = array();
    $result = mysqli_query($link,'SHOW TABLES');
    while($row = mysqli_fetch_row($result))
    {
        $filename = $dbconn['database'] . "-" . $row[0] . "-" . date('Y-m-d-H-i-s') . '-dump.sql';
        $cmd = 'D:\wamp\bin\mysql\mysql5.6.17\bin\mysqldump.exe ';
        $cmd .= ' -u ' . $dbconn['username'];
        $cmd .= ' --password="' . $dbconn['password'] . '"';
        $cmd .= ' ' . $dbconn['database'];
        $cmd .= ' ' . $row[0];
        $cmd .= ' > ' . $dir . DIRECTORY_SEPARATOR . $filename;

        exec($cmd);
        $files[] = $dir . DIRECTORY_SEPARATOR . $filename;
    }

    
    compress($dir . DIRECTORY_SEPARATOR . $dbconn['database'] . '-' . date('Y-m-d-H-i-s') . "-win-multiple.sql.zip", $dir, $files );
    foreach($files as $source) {
        unlink($source);
    }
}

function linux_backup_mysqldump_single($dbconn, $dir="backups")
{    
    $filename = $dbconn['database'] . "-" . date('Y-m-d-H-i-s') . '-linux-single.sql';
    
    $cmd = 'mysqldump ';
    $cmd .= ' -u ' . $dbconn['username'];
    $cmd .= ' --password="' . $dbconn['password'] . '"';
    $cmd .= ' ' . $dbconn['database'];
    $cmd .= ' > ' . $dir . DIRECTORY_SEPARATOR . $filename;

    exec($cmd);
    compress($dir . DIRECTORY_SEPARATOR . $filename . ".zip", $dir, array( $dir . DIRECTORY_SEPARATOR . $filename ) );
    unlink($dir . DIRECTORY_SEPARATOR . $filename);
}

function linux_backup_mysqldump_multiple($dbconn, $dir="backups")
{    
    
    $link = mysqli_connect($dbconn['hostname'],$dbconn['username'],$dbconn['password']);
    mysqli_select_db($link, $dbconn['database']);
    mysqli_query($link,"SET NAMES 'utf8'");


    $tables = array();
    $result = mysqli_query($link,'SHOW TABLES');
    while($row = mysqli_fetch_row($result))
    {
        $filename = $dbconn['database'] . "-" . $row[0] . "-" . date('Y-m-d-H-i-s') . '-dump.sql';
        $cmd = 'mysqldump ';
        $cmd .= ' -u ' . $dbconn['username'];
        $cmd .= ' --password="' . $dbconn['password'] . '"';
        $cmd .= ' ' . $dbconn['database'];
        $cmd .= ' ' . $row[0];
        $cmd .= ' > ' . $dir . DIRECTORY_SEPARATOR . $filename;

        exec($cmd);
        $files[] = $dir . DIRECTORY_SEPARATOR . $filename;
    }

    
    compress($dir . DIRECTORY_SEPARATOR . $dbconn['database'] . '-' . date('Y-m-d-H-i-s') . "-linux-multiple.sql.zip", $dir, $files );
    foreach($files as $source) {
        unlink($source);
    }
}

if( isset($_GET['type']) ) {
    switch($_GET['type']) {
        case 'php_single':
            php_backup_tables_single($dbconn);
        break;
        case 'php_multiple':
            php_backup_tables_multiple($dbconn);
        break;
        case 'win_mysqldump_single':
            win_backup_mysqldump_single($dbconn);
        break;
        case 'win_mysqldump_multiple':
            win_backup_mysqldump_multiple($dbconn);
        break;
        case 'linux_mysqldump_single':
            linux_backup_mysqldump_single($dbconn);
        break;
        case 'linux_mysqldump_multiple':
            linux_backup_mysqldump_multiple($dbconn);
        break;
    }
}

$backup_url = $config['base_url'] . (($config['index_page']!='') ? $config['index_page'] . "/" : '') . "system_backup";
header('location: ' . $backup_url);
exit;