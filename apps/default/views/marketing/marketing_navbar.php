<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container">
	<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#marketing-navbar-collapse" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <div class="navbar-brand">Marketing</div>
    </div>

    <div class="collapse navbar-collapse" id="marketing-navbar-collapse">

      <ul class="nav navbar-nav navbar-right">
       
            <li class="active"><a href="<?php echo site_url('marketing_mlm'); ?>">Multi-level Marketing</a></li>

      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
</div>