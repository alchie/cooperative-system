<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container">
    <!-- Fixed navbar -->
    <nav class="navbar navbar-inverse navbar-success navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mainNavBarCollapse" aria-expanded="false" aria-controls="navbar" id="mainNavBarToggle">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <span class="navbar-brand" href="<?php echo site_url(); ?>">COOP</span>
        </div>
        <div id="mainNavBarCollapse" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
          <li>
              <a href="<?php echo site_url("welcome");; ?>" class="body_wrapper">Home</a>
          </li>
<?php 

$main_menu = array(
  'membership' => array(
      'title' => 'Membership',
      'uri' => 'membership',
      'permission' => 'membership',
      'sub_menus' => array(
          'membership_members' => array(
            'title' => 'Members',
            'uri' => 'membership_members',
            'permission' => 'members',
          ),
          'membership_companies' => array(
            'title' => 'Companies',
            'uri' => 'membership_companies',
            'permission' => 'companies',
          )
        )
    ),

  'services' => array(
      'title' => 'Services',
      'uri' => 'services',
      'permission' => 'services',
      'sub_menus' => array(
          'services_shares' => array(
            'title' => 'Share Capital',
            'uri' => 'services_shares',
            'permission' => 'shares',
          ),
          'services_lending' => array(
            'title' => 'Lending',
            'uri' => 'services_lending',
            'permission' => 'lending',
          ),
        )
    ),
/*
  'marketing' => array(
      'title' => 'Marketing',
      'uri' => 'marketing',
      'sub_menus' => array(
          'marketing_mlm' => array(
            'title' => 'Multi-level Marketing',
            'uri' => 'marketing_mlm',
          ),
        )
    ),
*/
  'accounting' => array(
      'title' => 'Accounting',
      'uri' => 'accounting',
      'permission' => 'accounting',
      'sub_menus' => array(
          'accounting_charts' => array(
            'title' => 'Chart of Accounts',
            'uri' => 'accounting_charts',
            'permission' => 'chart_of_accounts',
          ),
          'accounting_sales_receipts' => array(
            'title' => 'Sales Receipts',
            'uri' => 'accounting_sales_receipts',
            'permission' => 'sales_receipts',
          ),
          'accounting_write_checks' => array(
            'title' => 'Write Checks',
            'uri' => 'accounting_write_checks',
            'permission' => 'write_checks',
          ),
          'accounting_deposits' => array(
            'title' => 'Deposits',
            'uri' => 'accounting_deposits',
            'permission' => 'deposits',
          ),
          'accounting_reconciliation' => array(
            'title' => 'Bank Reconciliation',
            'uri' => 'accounting_reconciliation',
            'permission' => 'reconciliation',
          ),
          'accounting_general_journals' => array(
            'title' => 'General Journals',
            'uri' => 'accounting_general_journals',
            'permission' => 'general_journals',
          ),
        )
    ),

  'reports' => array(
    'title' => 'Reports',
      'uri' => 'reports',
      'permission' => 'report',
       'sub_menus' => array(
          'report_balance_sheet' => array(
            'title' => 'Balance Sheet',
            'uri' => 'report_balance_sheet',
            'permission' => 'balance_sheet',
          ),
          'report_income_statement' => array(
            'title' => 'Income Statement',
            'uri' => 'report_income_statement',
            'permission' => 'income_statement',
          ),
          'report_transactions' => array(
            'title' => 'Transactions',
            'uri' => 'report_transactions',
            'permission' => 'transactions',
          ),
            'report_entries' => array(
            'title' => 'Entries',
            'uri' => 'report_entries',
            'permission' => 'entries',
          ),
          'report_lending' => array(
            'title' => 'Lending',
            'uri' => 'report_lending',
            'permission' => 'lending',
          ),
        ),
    ),

  'system' => array(
      'title' => 'System',
      'uri' => 'system',
      'permission' => 'system',
      'sub_menus' => array(
          'system_names' => array(
            'title' => 'Names List',
            'uri' => 'system_names',
            'permission' => 'names',
          ),
          'system_users' => array(
            'title' => 'User Accounts',
            'uri' => 'system_users',
            'permission' => 'users',
          ),
          'system_settings' => array(
            'title' => 'Settings',
            'uri' => 'system_settings',
            'permission' => 'settings',
          ),
          'system_backup' => array(
            'title' => 'Database Backup',
            'uri' => 'system_backup',
            'permission' => 'backup',
          ),
        )
    ),
);

foreach($main_menu as $main=>$menu): 
  if( ! isset( $menu['permission'] ) ) {
    continue;
  }
  if( ! isset( $this->session->menu_module[$menu['permission']] ) ) {
    continue;
  }
?>
          <li class="dropdown">
              <a href="#<?php echo $menu['uri']; ?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $menu['title']; ?> <span class="caret hidden-xs"></span></a>
                  <ul class="dropdown-menu">
                  <?php foreach($menu['sub_menus'] as $sub=>$sub_menu): 
                    if( ! isset( $sub_menu['permission'] ) ) {
                      continue;
                    }
                    if( ($sub_menu['permission']) && (! in_array($sub_menu['permission'], $this->session->menu_module[$menu['permission']] ) ) ) {
                      continue;
                    }
                  ?>
                    <?php if( isset($sub_menu['header']) && ($sub_menu['header']) ) { ?>
                        <?php if( isset($sub_menu['separator']) && ($sub_menu['separator']) ) { ?>
                          <li role="separator" class="divider"></li>
                        <?php } ?>
                        <?php if( isset($sub_menu['title']) && ($sub_menu['title'] != '') ) { ?>
                          <span class="dropdown-header"><?php echo $sub_menu['title']; ?></span>
                        <?php } ?>
                    <?php } else { ?>
                    <li><a class="body_wrapper" href="<?php echo site_url($sub_menu['uri']); ?>"><?php echo $sub_menu['title']; ?></a></li>
                    <?php } ?>
                  <?php endforeach; ?>
                  </ul>
          </li>
<?php endforeach; ?>

          </ul>

          <ul class="nav navbar-nav navbar-right">
<?php if( hasAccess('membership', 'members', 'view') ) { ?>
<?php if( count( $this->session->menu_module ) > 0 ) { ?>
          <li class="hidden-xs hidden-sm searchbox">
                  <form method="get" class="navbar-form navbar-right" role="search" action="<?php echo site_url('membership_members'); ?>">
        <div class="form-group">
          <input name="q" type="text" class="form-control autocomplete-member_change" data-source="<?php echo site_url("welcome/ajax/change_member"); ?>" data-current_sub_uri="<?php echo uri_string(); ?>" placeholder="Search Member">
        </div>

      </form>
          </li>
<?php } ?>
<?php } ?>
            <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
          <span class="visible-xs"><?php echo $this->session->name; ?>
            <span class="caret hidden-xs"></span>
          </span>
          <span class="glyphicon glyphicon-user hidden-xs"></span></a>
          <ul class="dropdown-menu">
            <span class="dropdown-header"><?php echo $this->session->name; ?></span>
            <li><a href="javascript:void(0);" class="ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Account Settings" data-url="<?php echo site_url("account/settings/ajax") . "?next=" . uri_string(); ?>">Account Settings</a></li>
            <li><a href="javascript:void(0);" class="ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Change Password" data-url="<?php echo site_url("account/change_password/ajax") . "?next=" . uri_string(); ?>">Change Password</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="<?php echo site_url('account/logout') . "?next=" . urlencode( uri_string() ); ?>">Logout</a></li>
          </ul>
        </li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
</div>