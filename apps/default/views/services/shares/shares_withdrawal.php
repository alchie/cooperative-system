<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>

<?php if( ! $inner_page ): ?>

<?php 
  if( isset( $member ) ) { 
      $this->load->view('membership/members/members_navbar'); 
    } else {
      $this->load->view('services/services_navbar');
   }
?>

<?php $this->load->view('services/shares/shares_navbar'); ?>

<div class="container">
<div class="row">
  <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">

<?php if( isset( $new_withdrawals ) && ($new_withdrawals) ) { ?>

          <button type="button" class="btn btn-success btn-xs pull-right ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Add Withdrawal" data-url="<?php echo site_url("services_shares/new_withdrawals/".((isset($member))?$member->id:0)."/ajax") . "?next=" . uri_string(); ?>">Add Withdrawal</button>

        <?php } ?> 
       
          <h3 class="panel-title">Withdrawals</h3>

        </div>
        <div class="panel-body" id="ajaxBodyInnerPage" data-url="<?php echo site_url(uri_string()); ?>">

<?php endif; ?>

<?php if( isset($shares_withdrawal) ) { ?>
<?php if( $shares_withdrawal ) { ?>

 <table class="table table-condensed hidden-xs">
          <thead>
            <tr>
<?php if( !isset( $member ) ) { ?>
              <th class="text-left">Name</th>
<?php } ?>
              <th class="text-left">Bank Account</th>
              <th class="text-left">Date Withdrawn</th>
              <th class="text-center">Check Number</th>
              <th class="text-right">Amount</th>
<?php if( isset( $member ) ) { ?>
              <th class="text-right" width="80px"></th>
<?php } ?>
            </tr>
          </thead>
          <tbody>
<?php 
$total_capital = 0;
foreach($shares_withdrawal as $withdrawal) { ?>
            <tr>
<?php if( !isset( $member ) ) { ?>
              <td class="text-left">
              <a href="<?php echo site_url("services_shares/withdrawals/{$withdrawal->member_id}"); ?>">
              <?php echo $withdrawal->full_name; ?>
              </a>
              </td>
<?php } ?>
              <td class="text-left"><?php echo $withdrawal->bank_account; ?></td>
              <td class="text-left"><?php echo date("F d, Y", strtotime($withdrawal->check_date)); ?></td>
              <td class="text-center"><a href="<?php echo site_url("accounting_write_checks/check_items/{$withdrawal->check_id}"); ?>"><?php echo $withdrawal->check_number; ?></a></td>
              <td class="text-right"><?php echo number_format($withdrawal->amount,2); $total_capital+=$withdrawal->amount; ?></td>
<?php if( isset( $member ) ) { ?>
              <td class="text-right">

              <a class="btn btn-danger btn-xs confirm" href="<?php echo site_url("services_shares/delete_withdrawal/{$withdrawal->id}") . "?next=" . uri_string(); ?>">Delete</a>
              </td>
<?php } ?>
            </tr>
<?php } ?>
          </tbody>
          </table>

<ul class="list-group visible-xs">
  <?php foreach($shares_withdrawal as $withdrawal) { ?>
      <a href="<?php echo site_url("services_lending/payment/{$withdrawal->id}"); ?>" class="list-group-item">
        <?php echo date("F d, Y", strtotime($withdrawal->check_date)); ?> <strong class="pull-right"><?php echo number_format($withdrawal->amount,2); ?></strong>
      </a>
    <?php } ?>
</ul>

<?php echo ($pagination!='') ? '<center>' . $pagination . '</center>' : ''; ?>

<?php } else { ?>
  <div class="text-center">No Withdrawal Yet!</div>
<?php } ?>

<?php } else { ?>
  Please Wait...
<?php } ?>

<?php if( ! $inner_page ): ?>

        </div>
      </div>
    </div>
</div>
</div>

<?php $this->load->view('footer'); ?>

<?php endif; ?>