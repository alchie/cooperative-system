<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( !isset($output) || ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('services/services_navbar'); ?>

<?php $this->load->view('services/shares/shares_navbar'); ?>

<div class="container">
<div class="row">

	<div class="col-md-8 col-md-offset-2">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
	    		<h3 class="panel-title">Add Dividend</h3>
	    	</div>
	    	<form method="post">
	    	<div class="panel-body">

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>
<?php endif; ?>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label class="control-label">Resolution Date</label>
            <input class="form-control datepicker text-center" type="text" name="res_date" value="<?php echo date('m/d/Y'); ?>">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="control-label">Resolution Number</label>
            <input class="form-control text-center" type="text" name="res_number" value="<?php echo $this->input->post('res_number'); ?>">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="control-label">Amount</label>
            <input class="form-control text-right" type="text" name="amount" value="<?php echo $this->input->post('amount'); ?>">
        </div>
    </div>

</div>

<?php if( !isset($output) || ($output!='ajax') ) : ?>

	    	</div>
	    	<div class="panel-footer">

	    		<button type="submit" class="btn btn-success">Submit</button>
	    		<a href="<?php echo site_url("services_shares/dividends"); ?>" class="btn btn-warning">Back</a>
	    	</div>

	    	</form>
	    </div>
    </div>
</div>
</div>

<?php $this->load->view('footer'); ?>

<?php endif; ?>