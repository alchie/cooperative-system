<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="container">
	<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#services-shares-navbar-collapse" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <div class="navbar-brand"><?php echo $current_page; ?></div>
    </div>


    <div class="collapse navbar-collapse" id="services-shares-navbar-collapse">

      <ul class="nav navbar-nav navbar-right">
<?php 

$overview = 'services_shares/overview';
$share_capital = 'services_shares/share_capital';
$dividends = 'services_shares/dividends';
$withdrawals = 'services_shares/withdrawals';

if( isset($member) ) {
  $overview .= '/' . $member->id;
  $share_capital .= '/' . $member->id;
  $dividends .= '/' . $member->id;
  $withdrawals .= '/' . $member->id;
} 

$url['services_shares_overview'] = array('uri' => $overview, 'title'=>'Overview');
$url['services_shares_share_capital'] = array('uri' => $share_capital, 'title'=>'Share Capital');
$url['services_shares_dividends'] = array('uri' => $dividends, 'title'=>'Dividends');
$url['services_shares_withdrawals'] = array('uri' => $withdrawals, 'title'=>'Withdrawals');
foreach($url as $k=>$v) {
?>
  <li class="<?php echo ($k==$current_sub_uri) ? 'active' : ''; ?>"><a class="body_wrapper" href="<?php echo site_url($v['uri']); ?>"><?php echo $v['title']; ?></a></li>
<?php } ?> 
         
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
</div>