<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( !isset($output) || ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('membership/members/members_navbar'); ?>

<?php $this->load->view('services/shares/shares_navbar'); ?>

<div class="container">
<div class="row">

	<div class="col-md-8 col-md-offset-2">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
	    		<h3 class="panel-title">Withdraw Capital</h3>
	    	</div>
	    	<form method="post">
	    	<div class="panel-body">

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

<?php endif; ?>

<?php if( isset($new_withdrawal) && ($new_withdrawal) ) { ?>

<input type="hidden" name="hashcode" value="<?php echo sha1($new_withdrawal->trn_id); ?>" ?>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">Bank Account</label>
            <div class="form-control text-center"><?php echo $new_withdrawal->bank_name; ?></div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">Check Date</label>
            <div class="form-control text-center"><?php echo date('m/d/Y', strtotime($new_withdrawal->check_date)); ?></div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">Check Number</label>
            <div class="form-control text-center"><?php echo $new_withdrawal->check_number; ?></div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">Amount</label>
            <div class="form-control text-center"><?php echo number_format($new_withdrawal->debit,2); ?></div>
        </div>
    </div>

</div>

<div class="row">

    <div class="col-md-12">
        <div class="form-group">
            <label class="control-label">Memo</label>
            <div class="form-control text-center"><?php echo $new_withdrawal->ae_memo; ?></div>
        </div>
    </div>

</div>
	
<?php } ?>

<?php if( !isset($output) || ($output!='ajax') ) : ?>

	    	</div>
	    	<div class="panel-footer">

	    		<button type="submit" class="btn btn-success">Submit</button>
	    		<a href="<?php echo site_url("services_shares/share_capital/" . $member->id); ?>" class="btn btn-warning">Back</a>
	    	</div>

	    	</form>
	    </div>
    </div>
</div>
</div>

<?php $this->load->view('footer'); ?>

<?php endif; ?>