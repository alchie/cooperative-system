<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( !isset($output) || ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('membership/members/members_navbar'); ?>

<?php $this->load->view('services/shares/shares_navbar'); ?>

<div class="container">
<div class="row">

	<div class="col-md-8 col-md-offset-2">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
	    		<h3 class="panel-title">Withdraw Capital</h3>
	    	</div>
	    	<form method="post">
	    	<div class="panel-body">

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

<?php endif; ?>

<div class="row">

    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">Bank Account</label>
            <select name="bank_id" class="form-control" title="Select Bank Account" required>
<?php 
function display_list($withdrawal, $main_accounts , $pre="") { 
                 foreach( $main_accounts as $main_account) { 
                  ?>
                  <option <?php echo ($main_account->reconcile==1) ? '' : 'disabled'; ?> value="<?php echo $main_account->id; ?>" <?php echo (isset($withdrawal->bank_id)&&($withdrawal->bank_id==$main_account->id)) ? 'selected' : ''; ?> title="<?php echo $main_account->title; ?>"><?php echo $pre; ?><?php echo $main_account->title; ?></option>
          <?php 
          if( $main_account->children ) {
            display_list( $withdrawal, $main_account->children, $pre . "- - - - " );
          }
          ?>
                  <?php } ?> 
<?php } 

display_list( $withdrawal, $account_titles ); 

?>        
              </select>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">Check Date</label>
            <input class="form-control datepicker text-center" type="text" name="date" value="<?php echo date('m/d/Y', strtotime($withdrawal->check_date)); ?>">
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">Check Number</label>
            <input class="form-control text-center" type="text" name="check_number" value="<?php echo $withdrawal->check_number; ?>">
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">Amount</label>
            <input class="form-control text-right" type="text" name="amount" value="<?php echo number_format($withdrawal->amount,2,".",""); ?>">
        </div>
    </div>

</div>

<div class="row">

    <div class="col-md-12">
        <div class="form-group">
            <label class="control-label">Memo</label>
            <input class="form-control" type="text" name="memo" value="<?php echo $withdrawal->memo; ?>">
        </div>
    </div>

</div>

<?php if( isset($output) && ($output=='ajax') ) { ?>
    <a href="<?php echo site_url("services_shares/delete_withdrawal/{$withdrawal->id}/{$withdrawal->member_id}"); ?>" class="btn btn-xs btn-danger confirm">Delete Withdrawal</a>
<?php } ?>
		
<?php if( !isset($output) || ($output!='ajax') ) : ?>

	    	</div>
	    	<div class="panel-footer">

	    		<button type="submit" class="btn btn-success">Submit</button>
	    		<a href="<?php echo site_url(($this->input->get('next')) ? $this->input->get('next') : "services_shares/withdrawals/" . $member->id); ?>" class="btn btn-warning">Back</a>
	    	</div>

	    	</form>
	    </div>
    </div>
</div>
</div>

<?php $this->load->view('footer'); ?>

<?php endif; ?>