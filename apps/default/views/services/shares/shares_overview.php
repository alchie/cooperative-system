<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
<?php 
  if( isset( $member ) ) { 
      $this->load->view('membership/members/members_navbar'); 
    } else {
      $this->load->view('services/services_navbar');
   }
?>

<?php $this->load->view('services/shares/shares_navbar'); ?>

<div class="container">
<div class="row">

  <div class="col-md-3">

      <div class="panel panel-default">
        <div class="panel-heading">
<?php if( hasAccess('services', 'shares', 'add') ) { ?>
  <?php if( isset( $new_capitals ) && ($new_capitals) ) { ?>

          <button type="button" class="btn btn-success btn-xs pull-right ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Add Capital" data-url="<?php echo site_url("services_shares/new_capitals/".((isset($member))?$member->id:0)."/ajax") . "?next=" . uri_string(); ?>" data-hide_footer="1">Add Capital</button>
  <?php } ?>  
<?php } ?>  

          <h3 class="panel-title">Share Capital</h3>
        </div>
        <div class="panel-body text-center bold">
          <?php echo number_format($overview->share_capital,2); ?>
        </div>
      </div>

    </div>

    <div class="col-md-3">

      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Dividends</h3>
        </div>
         <div class="panel-body text-center bold">
          <?php echo number_format($overview->share_dividend,2); ?>
        </div>
      </div>

    </div>

    <div class="col-md-3">

      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Withdrawals</h3>
        </div>
         <div class="panel-body text-center bold">
          <?php echo number_format($overview->share_withdrawal,2); ?>
        </div>
      </div>

    </div>

    <div class="col-md-3">

      <div class="panel panel-default">
        <div class="panel-heading">
          <?php if( !isset($member) ) { ?>
            <a href="<?php echo site_url("services_shares/balance"); ?>" class="btn btn-success pull-right btn-xs"><span class="fa fa-search"></span></a>
          <?php } ?>
          <h3 class="panel-title">Balance</h3>

        </div>
         <div class="panel-body text-center bold">
          <?php echo number_format(($overview->share_capital+$overview->share_dividend-$overview->share_withdrawal),2); ?>
        </div>
      </div>

    </div>

</div>
</div>



<?php $this->load->view('footer'); ?>