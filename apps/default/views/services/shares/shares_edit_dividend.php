<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( !isset($output) || ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('services/services_navbar'); ?>

<?php $this->load->view('services/shares/shares_navbar'); ?>

<div class="container">
<div class="row">

	<div class="col-md-8 col-md-offset-2">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
	    		<h3 class="panel-title">Add Dividend</h3>
	    	</div>
	    	<form method="post">
	    	<div class="panel-body">

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>
<?php endif; ?>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label class="control-label">Resolution Date</label>
            <input class="form-control datepicker text-center" type="text" name="res_date" value="<?php echo date('m/d/Y', strtotime($dividend->res_date)); ?>">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="control-label">Resolution Number</label>
            <input class="form-control text-center" type="text" name="res_number" value="<?php echo $dividend->res_number; ?>">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="control-label">Amount</label>
            <input class="form-control text-right" type="text" name="amount" value="<?php echo number_format($dividend->amount,2,".",""); ?>">
        </div>
    </div>

</div>

<?php if( isset($output) && ($output=='ajax') ) { ?>
    <a href="<?php echo site_url("services_shares/delete_dividend/{$dividend->id}"); ?>" class="btn btn-xs btn-danger confirm">Delete Dividend</a>
<?php } ?>

<?php if( !isset($output) || ($output!='ajax') ) : ?>

	    	</div>
	    	<div class="panel-footer">

                <a href="<?php echo site_url("services_shares/dividend_recipients/{$dividend->id}"); ?>" class="btn btn-info pull-right">Recipients</a>

	    		<button type="submit" class="btn btn-success">Submit</button>
	    		<a href="<?php echo site_url(($this->input->get('next')) ? $this->input->get('next') : "services_shares/dividends"); ?>" class="btn btn-warning">Back</a>
	    	</div>

	    	</form>
	    </div>
    </div>
</div>
</div>

<?php $this->load->view('footer'); ?>

<?php endif; ?>