<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>

<?php $this->load->view('services/services_navbar'); ?>

<?php $this->load->view('membership/members/members_navbar'); ?>

<div class="container">
<div class="row">
  <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">

        <a href="<?php echo site_url( "services_shares/add_capital/{$member->id}" ); ?>" class="btn btn-danger btn-xs pull-right">Add Capital</a>

        <button type="button" class="btn btn-success btn-xs pull-right ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Add Capital" data-url="<?php echo site_url("services_shares/add_capital/{$member->id}/ajax") . "?next=" . uri_string(); ?>">Add Capital</button>

          <h3 class="panel-title">Share Capital</h3>

        </div>
        <div class="panel-body">


        </div>
      </div>
    </div>
</div>
</div>

<?php $this->load->view('footer'); ?>