<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>

<?php $this->load->view('services/services_navbar'); ?>

<div class="container">
<div class="row">
  <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">

          <h3 class="panel-title">Share Capital</h3>

        </div>
        <div class="panel-body">


        </div>
      </div>
    </div>
</div>
</div>

<!-- #searchMember Modal -->
<div class="modal fade" id="searchName" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalLabel">Add Capital</h4>
      </div>
<form method="get" action="<?php echo site_url('services_shares/search_name'); ?>">
      <div class="modal-body">

        <div class="form-group">
            <label class="control-label">Search Member</label>
            <input type="text" class="form-control" name="q" value="">
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Search</button>
      </div>
</form>
    </div>
  </div>
</div>

<?php $this->load->view('footer'); ?>