<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); 
	$s = array();
	if($settings) {
		foreach($settings as $set) {
			$s[$set->key] = $set->value;
		}
	}

?>

<?php $this->load->view('services/services_navbar'); ?>

<div class="container">
<div class="row">

  <div class="col-md-6 col-md-offset-3">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Configurations</h3>
        </div>
        <form method="post" action="<?php echo site_url("services_lending/save_configuration"); ?>">
        <div class="panel-body">


          <div class="form-group">
            <label>Receivable Account</label>
            <select name="receivable" class="form-control" title="- - Select Account - -">
<?php 
function display_list($setting, $main_accounts , $pre="", $set='receivable', $filter_type=NULL) {
                 foreach( $main_accounts as $main_account) { 
                    if( ( $filter_type ) && ( $main_account->type != $filter_type ) ) {
                      continue;
                    }
                  ?>
                  <option value="<?php echo $main_account->id; ?>" <?php echo (isset($setting[$set])&&($setting[$set]==$main_account->id)) ? 'selected' : ''; ?> title="<?php echo $main_account->title; ?>"><?php echo $pre; ?><?php echo $main_account->title; ?></option>
          <?php 
          if( $main_account->children ) {
            display_list( $setting, $main_account->children, $pre . "- - - - ", $set, $filter_type );
          }
          ?>
                  <?php } ?> 
<?php } ?>
<?php display_list( $s, $account_titles, '', 'receivable', '010_BS_ASS_ACR' ); ?>        
              </select>
          </div>

          <div class="form-group">
            <label>Interest Income</label>
            <select name="interest_income" class="form-control" title="- - Select Account - -">
<?php display_list( $s, $account_titles, '', 'interest_income', '100_IS_INC_INC' ); ?>        
              </select>
          </div>
          
          <div class="form-group">
            <label>Class Name</label>
            <select name="class_name" class="form-control" title="- - Select Class - -">
<?php 
function display_class($setting, $class_list , $pre="", $set='class_name') {
                 foreach( $class_list as $class) { 
                  ?>
                  <option value="<?php echo $class->id; ?>" <?php echo (isset($setting[$set])&&($setting[$set]==$class->id)) ? 'selected' : ''; ?> title="<?php echo $class->value; ?>"><?php echo $pre; ?><?php echo $class->value; ?></option>
          <?php 
          if( $class->children ) {
            display_class( $setting, $class->children, $pre . "- - - - ", $set);
          }
          ?>
                  <?php } ?> 
<?php } ?>
<?php display_class( $s, $class_list ); ?>        
              </select>
          </div>

          <div class="form-group">
            <label>Releaser</label>
            <select name="releaser" class="form-control" title="- - Select User - -">
<?php 
function display_users($setting, $users , $set='releaser') {
                 foreach( $users as $user) { 
                  ?>
                  <option value="<?php echo $user->id; ?>" <?php echo (isset($setting[$set])&&($setting[$set]==$user->id)) ? 'selected' : ''; ?> title="<?php echo $user->name; ?>"><?php echo $user->name; ?></option>
                  <?php } ?> 
<?php } ?>
<?php display_users( $s, $users, 'releaser' ); ?>        
              </select>
          </div>

                    <div class="form-group">
            <label>Approver</label>
            <select name="approver" class="form-control" title="- - Select User - -">
<?php display_users( $s, $users, 'approver' ); ?>        
              </select>
          </div>

        </div>
        <div class="panel-footer">
          <button type="submit" class="btn btn-success">Submit</button>
          <a href="<?php echo site_url("system_settings"); ?>" class="btn btn-warning">Back</a>
        </div>


        </form>
      </div>
    </div>
</div>
</div>


<?php $this->load->view('footer'); ?>