<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
<?php 
  if( isset( $member ) ) { 
      $this->load->view('membership/members/members_navbar'); 
    } else {
      $this->load->view('services/services_navbar');
   }
?>

<?php $this->load->view('services/lending/lending_navbar'); ?>

<div class="container">
<div class="row">

    <div class="col-md-4">

      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Loans</h3>
        </div>
        <div class="panel-body">
         <table class="table table-default">
          <tr>
             <td class="bold">Total Loans</td>
             <td class="text-right bold">
                <a href="<?php echo site_url( (isset($member)) ? "services_lending/loans/{$member->id}" : "services_lending/index/loans"); ?>" class="item body_wrapper">
                             <?php echo number_format(($overview->total_principal+$overview->total_interest),2); ?>
                </a>
             </td>
           </tr>
           <tr>
             <td class="bold">Total Principal</td>
             <td class="text-right bold">

             <?php echo number_format($overview->total_principal,2); ?>

             </td>
           </tr>
           <tr>
             <td class="bold">Total Interest</td>
             <td class="text-right bold">

             <?php echo number_format($overview->total_interest,2); ?>

             </td>
           </tr>

<?php if( $overview->total_pending ) { ?>
           <tr class="warning">
             <td class="bold">Pending Loans</td>
             <td class="text-right bold">
<a href="<?php echo site_url( (isset($member)) ? "services_lending/loans/{$member->id}/pending" : "services_lending/index/loans/pending"); ?>" class="item  body_wrapper">
             <?php echo number_format($overview->total_pending,2); ?>
</a>
             </td>
           </tr>
<?php } ?>
<?php if( $overview->total_inactive ) { ?>
           <tr class="danger">
             <td class="bold">Inactive Loans</td>
             <td class="text-right bold">
<a href="<?php echo site_url( (isset($member)) ? "services_lending/loans/{$member->id}/inactive" : "services_lending/index/loans/inactive"); ?>" class="item body_wrapper">
             <?php echo number_format($overview->total_inactive,2); ?>
</a>
             </td>
           </tr>
<?php } ?>
<?php if( $overview->new_loans ) { ?>
                      <tr class="success">
             <td class="bold">New Loans</td>
             <td class="text-right bold">
<?php if( hasAccess('services', 'lending', 'edit') ) { ?>
<a href="#add-new-loan" class="item ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Add Loan" data-url="<?php echo site_url("services_lending/add/0/ajax") . "?next=" . uri_string(); ?>" data-hide_footer="1">
<?php } ?>
             <?php echo number_format($overview->new_loans,2); ?>
<?php if( hasAccess('services', 'lending', 'edit') ) { ?>
</a>
<?php } ?>

             </td>
           </tr>
<?php } ?>
         </table>
        </div>
      </div>

    </div>

    <div class="col-md-4">

      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Payments</h3>
        </div>
         <div class="panel-body">
         <table class="table table-default">
           <tr>
             <td class="bold">Total Payments</td>
             <td class="text-right bold">
             <a href="<?php echo site_url( (isset($member)) ? "services_lending/payments/{$member->id}" : "services_lending/index/payments"); ?>" class="item body_wrapper">
             <?php echo number_format($overview->total_payments,2); ?>
             </a>
             </td>
           </tr>
            <tr>
             <td class="bold">Total Applied Principal</td>
             <td class="text-right bold">
             <a href="<?php echo site_url( (isset($member)) ? "services_lending/payments/{$member->id}/applied" : "services_lending/index/payments/applied"); ?>" class="item body_wrapper">
             <?php echo number_format($overview->total_applied_principal,2); ?>
             </a>
             </td>
           </tr>
            <tr>
             <td class="bold">Total Applied Interest</td>
             <td class="text-right bold">
             <a href="<?php echo site_url( (isset($member)) ? "services_lending/payments/{$member->id}/applied" : "services_lending/index/payments/applied"); ?>" class="item body_wrapper">
             <?php echo number_format($overview->total_applied_interest,2); ?>
             </a>
             </td>
           </tr>
           <tr>
             <td class="bold">Total Applied Payments</td>
             <td class="text-right bold">
             <a href="<?php echo site_url( (isset($member)) ? "services_lending/payments/{$member->id}/applied" : "services_lending/index/payments/applied"); ?>" class="item body_wrapper">
             <?php echo number_format($overview->total_applied,2); ?>
             </a>
             </td>
           </tr>
<?php if( round($overview->total_payments-$overview->total_applied) > 0 ) { ?>
            <tr class="danger">
             <td class="bold">Total Unapplied Payments</td>
             <td class="text-right bold">
            <!-- <a href="<?php echo site_url(  (isset($member) ) ? "services_lending/payments/{$member->id}/unapplied" : "services_lending/index/payments/unapplied"); ?>" class="item"> -->
<a href="#unapplied-payments" class="item ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Unapplied Payments" data-url="<?php echo site_url("services_lending/unapplied_payments/".((isset($member))?$member->id:0)."/ajax") . "?next=" . uri_string(); ?>" data-hide_footer="1">
             <?php echo number_format(($overview->total_payments-$overview->total_applied),2); ?>
              </a>
             </td>
           </tr>
<?php } ?>
<?php if( $overview->new_payments ) { ?>
                      <tr class="success">
             <td class="bold">New Payments</td>
             <td class="text-right bold">

<?php if( hasAccess('services', 'lending', 'edit') ) { ?>
<a href="#add-new-payments" class="item ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="New Payments" data-url="<?php echo site_url("services_lending/new_payments/".((isset($member))?$member->id:0)."/ajax") . "?next=" . uri_string(); ?>" data-hide_footer="1">
<?php } ?>
             <?php echo number_format($overview->new_payments,2); ?>
<?php if( hasAccess('services', 'lending', 'edit') ) { ?>
</a>
<?php } ?>
             </td>
           </tr>
<?php } ?>
         </table>
        </div>
      </div>

    </div>

    <div class="col-md-4">

      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Balance</h3>
        </div>
         <div class="panel-body">
         <table class="table table-default">
           <tr>
             <td class="bold">Unpaid Balance</td>
             <td class="text-right bold">
             
             <?php echo number_format($overview->total_unpaid,2); ?>

             </td>
           </tr>
           <tr>
             <td class="bold">Unpaid Principal</td>
             <td class="text-right bold">
             
             <?php echo number_format($overview->total_principal-$overview->total_applied_principal,2); ?>

             </td>
           </tr>
           <tr>
             <td class="bold">Unpaid Interests</td>
             <td class="text-right bold">
             
             <?php echo number_format($overview->total_interest-$overview->total_applied_interest,2); ?>

             </td>
           </tr>
        </table>
          
        </div>
      </div>

    </div>


</div>
</div>



<?php $this->load->view('footer'); ?>