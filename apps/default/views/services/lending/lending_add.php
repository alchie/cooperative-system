<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('services/services_navbar'); ?>

<?php $this->load->view('membership/members/members_navbar'); ?>

<div class="container">
<div class="row">

<div class="col-md-6 col-md-offset-3">

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

<?php endif; ?>




<?php if( $new_loans ) { ?>
<ul class="list-group">
    <?php foreach($new_loans as $new_loan) { ?>

<?php if( isset($output) && ($output!='ajax') ) { ?>
   <a class="list-group-item" href="<?php echo site_url("services_lending/add_loan/{$new_loan->ae_name_id}/{$new_loan->ae_id}"); ?>">
<?php } else { ?>
	 <a class="list-group-item ajax-modal-inner" href="<?php echo site_url("services_lending/add_loan/{$new_loan->ae_name_id}/{$new_loan->ae_id}/ajax") . "?next=" . uri_string(); ?>">
<?php } ?>
    <span class="badge"><?php echo number_format($new_loan->debit,2); ?></span>
    <h4 class="list-group-item-heading"><?php echo $new_loan->lastname; ?>, <?php echo $new_loan->firstname; ?> <?php echo $new_loan->middlename; ?></h4>
    <?php echo $new_loan->entry_type; ?> &middot;
    <?php echo date('F d, Y', strtotime($new_loan->loan_date)); ?> &middot;
    <?php echo $new_loan->loan_number; ?> 
  </a>
  <?php } ?>
</ul>

<?php echo ($pagination!='') ? '<center>' . $pagination . '</center>' : ''; ?>

<?php } else { ?>
<p class="text-center">No New Release Loan!</p>
<?php } ?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

    </div>

</div>
</div>

<?php $this->load->view('footer'); ?>

<?php endif; ?>