<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container">
	<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#services-lending-navbar-collapse" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <div class="navbar-brand">Lending</div>
    </div>


    <div class="collapse navbar-collapse" id="services-lending-navbar-collapse">

      <ul class="nav navbar-nav navbar-right">
<?php 
if( isset($member) ) {
  $url['services_lending_overview'] = array('uri' => 'services_lending/overview/' . $member->id, 'title'=>'Overview');
  $url['services_lending_loans'] = array('uri' => 'services_lending/loans/' . $member->id, 'title'=>'Loans');
  $url['services_lending_payments'] = array('uri' => 'services_lending/payments/' . $member->id, 'title'=>'Payments');
  $url['services_lending_open_invoices'] = array('uri' => 'services_lending/open_invoices/' . $member->id, 'title'=>'Open Invoices');
} else {
  $url['services_lending_overview'] = array('uri' => 'services_lending/overview' , 'title'=>'Overview');
  $url['services_lending_loans'] = array('uri' => 'services_lending/index/loans' , 'title'=>'Loans');
  $url['services_lending_payments'] = array('uri' => 'services_lending/index/payments' , 'title'=>'Payments');
  $url['services_lending_open_invoices'] = array('uri' => 'services_lending/index/open_invoices' , 'title'=>'Open Invoices');
}

foreach($url as $k=>$v) {
?>
  <li class="<?php echo ($k==$current_sub_uri) ? 'active' : ''; ?>"><a class="body_wrapper" href="<?php echo site_url($v['uri']); ?>"><?php echo $v['title']; ?></a></li>
<?php } ?> 
         
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
</div>