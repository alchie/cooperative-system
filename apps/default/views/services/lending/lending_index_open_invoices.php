<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>

<?php if( ! $inner_page ): ?>

<?php $this->load->view('services/services_navbar'); ?>

<?php $this->load->view('services/lending/lending_navbar'); ?>

<div class="container">
<div class="row">
  <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">
        
        <a target="_blank" href="<?php echo site_url("services_lending/print_open_invoices"); ?>" class="btn btn-primary btn-xs pull-right hidden-xs">Print</a>

          <h3 class="panel-title">Open Invoices

</h3>
        </div>
        <div class="panel-body" id="ajaxBodyInnerPage" data-url="<?php echo site_url(uri_string()); ?>">
<?php endif; // inner_page ?>

<?php if( isset($invoices) ) { ?>
<?php if( $invoices ) { ?>
          <table class="table table-default table-hover table-condensed">
          <thead>
            <tr>
              <th class="text-left">Paid By</th>
              <th class="text-right">Due Date</th>
              <th class="text-right">Principal Amount</th>
              <th class="text-right">Interest Addon</th>
              <th class="text-right">Total Amount Due</th>
              <th class="text-right">Balance</th>
            </tr>
          </thead>
          <tbody>
<?php 
foreach($invoices as $invoice) { 
?>
            <tr class="">
              <td class="text-left">
<a class="body_wrapper" href="<?php echo site_url("services_lending/open_invoices/{$invoice->member_id}"); ?>">
              <?php echo $invoice->lastname; ?>, <?php echo $invoice->firstname; ?>
</a>
              </td>
              <td class="text-right"><?php echo date('F d, Y', strtotime($invoice->due_date)); ?></td>
              <td class="text-right"><?php echo number_format($invoice->principal_due,2); ?></td>
              <td class="text-right"><?php echo number_format($invoice->interest_due,2); ?></td>
              <td class="text-right"><?php echo number_format(($invoice->principal_due+$invoice->interest_due),2); ?></td>
              <td class="text-right"><?php echo number_format($invoice->balance,2); ?></td>
            </tr>
<?php } ?>
          </tbody>
          </table>

<?php echo ($pagination!='') ? '<center>' . $pagination . '</center>' : ''; ?>

<?php } else { ?>
  <div class="text-center">No Loan Found!</div>
<?php } ?>

<?php } else { ?>
  Please Wait...
<?php } ?>

<?php if( ! $inner_page ): ?>

        </div>
      </div>
    </div>
</div>
</div>



<?php endif; // inner_page ?>

<?php $this->load->view('footer'); ?>