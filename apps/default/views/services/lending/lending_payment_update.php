<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( !isset($output) || ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('membership/members/members_navbar'); ?>

<?php $this->load->view('services/lending/lending_navbar'); ?>

<?php if( isset($current_loan) ) { ?>
    
    <div class="container">
    <nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <div class="navbar-brand"><?php echo date('m/d/Y', strtotime($current_loan->loan_date)); ?> : &#x20B1;<?php echo number_format($current_loan->principal,2); ?> @ <?php echo number_format($current_loan->interest_rate,2); ?>% for <?php echo $current_loan->months; ?> Months</div>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
      <ul class="nav navbar-nav navbar-right">
        <li class=""><a href="<?php echo site_url("services_lending/schedule/{$current_loan->id}"); ?>">Schedule</a></li>
        <li class="active"><a href="<?php echo site_url("services_lending/loan_payments/{$current_loan->id}"); ?>">Payments</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
</div>

<?php } ?>


<div class="container">
<div class="row">

	<div class="col-md-8 col-md-offset-2">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
<?php if( (!$payment->deposit_id) && (!$applied) ) { ?>
            <a href="<?php echo site_url("services_lending/payment_delete/{$payment->id}"); ?>" class="btn btn-danger btn-xs pull-right confirm">Delete</a>
<?php } ?>
	    		<h3 class="panel-title">Update Payment</h3>
	    	</div>
	    	<form method="post">
	    	<div class="panel-body">

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

<?php endif; ?>

<?php if( $member ) { ?>
 <div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label class="control-label">Received From</label>
            <div class="form-control text-left bold"><?php echo $member->lastname; ?>, <?php echo $member->firstname; ?> <?php echo $member->middlename; ?></div>
        </div>
    </div>
</div>
<?php } ?>
<div class="row">
    <div class="col-md-4">
	    <div class="form-group">
            <label class="control-label">Payment Date</label>
            <div class="form-control text-center"><?php echo date("m/d/Y", strtotime($payment->payment_date)); ?></div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="control-label">Receipt Number</label>
            <div class="form-control text-center"><?php echo $payment->receipt_number; ?></div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="control-label">Amount</label>
            <div class="form-control text-right"><?php echo number_format($payment->amount,2,".",""); ?></div>
        </div>
    </div>

</div>

<?php if( !isset($output) || ($output!='ajax') ) : ?>

	    	</div>
	    	<div class="panel-footer">

            <a href="<?php echo site_url("services_lending/payment_apply/" . $payment->id); ?><?php echo ($this->input->get('next')) ? "?next=" . $this->input->get('next') : ""; ?>" class="btn btn-primary pull-right">Apply Payment</a>

	    		<button type="submit" class="btn btn-success">Submit</button>
	    		<a href="<?php echo site_url( ($this->input->get('next')) ? $this->input->get('next') : "services_lending/payments/" . $member->id); ?>" class="btn btn-warning">Back</a>
	    	</div>


	    	</form>
	    </div>
    </div>
</div>
</div>

<?php $this->load->view('footer'); ?>

<?php endif; ?>