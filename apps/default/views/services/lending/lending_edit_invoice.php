<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('services/services_navbar'); ?>

<?php $this->load->view('membership/members/members_navbar'); ?>

    <div class="container">
    <nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <div class="navbar-brand"><?php echo date('m/d/Y', strtotime($current_loan->loan_date)); ?> : &#x20B1;<?php echo number_format($current_loan->principal,2); ?> @ <?php echo number_format($current_loan->interest_rate,2); ?>% for <?php echo $current_loan->months; ?> Months</div>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
      <ul class="nav navbar-nav navbar-right">
        <li class="active"><a href="<?php echo site_url("services_lending/schedule/{$current_loan->id}"); ?>">Schedule</a></li>
        <li class=""><a href="<?php echo site_url("services_lending/loan_payments/{$current_loan->id}"); ?>">Payments</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
</div>

<div class="container">
<div class="row">

	<div class="col-md-6 col-md-offset-3">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
	    		<h3 class="panel-title">Edit Invoice</h3>
	    	</div>
	    	<form method="post">
	    	<div class="panel-body">

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

<?php endif; ?>


        <div class="form-group ">
            <label class="control-label">Due Date</label>
            <input class="form-control datepicker text-right" type="text" name="due_date" value="<?php echo date('m/d/Y', strtotime($current_invoice->due_date)); ?>">
        </div>

        <div class="form-group">
            <label class="control-label">Principal Due</label>
            <input class="form-control text-right" type="text" name="principal_due" value="<?php echo number_format($current_invoice->principal_due,2,".",""); ?>">
        </div>

        <div class="form-group ">
            <label class="control-label">Interest Due</label>
            <input class="form-control text-right" type="text" name="interest_due" value="<?php echo number_format($current_invoice->interest_due,2,".",""); ?>">
        </div>

        <div class="form-group has-success">
            <label class="control-label">Total Due</label>
            <div class="form-control text-right success bold"><?php echo number_format(($current_invoice->principal_due+$current_invoice->interest_due),2); ?></div>
        </div>

<?php if( isset($output) && ($output!='ajax') ) : ?>

	    	</div>
	    	<div class="panel-footer">
	    		<button type="submit" class="btn btn-success">Submit</button>
	    		<a href="<?php echo site_url("services_lending/schedule/" . $current_loan->id); ?>" class="btn btn-warning">Back</a>
	    	</div>


	    	</form>
	    </div>
    </div>
</div>
</div>

<?php $this->load->view('footer'); ?>

<?php endif; ?>