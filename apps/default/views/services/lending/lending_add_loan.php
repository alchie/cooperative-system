<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('services/services_navbar'); ?>

<?php $this->load->view('membership/members/members_navbar'); ?>

<div class="container">
<div class="row">

<form method="post">

<div class="col-md-6 col-md-offset-3">

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>


	    <div class="panel panel-default">
	    	<div class="panel-heading">
	    		<h3 class="panel-title">Loan Details</h3>
	    	</div>

	    	<div class="panel-body">

<?php endif; ?>

<?php if( isset($new_loan) && ($new_loan) ) { ?>

<div class="row">
    <div class="col-md-6">
        <div class="form-group ">
            <label class="control-label">Name</label>
            <div class="form-control bold"><?php echo $new_loan->full_name; ?></div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group ">
            <label class="control-label">Payment Start Date</label>
            <input class="form-control datepicker text-center" type="text" name="payment_start" value="<?php echo date('m/d/Y'); ?>">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
		<div class="form-group">
            <label class="control-label">Interest Rate</label>
            <select class="form-control" name="interest_rate">
            <?php 
            echo '<option value="'.(0).'" '.(((0)==$interest_rate)?'selected':'').'>'.number_format((0),2).'</option>';
            echo '<option value="'.(0.50).'" '.(((0.50)==$interest_rate)?'selected':'').'>'.number_format((0.50),2).'</option>';
            $interest_rate = 3;
            for($i=1;$i<=10;$i++) {
                echo '<option value="'.$i.'" '.(($i==$interest_rate)?'selected':'').'>'.number_format($i,2).'</option>';
                if( $i < 10) {
                echo '<option value="'.($i+0.50).'" '.((($i+0.50)==$interest_rate)?'selected':'').'>'.number_format(($i+0.50),2).'</option>';
                }
            } ?>
            </select>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">Number of Months</label>
             <select class="form-control" name="months">
            <?php 
            $months = 12;
            for($i=1;$i<=24;$i++) {
                echo '<option value="'.$i.'" '.(($i==$months)?'selected':'').'>'.$i.'</option>';
            } ?>
            </select>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">Payment Skip Days</label>
            <select class="form-control" name="skip_days">
            <?php 
            $skip = 15;
            for($i=1;$i<=30;$i++) {
                echo '<option value="'.$i.'" '.(($i==$skip)?'selected':'').'>'.$i.'</option>';
            } ?>
            </select>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">Interest Type</label>
            <select class="form-control" name="interest_type">
            <?php foreach(array('fixed'=>'Fixed', 'diminishing'=>'Diminishing') as $key=>$value) { ?>
              <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
            <?php } ?>
            </select>
        </div>
    </div>
</div>

    <div class="form-group">
            <label class="control-label">Memo</label>
           <textarea class="form-control" name="memo"></textarea>
        </div>

<?php } else { ?>
<p class="text-center">No New Release Loan!</p>
<?php }  ?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

	    	</div>
	    	<div class="panel-footer">
	    		<button type="submit" class="btn btn-success">Submit</button>
	    		<a href="<?php echo site_url("services_lending/loans/" . $member->id); ?>" class="btn btn-warning">Back</a>
	    	</div>


	    </div>

    </div>

</form>

</div>
</div>

<?php $this->load->view('footer'); ?>

<?php endif; ?>