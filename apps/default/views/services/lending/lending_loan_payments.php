<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>

<?php $this->load->view('membership/members/members_navbar'); ?>

<?php $this->load->view('services/lending/lending_navbar'); ?>

<div class="container">
	<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-3" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <div class="navbar-brand">&#x20B1;<?php echo number_format($current_loan->principal,2); ?> @ <?php echo number_format($current_loan->interest_rate,2); ?>% <?php echo $current_loan->interest_type; ?> monthly for <?php echo $current_loan->months; ?> Months</div>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-3">
      <ul class="nav navbar-nav navbar-right">
      	<li class=""><a class="body_wrapper" href="<?php echo site_url("services_lending/schedule/{$current_loan->id}"); ?>">Schedule</a></li>
        <li class="active"><a class="body_wrapper" href="<?php echo site_url("services_lending/loan_payments/{$current_loan->id}"); ?>">Applied Payments</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
</div>

<div class="container">
<div class="row">
  <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">

<?php if( $new_payments ) { ?>
<?php if( hasAccess('services', 'lending', 'edit') ) { ?>
          <button type="button" class="btn btn-success btn-xs pull-right ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Add Payment" data-url="<?php echo site_url("services_lending/new_payments/{$member->id}/ajax") . "?next=" . uri_string(); ?>">Add Payment</button>
<?php } ?>
<?php } ?>

          <h3 class="panel-title">Loan Payments</h3>

        </div>
        <div class="panel-body">

<?php if( $payments ) { ?>

 <table class="table table-condensed">
          <thead>
            <tr>
              <th class="text-left">Paid By</th>
              <th class="text-center" width="10%">Receipt #</th>
              <th class="text-center">Payment Date</th>
              <th class="text-right">Amount Paid</th>
<?php if( hasAccess('services', 'lending', 'edit') ) { ?>
              <th class="text-right" width="70px"></th>
<?php } ?>
            </tr>
          </thead>
          <tbody>
<?php 
$total_payments = 0;
$n=0;
foreach($payments as $payment) { 

  ?>
            <tr>
              <td class="text-left"><?php echo $payment->full_name; ?></td>
              <td class="text-center"><?php echo $payment->receipt_number; ?></td>
              <td class="text-center"><?php echo date('F d, Y', strtotime($payment->payment_date)); ?></td>
              <td class="text-right bold"><?php echo number_format($payment->lp_amount,2); $total_payments+=$payment->lp_amount; ?></td>
<?php if( hasAccess('services', 'lending', 'edit') ) { ?>
              <td>
<?php if( $payment->payor_id == $payment->lender_id) { ?>
              <a class="btn btn-warning btn-xs body_wrapper" href="<?php echo site_url("services_lending/payment_apply/{$payment->payment_id}"); ?>">Update</a>
<?php } ?>
              </td>
<?php } ?>
            </tr>
          <?php $n++; 
        } ?>
<?php if($n > 1) { ?>
<tr class="success">
              <td class="text-left bold" colspan="3">Total Payments</td>
              <td class="text-right bold"><?php echo number_format($total_payments,2);  ?></td>
<?php if( hasAccess('services', 'lending', 'edit') ) { ?>
              <td></td>
<?php } ?>
            </tr>
<?php } ?>
          </tbody>
          </table>

<?php } else { ?>
  <div class="text-center">No Payment Yet!</div>
<?php } ?>

        </div>
      </div>
    </div>
</div>
</div>


<?php $this->load->view('footer'); ?>