<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('services/services_navbar'); ?>

<?php $this->load->view('membership/members/members_navbar'); ?>

<div class="container">
<div class="row">
  <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Applied Payments</h3>
        </div>
        <div class="panel-body">

<?php endif; ?>

<?php if( $payments ) { ?>

 <table class="table table-default table-hover table-condensed">
          <thead>
            <tr>
              <th class="text-left">Paid By</th>
              <th class="text-center" width="20%">Receipt #</th>
              <th class="text-center">Payment Date</th>
              <th class="text-right">Amount Paid</th>
              <th class="text-right" width="1px"></th>
            </tr>
          </thead>
          <tbody>
<?php 
$total_payments = 0;
$n = 0;
foreach($payments as $payment) { ?>
            <tr>
              <td class="text-left"><?php echo $payment->full_name; ?></td>
              <td class="text-center"><?php echo $payment->receipt_number; ?></td>
              <td class="text-center"><?php echo date("F d, Y", strtotime($payment->payment_date)); ?></td>
              <td class="text-right"><?php echo number_format($payment->amount,2); $total_payments+=$payment->amount; ?></td>
              <td>
<?php if( $payment->payor_id == $payment->lender_id) { ?>
<?php if( hasAccess('services', 'lending', 'edit') ) { ?>
              <a class="btn btn-warning btn-xs" href="<?php echo site_url("services_lending/payment_apply/{$payment->payment_id}"); ?>"><span class="glyphicon glyphicon-pencil"></span></a>
<?php } ?>
<?php } ?>
              </td>
            </tr>
          <?php 
  $n++;
} ?>

<?php if( $n > 1) { ?>
<tr class="success">
              <td class="text-left bold" colspan="3">Total Applied Payments</td>
              <td class="text-right bold"><?php echo number_format($total_payments,2);  ?></td>
              <td></td>
            </tr>
<?php } ?>

          </tbody>
          </table>

<?php } else { ?>
  <div class="text-center">No Payment Yet!</div>
<?php } ?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

        </div>
      </div>
    </div>
</div>
</div>


<?php $this->load->view('footer'); ?>

<?php endif; ?>