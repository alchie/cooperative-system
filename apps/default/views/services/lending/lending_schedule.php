<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>

<?php $this->load->view('membership/members/members_navbar'); ?>

<?php $this->load->view('services/lending/lending_navbar'); ?>

<div class="container">
	<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-3" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <div class="navbar-brand">&#x20B1;<?php echo number_format($current_loan->principal,2); ?> @ <?php echo number_format($current_loan->interest_rate,2); ?>% <?php echo $current_loan->interest_type; ?> monthly for <?php echo $current_loan->months; ?> Months</div>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-3">
      <ul class="nav navbar-nav navbar-right">
      	<li class="active"><a class="body_wrapper" href="<?php echo site_url("services_lending/schedule/{$current_loan->id}"); ?>">Schedule</a></li>
        <li class=""><a class="body_wrapper" href="<?php echo site_url("services_lending/loan_payments/{$current_loan->id}"); ?>">Applied Payments</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
</div>

<div class="container">
<div class="row">
  <div class="col-md-12">
      <div class="panel panel-<?php echo ($current_loan->inactive==1) ? 'warning' : 'default'; ?>">
        <div class="panel-heading">

<?php if( hasAccess('services', 'lending', 'edit') ) { ?>

<div class="btn-group pull-right">
  <button type="button" class="btn btn-success btn-xs ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Edit Loan" data-url="<?php echo site_url("services_lending/edit/{$current_loan->id}/ajax") . "?next=" . uri_string(); ?>">Edit Loan</button>
  <button type="button" class="btn btn-success btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <span class="caret"></span>
    <span class="sr-only">Toggle Dropdown</span>
  </button>
  <ul class="dropdown-menu">
    <?php if( (isset($invoices)) && (!$invoices) ) { ?>
    <li><a href="<?php echo site_url( "services_lending/sample_computation/" . $current_loan->id ); ?>" target="_blank">Sample Computation</a></li>
    <?php } else { ?>
    <li><a href="<?php echo site_url( "services_lending/print_statement/" . $current_loan->id ); ?>" target="_blank">Print Statement</a></li>
    <li><a href="<?php echo site_url( "services_lending/print_schedule/" . $current_loan->id ); ?>" target="_blank">Print Schedule</a></li>
    <li class="hidden-xs"><a href="#editInvoices" class="edit_invoices">Edit Invoices</a></li>
    <?php } ?>

  </ul>
</div>

<?php } ?>

<h3 class="panel-title <?php if( $invoices ) { ?>pull-left<?php } ?>">Invoices</h3>

<?php if( $invoices ) { ?>
<center>
  <div class="btn-group btn-group-xs" role="group">
    <button type="button" class="btn btn-success lending-schedule-details" data-type='minimal'>Minimal</button>
    <button type="button" class="btn btn-default lending-schedule-details" data-type='detailed'>Detailed</button>
</div>
</center>
<?php } ?>

        </div>
        <div class="panel-body">

<?php if( (isset($invoices)) && ( $invoices ) ) {  

$installments = ceil((30 / $current_loan->skip_days) * $current_loan->months);
$payment_date = date('m/d/Y', strtotime($current_loan->payment_start));
$principal = $current_loan->principal;
$principal_diminishing = $current_loan->principal;
$principal_decrement = $current_loan->principal;

if( $current_loan->interest_type == 'diminishing' ) {
  $principal_interest = $current_loan->principal + ((($current_loan->principal * ($current_loan->interest_rate / 100) ) / 2 ) * ($installments + 1) * (1/ceil((30 / $current_loan->skip_days))));
  $total_interest = ((($current_loan->principal * ($current_loan->interest_rate / 100) ) / 2 ) * ($installments + 1) * (1/ceil((30 / $current_loan->skip_days))));
} else {
  $interest = (($current_loan->principal * $current_loan->interest_rate) / 100) / ceil(30 / $current_loan->skip_days);
  $total_interest = ((($current_loan->principal * $current_loan->interest_rate) / 100) / ceil(30 / $current_loan->skip_days) * $installments);
  $principal_interest = $current_loan->principal + ($interest * $installments);
}
$total_loan = $principal + $total_interest;
$principal_total = 0;
$interest_total = 0;
$due_increment = 0;

  ?>
  <form method="post" class="hidden-xs">
        <table class="table table-condensed table-hover">
          <thead>
            <tr>
              <th class="text-center edit_invoices_item" width="10px"><button class="confirm btn btn-xs btn-danger" type="submit"><span class="glyphicon glyphicon-trash"></span></button></th>
              <th class="text-center" width="1%">#</th>
              <th class="text-center">Date</th>
              <th class="text-center" colspan="2">Principal Amount</th>
               <th class="text-right">Interest Amount</th>
               <th class="text-center" colspan="2">Gross Amount</th>
               <th class="text-right"><span class="detailed-schedule">Balance</span></th>
                <th class="text-right">Amount Paid</th>
                <th class="text-right">Amount Due</th>
               <th width="10px" class="edit_invoices_item"></th>
            </tr>
          </thead>
          <tbody>
            <tr class="detailed-schedule">
              <td class="text-center edit_invoices_item"><input type="checkbox" class="edit_invoices_select_all"></td>
              <td class="text-center"></td>
              <td></td>
              <td></td>
              <td class="text-right"><?php echo number_format($principal,2); ?></td>
              <td class="text-right"><?php echo number_format($total_interest,2); ?></td>
              <td></td>
              <td></td>
              <td class="text-right"><?php echo number_format($principal_interest,2); ?></td>
              <td></td>
              <td class="text-right"></td>
              <td class="edit_invoices_item"></td>
            </tr>
<?php 

$n=1;
$total_payments = 0;
$amount_due = 0;
foreach($invoices as $invoice) { 
$principal_total += $invoice->principal_due;
$principal_decrement -= $invoice->principal_due;
$principal_interest -= ($invoice->principal_due + $invoice->interest_due);
$due_increment += ($invoice->principal_due + $invoice->interest_due);
$interest_total += $invoice->interest_due; 
$total_payments += $invoice->total_applied;
?>
<?php 
$due_already = false;
$class_name = '';
if( time() > strtotime($invoice->due_date) ) {
  $due_already = true; 
  $class_name = 'danger';
}
$class_name = ( round($current_loan->total_payments) >= round($due_increment)) ? 'success' : $class_name; 
if( $this->input->get('highlight') && (( $this->input->get('highlight') == $invoice->id ) || ( $this->input->get('highlight') == $invoice->trn_id ) ) ) {
  $class_name = 'info';
}
            ?>
            <tr class="<?php echo $class_name; ?>">
              <td class="text-center edit_invoices_item">
<?php if( is_null($invoice->total_applied) || ($invoice->total_applied == '') ) { ?>
              <input type="checkbox" class="edit_invoices_delete_item" name="delete_invoice[]" value="<?php echo $invoice->id; ?>">
 <?php } ?>
              </td>
              <td class="text-center"><?php echo $n++; ?></td>
              <td class="text-center"><?php echo date('m/d/Y', strtotime($invoice->due_date)); ?></td>
              <td class="bold text-right"><?php echo number_format($invoice->principal_due,2); ?></td>
              <td class="text-right"><span class="detailed-schedule"><?php echo number_format($principal_decrement,2); ?></span></td>
              <td class="bold text-right"><?php echo number_format($invoice->interest_due,2); ?></td>
              <td class="bold text-right"><?php echo number_format($invoice->principal_due + $invoice->interest_due,2); ?></td>
              <td class="text-right"><span class="detailed-schedule"><?php echo number_format($due_increment,2); ?></span></td>
               <td class="text-right"><span class="detailed-schedule"><?php echo number_format($principal_interest,2); ?></span></td>
              <td class="text-right bold">
              
                <a href="" class="ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-hide_footer="1" data-title="Applied Payments" data-url="<?php echo site_url("services_lending/applied_payments/{$invoice->id}/ajax" ) . "?next=" . uri_string(); ?>">
                <?php echo number_format($invoice->total_applied,2); 

                ?>
                </a>
              
              </td>
             <td class="text-right bold">
               <?php 
if( $due_already ) {
               echo ((($invoice->principal_due + $invoice->interest_due)-$invoice->total_applied) > 0) ? number_format((($invoice->principal_due + $invoice->interest_due)- $invoice->total_applied),2) : ''; 
               $amount_due += (($invoice->principal_due + $invoice->interest_due)-$invoice->total_applied);
}
               ?>
             </td>
              <td class="text-right edit_invoices_item">
    <?php if( is_null($invoice->total_applied) || ($invoice->total_applied == '') ) { ?>
              <a href="#edit-invoice" class="pull-right ajax-modal btn btn-xs btn-warning" data-toggle="modal" data-target="#ajaxModal" data-title="Edit Invoice" data-url="<?php echo site_url("services_lending/edit_invoice/{$invoice->id}/ajax" ) . "?next=" . uri_string(); ?>"><span class="glyphicon glyphicon-pencil"></span></a>

    <?php } ?>
              </td>
            </tr>
<?php } ?>
             <tr>
              <td class="edit_invoices_item"><button class="confirm btn btn-xs btn-danger" type="submit"><span class="glyphicon glyphicon-trash"></span></button></td>
              <td class="text-center"></td>
              <td class="text-right"></td>
              
              <td class="bold text-right"><?php echo number_format($principal_total,2); ?></td>
              <td></td>
              <td class="bold text-right"><?php echo number_format($interest_total,2); ?></td>
              <td class="bold text-right"><?php echo number_format($due_increment,2); ?></td>
               <td></td>
              <td class="text-right"></td>
              <td class="bold text-right"><?php echo number_format($total_payments,2); ?></td>
              <td class="bold text-right"><?php echo number_format($amount_due,2); ?></td>
              <td class="text-right edit_invoices_item"></td>
            </tr>
<?php 
  $difference_principal = $principal - $principal_total;
  $difference_interest = $total_interest - $interest_total;

  if( (round($difference_principal) > 0) || (round($difference_interest) > 0)) {
?>
<tr class="danger">
              <td class="edit_invoices_item"></td>
              <td class="text-center bold allcaps">Difference</td>
              <td class="text-right"></td>
              
              <td class="bold text-right"><?php echo number_format($difference_principal,2); ?></td>
              <td></td>
              <td class="bold text-right"><?php echo number_format($difference_interest,2); ?></td>
              <td class="bold text-right"><?php echo number_format(($difference_principal+$difference_interest),2); ?></td>
               <td></td>
              <td class="text-right"></td>
              <td class="bold text-right"><a href="<?php echo site_url("services_lending/add_invoice/{$current_loan->id}" ); ?>" class="pull-right btn btn-xs btn-warning">Create Invoice</a></td>
              <td class="text-right"></td>
              <td class="text-right edit_invoices_item"></td>
            </tr>
<?php } ?>
          </tbody>
        </table>
</form>

<div class="panel-group visible-xs" id="accordion" role="tablist" aria-multiselectable="true">
  <?php 
$due_increment = 0;
$total_loan = 0;
foreach($invoices as $invoice) { 
  $due_increment += ($invoice->principal_due + $invoice->interest_due);
  $total_loan += $invoice->principal_due + $invoice->interest_due;
    ?>
  <div class="panel panel-<?php 
$due_already = 'default';            
if( time() > strtotime($invoice->due_date) ) {
  $due_already = 'danger'; 
}
$class_name = ( round($current_loan->total_payments) >= round($due_increment)) ? 'success' : $due_already; 
if( $this->input->get('highlight') && ( $this->input->get('highlight') == $invoice->id ) ) {
  $class_name = 'info';
}
echo $class_name;
?>">
    <div class="panel-heading" role="tab" id="headingOne">
    <a style="color:#555;text-decoration: none;" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $invoice->id; ?>" aria-expanded="true" aria-controls="collapseOne">
      <h4 class="panel-title">
        
        <strong class="pull-right"><?php echo number_format($invoice->principal_due + $invoice->interest_due,2); ?></strong>
          <?php echo date('m/d/Y', strtotime($invoice->due_date)); ?> 
        
      </h4></a>
    </div>
    <div id="collapse<?php echo $invoice->id; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body">
        
<ul class="list-group">
  <li class="list-group-item">Principal: <strong class="pull-right"><?php echo number_format($invoice->principal_due,2); ?></strong></li>
  <li class="list-group-item">Interest: <strong class="pull-right"><?php echo number_format($invoice->interest_due,2); ?></strong></li>
  <li class="list-group-item">Total: <strong class="pull-right"><?php echo number_format($invoice->principal_due + $invoice->interest_due,2); ?></strong></li>
  <li class="list-group-item">Paid: <strong class="pull-right"><?php echo number_format($invoice->total_applied,2); ?></strong></li>
</ul>

      </div>
    </div>
  </div>
  <?php } ?>
</div>

<ul class="list-group visible-xs">
  <li class="list-group-item"><strong>TOTAL:</strong> <strong class="pull-right"><?php echo number_format($total_loan,2); ?></strong></li>
</ul>

<?php } else { ?>

  <div class="text-center">

  <p><strong>No Invoice Yet!</strong> <br>Click button below to create invoices!</p>

<form method="post">
            <button type="submit" name="create_invoices" value="<?php echo $current_loan->id; ?>" class="btn btn-warning btn-xs" style="margin-right:10px">Create Invoices</button>
        </form>

  </div>

          
        


<?php } ?>
        </div>
      </div>
    </div>
</div>
</div>


<?php $this->load->view('footer'); ?>