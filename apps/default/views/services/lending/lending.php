<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>

<?php $this->load->view('services/services_navbar'); ?>

<div class="container">
<div class="row">
  <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">
        
<a data-toggle="modal" data-target="#searchName" class="btn btn-success btn-xs pull-right">Add Loan</a>



          <h3 class="panel-title">Loans
          <!-- Single button -->
<div class="btn-group">
  <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <?php echo (isset($loan_page_title)) ? $loan_page_title : 'Outstanding'; ?> <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
    <li><a href="<?php echo site_url('services_lending/index/outstanding'); ?>">Outstanding</a></li>
    <li><a href="<?php echo site_url('services_lending/index/inactive'); ?>">Inactive</a></li>
    <li><a href="<?php echo site_url('services_lending/index/pending'); ?>">Pending</a></li>
  </ul>
</div>

</h3>
        </div>
        <div class="panel-body">

<?php if( $loans ) { ?>
          <table class="table table-default">
            <thead>
              <tr>
                <th>Lender</th>
                <th>Loan Date</th>
                <!--<th>Principal</th>
                <th>Interests</th>-->
                <th>Gross Amount</th>
                <!-- <th>Interest Type</th> -->
                <th>Amount Paid</th>
                <th>Balance</th>
                <th width="95px">Action</th>
              </tr>
            </thead>
            <tbody>
            <?php foreach($loans as $loan) { 

$interest_rate = ($loan->interest_rate) ? $loan->interest_rate : 0;
$skip = ($loan->skip_days) ? $loan->skip_days : 1;
$installments = ceil((30 / $skip) * $loan->months);
$payment_date = date('m/d/Y', strtotime($loan->payment_start));
$principal = $loan->principal;
$principal_diminishing = $loan->principal;
$principal_total = 0;
$principal_decrement = $loan->principal; 

if( $loan->interest_type == 'diminishing' ) {
  $principal_interest = $loan->principal + ((($loan->principal * ($interest_rate / 100) ) / 2 ) * ($installments + 1) * (1/ceil((30 / $skip))));
  $total_interest = ((($loan->principal * ($interest_rate / 100) ) / 2 ) * ($installments + 1) * (1/ceil((30 / $skip))));
} else {
  $interest = (($loan->principal * $interest_rate) / 100) / ceil(30 / $skip);
  $total_interest = ((($loan->principal * $interest_rate) / 100) / ceil(30 / $skip) * $installments);
  $principal_interest = $loan->principal + ($interest * $installments);
}
              ?>
              <tr >
                <td><a href="<?php echo site_url("services_lending/loans/{$loan->member_id}"); ?>"><?php echo $loan->lastname; ?>, <?php echo $loan->firstname; ?> <?php echo substr($loan->middlename,0,1); ?></a></td>
                <td><?php echo date('F d, Y', strtotime($loan->loan_date)); ?></td>
               <!-- <td><?php echo number_format($loan->principal,2); ?></td>
                <td><?php echo number_format($total_interest,2); ?></td> -->
                <td>
                <span class="hoverPopRight" title="Gross Amount" data-content="<strong>Principal:</strong> <?php echo number_format($loan->principal,2); ?><br><strong>Interest:</strong> <?php echo number_format($total_interest,2); ?> <hr><small><strong>Interest Type:</strong> <?php echo ucwords($loan->interest_type); ?><br><strong>Interest Rate:</strong> <?php echo round($interest_rate,2); ?>% Monthly</small>">
                <?php echo number_format($loan->principal+$total_interest,2); ?>
                  </span>
                </td>
                <!--<td><?php echo ucwords($loan->interest_type); ?></td>-->
                <td><?php echo number_format($loan->payments,2); ?></td>
              <td><?php echo number_format(($loan->principal + $total_interest) - $loan->payments,2); ?></td>
                <td>
                <a href="<?php echo site_url("services_lending/schedule/" . $loan->id); ?>" class="btn btn-warning btn-xs">View Loan</a>
                </td>
              </tr>
            <?php } ?>
            </tbody>
          </table>


<?php echo ($pagination!='') ? '<center>' . $pagination . '</center>' : ''; ?>

<?php } else { ?>
  <div class="text-center">No Account Found!</div>
<?php } ?>


        </div>
      </div>
    </div>
</div>
</div>


<!-- #searchMember Modal -->
<div class="modal fade" id="searchName" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalLabel">Add Loan</h4>
      </div>
<form method="get" action="<?php echo site_url('services_lending/search_name'); ?>">
      <div class="modal-body">

        <div class="form-group">
            <label class="control-label">Search Member</label>
            <input type="text" class="form-control" name="q" value="">
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Search</button>
      </div>
</form>
    </div>
  </div>
</div>

<?php $this->load->view('footer'); ?>