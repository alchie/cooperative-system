<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<html>
<head>
  <title><?php echo $member->lastname; ?>, <?php echo $member->firstname; ?> <?php echo $member->middlename; ?> Loan Schedule</title>
  <style>
    <!--
    body {
      font-family: Arial;
      padding:0;
      margin:0;
      font-size: 11px;
    }
    .container {
      width: 100%;
    }
    .wrapper {
      border: 1px solid #000;
      padding:10px;
      margin-bottom: 10px;
    }
    .allcaps {
      text-transform: uppercase;
    }
    .label {
      color:#000;
      text-transform: uppercase;
      font-size: 10px;
      text-decoration: underline;
    }
    .detail {
      font-size: 11px;
      margin-left: 10px;
      margin-top: 10px;
      font-weight: bold;
    }
    table td {
      vertical-align: middle;
      padding: 3px;
    }
    .border-bottom td {
      border-bottom: 1px solid #000;
    }
    .text-center {
      text-align: center;
    }
    th {
      font-size: 11px;
    }
    td {
      font-size: 11px;
    }
    .signature {
      border-top:1px solid #000;
      text-align: center;
      margin: 40px auto 0;
      font-size: 11px;
      font-weight: bold;
    }
    .header-label {
      margin: 0 0 20px 0;
      border-bottom: 1px solid #000;
      padding: 10px;
      background-color: #CCC;
    }
    .total {
      font-size: 11px;
      font-weight: bold;
    }
    -->
  </style>
</head>
<body>
<div class="wrapper">
<h3 class="text-center allcaps header-label">Check Voucher</h3>
<table width="100%" class="border-bottom">
    <tr>
      <td width="66.67%">
        <span class="label">Paid to</span>
        <div class="detail allcaps"><?php echo $member->lastname; ?>, <?php echo $member->firstname; ?> <?php echo $member->middlename; ?></div>
      </td>
       <td width="33.33%">
        <span class="label">Date</span>
        <div class="detail"><?php echo date('F d, Y', strtotime($current_loan->loan_date)); ?></div>
      </td>
    </tr>
  </table>


  <table width="100%" class="border-bottom">
    <tr>
      <td width="66.67%">
        <span class="label">Address</span>
        <div class="detail"><?php echo ($member->unit) ? $member->unit . "," : ""; ?> <?php echo ($member->building) ? $member->building. "," : ""; ?> <?php echo ($member->lot_block) ? $member->lot_block. "," : ""; ?> <?php echo ($member->street) ? $member->street. "," : ""; ?> <?php echo ($member->subdivision) ? $member->subdivision. "," : ""; ?> <?php echo ($member->barangay) ? $member->barangay. "," : ""; ?> <?php echo ($member->city) ? $member->city. "," : ""; ?> <?php echo ($member->province) ? $member->province. "," : ""; ?> <?php echo $member->zip; ?></div>
      </td>
      <td width="33.33%">
        <span class="label">CV #: </span>
        <div class="detail"><?php echo $current_loan->trn_id; ?></div>
      </td>
    </tr>
  </table>

  <table width="100%" class="border-bottom">
    <tr>
      <td width="66.67%">
        <span class="label">Account</span>
        <div class="detail"></div>
      </td>
      <td width="33.33%">
        <span class="label">Amount</span>
        <div class="detail"><?php echo number_format($current_loan->principal,2); ?></div>
      </td>
    </tr>
  </table>

  </div>

<div class="wrapper">
<table width="100%">
    <tr>
      <td width="50%" class="text-center">
        <div class="signature allcaps"><?php echo $prepared->name; ?></div>
        <div class="allcaps">Prepared By</div>
      </td>
      <td width="50%" class="text-center">
        <div class="signature allcaps"><?php echo $approver->name; ?></div>
        <div class="allcaps">Approved By</div>
      </td>
  </tr>
  <tr>
    <td width="50%" class="text-center">
        <div class="signature allcaps"><?php echo $releaser->name; ?></div>
        <div class="allcaps">Released By</div>
      </td>
      <td width="50%" class="text-center">
        <div class="signature allcaps"><?php echo strtoupper($member->lastname); ?>, <?php echo strtoupper($member->firstname); ?> <?php echo strtoupper(substr($member->middlename, 0, 1)); ?>.</div>
        <div class="allcaps">Received By</div>
      </td>
  </tr>
  </table>
    
</div> 
</body>
</html>