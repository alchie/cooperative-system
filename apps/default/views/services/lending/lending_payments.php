<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>

<?php if( ! $inner_page ): ?>

<?php $this->load->view('membership/members/members_navbar'); ?>

<?php $this->load->view('services/lending/lending_navbar'); ?>

<div class="container">
<div class="row">
  <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">
<?php if( $new_payments ) { ?>
<?php if( hasAccess('services', 'lending', 'edit') ) { ?>
          <button type="button" class="btn btn-success btn-xs pull-right ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Add Payment" data-url="<?php echo site_url("services_lending/new_payments/{$member->id}/ajax") . "?next=" . uri_string(); ?>" data-hide_footer="1">Add Payment</button>
<?php } ?>
<?php } ?>
          <h3 class="panel-title">Payments

<div class="btn-group">
  <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <?php echo (isset($payment_page_title)) ? $payment_page_title : 'All Payments'; ?> <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
    <li class="<?php echo (isset($payment_page_title)&&($payment_page_title=='All Payments')) ? 'active' : ''; ?>"><a class="ajaxPage" href="<?php echo site_url("services_lending/payments/{$member->id}"); ?>">All Payments</a></li>
    <li class="<?php echo (isset($payment_page_title)&&($payment_page_title=='Applied Payments')) ? 'active' : ''; ?>"><a class="ajaxPage" href="<?php echo site_url("services_lending/payments/{$member->id}/applied"); ?>">Applied Payments</a></li>
    <li class="<?php echo (isset($payment_page_title)&&($payment_page_title=='Un-applied Payments')) ? 'active' : ''; ?>"><a class="ajaxPage" href="<?php echo site_url("services_lending/payments/{$member->id}/unapplied"); ?>">Un-applied Payments</a></li>
  </ul>
</div>
          </h3>
        </div>

        <div class="panel-body" id="ajaxBodyInnerPage" data-url="<?php echo site_url(uri_string()); ?>">

<?php endif; // inner_page ?>

<?php if( isset($payments) ) { ?>
<?php if( $payments ) { ?>

 <table class="table table-condensed hidden-xs">
          <thead>
            <tr>
              <th class="">Payment Date</th>
               <th class="text-center">Receipt #</th>
              <th class="text-right">Amount Paid</th>
              <th class="text-right">Amount Applied</th>
<?php if( hasAccess('services', 'lending', 'edit') ) { ?>
              <th class="text-right" width="200px"></th>
<?php } ?>
            </tr>
          </thead>
          <tbody>
<?php 
$total_payments = 0;
$total_applied = 0;
foreach($payments as $payment) { ?>
     <tr class="<?php echo (round($payment->amount,2) > round($payment->total_applied)) ? 'danger' : ''; ?>">
              <td class=""><?php echo date("F d, Y", strtotime($payment->payment_date)); ?></td>
              <td class="text-center"><?php echo $payment->receipt_number; ?></td>
              <td class="text-right"><?php echo number_format($payment->amount,2); $total_payments+=$payment->amount; ?></td>
              <td class="text-right"><?php echo number_format($payment->total_applied,2); $total_applied+=$payment->total_applied; ?></td>
              
<?php if( hasAccess('services', 'lending', 'edit') ) { ?>
              <td class="text-right">
              <a class="btn btn-primary btn-xs body_wrapper" href="<?php echo site_url("services_lending/payment_apply/{$payment->id}"); ?>">Apply Payments</a>
              <?php if( ROUND($payment->total_applied) == 0) { ?>
              <a class="confirm" href="<?php echo site_url("services_lending/payment_delete/{$payment->id}/{$payment->member_id}"); ?>"><span class="glyphicon glyphicon-trash"></span></a>
              <?php } ?>
              </td>
<?php } ?>

            </tr>
<?php } ?>
          </tbody>
          </table>

<div class="panel-group visible-xs" id="accordion" role="tablist" aria-multiselectable="true">
  <?php foreach($payments as $payment) { ?>
 <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="heading-<?php echo $payment->id ?>">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-<?php echo $payment->id ?>" aria-expanded="false" aria-controls="collapse-<?php echo $payment->id ?>">
          <?php echo date("F d, Y", strtotime($payment->payment_date)); ?> <strong class="pull-right"><?php echo number_format($payment->amount,2); ?></strong>
        </a>
      </h4>
    </div>
    <div id="collapse-<?php echo $payment->id ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-<?php echo $payment->id ?>">
      <div class="panel-body">
       

<div class="list-group">
  <div class="list-group-item">Payment Date: <strong class="pull-right"><?php echo date("m/d/Y", strtotime($payment->payment_date)); ?></strong></div>
  <div class="list-group-item">Receipt Number: <strong class="pull-right"><?php echo $payment->receipt_number; ?></strong></div>
  <div class="list-group-item">Amount: <strong class="pull-right"><?php echo number_format($payment->amount,2); ?></strong></div>
  <div class="list-group-item">Applied: <strong class="pull-right"><?php echo number_format($payment->total_applied,2); ?></strong></div>
  
  <a class="list-group-item text-center active" href="<?php echo site_url("services_lending/payment/{$payment->id}"); ?>"">View Payment</a>
</div>

      </div>
    </div>
  </div>

    <?php } ?>
</div>

<?php echo ($pagination!='') ? '<center>' . $pagination . '</center>' : ''; ?>

<?php } else { ?>
  <div class="text-center">No Payment Yet!</div>
<?php } ?>

<?php } else { ?>
  Please wait...
<?php } ?>

<?php if( ! $inner_page ): ?>

        </div>
      </div>
    </div>
</div>
</div>

<?php endif; // inner_page ?>

<?php $this->load->view('footer'); ?>