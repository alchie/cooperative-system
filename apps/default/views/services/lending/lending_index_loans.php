<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>

<?php if( ! $inner_page ): ?>

<?php $this->load->view('services/services_navbar'); ?>

<?php $this->load->view('services/lending/lending_navbar'); ?>

<div class="container">
<div class="row">
  <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">
        
        <a data-toggle="modal" data-target="#printSample" class="btn btn-primary btn-xs pull-right hidden-xs">Print Sample</a>

<?php if( $new_loans ) { ?>
          <button type="button" class="btn btn-success btn-xs pull-right ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Add Loan" data-url="<?php echo site_url("services_lending/add/0/ajax") . "?next=" . uri_string(); ?>" style="margin-right:10px;">Add Loan</button>
<?php } ?>

          <h3 class="panel-title"><?php echo (isset($loan_page_title)) ? $loan_page_title : 'Outstanding'; ?> Loans
          <!-- Single button -->
<div class="btn-group">
  <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <?php echo (isset($loan_page_title)) ? $loan_page_title : 'Outstanding'; ?> <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
    <li class="<?php echo (isset($loan_page_title)&&($loan_page_title=='Outstanding')) ? 'active' : ''; ?>"><a class="ajaxPage" href="<?php echo site_url('services_lending/index/loans/outstanding'); ?>">Outstanding</a></li>
    <li class="<?php echo (isset($loan_page_title)&&($loan_page_title=='Inactive')) ? 'active' : ''; ?>"><a class="ajaxPage" href="<?php echo site_url('services_lending/index/loans/inactive'); ?>">Inactive</a></li>
    <li class="<?php echo (isset($loan_page_title)&&($loan_page_title=='Pending')) ? 'active' : ''; ?>"><a class="ajaxPage" href="<?php echo site_url('services_lending/index/loans/pending'); ?>">Pending</a></li>
    <li class="<?php echo (isset($loan_page_title)&&($loan_page_title=='Archived')) ? 'active' : ''; ?>"><a class="ajaxPage" href="<?php echo site_url('services_lending/index/loans/archived'); ?>">Archived</a></li>
  </ul>
</div>

</h3>
        </div>
        <div class="panel-body" id="ajaxBodyInnerPage" data-url="<?php echo site_url(uri_string()); ?>">
<?php endif; // inner_page ?>

<?php if( isset($loans) ) { ?>
<?php if( $loans ) { ?>
          <table class="table table-default table-hover table-condensed hidden-xs">
            <thead>
              <tr>
                <th>Lender</th>
                <th>Loan Date</th>
                <!--<th>Principal</th>
                <th>Interests</th>-->
                <th>Gross Amount</th>
                <!-- <th>Interest Type</th> -->
                <th>Amount Paid</th>
                <th>Balance</th>
                <th  class="text-right" width="95px">Action</th>
              </tr>
            </thead>
            <tbody>
<?php foreach($loans as $loan) { 

$interest_rate = ($loan->interest_rate) ? $loan->interest_rate : 0;
$skip = ($loan->skip_days) ? $loan->skip_days : 1;
$installments = ceil((30 / $skip) * $loan->months);
$payment_date = date('m/d/Y', strtotime($loan->payment_start));
$principal = $loan->principal;
$principal_diminishing = $loan->principal;
$principal_total = 0;
$principal_decrement = $loan->principal; 

if( $loan->interest_type == 'diminishing' ) {
  $principal_interest = $loan->principal + ((($loan->principal * ($interest_rate / 100) ) / 2 ) * ($installments + 1) * (1/ceil((30 / $skip))));
  $total_interest = ((($loan->principal * ($interest_rate / 100) ) / 2 ) * ($installments + 1) * (1/ceil((30 / $skip))));
} else {
  $interest = (($loan->principal * $interest_rate) / 100) / ceil(30 / $skip);
  $total_interest = ((($loan->principal * $interest_rate) / 100) / ceil(30 / $skip) * $installments);
  $principal_interest = $loan->principal + ($interest * $installments);
}
?>
              <tr class="<?php echo ($loan->entry_exist==0) ? 'danger' : ''; ?>">
                <td><a class="body_wrapper" href="<?php echo site_url("services_lending/loans/{$loan->member_id}"); ?>"><?php echo $loan->lastname; ?>, <?php echo $loan->firstname; ?> <?php echo substr($loan->middlename,0,1); ?></a></td>
                <td><?php echo date('F d, Y', strtotime($loan->loan_date)); ?></td>
               <!-- <td><?php echo number_format($loan->principal,2); ?></td>
                <td><?php echo number_format($total_interest,2); ?></td> -->
                <td>
                <span class="hoverPopRight" title="Gross Amount" data-content="<strong>Principal:</strong> <?php echo number_format($loan->principal,2); ?><br><strong>Interest:</strong> <?php echo number_format($total_interest,2); ?> <hr><small><strong>Interest Type:</strong> <?php echo ucwords($loan->interest_type); ?><br><strong>Interest Rate:</strong> <?php echo round($interest_rate,2); ?>% Monthly</small>">
                <?php echo number_format($loan->principal+$total_interest,2); ?>
                  </span>
                </td>
                <!--<td><?php echo ucwords($loan->interest_type); ?></td>-->
                <td><?php echo number_format($loan->payments,2); ?></td>
              <td><?php echo number_format(($loan->principal + $total_interest) - $loan->payments,2); ?></td>
                <td class="text-right">
                <a href="<?php echo site_url("services_lending/schedule/" . $loan->id); ?>" class="btn btn-warning btn-xs body_wrapper">View Loan</a>
                </td>
              </tr>
            <?php } ?>
            </tbody>
          </table>

<ul class="list-group visible-xs">
  <?php foreach($loans as $loan) { 

$interest_rate = ($loan->interest_rate) ? $loan->interest_rate : 0;
$skip = ($loan->skip_days) ? $loan->skip_days : 1;
$installments = ceil((30 / $skip) * $loan->months);
$payment_date = date('m/d/Y', strtotime($loan->payment_start));
$principal = $loan->principal;
$principal_diminishing = $loan->principal;
$principal_total = 0;
$principal_decrement = $loan->principal; 
?>
      <a href="<?php echo site_url("services_lending/schedule/{$loan->member_id}"); ?>" class="list-group-item">
        <h4 class="list-group-item-heading"><?php echo $loan->lastname; ?>, <?php echo $loan->firstname; ?> <?php echo substr($loan->middlename,0,1); ?></h4>
        <p class="list-group-item-text"><?php echo date('F d, Y', strtotime($loan->loan_date)); ?> &middot; <?php echo number_format($loan->principal,2); ?></p>
      </a>
    <?php } ?>
</ul>

<?php echo ($pagination!='') ? '<center>' . $pagination . '</center>' : ''; ?>

<?php } else { ?>
  <div class="text-center">No Loan Found!</div>
<?php } ?>

<?php } else { ?>
  Please Wait...
<?php } ?>

<?php if( ! $inner_page ): ?>

        </div>
      </div>
    </div>
</div>
</div>



<!-- #printSample Modal -->
<div class="modal fade" id="printSample" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalLabel">Print Sample Computation</h4>
      </div>
<form method="get" action="<?php echo site_url('services_lending/print_sample'); ?>" target="_blank">
      <div class="modal-body">

<div class="row">

    <div class="col-md-6">
        <div class="form-group ">
            <label class="control-label">Payment Start Date</label>
            <input class="form-control datepicker" type="text" name="payment_start" value="<?php echo date('m/d/Y'); ?>">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group ">
            <label class="control-label">Principal Amount</label>
            <input class="form-control text-right" type="text" name="principal" value="5000">
        </div>
    </div>

</div>
<div class="row">
    <div class="col-md-6">
    <div class="form-group">
            <label class="control-label">Interest Rate</label>
            <select class="form-control" name="interest_rate">
            <?php 
            echo '<option value="'.(0).'" '.(((0)==$interest_rate)?'selected':'').'>'.number_format((0),2).'</option>';
            echo '<option value="'.(0.50).'" '.(((0.50)==$interest_rate)?'selected':'').'>'.number_format((0.50),2).'</option>';
            $interest_rate = 3;
            for($i=1;$i<=10;$i++) {
                echo '<option value="'.$i.'" '.(($i==$interest_rate)?'selected':'').'>'.number_format($i,2).'</option>';
                if( $i < 10) {
                echo '<option value="'.($i+0.50).'" '.((($i+0.50)==$interest_rate)?'selected':'').'>'.number_format(($i+0.50),2).'</option>';
                }
            } ?>
            </select>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">Number of Months</label>
             <select class="form-control" name="months">
            <?php 
            $months = 6;
            for($i=1;$i<=24;$i++) {
                echo '<option value="'.$i.'" '.(($i==$months)?'selected':'').'>'.$i.'</option>';
            } ?>
            </select>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">Payment Skip Days</label>
            <select class="form-control" name="skip_days">
            <?php 
            $skip = 15;
            for($i=1;$i<=30;$i++) {
                echo '<option value="'.$i.'" '.(($i==$skip)?'selected':'').'>'.$i.'</option>';
            } ?>
            </select>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">Interest Type</label>
            <select class="form-control" name="interest_type">
            <?php foreach(array('fixed'=>'Fixed', 'diminishing'=>'Diminishing') as $key=>$value) { ?>
              <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
            <?php } ?>
            </select>
        </div>
    </div>
</div>


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
</form>
    </div>
  </div>
</div>

<?php endif; // inner_page ?>

<?php $this->load->view('footer'); ?>