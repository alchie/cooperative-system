<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>

<?php if( ! $inner_page ): ?>

<?php $this->load->view('services/services_navbar'); ?>

<?php $this->load->view('services/lending/lending_navbar'); ?>

<div class="container">
<div class="row">
  <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">
        
<?php if( $new_payments ) { ?>
          <button type="button" class="btn btn-success btn-xs pull-right ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Add Payment" data-url="<?php echo site_url("services_lending/new_payments/0/ajax") . "?next=" . uri_string(); ?>" data-hide_footer="1">Add Payment</button>
<?php } ?>

          <h3 class="panel-title">Payments
          <!-- Single button -->
<div class="btn-group">
  <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <?php echo (isset($loan_page_title)) ? $loan_page_title : 'All Payments'; ?> <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
    <li class="<?php echo (isset($loan_page_title)&&($loan_page_title=='All Payments')) ? 'active' : ''; ?>"><a class="ajaxPage" href="<?php echo site_url('services_lending/index/payments'); ?>">All Payments</a></li>
    <li class="<?php echo (isset($loan_page_title)&&($loan_page_title=='Applied Payments')) ? 'active' : ''; ?>"><a class="ajaxPage" href="<?php echo site_url('services_lending/index/payments/applied'); ?>">Applied Payments</a></li>
    <li class="<?php echo (isset($loan_page_title)&&($loan_page_title=='Un-applied Payments')) ? 'active' : ''; ?>"><a class="ajaxPage" href="<?php echo site_url('services_lending/index/payments/unapplied'); ?>">Un-applied Payments</a></li>
  </ul>
</div>

</h3>
        </div>
        <div class="panel-body" id="ajaxBodyInnerPage" data-url="<?php echo site_url(uri_string()); ?>">

<?php endif; // inner_page ?>

<?php if( isset($payments) ) { ?>
<?php if( $payments ) { ?>

 <table class="table table-default table-hover table-condensed">
          <thead>
            <tr>
              <th class="text-left">Paid By</th>
              <th class="text-center" width="10%">Receipt #</th>
              <th class="text-center">Payment Date</th>
              <th class="text-right">Amount Paid</th>
              <th class="text-right">Amount Applied</th>
              <th class="text-right" width="100px"></th>
            </tr>
          </thead>
          <tbody>
<?php 
$total_payments = 0;
$total_applied = 0;
$n=0;
foreach($payments as $payment) { 

  if( $payment->member == 1) {
    $name_uri = "services_lending/payments/{$payment->member_id}";
    $apply_uri = "services_lending/payment_apply/{$payment->id}";
  } else {
    $name_uri = "membership_companies/payments/{$payment->member_id}";
    $apply_uri = "membership_companies/payment_apply/{$payment->id}";
  }

?>
            <tr class="<?php echo (round($payment->amount,2) > round($payment->total_applied)) ? 'danger' : ''; ?>">
              <td class="text-left">
              <a class="body_wrapper" href="<?php echo site_url($name_uri); ?>">
              <?php echo $payment->full_name; ?>
              </a>
              </td>
              <td class="text-center"><?php echo $payment->receipt_number; ?></td>
              <td class="text-center"><?php echo date("F d, Y", strtotime($payment->payment_date)); ?></td>
              <td class="text-right"><?php echo number_format($payment->amount,2); $total_payments+=$payment->amount; ?></td>
              <td class="text-right"><?php echo number_format($payment->total_applied,2); $total_applied+=$payment->total_applied; ?></td>
              <td class="text-right">
              <a class="btn btn-primary btn-xs body_wrapper" href="<?php echo site_url($apply_uri); ?>">Apply Payments</a>
              </td>
            </tr>
<?php } ?>
          </tbody>
          </table>

<?php echo ($pagination!='') ? '<center>' . $pagination . '</center>' : ''; ?>

<?php } else { ?>
  <div class="text-center">No Payment Yet!</div>
<?php } ?>

<?php } else { ?>
  Please Wait...
<?php } ?>

<?php if( ! $inner_page ): ?>

        </div>
      </div>
    </div>
</div>
</div>

<?php endif; // inner_page ?>

<?php $this->load->view('footer'); ?>