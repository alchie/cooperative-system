<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<html>
<head>
  <title>Open Invoices</title>
  <link href="<?php echo base_url('assets/css/print.css'); ?>" rel="stylesheet">
</head>
<body>

<?php if( isset($output) && ($output!='iframe') ) : ?>
  
<div class="wrapper">
<h3 class="text-center allcaps header-label">
Open Invoices
<br><small><?php echo date('F d, Y', strtotime($filters['date_range_beg'])); ?> - <?php echo date('F d, Y', strtotime($filters['date_range_end'])); ?></small>
</h3>

<?php else: ?>

  <body style="padding:10px">

<?php endif; ?>

<?php if( $invoices ) { ?>

         <table width="100%" cellpadding="0" cellspacing="0">
          <thead>
            <tr>
              <th class="text-left">Lender</th>
              <th class="text-center">Due Date</th>
              <th class="text-right">Principal</th>
              <th class="text-right">Interest</th>
              <th class="text-right">Gross</th>
              <th class="text-right">Paid</th>
              <th class="text-right">Balance</th>
            </tr>
          </thead>
          <tbody>
<?php 
$total_principal_due = 0;
$total_interest_due = 0;
$total_gross_amount = 0;
$total_paid = 0;
$total_balance = 0;
$n=1;
foreach($invoices as $invoice) { ?>
            <tr>
              <td class="text-left"><?php echo $n++; ?>. <?php echo $invoice->lastname; ?>, <?php echo $invoice->firstname; ?> <?php echo substr($invoice->middlename,0,1); ?></td>
              <td class="text-center"><?php echo date("F d, Y", strtotime($invoice->due_date)); ?></td>
              <td class="text-right"><?php echo number_format($invoice->principal_due,2); $total_principal_due+=$invoice->principal_due; ?></td>
              <td class="text-right"><?php echo number_format($invoice->interest_due,2); $total_interest_due+=$invoice->interest_due; ?></td>
              <td class="text-right"><?php echo number_format($invoice->gross_amount,2); $total_gross_amount+=$invoice->gross_amount; ?></td>
              <td class="text-right"><?php echo number_format($invoice->total_paid,2); $total_paid+=$invoice->total_paid; ?></td>
              <td class="text-right"><?php echo number_format($invoice->balance,2); $total_balance+=$invoice->balance; ?></td>
            </tr>
<?php } ?>
<?php if($n>2) { ?>
<tr class="overalltotal">
              <td class="text-left bold" colspan="2">Total</td>
              <td class="text-right bold"><?php echo number_format($total_principal_due,2);  ?></td>
              <td class="text-right bold"><?php echo number_format($total_interest_due,2);  ?></td>
              <td class="text-right bold"><?php echo number_format($total_gross_amount,2);  ?></td>
              <td class="text-right bold"><?php echo number_format($total_paid,2);  ?></td>
              <td class="text-right bold"><?php echo number_format($total_balance,2);  ?></td>
            </tr>
<?php } ?>
          </tbody>
          </table>

<?php } else { ?>
  <div class="wrapper text-center allcaps bold">
  Empty Result
  </div>
 <?php } ?>
 

<?php if( isset($output) && ($output!='iframe') ) : ?>

</div>

<?php endif; ?>

</body>
</html>