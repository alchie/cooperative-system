<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>

<?php if( ! $inner_page ): ?>

<?php $this->load->view('reports/reports_navbar'); ?>

<div class="container">
<div class="row">
	<div class="col-md-12">
	    <div class="panel panel-default">
	    	<div class="panel-heading">

<div class="btn-group btn-group-xs pull-right">
<?php if( $filters ) { ?>
  <a type="button" class="btn btn-success btn-xs" href="<?php echo site_url("report_transactions/save_report") . "?" . $filters_uri; ?>"><span class="fa fa-pencil"></span> Save</a>
<?php } ?>
  <button type="button" class="btn btn-primary btn-xs ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Modify Report" data-url="<?php echo site_url("report_transactions/modify/ajax") . "?" . $filters_uri; ?>"><span class="fa fa-pencil"></span> Modify</button>
  <a target="print" class="btn btn-warning" href="<?php echo site_url("report_transactions/generate_report") . "?" . $filters_uri; ?>"><span class="fa fa-print"></span> Print</a>
</div>


	    		<h3 class="panel-title">Entries</h3>
	    	</div>
	    	<div class="panel-body" id="ajaxBodyInnerPage" data-url="<?php echo site_url(uri_string()) . "?" . $filters_uri; ?>">

<?php endif; ?>

<?php if( isset($entries) ) { ?>
<?php if( $entries ) { ?>
	    		<table class="table table-condensed table-hover">
	    			<thead>
	    				<tr>
	    					<th>Type</th>
	    					<th width="15%">Date</th>
	    					<th>Number</th>
	    					<th>Name</th>
	    					<th>Memo</th>
	    					<th class="text-right">Debit</th>
	    					<th class="text-right">Credit</th>
	    					<th width="10px" class="text-right"></th>
	    				</tr>
	    			</thead>
	    			<tbody>
	    			<?php foreach($entries as $entry) { ?>
	    				<tr class="<?php echo ($entry->trn_exist) ? '' : 'danger'; ?>">
	    					<td>
<span class="hoverPopRight" title="<?php echo $entry->type; ?>" data-content="<strong>Department:</strong> <?php echo $entry->dept; ?><br><strong>Section:</strong> <?php echo $entry->sect; ?>">
                <?php echo $entry->type; ?>
                  </span>
	    					</td>
	    					<td><?php echo date('m/d/Y', strtotime($entry->date)); ?></td>
	    					<td><?php echo $entry->number; ?></td>
	    					<td><?php echo $entry->full_name; ?></td>
	    					<td><?php echo $entry->memo; ?></td>
	    					<td class="text-right"><?php echo (round($entry->debit)) ? number_format($entry->debit,2) : ''; ?></td>
	    					<td class="text-right"><?php echo (round($entry->credit)) ? number_format($entry->credit,2) : ''; ?></td>
	    					<td class="text-right">
	    					<a href="<?php echo site_url('report_transactions/redirect/' .  $entry->id ) . "?next=" . uri_string(); ?>" class="item"><span class="fa fa-eye"></span></a>
	    					</td>
	    				</tr>
	    			<?php } ?>
	    			</tbody>
	    		</table>

<?php echo ($pagination!='') ? '<center>' . $pagination . '</center>' : ''; ?>

<?php } else { ?>
	<div class="text-center">No Transaction Found!</div>
<?php } ?>
<?php } else { ?>
	Please wait...
<?php } ?>

<?php if( ! $inner_page ): ?>

	    	</div>
	    </div>
    </div>
</div>
</div>

<?php endif; ?>

<?php $this->load->view('footer'); ?>