<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<html>
<head>
  <title><?php echo ($filters['report_title']) ? $filters['report_title'] : 'Transactions Report'; ?></title>
  <link href="<?php echo base_url('assets/css/print.css'); ?>" rel="stylesheet">
</head>
<body>

<?php if( isset($output) && ($output!='iframe') ) : ?>

<div class="wrapper">
<h3 class="text-center allcaps header-label">
<?php echo ($filters['report_title']) ? $filters['report_title'] : 'Transactions Report'; ?>
<br><small><?php echo (isset($filters['date_range_beg'])) ? date('F d, Y', strtotime($filters['date_range_beg'])) : date('F 01, Y'); ?> - <?php echo (isset($filters['date_range_end'])) ? date('F d, Y', strtotime($filters['date_range_end'])) : date('F d, Y'); ?></small>
</h3>

<?php else: ?>

  <body style="padding:10px">

<?php endif; ?>

<?php if( $transactions ) { ?>
          <table width="100%" cellpadding="0" cellspacing="0">
            <thead>
              <tr>
<?php 
$filter_columns = ($filters['columns']) ? explode('|', urldecode($filters['columns']) ) : array('type', 'date', 'number', 'full_name', 'memo', 'amount');
foreach(array(
                    'id'=>'ID',
                    'dept'=>'Department',
                    'sect'=>'Section',
                    'type'=>'Type',
                    'date'=>'Date',
                    'memo'=>'Memo',
                    'number'=>'Number',
                    'full_name'=>'Name',
                    'amount'=>'Amount',
                    ) as $key=>$value) { 
                    if(!in_array($key, $filter_columns)) {
                        continue;
                    }
?>
                <th class="transaction transaction-<?php echo $key; ?>"><?php echo $value; ?></th>
<?php } ?>
              </tr>
            </thead>
            <tbody>
           <?php foreach($transactions as $transaction) { ?>
              <tr>
<?php 
foreach(array(
                    'id'=>'ID',
                    'dept'=>'Department',
                    'sect'=>'Section',
                    'type'=>'Type',
                    'date'=>'Date',
                    'memo'=>'Memo',
                    'number'=>'Number',
                    'full_name'=>'Name',
                    'amount'=>'Amount',
                    ) as $key=>$value) { 
                    if(!in_array($key, $filter_columns)) {
                        continue;
                    }
echo '<td class="transaction-'.$key.'">';                    
  switch($key) {
    case 'date':
      echo date('m/d/Y', strtotime( $transaction->$key ) ); 
    break;
    case 'amount':
      echo number_format( $transaction->$key, 2 ); 
    break;
    default:
      echo $transaction->$key;
    break;
  }
  echo '</td>';

} ?>

              </tr>
            <?php } ?>
            </tbody>
          </table>

<?php } else { ?>
  <div class="wrapper text-center allcaps bold">
  Empty Result
  </div>
 <?php } ?>
 

<?php if( isset($output) && ($output!='iframe') ) : ?>

</div>

<?php endif; ?>

</body>
</html>