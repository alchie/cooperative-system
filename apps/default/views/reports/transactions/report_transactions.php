<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
<?php 
$columns = array(
                    'id'=>'ID',
                    'dept'=>'Department',
                    'sect'=>'Section',
                    'type'=>'Type',
                    'date'=>'Date',
                    'memo'=>'Memo',
                    'number'=>'Number',
                    'full_name'=>'Name',
                    'debit'=>'Debit',
                    'credit'=>'Credit',
                    'amount'=>'Amount',
                    );
?>
<?php if( ! $inner_page ): ?>

<?php $this->load->view('reports/reports_navbar'); ?>

<div class="container">
<div class="row">
	<div class="col-md-12">
	    <div class="panel panel-default">
	    	<div class="panel-heading">

<div class="btn-group btn-group-xs pull-right">
<?php 
$save_uri = "report_transactions/save_report"; 
$generate_uri = "report_transactions/generate_report"; 
  if( isset($current_report) && $current_report ) {
    $save_uri .= '/' . $current_report->id;
    $generate_uri .= '/' . $current_report->id;
  }
?>
<?php if(hasAccess('report', 'transactions', 'edit')) { ?>
<?php if( $this->input->get('filter') ) { ?>
  <a type="button" class="btn btn-success btn-xs" href="<?php echo site_url($save_uri) . "?" . $filters_uri; ?>"><span class="fa fa-pencil"></span> Save</a>
<?php } ?>
<?php } ?>
  <button type="button" class="btn btn-primary btn-xs ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Modify Report" data-url="<?php echo site_url("report_transactions/modify/ajax") . "?next=" . uri_string() . "&" . $filters_uri; ?>"><span class="fa fa-pencil"></span> Modify</button>
  <a target="print" class="btn btn-warning" href="<?php echo site_url($generate_uri) . "?" . $filters_uri; ?>"><span class="fa fa-print"></span> Print</a>
</div>

	    		<h3 class="panel-title">
<?php if(hasAccess('report', 'transactions', 'delete')) { ?>
<?php if( isset($current_report) && $current_report ) { ?>
<a href="<?php echo site_url("report_transactions/delete_report/{$current_report->id}"); ?>" class="confirm">
 <span class="glyphicon glyphicon-trash"></span>
</a>
<?php } ?>
<?php } ?>
          <?php echo (isset($filters['report_title'])) ? $filters['report_title'] : 'Transactions Report'; ?>
         
            <small><?php echo (isset($filters['date_range_beg'])) ? date('F d, Y', strtotime($filters['date_range_beg'])) : date('F 01, Y'); ?> - <?php echo (isset($filters['date_range_end'])) ? date('F d, Y', strtotime($filters['date_range_end'])) : date('F d, Y'); ?></small>


          </h3>
	    	</div>
	    	<div class="panel-body" id="ajaxBodyInnerPage" data-url="<?php echo site_url(uri_string()) . "?" . $filters_uri; ?>">

<?php endif; ?>

<?php if( isset($transactions) ) { ?>
<?php if( $transactions ) { ?>
	    		<table class="table table-condensed table-hover">
	    			<thead>
	    				<tr>
<?php 
$filter_columns = (isset($filters['columns'])) ? explode('|', urldecode($filters['columns']) ) : array('type', 'date', 'number', 'full_name', 'memo', 'amount');
foreach($columns as $key=>$value) { 
                    if(!in_array($key, $filter_columns)) {
                        continue;
                    }
?>
	    					<th class="transaction transaction-<?php echo $key; ?>"><?php echo $value; ?></th>
<?php } ?>

	    					<th width="50px" class="text-right"></th>
	    				</tr>
	    			</thead>
	    			<tbody>
	    			<?php foreach($transactions as $transaction) { ?>
	    				<tr class="<?php echo (($transaction->entries == 0) || ($transaction->debit != $transaction->credit) || ($transaction->debit != $transaction->amount) || ($transaction->amount != $transaction->credit)) ? 'danger' : ''; ?>">
<?php 
foreach($columns as $key=>$value) { 
                    if(!in_array($key, $filter_columns)) {
                        continue;
                    }

echo '<td class="transaction-'.$key.'">';                    
  switch($key) {
    case 'date':
      echo date('m/d/Y', strtotime( $transaction->$key ) ); 
    break;
    case 'debit':
    case 'credit':
    case 'amount':
      echo number_format( $transaction->$key, 2 ); 
    break;
    default:
      echo $transaction->$key;
    break;
  }
  echo '</td>';
	    					
} ?>
	    					<td class="text-right">
<?php if($transaction->entries) { ?>
	    					<a target="entries" href="<?php echo site_url('report_entries/transaction/' .  $transaction->id ) ?>" class="item"><span class="glyphicon glyphicon-align-justify"></span></a>
                <a target="redirect" href="<?php echo site_url('report_transactions/redirect/' .  $transaction->id ) . "?next=" . uri_string(); ?>" class="item"><span class="fa fa-eye"></span></a>
<?php } else { ?>
  
  <a target="redirect" href="<?php echo site_url('report_transactions/delete/' .  $transaction->id ) . "?next=" . uri_string(); ?>" class="item"><span class="glyphicon glyphicon-trash"></span></a>

<?php } ?>
	    					
	    					</td>
	    				</tr>
	    			<?php } ?>
	    			</tbody>
	    		</table>

<?php echo ($pagination!='') ? '<center>' . $pagination . '</center>' : ''; ?>

<?php } else { ?>

  <div class="text-center">No Transaction Found!</div>

<?php } ?>
<?php } else { ?>

	Please wait...

<?php } ?>

<?php if( ! $inner_page ): ?>

	    	</div>
	    </div>
    </div>
</div>
</div>

<?php endif; ?>

<?php $this->load->view('footer'); ?>