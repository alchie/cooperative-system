<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); 
$types = array();
foreach($coa_types as $type) {
	foreach($type as $k=>$t) {
		$types[$k] = $t;
	}
}
$net_income = 0;
function display_list($accounts , $pre="", $post="", $filter='', $level=0, $results=array()) {
		for($i=$level;$i>=0;$i--) {
			if( !isset($results[$level]) ) {
				$results[$i] = 0;
			} 
		}
		foreach($accounts as $account) { 
				if( !strpos($account->type, $filter) ) {
					continue;
				}
				$detail_url = site_url("report_income_statement/details/{$account->id}"); 
			?>
	    				<tr>
	    					<td><?php echo $account->number; ?></td>
	    					<td><?php echo $pre; ?><a href="<?php echo $detail_url; ?>" class="item body_wrapper"><?php echo $account->title; ?></a></td>
	    					<td class="text-right"><a href="<?php echo $detail_url; ?>" class="item body_wrapper"><?php 
	    					
							
							if( (strpos($account->type, '_IS_EXP_') ) ) {
								$balance = ($account->debit_balance - $account->credit_balance);
							} else {
								$balance = ($account->credit_balance - $account->debit_balance);
							}
							
							for($i=$level;$i>=0;$i--) {
								if( !isset($results[$level]) ) {
									$results[$i] = 0;
								} else {
									$results[$i] += $balance;
								}
							}

	    					echo (intval($balance)!=0) ? number_format($balance,2) : ''; 
	    					?>
	    						<?php echo $post; ?>
	    					</a></td>
	    				</tr>
	    		<?php 
					if( $account->children ) {
						$new_level = $level++;
						$results = display_list( $account->children, $pre . "- - - - ", $post . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", $filter, $new_level, $results );
						?>
							<tr>
		    					<td></td>
		    					<td><?php echo $pre; ?><strong>Total <?php echo $account->title; ?></strong></td>
		    					<td class="text-right"><strong><?php echo (intval($results[$new_level]) != 0 ) ? number_format($results[$new_level],2) : ''; ?></strong><?php echo $post; ?></td>
	    					</tr>
						<?php
					}
	} 
	return $results;
}
?>
<?php if( ! $inner_page ): ?>
<style>
<!-- 
a.item, a.item:hover {
	color:#555555;
	text-decoration: none;
}
-->
</style>
<?php $this->load->view('reports/reports_navbar'); ?>

<div class="container">
<div class="row">
	<div class="col-md-12">
	    <div class="panel panel-default">
	    	<div class="panel-heading">

<div class="btn-group btn-group-xs pull-right">
  <button type="button" class="btn btn-primary btn-xs ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Modify Report" data-url="<?php echo site_url("report_income_statement/modify/ajax") . "?next=" . uri_string() . "&" . $filters_uri; ?>"><span class="fa fa-pencil"></span> Modify</button>
  <a target="_blank" href="<?php echo site_url("report_income_statement/print_report") . "?next=" . uri_string() . "&" . $filters_uri; ?>" class="btn btn-warning">Print Report</a>
</div>

	    		<h3 class="panel-title"><strong><?php echo (isset($filters['report_title'])) ? $filters['report_title'] : 'Income Statement'; ?></strong></h3>

<small>
<?php echo ($this->input->get('from')) ? date("F d, Y", strtotime($this->input->get('from'))) : date("F d, Y", strtotime($date_from)); ?> - <?php echo ($this->input->get('to')) ? date("F d, Y", strtotime($this->input->get('to'))) : date("F d, Y", strtotime($date_to)); ?> 
</small>
	    	</div>

	    	<div class="panel-body" id="ajaxBodyInnerPage" data-url="<?php echo site_url(uri_string()) . "?" . $filters_uri; ?>">

<?php endif; ?>

<?php if( isset($accounts) ) { ?>
<?php if( $accounts ) { ?>
	    		<table class="table table-default">
	    			<tbody>

<tr>
	<td colspan="3"><strong>INCOME</strong></td>
</tr>

<?php $results = display_list($accounts, '', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', '_IS_INC_'); ?>

<tr class="success">
	<td colspan="2"><strong>TOTAL INCOME</strong></td>
	<td class="text-right"><strong><?php echo number_format($results[0],2); $net_income = $net_income + $results[0]; ?><strong></td>
</tr>
<tr>
	<td colspan="3"></td>
</tr>
<tr>
	<td colspan="3"><strong>EXPENSES</strong></td>
</tr>
<?php $results = display_list($accounts, '', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', '_IS_EXP_'); ?>

<tr class="success">
	<td colspan="2"><strong>TOTAL EXPENSES</strong></td>
	<td class="text-right"><strong><?php echo number_format($results[0],2); $net_income = $net_income - $results[0]; ?><strong></td>
</tr>
<tr>
	<td colspan="3"></td>
</tr>
<tr class="success">
	<td colspan="2"><strong>NET INCOME</strong></td>
	<td class="text-right"><strong><?php echo number_format($net_income,2); ?><strong></td>
</tr>

	    			</tbody>
	    		</table>
<?php } else { ?>
	<div class="text-center">No Account Found!</div>
<?php } ?>
<?php } else { ?>
	Please wait...
<?php } ?>

	    	</div>
	    </div>
    </div>
</div>
</div>

<?php $this->load->view('footer'); ?>