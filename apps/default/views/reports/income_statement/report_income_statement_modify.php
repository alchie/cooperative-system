<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('reports/reports_navbar'); ?>

<div class="container">
<div class="row">
	<div class="col-md-6 col-md-offset-3">

<form method="post">

	    <div class="panel panel-default">

	    	<div class="panel-heading">

	    		<h3 class="panel-title">Modify Balance Sheet Report</h3>

	    	</div>
	    	<div class="panel-body">

<?php endif; ?>



<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingHeader">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseHeader" aria-expanded="true" aria-controls="collapseHeader">
          Header
        </a>
      </h4>
    </div>
    <div id="collapseHeader" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingHeader">
      <div class="panel-body">
        
            <div class="form-group ">
            <label class="control-label">Report Title</label>
            <input class="form-control" type="text" name="filter[report_title]" value="<?php echo (isset($filters['report_title'])) ? $filters['report_title'] : "Income Statement"; ?>">
            </div>

      </div>
    </div>
  </div>


  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingDateRange">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseDateRange" aria-expanded="false" aria-controls="collapseDateRange">
          Date Range
        </a>
      </h4>
    </div>
    <div id="collapseDateRange" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingDateRange">
      <div class="panel-body">
        <div class="row">
    <div class="col-md-6">
        <div class="form-group ">
            <label class="control-label">Beginning</label>
            <input class="form-control datepicker text-center" type="text" name="filter[date_range_beg]" value="<?php echo (isset($filters['date_range_beg'])) ? $filters['date_range_beg'] : date('m/01/Y'); ?>">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group ">
            <label class="control-label">Ending</label>
            <input class="form-control datepicker text-center" type="text" name="filter[date_range_end]" value="<?php echo (isset($filters['date_range_end'])) ? $filters['date_range_end'] : date('m/d/Y'); ?>">
        </div>
    </div>
</div>

      </div>
    </div>
  </div>



  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingFooter">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFooter" aria-expanded="false" aria-controls="collapseFooter">
          Footer
        </a>
      </h4>
    </div>
    <div id="collapseFooter" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFooter">
      <div class="panel-body">
        
      </div>
    </div>
  </div>

</div>

<?php if( isset($output) && ($output!='ajax') ) : ?>
	    	</div>
	    	<div class="panel-footer">
	    		<button type="submit" class="btn btn-success">Generate Report</button>
	    	</div>
	    </div>

</form>

    </div>
</div>
</div>

<?php $this->load->view('footer'); ?>

<?php endif; ?>