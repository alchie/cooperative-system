<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<html>
<head>
  <title<?php echo (isset($filters['report_title'])) ? $filters['report_title'] : 'Balance Sheet'; ?> (<?php echo (isset($filters['date_range_end'])) ? date("F d, Y", strtotime($filters['date_range_end'])) : date("F d, Y"); ?>)</title>
  <style>
    <!--
    body {
      font-family: Arial;
      padding:0;
      margin:0;
      font-size: 12px;
    }
    .container {
      width: 100%;
    }
    .wrapper {
      border: 1px solid #000;
      padding:10px;
      margin-bottom: 10px;
    }
    .allcaps {
      text-transform: uppercase;
    }
    .label {
      color:#000;
      text-transform: uppercase;
      text-decoration: underline;
    }
    .detail {
      font-size: 12px;
      margin-left: 10px;
      margin-top: 10px;
      font-weight: bold;
    }
    table td {
      vertical-align: middle;
      padding: 3px;
    }
    .border-bottom td {
      border-bottom: 1px solid #000;
    }
    .text-center {
      text-align: center;
    }
    th {
      font-size: 12px;
    }
    td {
      font-size: 12px;
    }
    .signature {
      border-top:1px solid #000;
      text-align: center;
      margin: 40px auto 0;
      font-size: 12px;
      font-weight: bold;
    }
    .header-label {
      margin: 0 0 20px 0;
      border-bottom: 1px solid #000;
      padding: 10px;
      background-color: #EEE;
    }
    .header-label small {
    	font-weight: normal;
    }
    .total {
      font-size: 12px;
      font-weight: bold;
    }
    .text-right {
    	text-align: right;
    }
    table td {
    	border-bottom: 1px solid #CCC;
    }
    tr.overalltotal {
    	background-color: #EEE;
    }
    tr.overalltotal td {
    	padding: 5px;
    	font-size: 14px;
    }
    -->
  </style>
</head>
<body>
<div class="wrapper">

<h3 class="text-center allcaps header-label"><?php echo (isset($filters['report_title'])) ? $filters['report_title'] : 'Balance Sheet'; ?>
<br><small>as of <?php echo (isset($filters['date_range_end'])) ? date("F d, Y", strtotime($filters['date_range_end'])) : date("F d, Y"); ?></small>
</h3>

<?php 
$types = array();
foreach($coa_types as $type) {
	foreach($type as $k=>$t) {
		$types[$k] = $t;
	}
}

function display_list($accounts , $pre="", $post="", $filter='', $level=0, $results=array(), $settings=false, $surplus=false) {

		for($i=$level;$i>=0;$i--) {
			if( !isset($results[$level]) ) {
				$results[$i] = 0;
			} 
		}

		foreach($accounts as $account) { 

			$detail_url = site_url("report_balance_sheet/details/{$account->id}"); 

			if( ! strpos($account->type, $filter) ) {
				continue;
			}

			if( $settings && $surplus ) {
				if( $settings->fund_surplus == $account->id ) {
					$account->debit_balance += $surplus->debit_balance;
					$account->credit_balance += $surplus->credit_balance;
				}
			}

			?>
	    				<tr>
	    					<td><?php echo $account->number; ?></td>
	    					<td><?php echo $pre; ?><?php echo $account->title; ?></td>
	    					<td class="text-right">
	    					
	    					<?php 
	    					
							
							if( (strpos($account->type, '_BS_ASS_') ) ) {
								$balance = ($account->debit_balance - $account->credit_balance);
							} else {
								$balance = ($account->credit_balance - $account->debit_balance);
							}
							
							for($i=$level;$i>=0;$i--) {
								if( !isset($results[$level]) ) {
									$results[$i] = 0;
								} else {
									$results[$i] += $balance;
								}
							}

	    					echo (intval($balance)!=0) ? number_format($balance,2) : ''; 
	    					?>

	    						<?php echo $post; ?>

	    					</td>
	    				</tr>
	    		<?php 
					if( $account->children ) {
						$new_level = $level++;
						$results = display_list( $account->children, $pre . "- - - - ", $post . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", $filter, $new_level, $results, $settings, $surplus );
						?>
							<tr>
		    					<td></td>
		    					<td><?php echo $pre; ?><strong>Total <?php echo $account->title; ?></strong></td>
		    					<td class="text-right">
		    					<strong><?php echo (intval($results[$new_level]) != 0 ) ? number_format($results[$new_level],2) : ''; ?></strong>
		    						<?php echo $post; ?>
		    					</td>
	    					</tr>
						<?php
					}
	} 
	return $results;
} ?>

<?php if( $accounts ) { ?>
	    		<table width="100%" cellpadding="0" cellspacing="0">
	    			<tbody>
<tr>
	<td colspan="3"><strong>ASSETS</strong></td>
</tr>
<?php $results = display_list($accounts, '', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', '_BS_ASS_'); ?>
<tr class="overalltotal">
	<td colspan="2"><strong>TOTAL ASSETS</strong></td>
	<td class="text-right"><strong><?php echo number_format($results[0],2); ?><strong></td>
</tr>

<tr>
	<td colspan="3"></td>
</tr>
<?php $total_ll_eq = 0; ?>
<tr>
	<td colspan="3"><strong>LIABILITIES</strong></td>
</tr>
<?php  $results = display_list($accounts, '', '', '_BS_LIB_'); ?>
<tr class="">
	<td colspan="2"><strong>TOTAL LIABILITIES</strong></td>
	<td class="text-right"><strong><?php echo number_format($results[0],2); $total_ll_eq += $results[0]; ?><strong></td>
</tr>

<tr>
	<td colspan="3"><strong>EQUITY</strong></td>
</tr>
<?php  $results = display_list($accounts, '', '', '_BS_EQU_', 0, array(), $settings, $surplus); ?>
<tr class="">
	<td colspan="2"><strong>TOTAL EQUITY</strong></td>
	<td class="text-right"><strong><?php echo number_format($results[0],2); $total_ll_eq += $results[0]; ?><strong></td>
</tr>
<tr>
	<td colspan="3"></td>
</tr>
<tr class="overalltotal">
	<td colspan="2"><strong>TOTAL LIABILITIES AND EQUITY</strong></td>
	<td class="text-right"><strong><?php echo number_format($total_ll_eq,2); ?><strong></td>
</tr>


	    			</tbody>
	    		</table>
<?php } else { ?>
	<div class="text-center">No Account Found!</div>
<?php } ?>

</div> 
</body>
</html>