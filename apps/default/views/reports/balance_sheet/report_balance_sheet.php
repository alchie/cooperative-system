<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
<?php

$types = array();
foreach($coa_types as $type) {
	foreach($type as $k=>$t) {
		$types[$k] = $t;
	}
}

function display_list($accounts , $pre="", $post="", $filter='', $level=0, $results=array(), $settings=false, $surplus=false) {

		for($i=$level;$i>=0;$i--) {
			if( !isset($results[$level]) ) {
				$results[$i] = 0;
			} 
		}

		foreach($accounts as $account) { 

			$detail_url = site_url("report_balance_sheet/details/{$account->id}"); 

			if( ! strpos($account->type, $filter) ) {
				continue;
			}

			if( $settings && $surplus ) {
				if( $settings->fund_surplus == $account->id ) {
					$account->debit_balance += $surplus->debit_balance;
					$account->credit_balance += $surplus->credit_balance;
					$detail_url = site_url("report_income_statement");
				}
			}

			?>
	    				<tr>
	    					<td><?php echo $account->number; ?></td>
	    					<td><?php echo $pre; ?><a href="<?php echo $detail_url; ?>" class="item body_wrapper"><?php echo $account->title; ?></a></td>
	    					<td class="text-right">
	    					<a href="<?php echo $detail_url; ?>" class="item body_wrapper">
	    					<?php 
	    					
							
							if( (strpos($account->type, '_BS_ASS_') ) ) {
								$balance = ($account->debit_balance - $account->credit_balance);
							} else {
								$balance = ($account->credit_balance - $account->debit_balance);
							}
							
							for($i=$level;$i>=0;$i--) {
								if( !isset($results[$level]) ) {
									$results[$i] = 0;
								} else {
									$results[$i] += $balance;
								}
							}

	    					echo (intval($balance)!=0) ? number_format($balance,2) : ''; 
	    					?></a>
	    						<?php echo $post; ?>

	    					</td>
	    				</tr>
	    		<?php 
					if( $account->children ) {
						$new_level = $level++;
						$results = display_list( $account->children, $pre . "- - - - ", $post . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", $filter, $new_level, $results, $settings, $surplus );

						?>
							<tr>
		    					<td></td>
		    					<td><?php echo $pre; ?><a href="<?php echo $detail_url; ?>" class="item body_wrapper"><strong>Total <?php echo $account->title; ?></strong></a></td>
		    					<td class="text-right">
		    					<a href="<?php echo $detail_url; ?>" class="item body_wrapper"><strong><?php echo (intval($results[$new_level]) != 0 ) ? number_format($results[$new_level],2) : ''; ?></strong></a>
		    						<?php echo $post; ?>
		    					</td>
	    					</tr>
						<?php
					}
	} 
	return $results;
} 

?>

<?php if( ! $inner_page ): ?>

<style>
<!-- 
a.item, a.item:hover {
	color:#555555;
	text-decoration: none;
}
-->
</style>
<?php $this->load->view('reports/reports_navbar'); ?>

<div class="container">
<div class="row">
	<div class="col-md-12">
	    <div class="panel panel-default">

	    	<div class="panel-heading">

<div class="btn-group btn-group-xs pull-right">

   <button type="button" class="btn btn-primary btn-xs ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Modify Report" data-url="<?php echo site_url("report_balance_sheet/modify/ajax") . "?next=" . uri_string() . "&" . $filters_uri; ?>"><span class="fa fa-pencil"></span> Modify</button>

  <a target="_blank" class="btn btn-warning" href="<?php echo site_url("report_balance_sheet/print_report") . "?next=" . uri_string() . "&" . $filters_uri; ?>">Print Report</a>
</div>
	    		<h3 class="panel-title"><strong><?php echo (isset($filters['report_title'])) ? $filters['report_title'] : 'Balance Sheet'; ?></strong></h3>

<small class="text-center">
	    		<?php echo (isset($filters['date_range_end'])) ? date("F d, Y", strtotime($filters['date_range_end'])) : date("F d, Y"); ?> 
</small>
	    	</div>
	    	
	    	<div class="panel-body" id="ajaxBodyInnerPage" data-url="<?php echo site_url(uri_string()) . "?" . $filters_uri; ?>">

<?php endif; ?>

<?php if( isset($accounts) ) { ?>
<?php if( $accounts ) { ?>
	    		<table class="table table-default table-hover">
	    			<tbody>
<tr>
	<td colspan="3"><strong>ASSETS</strong></td>
</tr>
<?php $results = display_list($accounts, '', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', '_BS_ASS_'); ?>
<tr class="success">
	<td colspan="2"><strong>TOTAL ASSETS</strong></td>
	<td class="text-right"><strong><?php echo number_format($results[0],2); ?><strong></td>
</tr>

<tr>
	<td colspan="3"></td>
</tr>
<?php $total_ll_eq = 0; ?>
<tr>
	<td colspan="3"><strong>LIABILITIES</strong></td>
</tr>
<?php  $results = display_list($accounts, '', '', '_BS_LIB_'); ?>
<tr class="warning">
	<td colspan="2"><strong>TOTAL LIABILITIES</strong></td>
	<td class="text-right"><strong><?php echo number_format($results[0],2); $total_ll_eq += $results[0]; ?><strong></td>
</tr>

<tr>
	<td colspan="3"><strong>EQUITY</strong></td>
</tr>
<?php  $results = display_list($accounts, '', '', '_BS_EQU_', 0, array(), $settings, $surplus); ?>
<tr class="warning">
	<td colspan="2"><strong>TOTAL EQUITY</strong></td>
	<td class="text-right"><strong><?php echo number_format($results[0],2); $total_ll_eq += $results[0]; ?><strong></td>
</tr>
<tr>
	<td colspan="3"></td>
</tr>
<tr class="success">
	<td colspan="2"><strong>TOTAL LIABILITIES AND EQUITY</strong></td>
	<td class="text-right"><strong><?php echo number_format($total_ll_eq,2); ?><strong></td>
</tr>


	    			</tbody>
	    		</table>
<?php } else { ?>
	<div class="text-center">No Account Found!</div>
<?php } ?>
<?php } else { ?>
	Please wait... 
<?php } ?>

	    	</div>
	    </div>
    </div>
</div>
</div>

<?php $this->load->view('footer'); ?>