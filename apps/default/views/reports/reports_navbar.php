<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container">
	<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <div class="navbar-brand">Reports</div>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

      <ul class="nav navbar-nav navbar-right">

        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $current_page; ?> <span class="caret"></span></a>
          <ul class="dropdown-menu">

<?php 
$url['report_balance_sheet'] = array('uri' => 'report_balance_sheet', 'title'=>'Balance Sheet', 'access'=>hasAccess('report', 'balance_sheet', 'view'));
$url['report_income_statement'] = array('uri' => 'report_income_statement', 'title'=>'Income Statement', 'access'=>hasAccess('report', 'income_statement', 'view'));
$url['report_transactions'] = array('uri' => 'report_transactions', 'title'=>'Transactions', 'access'=>hasAccess('report', 'transactions', 'view'));
$url['report_entries'] = array('uri' => 'report_entries', 'title'=>'Entries', 'access'=>hasAccess('report', 'entries', 'view'));
$url['report_lending'] = array('uri' => 'report_lending', 'title'=>'Lending', 'access'=>hasAccess('report', 'lending', 'view'));

foreach($url as $k=>$v) {
?>
<?php if( is_array($v) ) { 
  if( $v['access'] ) {
  ?>
  <li class="<?php echo ($k==$current_uri) ? 'active' : ''; ?>"><a class="body_wrapper" href="<?php echo site_url($v['uri']); ?>"><?php echo $v['title']; ?></a></li>
<?php 
  }
} else { ?>
  <li role="separator" class="divider"></li>
<?php } ?>
<?php } ?>
  
          </ul>
        </li>
<?php if( isset( $current_uri ) && ($current_uri=='report_lending') ) { ?>
 <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo (isset($current_sub_page)) ? $current_sub_page : "Lending Reports"; ?> <span class="caret"></span></a>
          <ul class="dropdown-menu">

<?php 
$sub_url['report_lending_outstanding_loans'] = array('uri' => 'report_lending/outstanding_loans', 'title'=>'Outstanding Loans');
$sub_url['report_lending_pending_loans'] = array('uri' => 'report_lending/pending_loans', 'title'=>'Pending Loans');
$sub_url['report_lending_archived_loans'] = array('uri' => 'report_lending/archived_loans', 'title'=>'Archived Loans');
$sub_url['separator1'] = 'separator';
$sub_url['report_lending_payments'] = array('uri' => 'report_lending/payments', 'title'=>'Payments');
$sub_url['report_lending_applied_payments'] = array('uri' => 'report_lending/applied_payments', 'title'=>'Applied Payments');
$sub_url['separator2'] = 'separator';
$sub_url['report_lending_open_invoices'] = array('uri' => 'report_lending/open_invoices', 'title'=>'Open Invoices');

foreach($sub_url as $k=>$v) {
 ?>
<?php if( is_array($v) ) { ?>
  <li class="<?php echo (isset($current_sub_uri) && ($k==$current_sub_uri)) ? 'active' : ''; ?>"><a href="<?php echo site_url($v['uri']); ?>"><?php echo $v['title']; ?></a></li>
<?php } else { ?>
  <li role="separator" class="divider"></li>
<?php } ?>
<?php } ?>


          </ul>
</li>
<?php } ?>
<?php if( isset( $reports ) && $reports ) { ?>
 <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Saved Reports<span class="caret"></span></a>
          <ul class="dropdown-menu">
<?php foreach($reports as $report) { ?>
          <li class="<?php echo (isset($current_report) && ($current_report->id==$report->id)) ? 'active' : ''; ?>"><a class="body_wrapper" href="<?php echo site_url("{$current_uri}/view_report/{$report->id}"); ?>"><?php echo $report->name; ?></a></li>
<?php } ?>
          </ul>
</li>
<?php } ?>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
</div>