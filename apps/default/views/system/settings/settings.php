<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>

<?php $this->load->view('system/system_navbar'); ?>

<div class="container">
<div class="row">
  <div class="col-md-6">


    			<div class="panel panel-default">
    				<div class="panel-heading">
    					<h3 class="panel-title bold">Accounting Settings</h3>
    				</div>
    				<div class="panel-body">
    					<div class="list-group list-group-icons">
						  <a href="<?php echo site_url('accounting_settings/configuration'); ?>" class="list-group-item body_wrapper">
						    <span class="fa fa-wrench" aria-hidden="true"></span>
						    Config
						  </a>
						  <a href="<?php echo site_url('accounting_settings/class_list'); ?>" class="list-group-item body_wrapper">
						    <span class="glyphicon glyphicon-list" aria-hidden="true"></span> Class List
						  </a>
              <a href="<?php echo site_url('accounting_settings/item_list'); ?>" class="list-group-item body_wrapper">
                <span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> Item List
              </a>
              <a href="<?php echo site_url('accounting_checkbooks'); ?>" class="list-group-item body_wrapper">
                <span class="fa fa-book" aria-hidden="true"></span> Check Books
              </a>
						</div>
					</div>

    			</div>
  </div>


    <div class="col-md-6">


    			<div class="panel panel-default">
    				<div class="panel-heading">
    					<h3 class="panel-title bold">Services Settings</h3>
    				</div>
    				<div class="panel-body">
    					<div class="list-group list-group-icons">
						  <a href="<?php echo site_url('services_shares/configuration'); ?>" class="list-group-item body_wrapper">
						    <span class="glyphicon glyphicon-piggy-bank" aria-hidden="true"></span>
						    Share Capital
						  </a>

                           <a href="<?php echo site_url('services_lending/configuration'); ?>" class="list-group-item body_wrapper">
                            <span class="glyphicon glyphicon-credit-card" aria-hidden="true"></span>
                            Lending
                          </a>

						</div>
					</div>

    			</div>
  </div>

</div>
</div>

<?php $this->load->view('footer'); ?>