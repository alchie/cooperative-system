<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>

<?php if( count( $this->session->menu_module ) > 0 ) { ?>

<?php $this->load->view('welcome/welcome_navbar'); ?>

    <div class="container" id="homepage">

	<div class="row">
		<div class="col-md-8">

<div class="row">

<?php if( isset( $this->session->menu_module['membership'] ) ) { ?>
    		<div class="col-md-6">
    			<div class="panel panel-default">
    				<div class="panel-heading">
    					<h3 class="panel-title bold">Membership</h3>
    				</div>
    				<div class="panel-body">
    					<div class="list-group list-group-icons">
    					<?php if( in_array('members', $this->session->menu_module['membership'] ) ) { ?>
						  <a href="<?php echo site_url('membership_members'); ?>" class="list-group-item body_wrapper">
						    <span class="fa fa-users" aria-hidden="true"></span>
						    Members
						  </a>
						<?php } ?>
						<?php if( in_array('companies', $this->session->menu_module['membership'] ) ) { ?>
						  <a href="<?php echo site_url('membership_companies'); ?>" class="list-group-item body_wrapper">
						    <span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span> Companies
						  </a>
						<?php } ?>
						</div>
					</div>
    			</div>
    		</div>
<?php } ?>
<?php if( isset( $this->session->menu_module['services'] ) ) { ?>
    		<div class="col-md-6">
    			<div class="panel panel-default">
    				<div class="panel-heading">
    					<h3 class="panel-title bold">Services</h3>
    					</div>
    					<div class="panel-body">
		    				<div class="list-group list-group-icons">
		    				<?php if( in_array('shares', $this->session->menu_module['services'] ) ) { ?>
							  <a href="<?php echo site_url('services_shares'); ?>" class="list-group-item body_wrapper">
							    <span class="glyphicon glyphicon-piggy-bank" aria-hidden="true"></span> Share Capital
							  </a>
							<?php } ?>
							<?php if( in_array('lending', $this->session->menu_module['services'] ) ) { ?>
							  <a href="<?php echo site_url('services_lending'); ?>" class="list-group-item body_wrapper">
							    <span class="fa fa-money" aria-hidden="true"></span> Lending
							  </a>
							<?php } ?>
							</div>
						</div>
    			</div>
    		</div>
<?php } ?>
    		
</div>
    <div class="row">
<?php if( isset( $this->session->menu_module['report'] ) ) { 

if(
	( in_array('balance_sheet', $this->session->menu_module['report'] ) ) 
	|| (  in_array('income_statement', $this->session->menu_module['report'] ) ) 
) {

	?>
    		<div class="col-md-6">
    			<div class="panel panel-default">
    				<div class="panel-heading">
    					<h3 class="panel-title bold">Reports</h3>
    				</div>
    				<div class="panel-body">
	    				<div class="list-group list-group-icons">
						<?php if( in_array('balance_sheet', $this->session->menu_module['report'] ) ) { ?>
						  <a href="<?php echo site_url('report_balance_sheet'); ?>" class="list-group-item body_wrapper">
						    <span class="glyphicon glyphicon-align-justify" aria-hidden="true"></span> Balance Sheet
						  </a>
						<?php } ?>
						<?php if( in_array('income_statement', $this->session->menu_module['report'] ) ) { ?>
						  <a href="<?php echo site_url('report_income_statement'); ?>" class="list-group-item body_wrapper">
						    <span class="glyphicon glyphicon-signal" aria-hidden="true"></span> Income Statement
						  </a>
						<?php } ?>
						</div>
					</div>
    			</div>
    		</div>
<?php } ?>
<?php } ?>
<?php if( isset( $this->session->menu_module['system'] ) ) { 

if(
	( in_array('users', $this->session->menu_module['system'] ) ) 
	|| (  in_array('settings', $this->session->menu_module['system'] ) ) 
	|| (  in_array('backup', $this->session->menu_module['system'] ) ) 
) {
	?>
    		<div class="col-md-6">
    			<div class="panel panel-default">
    				<div class="panel-heading">
    					<h3 class="panel-title bold">System</h3>
    				</div>
    				<div class="panel-body">
    					<div class="list-group list-group-icons">
						<?php if( in_array('users', $this->session->menu_module['system'] ) ) { ?>
						  <a href="<?php echo site_url('system_users'); ?>" class="list-group-item body_wrapper">
						    <span class="fa fa-key" aria-hidden="true"></span> User Accounts
						  </a>
						<?php } ?>
						<?php if( in_array('settings', $this->session->menu_module['system'] ) ) { ?>
						  <a href="<?php echo site_url('system_settings'); ?>" class="list-group-item body_wrapper">
						    <span class="glyphicon glyphicon-cog" aria-hidden="true"></span> Settings
						  </a>
						<?php } ?>
						<?php if( in_array('backup', $this->session->menu_module['system'] ) ) { ?>
						  <a href="<?php echo site_url('system_backup'); ?>" class="list-group-item body_wrapper">
						    <span class="glyphicon glyphicon-hdd" aria-hidden="true"></span> Backup
						  </a>
						<?php } ?>
						</div>
					</div>
    			</div>
    		</div>
<?php } ?>
<?php } ?>
    </div>


		</div>

<?php if( isset( $this->session->menu_module['accounting'] ) ) { ?>

		<div class="col-md-4">
			
    			<div class="panel panel-default">
    				<div class="panel-heading">
    					<h3 class="panel-title bold">Accounting</h3>
    				</div>
    				<div class="panel-body">
    					<div class="list-group list-group-icons">

<?php if( in_array('chart_of_accounts', $this->session->menu_module['accounting'] ) ) { ?>

						  <a href="<?php echo site_url('accounting_charts'); ?>" class="list-group-item body_wrapper">
						    <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span> Chart of Accounts
						  </a>
<?php } ?>
<?php if( in_array('write_checks', $this->session->menu_module['accounting'] ) ) { ?>
						   <a href="<?php echo site_url('accounting_write_checks'); ?>" class="list-group-item body_wrapper">
						    <span class="fa fa-edit" aria-hidden="true"></span> Write Check
						  </a>
<?php } ?>
<?php if( in_array('general_journals', $this->session->menu_module['accounting'] ) ) { ?>
						  <a href="<?php echo site_url('accounting_general_journals'); ?>" class="list-group-item body_wrapper">
						    <span class="octicon octicon-book" aria-hidden="true"></span> General Journals
						  </a>
<?php } ?>
<?php if( in_array('sales_receipts', $this->session->menu_module['accounting'] ) ) { ?>
						  <a href="<?php echo site_url('accounting_sales_receipts'); ?>" class="list-group-item body_wrapper">
						    <span class="glyphicon glyphicon-usd" aria-hidden="true"></span> Sales Receipt
						  </a>
<?php } ?>
<?php if( in_array('deposits', $this->session->menu_module['accounting'] ) ) { ?>
						  <a href="<?php echo site_url('accounting_deposits'); ?>" class="list-group-item body_wrapper">
						    <span class="fa fa-bank" aria-hidden="true"></span> Record Deposits
						  </a>
<?php } ?>
<?php if( in_array('reconciliation', $this->session->menu_module['accounting'] ) ) { ?>
						  <a href="<?php echo site_url('accounting_reconciliation'); ?>" class="list-group-item body_wrapper">
						    <span class="fa fa-compress" aria-hidden="true"></span> Bank Recon
						  </a>
<?php } ?>
						</div>
					</div>
    			</div>

		</div>

<?php } ?>

	</div>
    	
    </div>
<?php } else { ?>

<div class="container">
<div class="row">
    		<div class="col-md-4 col-md-offset-4">
    			<div class="panel panel-danger">
    				<div class="panel-heading">
    					<h3 class="panel-title bold">Account Restricted</h3>
    				</div>
    				<div class="panel-body text-center">
    				Your account have not been granted any access to the system! <br/> Please contact system administrator!
					</div>
    			</div>
    		</div>
</div>
</div>
<?php } ?>

<?php $this->load->view('footer'); ?>