<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!-- #updateCompanyInfo Modal -->
<div class="modal fade" id="updateCompanyInfo" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalLabel">Update Company Information</h4>
      </div>
<form method="post" action="<?php echo site_url('membership_companies/edit/' . $company->id); ?>">
<input type="hidden" name="action" value="company_info">
      <div class="modal-body">

        <div class="form-group">
            <label class="control-label">Company Name</label>
            <input type="text" class="form-control" name="name" value="<?php echo $company->name; ?>" required="required">
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
</form>
    </div>
  </div>
</div>

<!-- #searchMember Modal -->
<div class="modal fade" id="searchMember" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalLabel">Search Member</h4>
      </div>
<form method="get" action="<?php echo site_url('membership_companies/add_member/' . $company->id); ?>">
      <div class="modal-body">

        <div class="form-group">
            <label class="control-label">Search Name</label>
            <input type="text" class="form-control" name="q" value="">
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
</form>
    </div>
  </div>
</div>