<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php 
$marital_statuses = array(
                'single'=>'Single/Unmarried',
                'married'=>'Married',
                'widower' => 'Widow /   Widower',
                'legally_separated' => 'Legally Separated',
                'annulled' => 'Annulled'
              );
?>
<?php if( isset($output) && ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('membership/membership_navbar'); ?>

<?php $this->load->view('membership/members/members_navbar'); ?>

<div class="container">
<div class="row">

	<div class="col-md-6 col-md-offset-3">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
	    		<h3 class="panel-title">Edit Member</h3>
	    	</div>

<form method="post">

	    	<div class="panel-body">

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

<?php endif; ?>

	    		<div class="form-group">
	    			<label>Last Name</label>
	    			<input name="lastname" type="text" class="form-control" value="<?php echo $member->lastname; ?>">
	    		</div>
	    		
	    		<div class="form-group">
	    			<label>First Name</label>
	    			<input name="firstname" type="text" class="form-control" value="<?php echo $member->firstname; ?>">
	    		</div>
	    		<div class="form-group">
	    			<label>Middle Name</label>
	    			<input name="middlename" type="text" class="form-control" value="<?php echo $member->middlename; ?>">
	    		</div>

<div class="row">
  <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">Birthday</label>
           <input type="text" class="form-control datepicker" name="birthdate" value="<?php echo date('m/d/Y', strtotime($member->birthdate)); ?>">
        </div>
  </div>
  <div class="col-md-6">
        <div class="form-group">
             <label class="control-label">Birth Place</label>
           <input type="text" class="form-control" name="birthplace" value="<?php echo $member->birthplace; ?>">
        </div>
  </div>
</div>

<div class="row">
  <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">Gender</label>
            <select class="form-control" name="gender">
              <option value="male" <?php echo ($member->gender=='male') ? "selected" : ""; ?>>Male</option>
              <option value="female" <?php echo ($member->gender=='female') ? "selected" : ""; ?>>Female</option>
            </select>
        </div>
  </div>
  <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">Marital Status</label>
            <select class="form-control" name="marital_status">
            <?php foreach($marital_statuses as $key=>$value) { ?>
              <option value="<?php echo $key; ?>" <?php echo ($member->marital_status==$key) ? "selected" : ""; ?>><?php echo $value; ?></option>
              <?php } ?>
            </select>
        </div>
  </div>

  <?php if( isset($output) && ($output!='ajax') ) : ?>

</div>

	    	</div>
	    	<div class="panel-footer">
	    		<button type="submit" class="btn btn-success">Submit</button>
	    		<a href="<?php echo site_url("membership_members"); ?>" class="btn btn-warning">Back</a>
	    	</div>


	    	</form>


	    </div>
    </div>
</div>
</div>

<?php $this->load->view('footer'); ?>

<?php endif; ?>