<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php $this->load->view('header'); ?>

<?php if( ! $inner_page ): ?>

<?php $this->load->view('membership/membership_navbar'); ?>

<div class="container">
<div class="row">
	<div class="col-md-12">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
<?php if( hasAccess('membership', 'members', 'add') ) { ?>
	    		<a data-toggle="modal" data-target="#searchName" class="btn btn-success btn-xs pull-right">Add Member</a>
<?php } ?>
	    		<h3 class="panel-title"><span class="bold allcaps">Members</span></h3>
	    	</div>
	    	<div class="panel-body" id="ajaxBodyInnerPage" data-url="<?php echo site_url(uri_string()) . (($this->input->get('q'))?"?q=".$this->input->get('q'):''); ?>">

<?php endif; // inner_page ?>

<?php if( isset($members) ) { ?>
	<?php if( $members ) { ?>
		    		<table class="table table-default hidden-xs table-hover table-condensed">
		    			<thead>
		    				<tr>
		    					<th>Lastname</th>
		    					<th>Firstname</th>
		    					<th>Middlename</th>
		    					<th>Company</th>
		    					<th width="10px">Action</th>
		    				</tr>
		    			</thead>
		    			<tbody>
		    			<?php foreach($members as $member) { ?>
		    				<tr>
		    					<td><?php echo $member->lastname; ?></td>
		    					<td><?php echo $member->firstname; ?></td>
		    					<td><?php echo $member->middlename; ?></td>
		    					<td><?php echo $member->company_name; ?></td>
		    					<td><a href="<?php echo site_url("membership_members/member_data/" . $member->id); ?>" class="btn btn-warning btn-xs body_wrapper">View Member</a></td>
		    				</tr>
		    			<?php } ?>
		    			</tbody>
		    		</table>

	<ul class="list-group visible-xs">
		<?php foreach($members as $member) { ?>
	  		<a href="<?php echo site_url("membership_members/member_data/" . $member->id); ?>" class="list-group-item">
	  			<h4 class="list-group-item-heading"><?php echo $member->lastname; ?>, <?php echo $member->firstname; ?> <?php echo $member->middlename; ?></h4>
	  			<p class="list-group-item-text"><?php echo $member->company_name; ?></p>
	  		</a>
	  	<?php } ?>
	</ul>

	<?php if(isset($pagination)) { ?><div class="text-center"><?php echo $pagination; ?></div><?php } ?>

	<?php } else { ?>
		<div class="text-center">No Member Found!</div>
	<?php } ?>
<?php } else { ?>

	Please Wait...

<?php } ?>

<?php if( ! $inner_page ): ?>
	    	</div>
	    </div>
    </div>
    </div>
</div>

<!-- #searchMember Modal -->
<div class="modal fade" id="searchName" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalLabel">Add Member</h4>
      </div>
<form method="get" action="<?php echo site_url('membership_members/search_name'); ?>">
      <div class="modal-body">

        <div class="form-group">
            <label class="control-label">Search Name</label>
            <input type="text" class="form-control autocomplete-member_change" data-source="<?php echo site_url("membership_members/ajax/add_member"); ?>" data-current_sub_uri="<?php echo $current_sub_uri; ?>" name="q" value="">
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Search</button>
      </div>
</form>
    </div>
  </div>
</div>

<?php endif; // inner_page ?>
<?php $this->load->view('footer'); ?>
