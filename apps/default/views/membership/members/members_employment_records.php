<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>

<?php $this->load->view('membership/members/members_navbar'); ?>

<?php $this->load->view('membership/members/members_navbar2'); ?>

<div class="container">
<div class="row">
	<div class="col-md-12">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
<?php if( hasAccess('membership', 'members', 'edit') ) { ?>
	    		<a class="btn btn-success btn-xs pull-right ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Add Employment Record" data-url="<?php echo site_url("membership_members/add_employment/{$member->id}/ajax"); ?>">Add Record</a>
<?php } ?>
	    		<h3 class="panel-title">Employment Records</h3>
	    	</div>
	    	<div class="panel-body">

	    	</div>
	    </div>
    </div>
    </div>
</div>

<?php $this->load->view('footer'); ?>