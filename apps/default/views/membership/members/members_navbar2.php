<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container">
	<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#services-navbar-collapse" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <div class="navbar-brand"><?php echo $current_page; ?></div>
    </div>

    <div class="collapse navbar-collapse" id="services-navbar-collapse">

      <ul class="nav navbar-nav navbar-right">
<?php 
$url['membership_members_personal_info'] = array('uri' => 'membership_members/member_data/' . $member->id, 'title'=>'Personal Information');
$url['membership_members_employment'] = array('uri' => 'membership_members/employment/' . $member->id, 'title'=>'Employment Records');
foreach($url as $k=>$v) {
?>
  <li class="<?php echo ($k==$current_sub_uri) ? 'active' : ''; ?>"><a class="body_wrapper" href="<?php echo site_url($v['uri']); ?>"><?php echo $v['title']; ?></a></li>
<?php } ?> 
         
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
</div>