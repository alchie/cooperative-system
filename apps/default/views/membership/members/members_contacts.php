<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php 
$marital_statuses = array(
                'single'=>'Single/Unmarried',
                'married'=>'Married',
                'widower' => 'Widow /   Widower',
                'legally_separated' => 'Legally Separated',
                'annulled' => 'Annulled'
              );
?>
<?php if( isset($output) && ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('membership/membership_navbar'); ?>

<?php $this->load->view('membership/members/members_navbar'); ?>

<div class="container">
<div class="row">

    <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Edit Member</h3>
            </div>

<form method="post">

            <div class="panel-body">


<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

<?php endif; ?>

<div class="form-group">
    <label class="control-label">Mobile Phone Number</label>
    <input type="text" class="form-control" name="phone_mobile" value="<?php echo $member->phone_mobile; ?>">
</div>
<div class="form-group">
    <label class="control-label">Home Phone Number</label>
    <input type="text" class="form-control" name="phone_home" value="<?php echo $member->phone_home; ?>">
</div>
<div class="form-group">
    <label class="control-label">Email Address</label>
    <input type="text" class="form-control" name="email" value="<?php echo $member->email; ?>">
</div>

<?php if( isset($output) && ($output!='ajax') ) : ?>

</div>

            </div> 
            <div class="panel-footer">
                <button type="submit" class="btn btn-success">Submit</button>
                <a href="<?php echo site_url("membership_members"); ?>" class="btn btn-warning">Back</a>
            </div>


        </form>


        </div>
    </div>
</div>
</div>

<?php $this->load->view('footer'); ?>

<?php endif; ?>