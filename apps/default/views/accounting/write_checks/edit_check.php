<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( !isset($output) || ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('accounting/accounting_navbar'); ?>

<div class="container">
<div class="row">

<form method="post" action="">

  <div class="col-md-6 col-md-offset-3">


<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

        <div class="panel panel-default">
            <div class="panel-heading">
<?php if($check_data->loan_exists) { ?>
  <a href="<?php echo site_url("services_lending/schedule/{$check_data->loan_exists}"); ?>" class="btn btn-success btn-xs pull-right">View Loan</a>
<?php } elseif($check_data->withdrawal_exists) { ?>
  <a href="<?php echo site_url("services_shares/withdrawals/{$check_data->withdrawal_exists}"); ?>" class="btn btn-success btn-xs pull-right">View Withdrawal</a>
<?php } else { ?>
  <a href="<?php echo site_url("accounting_write_checks/delete_check/{$check_data->id}"); ?>" class="btn btn-danger btn-xs pull-right confirm">Delete</a>
<?php } ?>

<?php if( ($check_data->entries_cleared <> 0) || ($check_data->debit_cleared <> 0) || ($check_data->credit_cleared <> 0) ) { ?>
            <a style="margin-right:5px" href="<?php echo site_url("accounting_write_checks/delete_entries/{$check_data->id}"); ?>" class="btn btn-warning btn-xs pull-right confirm">Delete Entries</a>
<?php } ?>
                <h3 class="panel-title">Edit Check : <strong><?php echo $current_bank->title; ?></strong></h3>
            </div>

            <div class="panel-body">

<?php endif; ?>
        
        <div class="form-group ">
            <label class="control-label">Pay to the order of</label>
            <input id="write_check_name_id" name="name_id" type="hidden" value="<?php echo $check_data->name_id; ?>">
            <input name="" class="form-control autocomplete-name_select autocomplete-name_select-name-input-<?php echo time(); ?>" data-source="<?php echo site_url("accounting_write_checks/ajax/name"); ?>" data-name_id="write_check_name_id" type="text" style="display: none;">
            <div class="form-control autocomplete-name_select-name-display-<?php echo time(); ?>"><a class="badge" id="changeName" href="#changeName" data-id="<?php echo $check_data->name_id; ?>" data-name_id="write_check_name_id" data-timestamp="<?php echo time(); ?>"><?php echo $check_data->full_name; ?></a></div>
        </div>
<div class="row">
  <div class="col-md-4">
        <div class="form-group ">
            <label class="control-label">Check Number</label>
            <input class="form-control text-center" type="text" name="check_number" value="<?php echo $check_data->check_number; ?>" required>
        </div>
  </div>
  <div class="col-md-4">
        <div class="form-group ">
            <label class="control-label">Check Date</label>
            <input class="form-control datepicker text-center" type="text" name="check_date" value="<?php echo date("m/d/Y", strtotime($check_data->check_date)); ?>" required>
        </div>
  </div>
  <div class="col-md-4">
        <div class="form-group ">
            <label class="control-label">Amount</label>
            <input class="form-control text-right" type="text" name="amount" value="<?php echo number_format($check_data->amount,2,".",''); ?>" required>
        </div>
  </div>
</div>
         <div class="form-group">
            <label class="control-label">Memo</label>
            <textarea class="form-control" name="memo"><?php echo $check_data->memo; ?></textarea>
        </div>

         <div class="form-group ">
            <label class="control-label"><input type="checkbox" name="cancelled" value="1" <?php echo ($check_data->cancelled) ? 'checked' : ''; ?>> Cancelled</label>
           
        </div>

<?php if( isset($output) && ($output=='ajax') ) : ?>


<?php if($check_data->loan_exists) { ?>
  <a href="<?php echo site_url("services_lending/schedule/{$check_data->loan_exists}"); ?>" class="btn btn-success btn-xs">View Loan</a>
<?php } elseif($check_data->withdrawal_exists) { ?>
  <a href="<?php echo site_url("services_shares/withdrawals/{$check_data->withdrawal_exists}"); ?>" class="btn btn-success btn-xs">View Withdrawal</a>
<?php } else { ?>
  <a href="<?php echo site_url("accounting_write_checks/delete_check/{$check_data->id}"); ?>" class="btn btn-danger btn-xs confirm">Delete this check</a>
<?php } ?>

<?php if( ($check_data->entries_cleared <> 0) || ($check_data->debit_cleared <> 0) || ($check_data->credit_cleared <> 0) ) { ?>
            <a style="margin-right:5px" href="<?php echo site_url("accounting_write_checks/delete_entries/{$check_data->id}"); ?>" class="btn btn-warning btn-xs pull-right confirm">Delete Entries</a>
  <?php } ?>

<?php endif; ?>

<?php if( !isset($output) || ($output!='ajax') ) : ?>

            </div>

         <div class="panel-footer">
            <button type="submit" class="btn btn-success">Submit</button>
            <a href="<?php echo site_url("accounting_write_checks"); ?>" class="btn btn-warning">Back</a>

            <a href="<?php echo site_url("accounting_write_checks/check_items/{$check_data->id}"); ?>" class="btn btn-info pull-right">Items</a>
        </div>

        </div>


    </div>

    </form>

</div>
</div>

<?php $this->load->view('footer'); ?>

<?php endif; ?>