<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( !isset($output) || ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('accounting/accounting_navbar'); ?>

<div class="container">
<div class="row">

<form method="post">

  <div class="col-md-6 col-md-offset-3">


<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>



        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Write Check : <strong><?php echo $current_bank->title; ?></strong></h3>
            </div>

            <div class="panel-body">

<?php endif; ?>

        <div class="form-group ">
            <label class="control-label">Pay to the order of</label>
            <input id="write_check_name_id" name="name_id" type="hidden">
            <input class="form-control autocomplete-name_select" data-source="<?php echo site_url("accounting_write_checks/ajax/name"); ?>" data-name_id="write_check_name_id" type="text" required>
        </div>
<div class="row">
  <div class="col-md-4">
        <div class="form-group ">
            <label class="control-label">Check Number</label>
            <!--<input class="form-control text-center" type="text" name="check_number" value="<?php echo ($this->input->post('check_number')) ? $this->input->post('check_number') : ( ($lastcheck) ? (intval($lastcheck->check_number) + 1) : '1'); ?>" required>-->
            <select class="form-control" name="check_number">
            <?php foreach($checks as $check) { ?>
                <option value="<?php echo $check->check_number; ?>"><?php echo $check->check_number; ?></option>
            <?php } ?>
            </select>
        </div>
  </div>
  <div class="col-md-4">
        <div class="form-group ">
            <label class="control-label">Check Date</label>
            <input class="form-control datepicker text-center" type="text" name="check_date" value="<?php echo ($this->input->post('check_date')) ? $this->input->post('check_date') : date('m/d/Y'); ?>" required>
        </div>
  </div>
  <div class="col-md-4">
        <div class="form-group ">
            <label class="control-label">Amount</label>
            <input class="form-control text-right" type="text" name="amount" value="<?php echo $this->input->post('amount'); ?>" required>
        </div>
  </div>
</div>
         <div class="form-group ">
            <label class="control-label">Memo</label>
            <textarea class="form-control" name="memo"><?php echo $this->input->post('memo'); ?></textarea>
        </div>


<?php if( !isset($output) || ($output!='ajax') ) : ?>

            </div>

                    <div class="panel-footer">
          <button type="submit" class="btn btn-success">Submit</button>
          <a href="<?php echo site_url("accounting_write_checks"); ?>" class="btn btn-warning">Back</a>
        </div>

        </div>


    </div>

    </form>

</div>
</div>

<?php $this->load->view('footer'); ?>

<?php endif; ?>