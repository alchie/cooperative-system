<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header');  ?>
<?php $this->load->view('accounting/accounting_navbar'); ?>

<div class="container">
<div class="row">
	<div class="col-md-12">
  <form method="post">
	    <div class="panel panel-<?php echo (($check_data->trn_id) && (round($check_data->balance) == 0 ) && (!is_null($check_data->entries_cleared)) && ( round($check_data->entries_cleared) == 0 ) ) ? 'default' : 'danger'; ?>">
	    	<div class="panel-heading">

<a class="btn btn-xs btn-warning pull-right body_wrapper" href="<?php echo site_url("accounting_write_checks"); ?>" style="margin-left: 10px">Back</a>
<?php if( !$check_data->cancelled ) { ?>
<?php if(round($check_data->balance) == 0 ) { ?>
    <?php if((!$check_data->trn_id) || (!$check_data->entries_cleared)) { ?>
    	<a class="btn btn-xs btn-success pull-right" href="<?php echo site_url("accounting_write_checks/publish_check/{$check_data->id}"); ?>">Publish Check</a>
    <?php } ?>
<?php } else { ?>
          <button type="button" class="btn btn-success btn-xs pull-right ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Add Item" data-url="<?php echo site_url("accounting_write_checks/add_check_item/{$check_data->id}/ajax") . "?next=" . uri_string(); ?>">Add Item</button>
<?php } ?>
<?php } ?>



	    		<h3 class="panel-title">Check Items - <strong><?php echo $check_data->chart_title; ?> # <?php echo $check_data->check_number; ?> <small><?php echo date( 'm/d/Y', strtotime($check_data->check_date) ); ?> - <?php echo $check_data->full_name; ?></small> </strong>
<a href="#edit-receipt" class="ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Edit Check" data-url="<?php echo site_url("accounting_write_checks/edit_check/{$check_data->id}/ajax") . "?next=" . uri_string(); ?>"><span class="glyphicon glyphicon-pencil"></span></a>
	    		</h3> 
	    	</div>
	    	<div class="panel-body">

<?php if( $check_items ) { ?>

	    	<table class="table table-default table-hover">
	    		<thead>
	    			<tr>
	    				<th>Type</th>
	    				<th>Account Title</th>
			            <th width="20%">Name</th>
	    				<th width="15%">Class</th>
	    				<th width="30%">Memo</th>
	    				<th width="10%" class="text-right">Amount</th>
	    				<th width="10px" class="text-right">Action</th>
	    			</tr>
	    		</thead>
          <tbody>
<?php 
$n=0;
$total=0;
foreach($check_items as $item) { ?>
	<tr>
		<td><?php echo $item->item_type; ?></td>
		<td><?php echo $item->chart_title; ?></td>
		<td><?php echo $item->full_name; ?></td>
		<td><?php echo $item->class_name; ?></td>
		<td><?php echo $item->memo; ?></td>
		<td class="text-right"><?php echo number_format($item->amount,2); $total+=$item->amount;?></td>
		<td>
<button type="button" class="btn btn-warning btn-xs pull-right ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Update Item" data-url="<?php echo site_url("accounting_write_checks/edit_check_item/{$item->id}/ajax") . "?next=" . uri_string(); ?>">Update</button>
		</td>
	</tr>
<?php 
$n++;
} ?>
<?php if($n>1) { ?>
	<tr class="warning">
		<td class="bold">TOTAL</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td class="text-right bold"><?php echo number_format($total,2); ?></td>
		<td></td>
	</tr>
<?php } ?>
          </tbody>
	    	</table>

<?php } else { ?>
<?php if( $check_data->cancelled ) { ?>
  	<p class="text-center">Check is <strong>CANCELLED</strong>!</p>
<?php } else { ?>
	<p class="text-center">No Item Found!</p>
<?php } ?>
<?php } ?>

	    	</div>
	    </div>
</form>
    </div>
</div>
</div>
<?php $this->load->view('footer'); ?>