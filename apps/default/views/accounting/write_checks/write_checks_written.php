<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php $this->load->view('header'); ?>

<?php if( ! $inner_page ): ?>

<?php $this->load->view('accounting/accounting_navbar'); ?>

<div class="container">
<div class="row">
	<div class="col-md-12">
	    <div class="panel panel-default">
	    	<div class="panel-heading">

<?php if( hasAccess('accounting', 'write_checks', 'add') ) { ?>
 <?php if( $checkbooks ) {  ?>
<div class="btn-group pull-right">
  <button type="button" class="btn btn-success btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Write Check <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">

    <?php foreach($checkbooks as $checkbook) { ?>
      <li><a href="javascript:void(0);" class="ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Write Check: <?php echo $checkbook->title; ?>" data-url="<?php echo site_url("accounting_write_checks/write_check/{$checkbook->id}/ajax") . "?next=" . uri_string(); ?>"><?php echo $checkbook->title; ?></a></li>
    <?php } ?>
  </ul>
</div>
  <?php } ?>
<?php } ?>

	    		<h3 class="panel-title">Checks Written

<div class="btn-group">
  <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <?php echo (isset($current_bank)) ? $current_bank->title : "All Banks"; ?> <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
    <li><a class="ajaxPage" href="<?php echo site_url("accounting_write_checks"); ?>">All Banks</a></li>
  <?php if( $bank_accounts ) { ?>
    <?php foreach($bank_accounts as $acct) { ?>
      <li <?php echo (isset($current_bank) && ($current_bank->id==$acct->id)) ? "class='active'" : ""; ?>><a class="ajaxPage" href="<?php echo site_url("accounting_write_checks/index/{$acct->id}"); ?>"><?php echo $acct->title; ?></a></li>
    <?php } ?>
  <?php } ?>
  </ul>
</div>

          </h3>
	    	</div>
	    	<div class="panel-body" id="ajaxBodyInnerPage" data-url="<?php echo site_url(uri_string()); ?>">

<?php endif; // inner_page ?>

<?php if( isset($checks) ) { ?>
<?php if( $checks ) { ?>
          <table class="table table-default table-hover table-condensed">
            <thead>
              <tr>
                <th>Date</th>
                <th>Check Number</th>
                <th>Name</th>
                <th>Memo</th>
                <th class="text-right">Amount</th>
                <?php if( hasAccess('accounting', 'write_checks', 'edit') ) { ?>
                <th class="text-right" width="100px">Actions</th>
                <?php } ?>
              </tr>
            </thead>
            <tbody>
<?php foreach($checks as $check) { ?>
<tr class="<?php echo (($check->trn_id) && (round($check->balance) == 0 ) && (!is_null($check->entries_cleared)) && (round($check->entries_cleared) == 0 )) ? '' : (($check->cancelled)?'danger':'warning'); ?>">

    <td><?php echo date('F d, Y', strtotime($check->check_date)); ?></td>
    <td><?php echo $check->check_number; ?></td>
    <td><?php echo $check->full_name; ?></td>
    <td><?php echo $check->memo; ?></td>
    <td class="text-right"><?php echo number_format($check->amount,2); ?></td>
    <?php if( hasAccess('accounting', 'write_checks', 'edit') ) { ?>
    <td class="text-left">
    <button type="button" class="btn btn-warning btn-xs ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Edit Check" data-url="<?php echo site_url("accounting_write_checks/edit_check/{$check->id}/ajax") . "?next=" . uri_string(); ?>">Edit</button>
    <?php if( !$check->cancelled ) { ?>
    <a href="<?php echo site_url("accounting_write_checks/check_items/" . $check->id); ?>" class="btn btn-primary btn-xs body_wrapper">Items</a>
    <?php } ?>
    </td>
    <?php } ?>
 </tr>
<?php } ?>
            </tbody>
          </table>

<?php echo ($pagination!='') ? '<center>' . $pagination . '</center>' : ''; ?>


<?php } else { ?>
  <p class="text-center">No Checks Found!</p>
<?php } ?>
<?php } else { ?>
  Please wait...
<?php } ?>

<?php if( ! $inner_page ): ?>

	    	</div>
	    </div>
    </div>
</div>
</div>
<?php endif; ?>
<?php $this->load->view('footer'); ?>