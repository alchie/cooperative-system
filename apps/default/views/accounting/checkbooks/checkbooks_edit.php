<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( !isset($output) || ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('accounting/accounting_navbar'); ?>

<div class="container">
<div class="row">

  <div class="col-md-6 col-md-offset-3">
      <div class="panel panel-default">
        <div class="panel-heading">
        <a href="<?php echo site_url("accounting_checkbooks/delete/{$book->id}"); ?>" class="btn btn-danger btn-xs confirm pull-right">Delete</a>
          <h3 class="panel-title">Edit Journal</h3>
        </div>
        <form method="post">
        <div class="panel-body">

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

<?php endif; ?>

<div class="row">
  <div class="col-md-6">
          <div class="form-group">
            <label>Starting Number</label>
            <input name="num_start" type="text" class="text-center form-control" value="<?php echo $book->num_start; ?>">
          </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
            <label>Ending Number</label>
            <input name="num_end" type="text" class="form-control text-center" value="<?php echo $book->num_end; ?>">
          </div>
  </div>
</div>

<?php if( isset($output) && ($output=='ajax') ) : ?>
  <a href="<?php echo site_url("accounting_checkbooks/delete/{$book->id}"); ?>" class="btn btn-danger btn-xs confirm">Delete this checkbook</a>
<?php endif; ?>

<?php if( !isset($output) || ($output!='ajax') ) : ?>

        </div>
        <div class="panel-footer">
          <a href="<?php echo site_url("accounting_checkbooks/checks/{$book->id}"); ?>" class="btn btn-info pull-right">Checks</a>
          <button type="submit" class="btn btn-success">Submit</button>
          <a href="<?php echo site_url("accounting_checkbooks"); ?>" class="btn btn-warning">Back</a>
        </div>


        </form>
      </div>
    </div>
</div>
</div>

<?php $this->load->view('footer'); ?>

<?php endif; ?>