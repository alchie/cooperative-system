<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
<?php if( ! $inner_page ): ?>
<?php $this->load->view('accounting/settings/settings_navbar'); ?>

<div class="container">
<div class="row">
	<div class="col-md-12">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
<?php if( hasAccess('accounting', 'checkbooks', 'add') ) { ?>
<div class="btn-group pull-right">
  <button type="button" class="btn btn-success btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Add Checkbook <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
  <?php if( $bank_accounts ) { ?>
    <?php foreach($bank_accounts as $acct) { ?>
      <li><a href="javascript:void(0);" class="ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Add Checkbook: <?php echo $acct->title; ?>" data-url="<?php echo site_url("accounting_checkbooks/add/{$acct->id}/ajax") . "?next=" . uri_string(); ?>"><?php echo $acct->title; ?></a></li>
    <?php } ?>
  <?php } ?>
  </ul>
</div>
<?php } ?>
	    		<h3 class="panel-title">Checkbooks</h3>
	    	</div>
	    	<div class="panel-body" id="ajaxBodyInnerPage" data-url="<?php echo site_url(uri_string()); ?>">
<?php endif; ?>

<?php if( isset($books) ) { ?>
<?php if( $books ) { ?>
	    		<table class="table table-default table-hover table-condensed">
	    			<thead>
	    				<tr>
	    					<th>Bank Account</th>
	    					<th>Starting Number</th>
	    					<th>Ending Number</th>
	    					<?php if( hasAccess('accounting', 'checkbooks', 'edit') ) { ?>
	    						<th width="110px">Action</th>
	    					<?php } ?>
	    				</tr>
	    			</thead>
	    			<tbody>
	    			<?php foreach($books as $book) { ?>
	    				<tr>
	    					<td><?php echo $book->account_title; ?></td>
	    					<td><?php echo $book->num_start; ?></td>
	    					<td><?php echo $book->num_end; ?></td>
	    					<?php if( hasAccess('accounting', 'checkbooks', 'edit') ) { ?>
		    					<td>
		    					<button type="button" class="btn btn-warning btn-xs ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Edit Checkbook" data-url="<?php echo site_url("accounting_checkbooks/edit/{$book->id}/ajax") . "?next=" . uri_string(); ?>">Edit</button>
		    					<a href="<?php echo site_url("accounting_checkbooks/checks/" . $book->id); ?>" class="btn btn-primary btn-xs body_wrapper">Checks</a>
		    					</td>
	    					<?php } ?>
	    				</tr>
	    			<?php } ?>
	    			</tbody>
	    		</table>

	    		<?php echo ($pagination!='') ? '<center>' . $pagination . '</center>' : ''; ?>
	    		
<?php } else { ?>
	<div class="text-center">No Checkbook Found!</div>
<?php } ?>
<?php } else { ?>
	Please wait...
<?php } ?>

<?php if( ! $inner_page ): ?>
	    	</div>
	    </div>
    </div>
</div>
</div>
<?php endif; ?>
<?php $this->load->view('footer'); ?>