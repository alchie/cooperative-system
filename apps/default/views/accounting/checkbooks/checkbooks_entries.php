<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php 
$je = array();
$i_count = 0;
if( $journal_entries ) {
  foreach( $journal_entries as $j) {
    $je[$j->item_number] = $j;
  }
  $i_count = ($journal_entries[(count($je)-1)]->item_number + 1); 
}


function display_list($je, $main_accounts , $pre="", $i=0, $posts=array()) {
  $selected = ((isset($je[$i])&&($je[$i]->chart_id))) ? $je[$i]->chart_id : ((isset($posts[$i])) ? $posts[$i] : '');

          $coa_types = unserialize(CHART_ACCOUNT_TYPES);
          $charts = array_merge($coa_types['Balance Sheet'], $coa_types['Income Statement']);

          foreach( $main_accounts as $main_account) { 
                  ?>
                  <option data-subtext="<?php echo $charts[$main_account->type]; ?>" value="<?php echo $main_account->id; ?>" <?php echo ($selected==$main_account->id) ? 'selected' : ''; ?> title="<?php echo $main_account->title; ?>"><?php echo $pre; ?><?php echo $main_account->title; ?></option>
          <?php 
            if( $main_account->children ) {
              display_list( $je, $main_account->children, $pre . "- - - - ", $i, $posts );
            }
          }  
  } 

function display_class($je, $class_list , $pre="", $i=0, $posts=array()) {
  
  $selected = (isset($je[$i])&&($je[$i]->class)) ? $je[$i]->class : ((isset($posts[$i])) ? $posts[$i] : '');
                 foreach( $class_list as $class) { 
                  ?>
                  <option value="<?php echo $class->id; ?>" <?php echo ($selected==$class->id) ? 'selected' : ''; ?> title="<?php echo $class->value; ?>"><?php echo $pre; ?><?php echo $class->value; ?></option>
          <?php 
          if( $class->children ) {
            display_class( $je, $class->children, $pre . "- - - - ", $i, $posts);
          }
     } 
  } 

function display_item_type($je, $type_list, $i=0, $posts=array()) {
  
  $selected = (isset($je[$i])&&($je[$i]->item_type)) ? $je[$i]->item_type : ((isset($posts[$i])) ? $posts[$i] : '');
                 foreach( $type_list as $key=>$value) { 
                  ?>
                  <option value="<?php echo $key; ?>" <?php echo ($selected==$key) ? 'selected' : ''; ?> title="<?php echo $value ?>"><?php echo $value; ?></option>
          <?php 
     } 
  } 

?>
<?php $this->load->view('header');  ?>
<?php $this->load->view('accounting/accounting_navbar'); ?>

<div class="container">
<div class="row">
	<div class="col-md-12">
  <form method="post" action="<?php echo site_url("accounting_general_journals/save_entries/{$journal->id}"); ?>">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
          <button type="submit" class="btn btn-success btn-xs pull-right" id="saveEntriesButton">Save Entries</button>
	    		<h3 class="panel-title">
          <a href="#edit-journal" class="ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Record Receipt" data-url="<?php echo site_url("accounting_general_journals/edit/{$journal->id}/ajax") . "?next=" . uri_string(); ?>"><i class="glyphicon glyphicon-pencil"></i></a> 
          <?php echo date('m/d/Y', strtotime($journal->date)); ?> - <?php echo $journal->memo; ?> </h3> 
	    	</div>
	    	<div class="panel-body">
	    	<table class="table table-default">
	    		<thead>
	    			<tr>
              <th width="1%"></th>
	    				<th>Type</th>
	    				<th>Account Title</th>
	    				<th width="10%">Debit</th>
              <th width="10%">Credit</th>
	    				<th width="15%">Class</th>
              <th width="20%">Name</th>
	    				<th width="15%">Memo</th>
              <th width="1%"></th>
	    			</tr>
	    		</thead>
          <tbody>
<?php 
$limit = (count($i_count) > 5) ? $i_count : 5;
$limit = (($journal->max_items + 1) > $limit) ? ($journal->max_items + 1) : $limit;

for($i=0;$i<$limit;$i++) {  
$locked = (isset($je[$i]) && ($je[$i]->loan_id OR $je[$i]->withdrawal_id OR $je[$i]->payments OR $je[$i]->capitals)) ? true : false;

  ?>
            <tr>
            <td>
              <a href="javascript:void(0);" class="insertJournalLine">
                <span class="glyphicon glyphicon-plus"></span>
              </a>
            </td>
            <td>
            	
            <select name="item_type[]" class="form-control je_input-<?php echo $i; ?>" title="Select a Type..." <?php echo ($locked) ? 'disabled' : ''; ?>>
            <optgroup label="Check Items">
              <?php display_item_type( $je, $check_item_types, $i, $this->input->post('item_type') ); ?>
            </optgroup>
            <optgroup label="Receipt Items">
              <?php display_item_type( $je, $receipt_item_types, $i, $this->input->post('item_type') ); ?>
            </optgroup>
            </select>

            </td>
              <td><select type="text" class="form-control select_account_titles je_input-<?php echo $i; ?>" name="account_title[]" title="Select an Account Title..." <?php echo ($locked) ? 'disabled' : ''; ?>>
<?php 
display_list( $je, $account_titles, '', $i, $this->input->post('account_title') );
?>        
              </select></td>

              <td><input type="text" class="form-control text-right debit_amount je_input-<?php echo $i; ?>" name="debit[]" value="<?php echo (isset($je[$i]) && (number_format($je[$i]->debit,2,".","") > 0)) ? number_format($je[$i]->debit,2,".","") :  ((isset($this->input->post('debit')[$i])) ? $this->input->post('debit')[$i] : ''); ?>" <?php echo ($locked) ? 'disabled' : ''; ?>></td>

              <td><input type="text" class="form-control text-right credit_amount je_input-<?php echo $i; ?>" name="credit[]" value="<?php echo (isset($je[$i]) && (number_format($je[$i]->credit,2,".","") > 0)) ? number_format($je[$i]->credit,2,".","") :  ((isset($this->input->post('credit')[$i])) ? $this->input->post('credit')[$i] : ''); ?>" <?php echo ($locked) ? 'disabled' : ''; ?>></td>

              <td><select type="text" class="form-control je_input-<?php echo $i; ?>" name="class_name[]" title="Select a Class..." <?php echo ($locked) ? 'disabled' : ''; ?>>
<?php display_class( $je, $class_list, "", $i, $this->input->post('class_name') ); ?>
              </select></td>
              <td>
<?php if( isset( $je[$i] ) && ($je[$i]->name_id) && ($je[$i]->full_name) ) { ?>
            <input id="journal_name_id_<?php echo $i; ?>" name="name_id[]" type="hidden" value="<?php echo $je[$i]->name_id; ?>" <?php echo ($locked) ? 'disabled' : ''; ?> class="je_input-<?php echo $i; ?>">
            <input name="" class="form-control autocomplete-name_select je_input-<?php echo $i; ?> autocomplete-name_select-name-input-<?php echo $i; ?>-<?php echo time(); ?>" data-source="<?php echo site_url("accounting_general_journals/ajax/name"); ?>" data-name_id="journal_name_id_<?php echo $i; ?>" type="text" style="display: none;">
            <div class="form-control danger autocomplete-name_select autocomplete-name_select-name-display-<?php echo $i; ?>-<?php echo time(); ?>"><a class="badge changeName<?php echo ($locked) ? 'disabled' : ''; ?>" href="#changeName" data-id="<?php echo $je[$i]->name_id; ?>" data-name_id="journal_name_id_<?php echo $i; ?>" data-timestamp="<?php echo $i; ?>-<?php echo time(); ?>"><?php echo $je[$i]->full_name; ?></a>
<?php } else { ?>

              <input id="journal_name_id_<?php echo $i; ?>" name="name_id[]" type="hidden" class="je_input-<?php echo $i; ?>">
            <input class="form-control autocomplete-name_select je_input-<?php echo $i; ?>" data-source="<?php echo site_url("accounting_general_journals/ajax/name"); ?>" data-name_id="journal_name_id_<?php echo $i; ?>" type="text" <?php echo ($locked) ? 'disabled' : ''; ?>> 

<?php } ?>
            </td>

              <td><input type="text" class="form-control je_input-<?php echo $i; ?>" name="memo[]" value="<?php echo (isset($je[$i])) ? $je[$i]->memo : ((isset($this->input->post('memo')[$i])) ? $this->input->post('memo')[$i] : ''); ?>"></td>

              <td>
<?php if($locked) { ?>
              <a href="javascript:void(0);" class="unlockJournalLine" data-line_number="<?php echo $i; ?>">
                <span class="glyphicon glyphicon-lock"></span>
              </a>
<?php } else { ?>
            <a href="javascript:void(0);" class="removeJournalLine">
                <span class="glyphicon glyphicon-remove"></span>
              </a>
<?php } ?>
            </td>

            </tr>
<?php } ?>
<tr>
  <td></td>
  <td></td>
  <td></td>
  <td id="debit_total" class="text-center bold"></td>
  <td id="credit_total" class="text-center bold"></td>
  <td></td>
  <td></td>
  <td></td>
  <td></td>
</tr>
          </tbody>
	    	</table>
	    	</div>
	    </div>
</form>
    </div>
</div>
</div>
<?php $this->load->view('footer'); ?>