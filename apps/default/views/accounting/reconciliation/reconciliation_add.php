<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( !isset($output) || ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('accounting/accounting_navbar'); ?>

<div class="container">
<div class="row">
	<div class="col-md-6 col-md-offset-3">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
	    		<h3 class="panel-title">Add Reconciliation</h3>
	    	</div>
	    	        <form method="post">
        <div class="panel-body">

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>
  
<?php endif; ?>

<div class="row">
  <div class="col-md-6">
          <div class="form-group">
            <label>Last Bank Recon</label>
            <div class="form-control text-center" value=""><?php echo ($last_recon) ? date('F d, Y', strtotime($last_recon->date_end)) : 'None'; ?></div>
          </div>
   </div>
    <div class="col-md-6">
          <div class="form-group">
            <label>Beginning Balance</label>
            <div class="form-control text-right" value=""><?php echo ($last_recon) ? number_format($last_recon->balance_end, 2) : '0.00'; ?></div>
          </div>
    </div>
</div>

<div class="row">
 	<div class="col-md-6">
          <div class="form-group">
            <label>Statement Date End</label>
            <input name="date_end" type="text" class="form-control text-center datepicker" value="<?php echo ($this->input->post('date_end')) ? $this->input->post('date_end') : date("m/d/Y"); ?>">
          </div>
 	 </div>
    <div class="col-md-6">
          <div class="form-group">
            <label>Ending Balance</label>
            <input name="balance_end" type="text" class="form-control text-right" value="<?php echo $this->input->post('balance_end'); ?>">
          </div>
    </div>
</div>

          <div class="form-group">
            <label>Memo</label>
            <textarea name="memo" type="text" class="form-control"><?php echo $this->input->post('memo'); ?></textarea>
          </div>

<?php if( !isset($output) || ($output!='ajax') ) : ?>

        </div>
        <div class="panel-footer">
          <button type="submit" class="btn btn-success">Submit</button>
          <a href="<?php echo site_url("accounting_reconciliation"); ?>" class="btn btn-warning">Back</a>
        </div>


        </form>

	    </div>
    </div>
</div>
</div>

<?php $this->load->view('footer'); ?>

<?php endif; ?>