<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>

<?php $this->load->view('accounting/accounting_navbar'); ?>

<form method="post">
<div id="recon" class="container">
<div class="row">
	<div class="col-md-6">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
	    		<h3 class="panel-title">Disbursements</h3>
	    	</div>
	    	<div class="panel-body scrollable-box">

<?php if( $disbursements ) { ?>

<div class="list-group">
<?php foreach($disbursements as $disburse) { ?>
  	<label class="list-group-item text-center" id="list-group-item-<?php echo $disburse->trn_id; ?>">
  	
  	<span class="pull-left">
  	<input <?php echo ($disburse->recon_id) ? 'checked' : ''; ?> type="checkbox" data-amount="<?php echo number_format($disburse->total_amount,2,".",""); ?>" class="recon_item" data-type="disburse" name="item[]" value="<?php echo $disburse->trn_id; ?>">
  	<?php echo date('m/d/Y', strtotime($disburse->date)); ?>
  	<?php echo $disburse->trn_type; ?> <?php echo $disburse->number; ?>
  	<?php echo (trim($disburse->memo)!='') ? " - " . $disburse->memo : ''; ?>
  	</span>

  	<span class="pull-right balance_end"><?php echo number_format($disburse->total_amount,2); ?></span>
  	<a target="_transaction" href="<?php echo site_url("report_transactions/redirect/{$disburse->trn_id}"); ?>"><span class="glyphicon glyphicon-eye-open"></span></a>
  	<span class="clearfix"></span>
  	</label>
<?php } ?>
</div>

<?php } else { ?>

	<p class="text-center">No Disbursements Found!</p>

<?php } ?>

	    	</div>
	    </div>
    </div>
	<div class="col-md-6">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
	    		<h3 class="panel-title">Deposits</h3>
	    	</div>
	    	<div class="panel-body scrollable-box">

<?php if( $deposits ) { ?>

<div class="list-group">
<?php foreach($deposits as $deposit) { ?>
  	<label class="list-group-item text-center" id="list-group-item-<?php echo $deposit->trn_id; ?>">
  	<span class="pull-left">
  	<input <?php echo ($deposit->recon_id) ? 'checked' : ''; ?> type="checkbox" data-amount="<?php echo number_format($deposit->total_amount,2,".",""); ?>" class="recon_item" data-type="deposit" name="item[]" value="<?php echo $deposit->trn_id; ?>">
  	<?php echo date('m/d/Y', strtotime($deposit->date)); ?> -
  	<?php echo $deposit->trn_type; ?> <?php echo $deposit->number; ?>
  	<?php echo (trim($deposit->memo)!='') ? " - " . $deposit->memo : ''; ?>
  	</span>
  	<span class="pull-right balance_end"><?php echo number_format($deposit->total_amount,2); ?></span>
  	<a target="_transaction" href="<?php echo site_url("report_transactions/redirect/{$deposit->trn_id}"); ?>"><span class="glyphicon glyphicon-eye-open"></span></a>
  	<span class="clearfix"></span>
  	</label>
<?php } ?>
</div>

<?php } else { ?>

	<p class="text-center">No Deposits Found!</p>

<?php } ?>

	    	</div>
	    </div>
    </div>
</div>
<div class="row">
	<div class="col-md-12">
	    <div class="panel panel-default">
	    	<div class="panel-heading">

	    	<button type="submit" class="btn btn-success btn-xs pull-right hidden" id="reconcile_button">Reconcile Now</button>
	    		<h3 class="panel-title"><?php echo date('F Y', strtotime($recon->date_end)); ?> 
	    	<?php if( hasAccess('accounting', 'reconciliation', 'edit') ) { ?>
	    		<a href="#" class="ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Edit Recon Details" data-url="<?php echo site_url("accounting_reconciliation/edit_recon/{$recon->id}/ajax") . "?next=" . uri_string(); ?>"><span class="glyphicon glyphicon-pencil"></span></a>
	    	<?php } ?>
	    		</h3>
	    	</div>
	    	<div class="panel-body">
<?php
$beg_balance = ($last_recon) ? $last_recon->balance_end : 0;
?>

<div class="row">
	<div class="col-md-6">
		<table class="table table-default table-condensed">
			<tr>
				<td class="bold">Beginning Balance</td>
				<td class="text-right" id="recon_balance_beg"><?php echo number_format($beg_balance,2); ?></td>
			</tr>
			<tr>
				<td class="bold">Disbursements</td>
				<td class="text-right" id="recon_output_disburse"><?php echo number_format($recon->total_disburse,2); ?></td>
			</tr>
			<tr>
				<td class="bold">Deposits</td>
				<td class="text-right" id="recon_output_deposit"><?php echo number_format($recon->total_deposit,2); ?></td>
			</tr>
		</table>
	</div>
	<div class="col-md-6">
		<table class="table table-default table-condensed">
			<tr>
				<td class="bold">Ending Balance</td>
				<td class="text-right" id="recon_balance_end"><?php echo number_format($recon->balance_end,2); ?></td>
			</tr>
			<tr>
				<td class="bold">Cleared Balance</td>
				<td class="text-right" id="recon_output_cleared"><?php echo number_format((($beg_balance + $recon->total_deposit) - $recon->total_disburse),2); ?></td>
			</tr>
			<tr>
				<td class="bold">Difference</td>
				<td class="text-right" id="recon_output_difference"><?php echo number_format((($recon->balance_end-$beg_balance)+($recon->total_disburse-$recon->total_deposit)),2); ?></td>
			</tr>
		</table>
	</div>
</div>


	    	</div>
	    </div>
    </div>
</div>
</div>
</form>
<?php $this->load->view('footer'); ?>