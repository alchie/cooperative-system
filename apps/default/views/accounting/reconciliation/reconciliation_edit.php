<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( !isset($output) || ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('accounting/accounting_navbar'); ?>

<div class="container">
<div class="row">
	<div class="col-md-6 col-md-offset-3">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
<?php if( !$adv_recon ) { ?>
      <?php if( hasAccess('accounting', 'reconciliation', 'delete') ) { ?>
        <a href="<?php echo site_url("accounting_reconciliation/delete_recon/{$recon->id}"); ?>" class="btn btn-xs btn-danger pull-right confirm">Delete</a>
      <?php } ?>
<?php if($recon->recon_items > 0) { ?>
        <a href="<?php echo site_url("accounting_reconciliation/reset_recon/{$recon->id}"); ?>" class="btn btn-xs btn-warning pull-right confirm" style="margin-right: 5px;">Reset</a>
<?php } ?>
<?php } ?>
	    		<h3 class="panel-title">Edit Reconciliation</h3>
	    	</div>
	    	        <form method="post">
        <div class="panel-body">

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>
        
<?php endif; ?>

<div class="row">
  <div class="col-md-6">
          <div class="form-group">
            <label>Last Bank Recon</label>
            <div class="form-control text-center" value=""><?php echo ($last_recon) ? date('F d, Y', strtotime($last_recon->date_end)) : 'None'; ?></div>
          </div>
   </div>
    <div class="col-md-6">
          <div class="form-group">
            <label>Beginning Balance</label>
            <div class="form-control text-right" value=""><?php echo ($last_recon) ? number_format($last_recon->balance_end, 2) : '0.00'; ?></div>
          </div>
    </div>
</div>

<div class="row">
 	<div class="col-md-6">
          <div class="form-group">
            <label>Statement Date End</label>
            
<input <?php echo ( $recon->recon_items ) ? 'disabled' : ''; ?> name="date_end" type="text" class="form-control text-center datepicker" value="<?php echo date('m/d/Y', strtotime($recon->date_end)); ?>">
            
           
          </div>
 	 </div>
 	 <div class="col-md-6">
          <div class="form-group">
            <label>Ending Balance</label>
           
 <input <?php echo ( $recon->recon_items ) ? 'disabled' : ''; ?> name="balance_end" type="text" class="form-control text-right" value="<?php echo number_format($recon->balance_end, 2); ?>">           
          </div>
    </div>
</div>

          <div class="form-group">
            <label>Memo</label>
            <textarea name="memo" type="text" class="form-control"><?php echo $recon->memo; ?></textarea>
          </div>

<?php if( isset($output) && ($output=='ajax') ) :
if( !$adv_recon ) {
?>
  <?php if( hasAccess('accounting', 'reconciliation', 'delete') ) { ?>
    <a href="<?php echo site_url("accounting_reconciliation/delete_recon/{$recon->id}"); ?>" class="btn btn-danger btn-xs confirm">Delete this recon</a>
  <?php } ?>
 <?php if($recon->recon_items > 0) { ?>
        <a href="<?php echo site_url("accounting_reconciliation/reset_recon/{$recon->id}"); ?>" class="btn btn-xs btn-warning pull-right confirm">Reset this recon</a>
<?php } ?>
  <?php
} else {
  echo "<strong>DELETE DISABLED!</strong> Delete the latest recon(s) to delete this one!";
}
 endif; ?>

<?php if( !isset($output) || ($output!='ajax') ) : ?>

        </div>
        <div class="panel-footer">
          <a href="<?php echo site_url("accounting_reconciliation/recon_items/{$recon->id}"); ?>" class="btn btn-primary pull-right">Items</a>

          <button type="submit" class="btn btn-success">Submit</button>
          <a href="<?php echo site_url("accounting_reconciliation"); ?>" class="btn btn-warning">Back</a>
        </div>


        </form>

	    </div>
    </div>
</div>
</div>

<?php $this->load->view('footer'); ?>

<?php endif; ?>