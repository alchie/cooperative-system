<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container">
	<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <div class="navbar-brand">Accounting</div>
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
<?php if( isset($enable_search) && ($enable_search==true) ) { ?>
      <form class="navbar-form navbar-left" role="search">
       <div class="input-group">
          <input name="q" type="text" class="form-control" placeholder="Search Account" value="<?php echo $this->input->get('q'); ?>">
            <span class="input-group-btn">
            <button class="btn btn-info" type="button"><i class="glyphicon glyphicon-search"></i></button>
          </span>
        </div>
      </form>
<?php } ?>
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $current_page; ?> <span class="caret"></span></a>
          <ul class="dropdown-menu">

<?php 
$url['configuration'] = array('uri' => 'accounting_settings/configuration', 'title'=>'Configuration', 'access'=>(hasAccess('system', 'settings', 'view') && hasAccess('accounting', 'settings', 'view')));
$url['class_list'] = array('uri' => 'accounting_settings/class_list', 'title'=>'Class List', 'access'=>(hasAccess('system', 'settings', 'view') && hasAccess('accounting', 'settings', 'view')));
$url['item_list'] = array('uri' => 'accounting_settings/item_list', 'title'=>'Item List', 'access'=>(hasAccess('system', 'settings', 'view') && hasAccess('accounting', 'settings', 'view')));
$url['accounting_checkbooks'] = array('uri' => 'accounting_checkbooks', 'title'=>'Checkbooks', 'access'=>(hasAccess('system', 'settings', 'view') && hasAccess('accounting', 'checkbooks', 'view')));
foreach($url as $k=>$v) {
  if( $v['access'] ) {
?>
  <li class="<?php echo ($k==$current_uri) ? 'active' : ''; ?>"><a class="body_wrapper" href="<?php echo site_url($v['uri']); ?>"><?php echo $v['title']; ?></a></li>
<?php } } ?>

          </ul>
        </li>
<?php if( isset( $reports ) && $reports ) { ?>
 <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Saved Reports<span class="caret"></span></a>
          <ul class="dropdown-menu">
<?php foreach($reports as $report) { ?>
          <li class="<?php echo (isset($current_report) && ($current_report->id==$report->id)) ? 'active' : ''; ?>"><a href="<?php echo site_url("{$current_uri}/view/{$report->id}"); ?>"><?php echo $report->name; ?></a></li>
<?php } ?>
          </ul>
</li>
<?php } ?>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
</div>