<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>

<?php if( ! $inner_page ): ?>

<?php $this->load->view('accounting/settings/settings_navbar'); ?>

<div class="container">
<div class="row">
	<div class="col-md-12">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
<?php if( hasAccess('accounting', 'settings', 'add') ) { ?>
	    		<button type="button" class="btn btn-success btn-xs pull-right ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Add Item" data-url="<?php echo site_url("accounting_settings/add_item/ajax") . "?next=" . uri_string(); ?>">Add Item</button>
<?php } ?>
	    		<h3 class="panel-title">Item List</h3>
	    	</div>
	    	<div class="panel-body" id="ajaxBodyInnerPage">

<?php endif; ?>

<?php if( $item_list ) { ?>
	    		<table class="table table-default table-hover">
	    			<thead>
	    				<tr>
	    					<th>Item Name</th>
<?php if( hasAccess('accounting', 'settings', 'edit') || hasAccess('accounting', 'settings', 'delete') ) { ?>
	    					<th width="120px">Action</th>
<?php } ?>
	    				</tr>
	    			</thead>
	    			<tbody>
<?php 
function display_list($item_list , $pre="") {
	foreach($item_list as $item) { 
	?>
		    				<tr class="<?php echo ($item->active) ? '' : 'danger'; ?>">
		    					<td><?php echo $pre; ?><?php echo $item->value; ?></td>
		    					<td>
<?php if( hasAccess('accounting', 'settings', 'edit') ) { ?>
		    					<button type="button" class="btn btn-warning btn-xs ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Edit Item" data-url="<?php echo site_url("accounting_settings/edit_item/{$item->id}/ajax") . "?next=" . uri_string(); ?>">Edit</button>
<?php } ?>
<?php if( hasAccess('accounting', 'settings', 'delete') ) { ?>
		    					<a href="<?php echo site_url("accounting_settings/delete_item/" . $item->id); ?>" class="btn btn-danger btn-xs confirm">Delete</a>
<?php } ?>
		    					</td>
		    				</tr>
	<?php
		if( $item->children ) {
			display_list( $item->children, $pre . "- - - - " );
		}
	}
}
display_list($item_list);
?>
	    			</tbody>
	    		</table>

	    		<?php echo ($pagination!='') ? '<center>' . $pagination . '</center>' : ''; ?>

<?php } else { ?>
	<div class="text-center">No Item Found!</div>
<?php } ?>

<?php if( ! $inner_page ): ?>

	    	</div>
	    </div>
    </div>
</div>
</div>
<?php endif; ?>

<?php $this->load->view('footer'); ?>