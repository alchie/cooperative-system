<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php 
function display_list($selected, $main_accounts , $pre="", $i=0) {

          $coa_types = unserialize(CHART_ACCOUNT_TYPES);
          $charts = array_merge($coa_types['Balance Sheet'], $coa_types['Income Statement']);

          foreach( $main_accounts as $main_account) { 
                  ?>
                  <option data-subtext="<?php echo $charts[$main_account->type]; ?>" value="<?php echo $main_account->id; ?>" <?php echo ($selected==$main_account->id) ? 'selected' : ''; ?> title="<?php echo $main_account->title; ?>"><?php echo $pre; ?><?php echo $main_account->title; ?></option>
          <?php 
            if( $main_account->children ) {
              display_list( $selected, $main_account->children, $pre . "- - - - ", $i );
            }
          }  
  } 

function display_parent($item_list , $pre="") {
foreach($item_list as $item) { 
    echo "<option value=\"{$item->id}\">{$pre}{$item->value}</option>";
    if( $item->children ) {
      display_list( $item->children, $pre . "- - - - ");
    }

  }
}
?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('accounting/settings/settings_navbar'); ?>

<div class="container">
<div class="row">

  <div class="col-md-6 col-md-offset-3">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Add Item</h3>
        </div>
        <form method="post">
        <div class="panel-body">

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

<?php endif; ?>

          <div class="form-group">
            <label>Item Name</label>
            <input name="name" type="text" class="form-control" value="<?php echo $this->input->post('name'); ?>">
          </div>

          <div class="form-group">
          <label>Account</label>
          <select type="text" class="form-control select_account_titles selectpicker" name="account_title" title="Select an Account Title..." required="required">
          <?php 
          display_list( NULL, $account_titles, '', $i);
          ?>        
          </select>
          </div>

          <div class="form-group">
            <label>Parent Item</label>
           <select name="parent" type="text" class="form-control" title="Select Parent Item">
           <option value="0" selected>Root Item</option>
<?php 
display_parent($item_list, "");
?>
            </select>
          </div>

<?php if( isset($output) && ($output!='ajax') ) : ?>

        </div>
        <div class="panel-footer">
          <button type="submit" class="btn btn-success">Submit</button>
          <a href="<?php echo site_url("accounting_settings/item_list"); ?>" class="btn btn-warning">Back</a>
        </div>


        </form>
      </div>
    </div>
</div>
</div>

<?php $this->load->view('footer'); ?>

<?php endif; ?>