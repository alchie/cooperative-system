<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('accounting/settings/settings_navbar'); ?>

<div class="container">
<div class="row">

  <div class="col-md-6 col-md-offset-3">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Edit Class</h3>
        </div>
        <form method="post">
        <div class="panel-body">

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : '';  ?>

<?php endif; ?>

          <div class="form-group">
            <label>Class Name</label>
            <input name="name" type="text" class="form-control" value="<?php echo $current_class->value; ?>">
          </div>

          <div class="form-group">
            <label>Parent Class</label>
            <select name="parent" type="text" class="form-control" title="Select Parent Class">
<?php 
function display_list($classList , $pre="", $current_class) {
foreach($classList as $class) { 
  ?>
                    <option value="<?php echo $class->id; ?>" <?php echo ($class->id==$current_class->parent) ? 'SELECTED' : ''; ?>><?php echo $pre; ?><?php echo $class->value; ?></option>
<?php 
   
  if( $class->children ) {
    display_list( $class->children, $pre . "- - - - ", $current_class);
  }

  }
}
display_list($class_list, "", $current_class);
?>
            </select>
          </div>

          <div class="form-group">
            <label><input type="checkbox" name="active" value="1" <?php echo ($current_class->active==1) ? 'CHECKED' : ''; ?>> Active</label>
          </div>

<?php if( isset($output) && ($output!='ajax') ) : ?>

        </div>
        <div class="panel-footer">
          <button type="submit" class="btn btn-success">Submit</button>
          <a href="<?php echo site_url("accounting_settings/class_list"); ?>" class="btn btn-warning">Back</a>
        </div>


        </form>
      </div>
    </div>
</div>
</div>

<?php $this->load->view('footer'); ?>

<?php endif; ?>