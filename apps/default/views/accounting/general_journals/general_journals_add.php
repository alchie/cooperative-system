<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( !isset($output) || ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('accounting/accounting_navbar'); ?>

<div class="container">
<div class="row">

  <div class="col-md-6 col-md-offset-3">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Add Journal</h3>
        </div>
        <form method="post">
        <div class="panel-body">

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

<?php endif; ?>

<div class="row">
  <div class="col-md-6">
          <div class="form-group">
            <label>Date</label>
            <input name="date" type="text" class="text-center form-control datepicker" value="<?php echo ($this->input->post('date')) ? $this->input->post('date') : date("m/d/Y"); ?>">
          </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
            <label>Journal Number</label>
            <input name="number" type="text" class="form-control text-right" value="<?php echo (intval($journal->number) + 1); ?>">
          </div>
  </div>
</div>

          <div class="form-group">
            <label>Memo</label>
            <textarea name="memo" type="text" class="form-control" value="<?php echo ($this->input->post('date')) ? $this->input->post('date') : date("m/d/Y"); ?>"></textarea>
          </div>

<?php if( !isset($output) || ($output!='ajax') ) : ?>

        </div>
        <div class="panel-footer">
          <button type="submit" class="btn btn-success">Submit</button>
          <a href="<?php echo site_url("accounting_general_journals"); ?>" class="btn btn-warning">Back</a>
        </div>


        </form>
      </div>
    </div>
</div>
</div>

<?php $this->load->view('footer'); ?>

<?php endif; ?>