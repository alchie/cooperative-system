<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>

<?php if( ! $inner_page ): ?>

<style>
<!-- 
a.item, a.item:hover {
	color:#555555;
	text-decoration: none;
}
-->
</style>
<?php $this->load->view('accounting/accounting_navbar'); ?>

<div class="container">
<div class="row">
	<div class="col-md-12">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
	    		<a href="<?php echo site_url("accounting_charts"); ?>" class="btn btn-warning btn-xs pull-right body_wrapper">Back to Charts</a>
	    		<h3 class="panel-title"><?php echo $charts_data->title; ?></h3>
	    	</div>
	    	<div class="panel-body" id="ajaxBodyInnerPage">

<?php endif; // inner_page ?>

<?php if( $entries_data ) { ?>
	    		<table class="table table-default table-hover table-condensed">
	    			<thead>
	    				<tr>
	    					<th>Type</th>
	    					<th>Date</th>
	    					<th>Name</th>
	    					<th>Memo</th>
	    					<th class="text-right">Amount</th>
	    					<th class="text-right" width="10px"></th>
	    				</tr>
	    			</thead>
	    			<tbody>
<?php 
$total = 0;
foreach($entries_data as $entry) { 

 ?>
<tr>
	<td><?php echo $entry->type; ?></td>
	<td><?php echo date('m/d/Y', strtotime($entry->date)); ?></td>
	<td><?php echo $entry->full_name; ?></td>
	<td><?php echo $entry->memo; ?></td>
	<td class="text-right">
<?php 
$balance = 0;
if( (strpos($charts_data->type, '_BS_ASS_') ) || (strpos($charts_data->type, '_IS_EXP_') ) ) {
	$balance = number_format(($entry->total_debit-$entry->total_credit),2);
} else {
	$balance = number_format(($entry->total_credit-$entry->total_debit),2);
}
echo $balance; 
?>
</td>
<td class="text-right"><a target="_detail" href="<?php echo site_url('report_transactions/redirect/' . $entry->trn_id) . "?next=" . uri_string(); ?>" class="item"><span class="fa fa-eye"></span></a></td>
</tr>
<?php } ?>
	    			</tbody>
	    		</table>

<?php echo ($pagination!='') ? '<center>' . $pagination . '</center>' : ''; ?>

<?php } else { ?>
	<div class="text-center">No Entries Found!</div>
<?php } ?>

<?php if( ! $inner_page ): ?>

	    	</div>
	    </div>
    </div>
</div>
</div>

<?php endif; // inner_page ?>

<?php $this->load->view('footer'); ?>