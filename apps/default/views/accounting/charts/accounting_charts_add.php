<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( !isset($output) || ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('accounting/accounting_navbar'); ?>

<div class="container">
<div class="row">

	<div class="col-md-6 col-md-offset-3">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
	    		<h3 class="panel-title">Add Account</h3>
	    	</div>
	    	<form method="post">
	    	<div class="panel-body">

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

<?php endif; ?>

          <div class="form-group">
            <label>Account Title</label>
            <input name="title" type="text" class="form-control" value="<?php echo $this->input->post('title'); ?>">
          </div>

<div class="row">
<div class="col-md-6">
          <div class="form-group">
            <label>Account Type</label>
            <select name="type" class="form-control">
             
              <?php foreach( $coa_types as $label=>$types) { ?>
                 <optgroup label="<?php echo $label; ?>">
                 <?php foreach( $types as $key=>$value) { ?>
                  <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                  <?php } ?>
                </optgroup>
              <?php } ?>
              
            </select>
          </div>
  </div>
  <div class="col-md-6">
          <div class="form-group">
            <label>Account Number</label>
            <input name="number" type="text" class="form-control" value="<?php echo $this->input->post('number'); ?>">
          </div>
  </div>

<?php if( !isset($output) || ($output!='ajax') ) : ?>

</div>

	    	</div>
	    	<div class="panel-footer">
	    		<button type="submit" class="btn btn-success">Submit</button>
	    		<a href="<?php echo site_url("accounting_charts"); ?>" class="btn btn-warning">Back</a>
	    	</div>


	    	</form>
	    </div>
    </div>
</div>
</div>

<?php $this->load->view('footer'); ?>

<?php endif; ?>