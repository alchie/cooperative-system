<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php 
function display_items($item_list, $pre="", $selected=NULL) {
  foreach($item_list as $item) { 
    echo "<option value=\"{$item->id}\"";
      if($selected==$item->id) {
        echo " SELECTED";
      }
    echo ">{$pre}{$item->value}</option>";
    if( $item->children ) {
      display_items( $item->children, $pre . "- - - - ", $selected);
    }
  }
}

function display_class($class_list , $pre="", $selected=NULL ) {
    foreach($class_list as $class) { 
      echo "<option value=\"{$class->id}\"";
        if($selected==$class->id) {
          echo " SELECTED";
        }
      echo ">{$pre}{$class->value}</option>";
      if( $class->children ) {
        display_items( $class->children, $pre . "- - - - ", $selected);
      }
    }
  }
?>
<?php if( !isset($output) || ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('accounting/accounting_navbar'); ?>

<div class="container">
<div class="row">

<form method="post">

  <div class="col-md-6 col-md-offset-3">


<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>



        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Add Receipt Item</h3>
            </div>

            <div class="panel-body">

<?php endif; ?>
        

          <div class="form-group">
            <label>Item Type</label>
            <select name="item_type" class="form-control">
              <?php foreach( $item_types as $key=>$value) { ?>
                  <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
              <?php } ?>
            </select>
          </div>

<div class="row">
  <div class="col-md-8">
          <div class="form-group">
            <label>Item</label>
           <select name="item_id" type="text" class="form-control" title="Select Item" required>
<?php 
display_items($item_list, "");
?>
            </select>
          </div>
  </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="control-label">Amount</label>
            <input class="form-control text-right" type="text" name="amount" value="<?php echo number_format($receipt->balance,2,".",""); ?>" required>
        </div>
  </div>
</div>
<div class="row">
  <div class="col-md-6">
        <div class="form-group ">
            <label class="control-label">Name</label>
            <input id="receipt_item_name_id" name="name_id" type="hidden">
            <input class="form-control autocomplete-name_select" data-source="<?php echo site_url("accounting_sales_receipts/ajax/name"); ?>" data-name_id="receipt_item_name_id" type="text">
        </div>
  </div>
  <div class="col-md-6">
        <div class="form-group ">
            <label class="control-label">Class</label>
            <select type="text" class="form-control" name="class_id" title="Select a Class...">
<?php display_class( $class_list, "" ); ?>
              </select>
        </div>
  </div>
</div>
         <div class="form-group ">
            <label class="control-label">Memo</label>
            <textarea class="form-control" name="memo"></textarea>
        </div>


<?php if( !isset($output) || ($output!='ajax') ) : ?>

            </div>

                    <div class="panel-footer">
          <button type="submit" class="btn btn-success">Submit</button>
          <a href="<?php echo site_url("accounting_sales_receipts/receipt_items/{$receipt->id}"); ?>" class="btn btn-warning">Back</a>
        </div>

        </div>


    </div>

    </form>

</div>
</div>

<?php $this->load->view('footer'); ?>

<?php endif; ?>