<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( !isset($output) || ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('accounting/accounting_navbar'); ?>

<div class="container">
<div class="row">
	<div class="col-md-12">
	    <div class="panel panel-<?php echo (($receipt->trn_id) && (round($receipt->balance) == 0 ) && ($receipt->trn_exist) && ($receipt->debit_cleared==0) && ($receipt->credit_cleared==0)) ? 'default' : 'danger'; ?>">
	    	<div class="panel-heading">

<?php if(hasAccess('accounting', 'sales_receipts', 'edit')) { ?>
<?php if(round($receipt->balance) > 0 ) { ?>
          <button type="button" class="btn btn-success btn-xs pull-right ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Add Item" data-url="<?php echo site_url("accounting_sales_receipts/add_receipt_item/{$receipt->id}/ajax") . "?next=" . uri_string(); ?>">Add Item</button>
<?php } else { ?>

    <?php if(($receipt->trn_id=='') || ($receipt->trn_exist==0) || (is_null($receipt->debit_cleared)) || ($receipt->debit_cleared > 0 ) ||(ROUND($receipt->credit_cleared) > 0) || (is_null($receipt->credit_cleared)) ) { ?>
    <a class="btn btn-xs btn-success pull-right" href="<?php echo site_url("accounting_sales_receipts/publish_receipt/{$receipt->id}"); ?>">Publish Receipt</a>
    <?php } ?>
<?php } ?>
<?php } ?>

                <h3 class="panel-title">
<?php if( $receipt->deposit_id ) { ?>
    <a href="<?php echo site_url("accounting_deposits/edit_deposit/{$receipt->deposit_id}"); ?>?highlight=<?php echo $receipt->trn_id; ?>" class="item"><span class="fa fa-bank"></span></a>
<?php } ?>
                Sales Receipts Items : <strong>Receipt # <?php echo $receipt->receipt_number; ?></strong> <small><?php echo date('m/d/Y', strtotime($receipt->receipt_date)); ?> - <?php echo $receipt->full_name; ?></small> 
<?php if(hasAccess('accounting', 'sales_receipts', 'edit')) { ?>
                <a href="#edit-receipt" class="ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Edit Receipt" data-url="<?php echo site_url("accounting_sales_receipts/edit_receipt/{$receipt->id}/ajax") . "?next=" . uri_string(); ?>"><span class="glyphicon glyphicon-pencil"></span></a>
<?php } ?>
                </h3>
	    	</div>
	    	<div class="panel-body">

<?php endif; ?>

<?php if( $receipt_items ) { ?>
          <table class="table table-default table-hover">
            <thead>
              <tr>
                <th width="10px"></th>
                <th>Type</th>
                <th>Item</th>
                <th>Name</th>
                <th>Class</th>
                <th>Memo</th>
                <th class="text-right">Amount</th>
<?php if( !isset($output) || ($output!='ajax') ) : ?>
<?php if(hasAccess('accounting', 'sales_receipts', 'edit')) { ?>
<?php if( !$receipt->deposit_id ) { ?>
                <th class="text-right" width="10px">Actions</th>
<?php } ?>
<?php } ?>
<?php endif; ?>
              </tr>
            </thead>
            <tbody>
<?php 
$n = 0;
foreach($receipt_items as $item) {  ?>
<tr class="<?php echo (($item->entry_id=='')||($item->entry_exist==0)||($item->entry_cleared!=0)) ? 'danger' : ''; ?>">
    <td>
<?php if ( ( round($receipt->balance) > 0 ) && ( !$receipt->deposit_id ) ) { ?>
    <a class="confirm item" href="<?php echo site_url("accounting_sales_receipts/duplicate_receipt_item/{$receipt->id}/{$item->id}"); ?>"><span class="glyphicon glyphicon-duplicate"></span></a>
<?php } else { ?>
  <span class="glyphicon glyphicon-lock"></span>
<?php } ?>
    </td>
    <td><?php echo $item->item_type; ?></td>
    <td><?php echo $item->item_name; ?></td>
    <td><?php echo $item->full_name; ?></td>
    <td><?php echo $item->class_name; ?></td>
    <td><?php echo $item->memo; ?></td>
    <td class="text-right"><?php echo number_format($item->amount,2); ?></td>

<?php if( !isset($output) || ($output!='ajax') ) : ?>
<?php if(hasAccess('accounting', 'sales_receipts', 'edit')) { ?>
<?php if( !$receipt->deposit_id ) { ?>
   <td class="text-right">
    <button type="button" class="btn btn-warning btn-xs pull-right ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Update Item" data-url="<?php echo site_url("accounting_sales_receipts/edit_receipt_item/{$item->id}/ajax") . "?next=" . uri_string(); ?>">Update</button>
   </td>
<?php } ?>
<?php } ?>
<?php endif; ?>

 </tr>
<?php $n++;
 }
if($n > 1) {
 ?>
 <tr class="warning">
     <td class="bold" colspan="6">TOTAL</td>
     <td class="text-right bold"><?php echo number_format($receipt->total_items,2); ?></td>
<?php if( !isset($output) || ($output!='ajax') ) : ?>
<?php if( !$receipt->deposit_id ) { ?>
               <td></td>
<?php } ?>
<?php endif; ?>
     
 </tr>
<?php } ?>
            </tbody>
          </table>

<?php } else { ?>
  <p class="text-center">No Item Found!</p>
<?php } ?>

<?php if( !isset($output) || ($output!='ajax') ) : ?>

	    	</div>
	    </div>
    </div>
</div>
</div>

<?php $this->load->view('footer'); ?>

<?php endif; ?>