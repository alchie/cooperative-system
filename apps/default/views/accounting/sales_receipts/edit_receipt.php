<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( !isset($output) || ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('accounting/accounting_navbar'); ?>

<div class="container">
<div class="row">

<form method="post">

  <div class="col-md-6 col-md-offset-3">


<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>


        <div class="panel panel-default">
            <div class="panel-heading">
<?php if( !$receipt->deposit_id ) { ?>
                <a href="<?php echo site_url("accounting_sales_receipts/delete_receipt/{$receipt->id}"); ?>" class="btn btn-danger btn-xs pull-right confirm">Delete</a>
<?php } else { ?>
    <a href="<?php echo site_url("accounting_deposits/edit_deposit/{$receipt->deposit_id}"); ?>?highlight=<?php echo $receipt->trn_id; ?>" class="btn btn-success btn-xs pull-right">Deposit</a>
<?php }  ?>
                <h3 class="panel-title">Record Receipt</h3>
            </div>

            <div class="panel-body">

<?php endif; ?>
        <span class="badge pull-right">TRN # <?php echo $receipt->trn_id; ?></span>
        <div class="form-group ">
            <label class="control-label">Received From</label>
            <input id="record_receipt_name_id" name="name_id" type="hidden" value="<?php echo $receipt->name_id; ?>">
            <input name="" class="form-control autocomplete-name_select autocomplete-name_select-name-input-<?php echo time(); ?>" data-source="<?php echo site_url("accounting_sales_receipts/ajax/name"); ?>" data-name_id="record_receipt_name_id" type="text" style="display: none;" <?php echo ( $receipt->deposit_id ) ? 'disabled' : ''; ?>>
            <div class="form-control autocomplete-name_select-name-display-<?php echo time(); ?>"><a class="badge" id="changeName<?php echo ( $receipt->deposit_id ) ? 'disabled' : 'required'; ?>" href="#changeName" data-id="<?php echo $receipt->name_id; ?>" data-name_id="record_receipt_name_id" data-timestamp="<?php echo time(); ?>"><?php echo $receipt->full_name; ?></a></div>
        </div>
<div class="row">
  <div class="col-md-4">
        <div class="form-group ">
            <label class="control-label">Receipt Number</label>
            <input class="form-control text-center" type="text" name="receipt_number" value="<?php echo $receipt->receipt_number; ?>" <?php echo ( $receipt->deposit_id ) ? 'disabled' : 'required'; ?>>
        </div>
  </div>
  <div class="col-md-4">
        <div class="form-group ">
            <label class="control-label">Date Received</label>
            <input class="form-control datepicker text-center" type="text" name="receipt_date" value="<?php echo date('m/d/Y', strtotime($receipt->receipt_date)); ?>" <?php echo ( $receipt->deposit_id ) ? 'disabled' : 'required'; ?>>
        </div>
  </div>
  <div class="col-md-4">
        <div class="form-group ">
            <label class="control-label">Amount</label>
            <input class="form-control text-right" type="text" name="amount" value="<?php echo number_format($receipt->amount,2,'.',''); ?>" <?php echo ( $receipt->deposit_id ) ? 'disabled' : 'required'; ?>>
        </div>
  </div>
</div>
         <div class="form-group ">
            <label class="control-label">Memo</label>
            <textarea class="form-control" name="memo"><?php echo $receipt->memo; ?></textarea>
        </div>

<?php if( isset($output) && ($output=='ajax') ) : ?>
    <?php if( !$receipt->deposit_id ) { ?>
 <a href="<?php echo site_url("accounting_sales_receipts/delete_receipt/{$receipt->id}"); ?>" class="btn btn-danger btn-xs confirm">Delete this receipt</a>
    <?php } else { ?>
 <a href="<?php echo site_url("accounting_deposits/edit_deposit/{$receipt->deposit_id}"); ?>?highlight=<?php echo $receipt->trn_id; ?>" class="btn btn-success btn-xs">View Deposit</a>
    <?php }  ?>

    <?php if(($receipt->debit_cleared!=0)||($receipt->reverse_credit!=0)||((is_null($receipt->total_items))) && (!is_null($receipt->debit_cleared)) ) { ?>
      <a href="<?php echo site_url("accounting_sales_receipts/clear_entries/{$receipt->id}"); ?>" class="btn btn-danger btn-xs pull-right confirm">Clear Entries</a>

    <?php }  ?>
<?php endif; ?>

<?php if( !isset($output) || ($output!='ajax') ) : ?>

            </div>

                    <div class="panel-footer">
          <button type="submit" class="btn btn-success">Submit</button>
          <a href="<?php echo site_url("accounting_sales_receipts"); ?>" class="btn btn-warning">Back</a>

          <a href="<?php echo site_url("accounting_sales_receipts/receipt_items/{$receipt->id}"); ?>" class="btn btn-info pull-right">Items</a>
        </div>

        </div>


    </div>

    </form>

</div>
</div>

<?php $this->load->view('footer'); ?>

<?php endif; ?>