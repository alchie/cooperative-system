<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>

<?php $this->load->view('accounting/accounting_navbar'); ?>

<?php 
$deposit_amount = 0;
foreach($entries_data as $eData) {
	$deposit_amount += $eData->total_amount;
}
?>
<div class="container">
<div class="row">
	<div class="col-md-6 col-md-offset-3">

<form method="post" action="">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
	    		<h3 class="panel-title">Deposit to <strong><?php echo $bank->title; ?></strong></h3>
	    	</div>
	    	<div class="panel-body">
<div class="row">
	<div class="col-md-6">
          <div class="form-group">
            <label>Date Deposited</label>
            <input name="deposit_date" type="text" class="form-control text-center datepicker" value="<?php echo ($this->input->post('deposit_date')) ? $this->input->post('deposit_date') : date('m/d/Y'); ?>">
          </div>
	</div>
	<div class="col-md-6">
          <div class="form-group">
            <label>Deposited Amount</label>
            <input type="hidden" name="deposit_amount" value="<?php echo $deposit_amount; ?>">
            <span class="form-control bold text-right"><?php echo number_format( $deposit_amount, 2); ?></span>
          </div>
	</div>
</div>

<div class="form-group">
            <label>Memo</label>
            <textarea name="deposit_memo" class="form-control"><?php echo $this->input->post('deposit_memo'); ?></textarea>
          </div>

	    	</div>
	    <div class="panel-footer">
          <button type="submit" class="btn btn-success">Deposit Now</button>
          <a href="<?php echo site_url("accounting_deposits/select_receipts/{$bank->id}"); ?>" class="btn btn-warning">Back</a>
        </div>

	    </div>
</form>
    </div>
</div>

<div class="row">
	<div class="col-md-12">

	    <div class="panel panel-default">
	    	<div class="panel-body">
<?php if( $undeposited ) { ?>

<?php if( $entries_data ) { ?>
	    		<table class="table table-condensed table-hover">
	    			<thead>
	    				<tr>
	    					<th>Number</th>
	    					<th>Date</th>
	    					<th>Name</th>
	    					<th>Class</th>
	    					<th>Memo</th>
	    					<th class="text-right">Amount</th>
	    				</tr>
	    			</thead>
	    			<tbody>
<?php 
$total = 0;
foreach($entries_data as $entry) { 
?>
<tr>
	<td><?php echo $entry->number; ?></td>
	<td><?php echo date('m/d/Y', strtotime($entry->date)); ?></td>
	<td><?php echo $entry->full_name; ?></td>
	<td><?php echo $entry->class_name; ?></td>
	<td><?php echo $entry->memo; ?></td>
	<td class="text-right"><?php echo number_format($entry->total_amount,2); $total+=$entry->total_amount; ?></td>
</tr>
<?php } ?>
<tr class="success">
	<td colspan="5"><strong>TOTAL DEPOSIT</strong></td>
	<td class="text-right bold"><?php echo number_format($total,2); ?></td>
</tr>
	    			</tbody>
	    		</table>

<?php } ?>

<?php } ?>
	    	</div>

	    </div>

    </div>
</div>

</div>

<?php $this->load->view('footer'); ?>