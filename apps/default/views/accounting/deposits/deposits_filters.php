<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('reports/reports_navbar'); ?>

<div class="container">
<div class="row">
	<div class="col-md-6 col-md-offset-3">

<form method="post">

	    <div class="panel panel-default">

	    	<div class="panel-heading">

	    		<h3 class="panel-title">Deposits Filter</h3>

	    	</div>
	    	<div class="panel-body">

<?php endif; ?>

  <div class="row">

      <div class="col-md-12">
        <div class="form-group ">
            <label class="control-label">Title</label>
            <input type="text" class="form-control" name="filter[title]" value="<?php echo (isset($filters['title'])) ? $filters['title'] : "Deposits"; ?>">
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group ">
            <label class="control-label">Bank Account</label>
                 <select name="filter[bank_account]" class="form-control" title="Filter Bank Account">
                <?php foreach($bank_accounts as $bank_account) { 
                    echo "<option value=\"{$bank_account->id}\"";
                    echo ( isset($filters['bank_account']) && ($filters['bank_account'] == $bank_account->id) ) ? " SELECTED" : "";
                    echo ">{$bank_account->title}</option>";
                 } ?>
                </select>
        </div>
    </div>

<div class="col-md-12">
        <div class="form-group ">
  <div class="row">
    <div class="col-md-6">
        <div class="form-group ">
            <label class="control-label">Date Range (Start)</label>
            <input class="form-control datepicker text-center" type="text" name="filter[date_range_beg]" value="<?php echo (isset($filters['date_range_beg'])) ? $filters['date_range_beg'] : date('m/01/Y'); ?>">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group ">
            <label class="control-label">Date Range (End)</label>
            <input class="form-control datepicker text-center" type="text" name="filter[date_range_end]" value="<?php echo (isset($filters['date_range_end'])) ? $filters['date_range_end'] : date('m/d/Y'); ?>">
        </div>
    </div>
</div>
</div>
  </div>


    <div class="col-md-12">
        <div class="form-group ">
            <label class="control-label"><input <?php echo ( isset($filters['show_items']) && ($filters['show_items'] == 1) ) ? "CHECKED" : ""; ?> type="checkbox" value="1" name="filter[show_items]"> Show Items on Print</label>
        </div>
    </div>

  </div>

<?php if( isset($output) && ($output!='ajax') ) : ?>
	    	</div>
	    	<div class="panel-footer text-right">
	    		<button type="submit" class="btn btn-success">Search</button>
	    	</div>
	    </div>

</form>

    </div>
</div>
</div>

<?php $this->load->view('footer'); ?>

<?php endif; ?>