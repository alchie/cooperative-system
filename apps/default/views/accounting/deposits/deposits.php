<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php $this->load->view('header'); ?>
<?php if( ! $inner_page ): ?>
<?php $this->load->view('accounting/accounting_navbar'); ?>

<div class="container">
<div class="row">
	<div class="col-md-12">
	    <div class="panel panel-default">
	    	<div class="panel-heading">

<?php if( hasAccess('accounting', 'deposits', 'add') ) { ?>
<div class="btn-group pull-right">
  <button type="button" class="btn btn-success btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Record Deposit <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
  <?php if( $bank_accounts ) { ?>
    <?php foreach($bank_accounts as $acct) { ?>
      <li>
      <a class="body_wrapper" href="<?php echo site_url('accounting_deposits/select_receipts/' . $acct->id ); ?>"><?php echo $acct->title; ?></a>
      </li>
    <?php } ?>
  <?php } ?>
  </ul>
</div>
<?php } ?>
	    		<h3 class="panel-title">

<?php echo (isset($filters['title'])) ? $filters['title'] : "Deposits"; ?>

<a href="javascript:void(0);" class="ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Filter Deposits" data-url="<?php echo site_url("accounting_deposits/filters/ajax") . "?next=" . urlencode(uri_string()) . "&" . $filters_uri; ?>"><span class="glyphicon glyphicon-search"></span></a>

<a target="_print" href="<?php echo site_url("accounting_deposits/pr1nt"). "?" . $filters_uri; ?>"><span class="glyphicon glyphicon-print"></span></a>

<?php if ($this->input->get('filter')) { 
$save_url = "accounting_deposits/save";
if( isset($saved_filters) ) {
	$save_url = "accounting_deposits/save/" . $saved_filters->id ;
}
	?>

<a href="<?php echo site_url($save_url). "?" . $filters_uri; ?>"><span class="glyphicon glyphicon-floppy-disk"></span></a>

<?php if( isset($saved_filters) ) { ?>
<a href="<?php echo site_url("accounting_deposits/delete/{$saved_filters->id}") ?>" class="confirm"><span class="glyphicon glyphicon-trash"></span></a>
<?php } ?>

<?php } ?>

</h3>



	    	</div>
	    	<div class="panel-body" id="ajaxBodyInnerPage" data-url="<?php echo site_url(uri_string()) . "?" . $filters_uri; ?>">
<?php endif; ?>

<?php if( isset($deposits) ) {  ?>
<?php if( $deposits ) {  ?>

<table class="table table-default table-hover table-condensed">
	    			<thead>
	    				<tr>
	    					<th width="10px"></th>
	    					<th>Date</th>
	    					<th>Bank</th>
	    					<th>Memo</th>
	    					<th class="text-right">Amount</th>
	    					<?php if( hasAccess('accounting', 'deposits', 'edit') ) { ?>
	    						<th class="text-right" width="100px">Actions</th>
	    					<?php } ?>
	    				</tr>
	    			</thead>
	    			<tbody>
<?php foreach($deposits as $deposit) { ?>
	<tr class="<?php echo (ROUND($deposit->amount)==ROUND($deposit->total_entries)) ? '' : 'danger'; ?>">
		<td>
<?php if( $deposit->recon ) { ?>
	<a href="<?php echo site_url("accounting_reconciliation/recon_items/{$deposit->recon_id}"); ?>?highlight=<?php echo $deposit->trn_id; ?>" class="item body_wrapper">
		<span class="glyphicon glyphicon-check"></span>
	</a>
<?php } ?>
		</td>
		<td><?php echo date('m/d/Y', strtotime($deposit->date_deposited)); ?></td>
		<td><?php echo $deposit->bank_name; ?></td>
		<td><?php echo $deposit->memo; ?></td>
		<td class="text-right"><?php echo number_format($deposit->amount,2); ?></td>
		<?php if( hasAccess('accounting', 'deposits', 'edit') ) { ?>
		 <td class="text-right">
    <button type="button" class="btn btn-warning btn-xs ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Edit Deposit" data-url="<?php echo site_url("accounting_deposits/edit_deposit/{$deposit->id}/ajax") . "?next=" . uri_string(); ?>">Edit</button>
    <a href="<?php echo site_url("accounting_deposits/deposit_items/" . $deposit->id); ?>" class="btn btn-primary btn-xs body_wrapper">Items</a>
    </td>
<?php } ?>
	</tr>
<?php } ?>
	    			</tbody>
</table>

<?php echo ($pagination!='') ? '<center>' . $pagination . '</center>' : ''; ?>

<?php } else { ?>
	<div class="text-center">No Deposit Found!</div>
<?php } ?>
<?php } else { ?>
	Please wait...
<?php } ?>

<?php if( ! $inner_page ): ?>

	    	</div>
	    </div>
    </div>
</div>
</div>
<?php endif; ?>
<?php $this->load->view('footer'); ?>