<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Accounting_receipts_model Class
 *
 * Manipulates `accounting_receipts` table on database

CREATE TABLE `accounting_receipts` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name_id` int(20) NOT NULL,
  `receipt_number` int(20) NOT NULL,
  `receipt_date` date NOT NULL,
  `amount` decimal(20,5) NOT NULL,
  `memo` text,
  `trn_id` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `receipt_number` (`receipt_number`)
);

 ALTER TABLE  `accounting_receipts` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
 ALTER TABLE  `accounting_receipts` ADD  `name_id` int(20) NOT NULL   ;
 ALTER TABLE  `accounting_receipts` ADD  `receipt_number` int(20) NOT NULL   UNIQUE KEY;
 ALTER TABLE  `accounting_receipts` ADD  `receipt_date` date NOT NULL   ;
 ALTER TABLE  `accounting_receipts` ADD  `amount` decimal(20,5) NOT NULL   ;
 ALTER TABLE  `accounting_receipts` ADD  `memo` text NULL   ;
 ALTER TABLE  `accounting_receipts` ADD  `trn_id` int(20) NULL   ;


 * @package			        Model
 * @version_number	        4.0.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG)
 */
 
class Accounting_receipts_model extends MY_Model {

	protected $id;
	protected $name_id;
	protected $receipt_number;
	protected $receipt_date;
	protected $amount;
	protected $memo;
	protected $trn_id;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'accounting_receipts';
		$this->_short_name = 'accounting_receipts';
		$this->_fields = array("id","name_id","receipt_number","receipt_date","amount","memo","trn_id");
		$this->_required = array("name_id","receipt_number","receipt_date","amount");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

		public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

		public function getId() {
			return $this->id;
		}
	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: name_id -------------------------------------- 

	/** 
	* Sets a value to `name_id` variable
	* @access public
	*/

		public function setNameId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('name_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `name_id` variable
	* @access public
	*/

		public function getNameId() {
			return $this->name_id;
		}
	
// ------------------------------ End Field: name_id --------------------------------------


// ---------------------------- Start Field: receipt_number -------------------------------------- 

	/** 
	* Sets a value to `receipt_number` variable
	* @access public
	*/

		public function setReceiptNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('receipt_number', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `receipt_number` variable
	* @access public
	*/

		public function getReceiptNumber() {
			return $this->receipt_number;
		}
	
// ------------------------------ End Field: receipt_number --------------------------------------


// ---------------------------- Start Field: receipt_date -------------------------------------- 

	/** 
	* Sets a value to `receipt_date` variable
	* @access public
	*/

		public function setReceiptDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('receipt_date', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `receipt_date` variable
	* @access public
	*/

		public function getReceiptDate() {
			return $this->receipt_date;
		}
	
// ------------------------------ End Field: receipt_date --------------------------------------


// ---------------------------- Start Field: amount -------------------------------------- 

	/** 
	* Sets a value to `amount` variable
	* @access public
	*/

		public function setAmount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('amount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `amount` variable
	* @access public
	*/

		public function getAmount() {
			return $this->amount;
		}
	
// ------------------------------ End Field: amount --------------------------------------


// ---------------------------- Start Field: memo -------------------------------------- 

	/** 
	* Sets a value to `memo` variable
	* @access public
	*/

		public function setMemo($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('memo', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `memo` variable
	* @access public
	*/

		public function getMemo() {
			return $this->memo;
		}
	
// ------------------------------ End Field: memo --------------------------------------


// ---------------------------- Start Field: trn_id -------------------------------------- 

	/** 
	* Sets a value to `trn_id` variable
	* @access public
	*/

		public function setTrnId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('trn_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `trn_id` variable
	* @access public
	*/

		public function getTrnId() {
			return $this->trn_id;
		}
	
// ------------------------------ End Field: trn_id --------------------------------------




}

/* End of file Accounting_receipts_model.php */
/* Location: ./application/models/Accounting_receipts_model.php */
