<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Shares_dividend_item_model Class
 *
 * Manipulates `shares_dividend_item` table on database

CREATE TABLE `shares_dividend_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dividend_id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `amount` decimal(20,5) NOT NULL,
  PRIMARY KEY (`id`)
);

 ALTER TABLE  `shares_dividend_item` ADD  `id` int(11) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
 ALTER TABLE  `shares_dividend_item` ADD  `dividend_id` int(11) NOT NULL   ;
 ALTER TABLE  `shares_dividend_item` ADD  `member_id` int(11) NOT NULL   ;
 ALTER TABLE  `shares_dividend_item` ADD  `amount` decimal(20,5) NOT NULL   ;


 * @package			        Model
 * @version_number	        4.0.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG)
 */
 
class Shares_dividend_item_model extends MY_Model {

	protected $id;
	protected $dividend_id;
	protected $member_id;
	protected $amount;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'shares_dividend_item';
		$this->_short_name = 'shares_dividend_item';
		$this->_fields = array("id","dividend_id","member_id","amount");
		$this->_required = array("dividend_id","member_id","amount");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

		public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

		public function getId() {
			return $this->id;
		}
	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: dividend_id -------------------------------------- 

	/** 
	* Sets a value to `dividend_id` variable
	* @access public
	*/

		public function setDividendId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('dividend_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `dividend_id` variable
	* @access public
	*/

		public function getDividendId() {
			return $this->dividend_id;
		}
	
// ------------------------------ End Field: dividend_id --------------------------------------


// ---------------------------- Start Field: member_id -------------------------------------- 

	/** 
	* Sets a value to `member_id` variable
	* @access public
	*/

		public function setMemberId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('member_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `member_id` variable
	* @access public
	*/

		public function getMemberId() {
			return $this->member_id;
		}
	
// ------------------------------ End Field: member_id --------------------------------------


// ---------------------------- Start Field: amount -------------------------------------- 

	/** 
	* Sets a value to `amount` variable
	* @access public
	*/

		public function setAmount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('amount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `amount` variable
	* @access public
	*/

		public function getAmount() {
			return $this->amount;
		}
	
// ------------------------------ End Field: amount --------------------------------------




}

/* End of file Shares_dividend_item_model.php */
/* Location: ./application/models/Shares_dividend_item_model.php */
