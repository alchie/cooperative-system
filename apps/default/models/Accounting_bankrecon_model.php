<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Accounting_bankrecon_model Class
 *
 * Manipulates `accounting_bankrecon` table on database

CREATE TABLE `accounting_bankrecon` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `chart_id` int(20) NOT NULL,
  `date_end` date NOT NULL,
  `balance_end` decimal(20,5) DEFAULT NULL,
  `memo` text,
  PRIMARY KEY (`id`)
);

 ALTER TABLE  `accounting_bankrecon` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
 ALTER TABLE  `accounting_bankrecon` ADD  `chart_id` int(20) NOT NULL   ;
 ALTER TABLE  `accounting_bankrecon` ADD  `date_end` date NOT NULL   ;
 ALTER TABLE  `accounting_bankrecon` ADD  `balance_end` decimal(20,5) NULL   ;
 ALTER TABLE  `accounting_bankrecon` ADD  `memo` text NULL   ;


 * @package			        Model
 * @version_number	        4.0.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG)
 */
 
class Accounting_bankrecon_model extends MY_Model {

	protected $id;
	protected $chart_id;
	protected $date_end;
	protected $balance_end;
	protected $memo;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'accounting_bankrecon';
		$this->_short_name = 'accounting_bankrecon';
		$this->_fields = array("id","chart_id","date_end","balance_end","memo");
		$this->_required = array("chart_id","date_end");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

		public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

		public function getId() {
			return $this->id;
		}
	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: chart_id -------------------------------------- 

	/** 
	* Sets a value to `chart_id` variable
	* @access public
	*/

		public function setChartId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('chart_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `chart_id` variable
	* @access public
	*/

		public function getChartId() {
			return $this->chart_id;
		}
	
// ------------------------------ End Field: chart_id --------------------------------------


// ---------------------------- Start Field: date_end -------------------------------------- 

	/** 
	* Sets a value to `date_end` variable
	* @access public
	*/

		public function setDateEnd($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('date_end', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `date_end` variable
	* @access public
	*/

		public function getDateEnd() {
			return $this->date_end;
		}
	
// ------------------------------ End Field: date_end --------------------------------------


// ---------------------------- Start Field: balance_end -------------------------------------- 

	/** 
	* Sets a value to `balance_end` variable
	* @access public
	*/

		public function setBalanceEnd($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('balance_end', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `balance_end` variable
	* @access public
	*/

		public function getBalanceEnd() {
			return $this->balance_end;
		}
	
// ------------------------------ End Field: balance_end --------------------------------------


// ---------------------------- Start Field: memo -------------------------------------- 

	/** 
	* Sets a value to `memo` variable
	* @access public
	*/

		public function setMemo($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('memo', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `memo` variable
	* @access public
	*/

		public function getMemo() {
			return $this->memo;
		}
	
// ------------------------------ End Field: memo --------------------------------------




}

/* End of file Accounting_bankrecon_model.php */
/* Location: ./application/models/Accounting_bankrecon_model.php */
