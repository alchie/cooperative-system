<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Accounting_receipts_items_model Class
 *
 * Manipulates `accounting_receipts_items` table on database

CREATE TABLE `accounting_receipts_items` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `receipt_id` int(20) NOT NULL,
  `item_type` varchar(50) DEFAULT NULL,
  `item_id` int(20) NOT NULL,
  `name_id` int(20) DEFAULT NULL,
  `class_id` int(20) DEFAULT NULL,
  `amount` decimal(20,5) NOT NULL,
  `memo` text,
  `entry_id` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `entry_id` (`entry_id`)
);

 ALTER TABLE  `accounting_receipts_items` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
 ALTER TABLE  `accounting_receipts_items` ADD  `receipt_id` int(20) NOT NULL   ;
 ALTER TABLE  `accounting_receipts_items` ADD  `item_type` varchar(50) NULL   ;
 ALTER TABLE  `accounting_receipts_items` ADD  `item_id` int(20) NOT NULL   ;
 ALTER TABLE  `accounting_receipts_items` ADD  `name_id` int(20) NULL   ;
 ALTER TABLE  `accounting_receipts_items` ADD  `class_id` int(20) NULL   ;
 ALTER TABLE  `accounting_receipts_items` ADD  `amount` decimal(20,5) NOT NULL   ;
 ALTER TABLE  `accounting_receipts_items` ADD  `memo` text NULL   ;
 ALTER TABLE  `accounting_receipts_items` ADD  `entry_id` int(20) NULL   UNIQUE KEY;


 * @package			        Model
 * @version_number	        4.0.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG)
 */
 
class Accounting_receipts_items_model extends MY_Model {

	protected $id;
	protected $receipt_id;
	protected $item_type;
	protected $item_id;
	protected $name_id;
	protected $class_id;
	protected $amount;
	protected $memo;
	protected $entry_id;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'accounting_receipts_items';
		$this->_short_name = 'accounting_receipts_items';
		$this->_fields = array("id","receipt_id","item_type","item_id","name_id","class_id","amount","memo","entry_id");
		$this->_required = array("receipt_id","item_id","amount");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

		public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

		public function getId() {
			return $this->id;
		}
	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: receipt_id -------------------------------------- 

	/** 
	* Sets a value to `receipt_id` variable
	* @access public
	*/

		public function setReceiptId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('receipt_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `receipt_id` variable
	* @access public
	*/

		public function getReceiptId() {
			return $this->receipt_id;
		}
	
// ------------------------------ End Field: receipt_id --------------------------------------


// ---------------------------- Start Field: item_type -------------------------------------- 

	/** 
	* Sets a value to `item_type` variable
	* @access public
	*/

		public function setItemType($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('item_type', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `item_type` variable
	* @access public
	*/

		public function getItemType() {
			return $this->item_type;
		}
	
// ------------------------------ End Field: item_type --------------------------------------


// ---------------------------- Start Field: item_id -------------------------------------- 

	/** 
	* Sets a value to `item_id` variable
	* @access public
	*/

		public function setItemId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('item_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `item_id` variable
	* @access public
	*/

		public function getItemId() {
			return $this->item_id;
		}
	
// ------------------------------ End Field: item_id --------------------------------------


// ---------------------------- Start Field: name_id -------------------------------------- 

	/** 
	* Sets a value to `name_id` variable
	* @access public
	*/

		public function setNameId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('name_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `name_id` variable
	* @access public
	*/

		public function getNameId() {
			return $this->name_id;
		}
	
// ------------------------------ End Field: name_id --------------------------------------


// ---------------------------- Start Field: class_id -------------------------------------- 

	/** 
	* Sets a value to `class_id` variable
	* @access public
	*/

		public function setClassId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('class_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `class_id` variable
	* @access public
	*/

		public function getClassId() {
			return $this->class_id;
		}
	
// ------------------------------ End Field: class_id --------------------------------------


// ---------------------------- Start Field: amount -------------------------------------- 

	/** 
	* Sets a value to `amount` variable
	* @access public
	*/

		public function setAmount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('amount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `amount` variable
	* @access public
	*/

		public function getAmount() {
			return $this->amount;
		}
	
// ------------------------------ End Field: amount --------------------------------------


// ---------------------------- Start Field: memo -------------------------------------- 

	/** 
	* Sets a value to `memo` variable
	* @access public
	*/

		public function setMemo($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('memo', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `memo` variable
	* @access public
	*/

		public function getMemo() {
			return $this->memo;
		}
	
// ------------------------------ End Field: memo --------------------------------------


// ---------------------------- Start Field: entry_id -------------------------------------- 

	/** 
	* Sets a value to `entry_id` variable
	* @access public
	*/

		public function setEntryId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('entry_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `entry_id` variable
	* @access public
	*/

		public function getEntryId() {
			return $this->entry_id;
		}
	
// ------------------------------ End Field: entry_id --------------------------------------




}

/* End of file Accounting_receipts_items_model.php */
/* Location: ./application/models/Accounting_receipts_items_model.php */
