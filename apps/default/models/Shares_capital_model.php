<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Shares_capital_model Class
 *
 * Manipulates `shares_capital` table on database

CREATE TABLE `shares_capital` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `entry_id` int(20) DEFAULT NULL,
  `member_id` int(20) NOT NULL,
  `receipt_number` int(20) DEFAULT NULL,
  `payment_date` date NOT NULL,
  `amount` decimal(20,5) NOT NULL,
  `trn_id` int(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `receipt_number` (`receipt_number`),
  UNIQUE KEY `entry_id` (`entry_id`)
);

 ALTER TABLE  `shares_capital` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
 ALTER TABLE  `shares_capital` ADD  `entry_id` int(20) NULL   UNIQUE KEY;
 ALTER TABLE  `shares_capital` ADD  `member_id` int(20) NOT NULL   ;
 ALTER TABLE  `shares_capital` ADD  `receipt_number` int(20) NULL   UNIQUE KEY;
 ALTER TABLE  `shares_capital` ADD  `payment_date` date NOT NULL   ;
 ALTER TABLE  `shares_capital` ADD  `amount` decimal(20,5) NOT NULL   ;
 ALTER TABLE  `shares_capital` ADD  `trn_id` int(20) NOT NULL   ;


 * @package			        Model
 * @version_number	        4.0.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG)
 */
 
class Shares_capital_model extends MY_Model {

	protected $id;
	protected $entry_id;
	protected $member_id;
	protected $receipt_number;
	protected $payment_date;
	protected $amount;
	protected $trn_id;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'shares_capital';
		$this->_short_name = 'shares_capital';
		$this->_fields = array("id","entry_id","member_id","receipt_number","payment_date","amount","trn_id");
		$this->_required = array("member_id","payment_date","amount","trn_id");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

		public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

		public function getId() {
			return $this->id;
		}
	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: entry_id -------------------------------------- 

	/** 
	* Sets a value to `entry_id` variable
	* @access public
	*/

		public function setEntryId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('entry_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `entry_id` variable
	* @access public
	*/

		public function getEntryId() {
			return $this->entry_id;
		}
	
// ------------------------------ End Field: entry_id --------------------------------------


// ---------------------------- Start Field: member_id -------------------------------------- 

	/** 
	* Sets a value to `member_id` variable
	* @access public
	*/

		public function setMemberId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('member_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `member_id` variable
	* @access public
	*/

		public function getMemberId() {
			return $this->member_id;
		}
	
// ------------------------------ End Field: member_id --------------------------------------


// ---------------------------- Start Field: receipt_number -------------------------------------- 

	/** 
	* Sets a value to `receipt_number` variable
	* @access public
	*/

		public function setReceiptNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('receipt_number', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `receipt_number` variable
	* @access public
	*/

		public function getReceiptNumber() {
			return $this->receipt_number;
		}
	
// ------------------------------ End Field: receipt_number --------------------------------------


// ---------------------------- Start Field: payment_date -------------------------------------- 

	/** 
	* Sets a value to `payment_date` variable
	* @access public
	*/

		public function setPaymentDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('payment_date', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `payment_date` variable
	* @access public
	*/

		public function getPaymentDate() {
			return $this->payment_date;
		}
	
// ------------------------------ End Field: payment_date --------------------------------------


// ---------------------------- Start Field: amount -------------------------------------- 

	/** 
	* Sets a value to `amount` variable
	* @access public
	*/

		public function setAmount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('amount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `amount` variable
	* @access public
	*/

		public function getAmount() {
			return $this->amount;
		}
	
// ------------------------------ End Field: amount --------------------------------------


// ---------------------------- Start Field: trn_id -------------------------------------- 

	/** 
	* Sets a value to `trn_id` variable
	* @access public
	*/

		public function setTrnId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('trn_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `trn_id` variable
	* @access public
	*/

		public function getTrnId() {
			return $this->trn_id;
		}
	
// ------------------------------ End Field: trn_id --------------------------------------




}

/* End of file Shares_capital_model.php */
/* Location: ./application/models/Shares_capital_model.php */
