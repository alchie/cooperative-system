<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Members_model Class
 *
 * Manipulates `members` table on database

CREATE TABLE `members` (
  `id` int(20) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `middlename` varchar(100) DEFAULT NULL,
  `active` int(20) NOT NULL DEFAULT '1',
  `gender` varchar(10) DEFAULT NULL,
  `marital_status` varchar(20) DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `birthplace` varchar(50) DEFAULT NULL,
  `lastmod_by` int(20) DEFAULT NULL,
  `added_by` int(20) DEFAULT NULL,
  `phone_mobile` varchar(100) DEFAULT NULL,
  `phone_home` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  KEY `id` (`id`),
  CONSTRAINT `coop_names_member_id` FOREIGN KEY (`id`) REFERENCES `coop_names` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
);

 ALTER TABLE  `members` ADD  `id` int(20) NOT NULL   ;
 ALTER TABLE  `members` ADD  `firstname` varchar(100) NOT NULL   ;
 ALTER TABLE  `members` ADD  `lastname` varchar(100) NOT NULL   ;
 ALTER TABLE  `members` ADD  `middlename` varchar(100) NULL   ;
 ALTER TABLE  `members` ADD  `active` int(20) NOT NULL   DEFAULT '1';
 ALTER TABLE  `members` ADD  `gender` varchar(10) NULL   ;
 ALTER TABLE  `members` ADD  `marital_status` varchar(20) NULL   ;
 ALTER TABLE  `members` ADD  `birthdate` date NULL   ;
 ALTER TABLE  `members` ADD  `birthplace` varchar(50) NULL   ;
 ALTER TABLE  `members` ADD  `lastmod_by` int(20) NULL   ;
 ALTER TABLE  `members` ADD  `added_by` int(20) NULL   ;
 ALTER TABLE  `members` ADD  `phone_mobile` varchar(100) NULL   ;
 ALTER TABLE  `members` ADD  `phone_home` varchar(100) NULL   ;
 ALTER TABLE  `members` ADD  `email` varchar(100) NULL   ;


 * @package			        Model
 * @version_number	        4.0.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG)
 */
 
class Members_model extends MY_Model {

	protected $id;
	protected $firstname;
	protected $lastname;
	protected $middlename;
	protected $active;
	protected $gender;
	protected $marital_status;
	protected $birthdate;
	protected $birthplace;
	protected $lastmod_by;
	protected $added_by;
	protected $phone_mobile;
	protected $phone_home;
	protected $email;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'members';
		$this->_short_name = 'members';
		$this->_fields = array("id","firstname","lastname","middlename","active","gender","marital_status","birthdate","birthplace","lastmod_by","added_by","phone_mobile","phone_home","email");
		$this->_required = array("id","firstname","lastname","active");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

		public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

		public function getId() {
			return $this->id;
		}
	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: firstname -------------------------------------- 

	/** 
	* Sets a value to `firstname` variable
	* @access public
	*/

		public function setFirstname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('firstname', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `firstname` variable
	* @access public
	*/

		public function getFirstname() {
			return $this->firstname;
		}
	
// ------------------------------ End Field: firstname --------------------------------------


// ---------------------------- Start Field: lastname -------------------------------------- 

	/** 
	* Sets a value to `lastname` variable
	* @access public
	*/

		public function setLastname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('lastname', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `lastname` variable
	* @access public
	*/

		public function getLastname() {
			return $this->lastname;
		}
	
// ------------------------------ End Field: lastname --------------------------------------


// ---------------------------- Start Field: middlename -------------------------------------- 

	/** 
	* Sets a value to `middlename` variable
	* @access public
	*/

		public function setMiddlename($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('middlename', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `middlename` variable
	* @access public
	*/

		public function getMiddlename() {
			return $this->middlename;
		}
	
// ------------------------------ End Field: middlename --------------------------------------


// ---------------------------- Start Field: active -------------------------------------- 

	/** 
	* Sets a value to `active` variable
	* @access public
	*/

		public function setActive($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('active', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `active` variable
	* @access public
	*/

		public function getActive() {
			return $this->active;
		}
	
// ------------------------------ End Field: active --------------------------------------


// ---------------------------- Start Field: gender -------------------------------------- 

	/** 
	* Sets a value to `gender` variable
	* @access public
	*/

		public function setGender($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('gender', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `gender` variable
	* @access public
	*/

		public function getGender() {
			return $this->gender;
		}
	
// ------------------------------ End Field: gender --------------------------------------


// ---------------------------- Start Field: marital_status -------------------------------------- 

	/** 
	* Sets a value to `marital_status` variable
	* @access public
	*/

		public function setMaritalStatus($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('marital_status', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `marital_status` variable
	* @access public
	*/

		public function getMaritalStatus() {
			return $this->marital_status;
		}
	
// ------------------------------ End Field: marital_status --------------------------------------


// ---------------------------- Start Field: birthdate -------------------------------------- 

	/** 
	* Sets a value to `birthdate` variable
	* @access public
	*/

		public function setBirthdate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('birthdate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `birthdate` variable
	* @access public
	*/

		public function getBirthdate() {
			return $this->birthdate;
		}
	
// ------------------------------ End Field: birthdate --------------------------------------


// ---------------------------- Start Field: birthplace -------------------------------------- 

	/** 
	* Sets a value to `birthplace` variable
	* @access public
	*/

		public function setBirthplace($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('birthplace', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `birthplace` variable
	* @access public
	*/

		public function getBirthplace() {
			return $this->birthplace;
		}
	
// ------------------------------ End Field: birthplace --------------------------------------


// ---------------------------- Start Field: lastmod_by -------------------------------------- 

	/** 
	* Sets a value to `lastmod_by` variable
	* @access public
	*/

		public function setLastmodBy($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('lastmod_by', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `lastmod_by` variable
	* @access public
	*/

		public function getLastmodBy() {
			return $this->lastmod_by;
		}
	
// ------------------------------ End Field: lastmod_by --------------------------------------


// ---------------------------- Start Field: added_by -------------------------------------- 

	/** 
	* Sets a value to `added_by` variable
	* @access public
	*/

		public function setAddedBy($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('added_by', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `added_by` variable
	* @access public
	*/

		public function getAddedBy() {
			return $this->added_by;
		}
	
// ------------------------------ End Field: added_by --------------------------------------


// ---------------------------- Start Field: phone_mobile -------------------------------------- 

	/** 
	* Sets a value to `phone_mobile` variable
	* @access public
	*/

		public function setPhoneMobile($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('phone_mobile', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `phone_mobile` variable
	* @access public
	*/

		public function getPhoneMobile() {
			return $this->phone_mobile;
		}
	
// ------------------------------ End Field: phone_mobile --------------------------------------


// ---------------------------- Start Field: phone_home -------------------------------------- 

	/** 
	* Sets a value to `phone_home` variable
	* @access public
	*/

		public function setPhoneHome($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('phone_home', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `phone_home` variable
	* @access public
	*/

		public function getPhoneHome() {
			return $this->phone_home;
		}
	
// ------------------------------ End Field: phone_home --------------------------------------


// ---------------------------- Start Field: email -------------------------------------- 

	/** 
	* Sets a value to `email` variable
	* @access public
	*/

		public function setEmail($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('email', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `email` variable
	* @access public
	*/

		public function getEmail() {
			return $this->email;
		}
	
// ------------------------------ End Field: email --------------------------------------




}

/* End of file Members_model.php */
/* Location: ./application/models/Members_model.php */
