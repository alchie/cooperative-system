<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Lending_loans_model Class
 *
 * Manipulates `lending_loans` table on database

CREATE TABLE `lending_loans` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `entry_id` int(20) DEFAULT NULL,
  `member_id` int(20) NOT NULL,
  `check_number` varchar(10) NOT NULL,
  `loan_date` date NOT NULL,
  `principal` decimal(20,5) NOT NULL,
  `payment_start` date NOT NULL,
  `interest_rate` float(10,5) NOT NULL DEFAULT '3.00000',
  `months` int(2) NOT NULL DEFAULT '1',
  `skip_days` int(2) NOT NULL DEFAULT '15',
  `interest_type` varchar(50) NOT NULL DEFAULT 'fixed',
  `deleted` int(1) DEFAULT '0',
  `trn_id` int(20) DEFAULT NULL,
  `bank_id` int(20) DEFAULT NULL,
  `memo` text,
  `inactive` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
);

 ALTER TABLE  `lending_loans` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
 ALTER TABLE  `lending_loans` ADD  `entry_id` int(20) NULL   ;
 ALTER TABLE  `lending_loans` ADD  `member_id` int(20) NOT NULL   ;
 ALTER TABLE  `lending_loans` ADD  `check_number` varchar(10) NOT NULL   ;
 ALTER TABLE  `lending_loans` ADD  `loan_date` date NOT NULL   ;
 ALTER TABLE  `lending_loans` ADD  `principal` decimal(20,5) NOT NULL   ;
 ALTER TABLE  `lending_loans` ADD  `payment_start` date NOT NULL   ;
 ALTER TABLE  `lending_loans` ADD  `interest_rate` float(10,5) NOT NULL   DEFAULT '3.00000';
 ALTER TABLE  `lending_loans` ADD  `months` int(2) NOT NULL   DEFAULT '1';
 ALTER TABLE  `lending_loans` ADD  `skip_days` int(2) NOT NULL   DEFAULT '15';
 ALTER TABLE  `lending_loans` ADD  `interest_type` varchar(50) NOT NULL   DEFAULT 'fixed';
 ALTER TABLE  `lending_loans` ADD  `deleted` int(1) NULL   DEFAULT '0';
 ALTER TABLE  `lending_loans` ADD  `trn_id` int(20) NULL   ;
 ALTER TABLE  `lending_loans` ADD  `bank_id` int(20) NULL   ;
 ALTER TABLE  `lending_loans` ADD  `memo` text NULL   ;
 ALTER TABLE  `lending_loans` ADD  `inactive` int(1) NULL   DEFAULT '0';


 * @package			        Model
 * @version_number	        4.0.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG)
 */
 
class Lending_loans_model extends MY_Model {

	protected $id;
	protected $entry_id;
	protected $member_id;
	protected $check_number;
	protected $loan_date;
	protected $principal;
	protected $payment_start;
	protected $interest_rate;
	protected $months;
	protected $skip_days;
	protected $interest_type;
	protected $deleted;
	protected $trn_id;
	protected $bank_id;
	protected $memo;
	protected $inactive;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'lending_loans';
		$this->_short_name = 'lending_loans';
		$this->_fields = array("id","entry_id","member_id","check_number","loan_date","principal","payment_start","interest_rate","months","skip_days","interest_type","deleted","trn_id","bank_id","memo","inactive");
		$this->_required = array("member_id","check_number","loan_date","principal","payment_start","interest_rate","months","skip_days","interest_type");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

		public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

		public function getId() {
			return $this->id;
		}
	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: entry_id -------------------------------------- 

	/** 
	* Sets a value to `entry_id` variable
	* @access public
	*/

		public function setEntryId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('entry_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `entry_id` variable
	* @access public
	*/

		public function getEntryId() {
			return $this->entry_id;
		}
	
// ------------------------------ End Field: entry_id --------------------------------------


// ---------------------------- Start Field: member_id -------------------------------------- 

	/** 
	* Sets a value to `member_id` variable
	* @access public
	*/

		public function setMemberId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('member_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `member_id` variable
	* @access public
	*/

		public function getMemberId() {
			return $this->member_id;
		}
	
// ------------------------------ End Field: member_id --------------------------------------


// ---------------------------- Start Field: check_number -------------------------------------- 

	/** 
	* Sets a value to `check_number` variable
	* @access public
	*/

		public function setCheckNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('check_number', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `check_number` variable
	* @access public
	*/

		public function getCheckNumber() {
			return $this->check_number;
		}
	
// ------------------------------ End Field: check_number --------------------------------------


// ---------------------------- Start Field: loan_date -------------------------------------- 

	/** 
	* Sets a value to `loan_date` variable
	* @access public
	*/

		public function setLoanDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('loan_date', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `loan_date` variable
	* @access public
	*/

		public function getLoanDate() {
			return $this->loan_date;
		}
	
// ------------------------------ End Field: loan_date --------------------------------------


// ---------------------------- Start Field: principal -------------------------------------- 

	/** 
	* Sets a value to `principal` variable
	* @access public
	*/

		public function setPrincipal($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('principal', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `principal` variable
	* @access public
	*/

		public function getPrincipal() {
			return $this->principal;
		}
	
// ------------------------------ End Field: principal --------------------------------------


// ---------------------------- Start Field: payment_start -------------------------------------- 

	/** 
	* Sets a value to `payment_start` variable
	* @access public
	*/

		public function setPaymentStart($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('payment_start', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `payment_start` variable
	* @access public
	*/

		public function getPaymentStart() {
			return $this->payment_start;
		}
	
// ------------------------------ End Field: payment_start --------------------------------------


// ---------------------------- Start Field: interest_rate -------------------------------------- 

	/** 
	* Sets a value to `interest_rate` variable
	* @access public
	*/

		public function setInterestRate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('interest_rate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `interest_rate` variable
	* @access public
	*/

		public function getInterestRate() {
			return $this->interest_rate;
		}
	
// ------------------------------ End Field: interest_rate --------------------------------------


// ---------------------------- Start Field: months -------------------------------------- 

	/** 
	* Sets a value to `months` variable
	* @access public
	*/

		public function setMonths($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('months', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `months` variable
	* @access public
	*/

		public function getMonths() {
			return $this->months;
		}
	
// ------------------------------ End Field: months --------------------------------------


// ---------------------------- Start Field: skip_days -------------------------------------- 

	/** 
	* Sets a value to `skip_days` variable
	* @access public
	*/

		public function setSkipDays($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('skip_days', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `skip_days` variable
	* @access public
	*/

		public function getSkipDays() {
			return $this->skip_days;
		}
	
// ------------------------------ End Field: skip_days --------------------------------------


// ---------------------------- Start Field: interest_type -------------------------------------- 

	/** 
	* Sets a value to `interest_type` variable
	* @access public
	*/

		public function setInterestType($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('interest_type', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `interest_type` variable
	* @access public
	*/

		public function getInterestType() {
			return $this->interest_type;
		}
	
// ------------------------------ End Field: interest_type --------------------------------------


// ---------------------------- Start Field: deleted -------------------------------------- 

	/** 
	* Sets a value to `deleted` variable
	* @access public
	*/

		public function setDeleted($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('deleted', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `deleted` variable
	* @access public
	*/

		public function getDeleted() {
			return $this->deleted;
		}
	
// ------------------------------ End Field: deleted --------------------------------------


// ---------------------------- Start Field: trn_id -------------------------------------- 

	/** 
	* Sets a value to `trn_id` variable
	* @access public
	*/

		public function setTrnId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('trn_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `trn_id` variable
	* @access public
	*/

		public function getTrnId() {
			return $this->trn_id;
		}
	
// ------------------------------ End Field: trn_id --------------------------------------


// ---------------------------- Start Field: bank_id -------------------------------------- 

	/** 
	* Sets a value to `bank_id` variable
	* @access public
	*/

		public function setBankId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('bank_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `bank_id` variable
	* @access public
	*/

		public function getBankId() {
			return $this->bank_id;
		}
	
// ------------------------------ End Field: bank_id --------------------------------------


// ---------------------------- Start Field: memo -------------------------------------- 

	/** 
	* Sets a value to `memo` variable
	* @access public
	*/

		public function setMemo($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('memo', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `memo` variable
	* @access public
	*/

		public function getMemo() {
			return $this->memo;
		}
	
// ------------------------------ End Field: memo --------------------------------------


// ---------------------------- Start Field: inactive -------------------------------------- 

	/** 
	* Sets a value to `inactive` variable
	* @access public
	*/

		public function setInactive($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('inactive', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `inactive` variable
	* @access public
	*/

		public function getInactive() {
			return $this->inactive;
		}
	
// ------------------------------ End Field: inactive --------------------------------------




}

/* End of file Lending_loans_model.php */
/* Location: ./application/models/Lending_loans_model.php */
