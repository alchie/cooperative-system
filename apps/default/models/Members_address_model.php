<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Members_address_model Class
 *
 * Manipulates `members_address` table on database

CREATE TABLE `members_address` (
  `mid` int(20) NOT NULL,
  `unit` varchar(100) DEFAULT NULL,
  `building` varchar(100) DEFAULT NULL,
  `lot_block` varchar(100) DEFAULT NULL,
  `street` varchar(100) DEFAULT NULL,
  `subdivision` varchar(100) DEFAULT NULL,
  `barangay` varchar(100) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `province` varchar(100) DEFAULT NULL,
  `zip` varchar(100) DEFAULT NULL,
  KEY `id` (`mid`),
  KEY `mid` (`mid`),
  CONSTRAINT `names_address_mid` FOREIGN KEY (`mid`) REFERENCES `coop_names` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
);

 ALTER TABLE  `members_address` ADD  `mid` int(20) NOT NULL   ;
 ALTER TABLE  `members_address` ADD  `unit` varchar(100) NULL   ;
 ALTER TABLE  `members_address` ADD  `building` varchar(100) NULL   ;
 ALTER TABLE  `members_address` ADD  `lot_block` varchar(100) NULL   ;
 ALTER TABLE  `members_address` ADD  `street` varchar(100) NULL   ;
 ALTER TABLE  `members_address` ADD  `subdivision` varchar(100) NULL   ;
 ALTER TABLE  `members_address` ADD  `barangay` varchar(100) NULL   ;
 ALTER TABLE  `members_address` ADD  `city` varchar(100) NULL   ;
 ALTER TABLE  `members_address` ADD  `province` varchar(100) NULL   ;
 ALTER TABLE  `members_address` ADD  `zip` varchar(100) NULL   ;


 * @package			        Model
 * @version_number	        4.0.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG)
 */
 
class Members_address_model extends MY_Model {

	protected $mid;
	protected $unit;
	protected $building;
	protected $lot_block;
	protected $street;
	protected $subdivision;
	protected $barangay;
	protected $city;
	protected $province;
	protected $zip;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'members_address';
		$this->_short_name = 'members_address';
		$this->_fields = array("mid","unit","building","lot_block","street","subdivision","barangay","city","province","zip");
		$this->_required = array("mid");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: mid -------------------------------------- 

	/** 
	* Sets a value to `mid` variable
	* @access public
	*/

		public function setMid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('mid', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `mid` variable
	* @access public
	*/

		public function getMid() {
			return $this->mid;
		}
	
// ------------------------------ End Field: mid --------------------------------------


// ---------------------------- Start Field: unit -------------------------------------- 

	/** 
	* Sets a value to `unit` variable
	* @access public
	*/

		public function setUnit($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('unit', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `unit` variable
	* @access public
	*/

		public function getUnit() {
			return $this->unit;
		}
	
// ------------------------------ End Field: unit --------------------------------------


// ---------------------------- Start Field: building -------------------------------------- 

	/** 
	* Sets a value to `building` variable
	* @access public
	*/

		public function setBuilding($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('building', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `building` variable
	* @access public
	*/

		public function getBuilding() {
			return $this->building;
		}
	
// ------------------------------ End Field: building --------------------------------------


// ---------------------------- Start Field: lot_block -------------------------------------- 

	/** 
	* Sets a value to `lot_block` variable
	* @access public
	*/

		public function setLotBlock($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('lot_block', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `lot_block` variable
	* @access public
	*/

		public function getLotBlock() {
			return $this->lot_block;
		}
	
// ------------------------------ End Field: lot_block --------------------------------------


// ---------------------------- Start Field: street -------------------------------------- 

	/** 
	* Sets a value to `street` variable
	* @access public
	*/

		public function setStreet($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('street', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `street` variable
	* @access public
	*/

		public function getStreet() {
			return $this->street;
		}
	
// ------------------------------ End Field: street --------------------------------------


// ---------------------------- Start Field: subdivision -------------------------------------- 

	/** 
	* Sets a value to `subdivision` variable
	* @access public
	*/

		public function setSubdivision($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('subdivision', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `subdivision` variable
	* @access public
	*/

		public function getSubdivision() {
			return $this->subdivision;
		}
	
// ------------------------------ End Field: subdivision --------------------------------------


// ---------------------------- Start Field: barangay -------------------------------------- 

	/** 
	* Sets a value to `barangay` variable
	* @access public
	*/

		public function setBarangay($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('barangay', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `barangay` variable
	* @access public
	*/

		public function getBarangay() {
			return $this->barangay;
		}
	
// ------------------------------ End Field: barangay --------------------------------------


// ---------------------------- Start Field: city -------------------------------------- 

	/** 
	* Sets a value to `city` variable
	* @access public
	*/

		public function setCity($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('city', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `city` variable
	* @access public
	*/

		public function getCity() {
			return $this->city;
		}
	
// ------------------------------ End Field: city --------------------------------------


// ---------------------------- Start Field: province -------------------------------------- 

	/** 
	* Sets a value to `province` variable
	* @access public
	*/

		public function setProvince($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('province', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `province` variable
	* @access public
	*/

		public function getProvince() {
			return $this->province;
		}
	
// ------------------------------ End Field: province --------------------------------------


// ---------------------------- Start Field: zip -------------------------------------- 

	/** 
	* Sets a value to `zip` variable
	* @access public
	*/

		public function setZip($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('zip', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `zip` variable
	* @access public
	*/

		public function getZip() {
			return $this->zip;
		}
	
// ------------------------------ End Field: zip --------------------------------------




}

/* End of file Members_address_model.php */
/* Location: ./application/models/Members_address_model.php */
