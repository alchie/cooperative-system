<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Accounting_entries_model Class
 *
 * Manipulates `accounting_entries` table on database

CREATE TABLE `accounting_entries` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `trn_id` int(20) NOT NULL,
  `chart_id` int(20) NOT NULL,
  `debit` decimal(20,5) DEFAULT '0.00000',
  `credit` decimal(20,5) DEFAULT '0.00000',
  `class` int(20) DEFAULT NULL,
  `name_id` int(20) DEFAULT NULL,
  `memo` varchar(200) DEFAULT NULL,
  `item_number` int(10) DEFAULT '0',
  `item_id` int(20) DEFAULT NULL,
  `item_type` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `name_id` (`name_id`)
);

 ALTER TABLE  `accounting_entries` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
 ALTER TABLE  `accounting_entries` ADD  `trn_id` int(20) NOT NULL   ;
 ALTER TABLE  `accounting_entries` ADD  `chart_id` int(20) NOT NULL   ;
 ALTER TABLE  `accounting_entries` ADD  `debit` decimal(20,5) NULL   DEFAULT '0.00000';
 ALTER TABLE  `accounting_entries` ADD  `credit` decimal(20,5) NULL   DEFAULT '0.00000';
 ALTER TABLE  `accounting_entries` ADD  `class` int(20) NULL   ;
 ALTER TABLE  `accounting_entries` ADD  `name_id` int(20) NULL   ;
 ALTER TABLE  `accounting_entries` ADD  `memo` varchar(200) NULL   ;
 ALTER TABLE  `accounting_entries` ADD  `item_number` int(10) NULL   DEFAULT '0';
 ALTER TABLE  `accounting_entries` ADD  `item_id` int(20) NULL   ;
 ALTER TABLE  `accounting_entries` ADD  `item_type` varchar(50) NULL   ;


 * @package			        Model
 * @version_number	        4.0.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG)
 */
 
class Accounting_entries_model extends MY_Model {

	protected $id;
	protected $trn_id;
	protected $chart_id;
	protected $debit;
	protected $credit;
	protected $class;
	protected $name_id;
	protected $memo;
	protected $item_number;
	protected $item_id;
	protected $item_type;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'accounting_entries';
		$this->_short_name = 'accounting_entries';
		$this->_fields = array("id","trn_id","chart_id","debit","credit","class","name_id","memo","item_number","item_id","item_type");
		$this->_required = array("trn_id","chart_id");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

		public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

		public function getId() {
			return $this->id;
		}
	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: trn_id -------------------------------------- 

	/** 
	* Sets a value to `trn_id` variable
	* @access public
	*/

		public function setTrnId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('trn_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `trn_id` variable
	* @access public
	*/

		public function getTrnId() {
			return $this->trn_id;
		}
	
// ------------------------------ End Field: trn_id --------------------------------------


// ---------------------------- Start Field: chart_id -------------------------------------- 

	/** 
	* Sets a value to `chart_id` variable
	* @access public
	*/

		public function setChartId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('chart_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `chart_id` variable
	* @access public
	*/

		public function getChartId() {
			return $this->chart_id;
		}
	
// ------------------------------ End Field: chart_id --------------------------------------


// ---------------------------- Start Field: debit -------------------------------------- 

	/** 
	* Sets a value to `debit` variable
	* @access public
	*/

		public function setDebit($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('debit', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `debit` variable
	* @access public
	*/

		public function getDebit() {
			return $this->debit;
		}
	
// ------------------------------ End Field: debit --------------------------------------


// ---------------------------- Start Field: credit -------------------------------------- 

	/** 
	* Sets a value to `credit` variable
	* @access public
	*/

		public function setCredit($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('credit', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `credit` variable
	* @access public
	*/

		public function getCredit() {
			return $this->credit;
		}
	
// ------------------------------ End Field: credit --------------------------------------


// ---------------------------- Start Field: class -------------------------------------- 

	/** 
	* Sets a value to `class` variable
	* @access public
	*/

		public function setClass($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('class', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `class` variable
	* @access public
	*/

		public function getClass() {
			return $this->class;
		}
	
// ------------------------------ End Field: class --------------------------------------


// ---------------------------- Start Field: name_id -------------------------------------- 

	/** 
	* Sets a value to `name_id` variable
	* @access public
	*/

		public function setNameId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('name_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `name_id` variable
	* @access public
	*/

		public function getNameId() {
			return $this->name_id;
		}
	
// ------------------------------ End Field: name_id --------------------------------------


// ---------------------------- Start Field: memo -------------------------------------- 

	/** 
	* Sets a value to `memo` variable
	* @access public
	*/

		public function setMemo($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('memo', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `memo` variable
	* @access public
	*/

		public function getMemo() {
			return $this->memo;
		}
	
// ------------------------------ End Field: memo --------------------------------------


// ---------------------------- Start Field: item_number -------------------------------------- 

	/** 
	* Sets a value to `item_number` variable
	* @access public
	*/

		public function setItemNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('item_number', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `item_number` variable
	* @access public
	*/

		public function getItemNumber() {
			return $this->item_number;
		}
	
// ------------------------------ End Field: item_number --------------------------------------


// ---------------------------- Start Field: item_id -------------------------------------- 

	/** 
	* Sets a value to `item_id` variable
	* @access public
	*/

		public function setItemId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('item_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `item_id` variable
	* @access public
	*/

		public function getItemId() {
			return $this->item_id;
		}
	
// ------------------------------ End Field: item_id --------------------------------------


// ---------------------------- Start Field: item_type -------------------------------------- 

	/** 
	* Sets a value to `item_type` variable
	* @access public
	*/

		public function setItemType($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('item_type', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `item_type` variable
	* @access public
	*/

		public function getItemType() {
			return $this->item_type;
		}
	
// ------------------------------ End Field: item_type --------------------------------------




}

/* End of file Accounting_entries_model.php */
/* Location: ./application/models/Accounting_entries_model.php */
