<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Accounting_deposits_model Class
 *
 * Manipulates `accounting_deposits` table on database

CREATE TABLE `accounting_deposits` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `bank_id` int(20) NOT NULL,
  `date_deposited` date NOT NULL,
  `amount` decimal(20,5) NOT NULL,
  `memo` text,
  `trn_id` int(20) NOT NULL,
  PRIMARY KEY (`id`)
);

 ALTER TABLE  `accounting_deposits` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
 ALTER TABLE  `accounting_deposits` ADD  `bank_id` int(20) NOT NULL   ;
 ALTER TABLE  `accounting_deposits` ADD  `date_deposited` date NOT NULL   ;
 ALTER TABLE  `accounting_deposits` ADD  `amount` decimal(20,5) NOT NULL   ;
 ALTER TABLE  `accounting_deposits` ADD  `memo` text NULL   ;
 ALTER TABLE  `accounting_deposits` ADD  `trn_id` int(20) NOT NULL   ;


 * @package			        Model
 * @version_number	        4.0.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG)
 */
 
class Accounting_deposits_model extends MY_Model {

	protected $id;
	protected $bank_id;
	protected $date_deposited;
	protected $amount;
	protected $memo;
	protected $trn_id;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'accounting_deposits';
		$this->_short_name = 'accounting_deposits';
		$this->_fields = array("id","bank_id","date_deposited","amount","memo","trn_id");
		$this->_required = array("bank_id","date_deposited","amount","trn_id");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

		public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

		public function getId() {
			return $this->id;
		}
	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: bank_id -------------------------------------- 

	/** 
	* Sets a value to `bank_id` variable
	* @access public
	*/

		public function setBankId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('bank_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `bank_id` variable
	* @access public
	*/

		public function getBankId() {
			return $this->bank_id;
		}
	
// ------------------------------ End Field: bank_id --------------------------------------


// ---------------------------- Start Field: date_deposited -------------------------------------- 

	/** 
	* Sets a value to `date_deposited` variable
	* @access public
	*/

		public function setDateDeposited($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('date_deposited', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `date_deposited` variable
	* @access public
	*/

		public function getDateDeposited() {
			return $this->date_deposited;
		}
	
// ------------------------------ End Field: date_deposited --------------------------------------


// ---------------------------- Start Field: amount -------------------------------------- 

	/** 
	* Sets a value to `amount` variable
	* @access public
	*/

		public function setAmount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('amount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `amount` variable
	* @access public
	*/

		public function getAmount() {
			return $this->amount;
		}
	
// ------------------------------ End Field: amount --------------------------------------


// ---------------------------- Start Field: memo -------------------------------------- 

	/** 
	* Sets a value to `memo` variable
	* @access public
	*/

		public function setMemo($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('memo', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `memo` variable
	* @access public
	*/

		public function getMemo() {
			return $this->memo;
		}
	
// ------------------------------ End Field: memo --------------------------------------


// ---------------------------- Start Field: trn_id -------------------------------------- 

	/** 
	* Sets a value to `trn_id` variable
	* @access public
	*/

		public function setTrnId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('trn_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `trn_id` variable
	* @access public
	*/

		public function getTrnId() {
			return $this->trn_id;
		}
	
// ------------------------------ End Field: trn_id --------------------------------------




}

/* End of file Accounting_deposits_model.php */
/* Location: ./application/models/Accounting_deposits_model.php */
