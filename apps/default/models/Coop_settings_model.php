<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Coop_settings_model Class
 *
 * Manipulates `coop_settings` table on database

CREATE TABLE `coop_settings` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `department` varchar(50) NOT NULL,
  `section` varchar(50) NOT NULL,
  `key` varchar(100) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id`)
);

 ALTER TABLE  `coop_settings` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
 ALTER TABLE  `coop_settings` ADD  `department` varchar(50) NOT NULL   ;
 ALTER TABLE  `coop_settings` ADD  `section` varchar(50) NOT NULL   ;
 ALTER TABLE  `coop_settings` ADD  `key` varchar(100) NOT NULL   ;
 ALTER TABLE  `coop_settings` ADD  `value` text NOT NULL   ;


 * @package			        Model
 * @version_number	        4.0.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG)
 */
 
class Coop_settings_model extends MY_Model {

	protected $id;
	protected $department;
	protected $section;
	protected $key;
	protected $value;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'coop_settings';
		$this->_short_name = 'coop_settings';
		$this->_fields = array("id","department","section","key","value");
		$this->_required = array("department","section","key","value");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

		public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

		public function getId() {
			return $this->id;
		}
	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: department -------------------------------------- 

	/** 
	* Sets a value to `department` variable
	* @access public
	*/

		public function setDepartment($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('department', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `department` variable
	* @access public
	*/

		public function getDepartment() {
			return $this->department;
		}
	
// ------------------------------ End Field: department --------------------------------------


// ---------------------------- Start Field: section -------------------------------------- 

	/** 
	* Sets a value to `section` variable
	* @access public
	*/

		public function setSection($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('section', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `section` variable
	* @access public
	*/

		public function getSection() {
			return $this->section;
		}
	
// ------------------------------ End Field: section --------------------------------------


// ---------------------------- Start Field: key -------------------------------------- 

	/** 
	* Sets a value to `key` variable
	* @access public
	*/

		public function setKey($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('key', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `key` variable
	* @access public
	*/

		public function getKey() {
			return $this->key;
		}
	
// ------------------------------ End Field: key --------------------------------------


// ---------------------------- Start Field: value -------------------------------------- 

	/** 
	* Sets a value to `value` variable
	* @access public
	*/

		public function setValue($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('value', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `value` variable
	* @access public
	*/

		public function getValue() {
			return $this->value;
		}
	
// ------------------------------ End Field: value --------------------------------------




}

/* End of file Coop_settings_model.php */
/* Location: ./application/models/Coop_settings_model.php */
