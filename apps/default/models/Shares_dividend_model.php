<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Shares_dividend_model Class
 *
 * Manipulates `shares_dividend` table on database

CREATE TABLE `shares_dividend` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `res_number` int(20) NOT NULL,
  `res_date` date NOT NULL,
  `amount` decimal(20,5) NOT NULL,
  `trn_id` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `res_number` (`res_number`)
);

 ALTER TABLE  `shares_dividend` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
 ALTER TABLE  `shares_dividend` ADD  `res_number` int(20) NOT NULL   UNIQUE KEY;
 ALTER TABLE  `shares_dividend` ADD  `res_date` date NOT NULL   ;
 ALTER TABLE  `shares_dividend` ADD  `amount` decimal(20,5) NOT NULL   ;
 ALTER TABLE  `shares_dividend` ADD  `trn_id` int(20) NULL   ;


 * @package			        Model
 * @version_number	        4.0.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG)
 */
 
class Shares_dividend_model extends MY_Model {

	protected $id;
	protected $res_number;
	protected $res_date;
	protected $amount;
	protected $trn_id;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'shares_dividend';
		$this->_short_name = 'shares_dividend';
		$this->_fields = array("id","res_number","res_date","amount","trn_id");
		$this->_required = array("res_number","res_date","amount");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

		public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

		public function getId() {
			return $this->id;
		}
	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: res_number -------------------------------------- 

	/** 
	* Sets a value to `res_number` variable
	* @access public
	*/

		public function setResNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('res_number', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `res_number` variable
	* @access public
	*/

		public function getResNumber() {
			return $this->res_number;
		}
	
// ------------------------------ End Field: res_number --------------------------------------


// ---------------------------- Start Field: res_date -------------------------------------- 

	/** 
	* Sets a value to `res_date` variable
	* @access public
	*/

		public function setResDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('res_date', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `res_date` variable
	* @access public
	*/

		public function getResDate() {
			return $this->res_date;
		}
	
// ------------------------------ End Field: res_date --------------------------------------


// ---------------------------- Start Field: amount -------------------------------------- 

	/** 
	* Sets a value to `amount` variable
	* @access public
	*/

		public function setAmount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('amount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `amount` variable
	* @access public
	*/

		public function getAmount() {
			return $this->amount;
		}
	
// ------------------------------ End Field: amount --------------------------------------


// ---------------------------- Start Field: trn_id -------------------------------------- 

	/** 
	* Sets a value to `trn_id` variable
	* @access public
	*/

		public function setTrnId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('trn_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `trn_id` variable
	* @access public
	*/

		public function getTrnId() {
			return $this->trn_id;
		}
	
// ------------------------------ End Field: trn_id --------------------------------------




}

/* End of file Shares_dividend_model.php */
/* Location: ./application/models/Shares_dividend_model.php */
