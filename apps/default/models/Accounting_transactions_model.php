<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Accounting_transactions_model Class
 *
 * Manipulates `accounting_transactions` table on database

CREATE TABLE `accounting_transactions` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `dept` varchar(50) NOT NULL,
  `sect` varchar(50) NOT NULL,
  `type` varchar(50) NOT NULL,
  `date` date NOT NULL,
  `memo` text,
  `number` varchar(100) DEFAULT NULL,
  `lines` int(99) DEFAULT NULL,
  `name_id` int(20) DEFAULT NULL,
  `amount` decimal(20,5) DEFAULT NULL,
  `item_id` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

 ALTER TABLE  `accounting_transactions` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
 ALTER TABLE  `accounting_transactions` ADD  `dept` varchar(50) NOT NULL   ;
 ALTER TABLE  `accounting_transactions` ADD  `sect` varchar(50) NOT NULL   ;
 ALTER TABLE  `accounting_transactions` ADD  `type` varchar(50) NOT NULL   ;
 ALTER TABLE  `accounting_transactions` ADD  `date` date NOT NULL   ;
 ALTER TABLE  `accounting_transactions` ADD  `memo` text NULL   ;
 ALTER TABLE  `accounting_transactions` ADD  `number` varchar(100) NULL   ;
 ALTER TABLE  `accounting_transactions` ADD  `lines` int(99) NULL   ;
 ALTER TABLE  `accounting_transactions` ADD  `name_id` int(20) NULL   ;
 ALTER TABLE  `accounting_transactions` ADD  `amount` decimal(20,5) NULL   ;
 ALTER TABLE  `accounting_transactions` ADD  `item_id` int(20) NULL   ;


 * @package			        Model
 * @version_number	        4.0.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG)
 */
 
class Accounting_transactions_model extends MY_Model {

	protected $id;
	protected $dept;
	protected $sect;
	protected $type;
	protected $date;
	protected $memo;
	protected $number;
	protected $lines;
	protected $name_id;
	protected $amount;
	protected $item_id;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'accounting_transactions';
		$this->_short_name = 'accounting_transactions';
		$this->_fields = array("id","dept","sect","type","date","memo","number","lines","name_id","amount","item_id");
		$this->_required = array("dept","sect","type","date");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

		public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

		public function getId() {
			return $this->id;
		}
	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: dept -------------------------------------- 

	/** 
	* Sets a value to `dept` variable
	* @access public
	*/

		public function setDept($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('dept', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `dept` variable
	* @access public
	*/

		public function getDept() {
			return $this->dept;
		}
	
// ------------------------------ End Field: dept --------------------------------------


// ---------------------------- Start Field: sect -------------------------------------- 

	/** 
	* Sets a value to `sect` variable
	* @access public
	*/

		public function setSect($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('sect', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `sect` variable
	* @access public
	*/

		public function getSect() {
			return $this->sect;
		}
	
// ------------------------------ End Field: sect --------------------------------------


// ---------------------------- Start Field: type -------------------------------------- 

	/** 
	* Sets a value to `type` variable
	* @access public
	*/

		public function setType($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('type', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `type` variable
	* @access public
	*/

		public function getType() {
			return $this->type;
		}
	
// ------------------------------ End Field: type --------------------------------------


// ---------------------------- Start Field: date -------------------------------------- 

	/** 
	* Sets a value to `date` variable
	* @access public
	*/

		public function setDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('date', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `date` variable
	* @access public
	*/

		public function getDate() {
			return $this->date;
		}
	
// ------------------------------ End Field: date --------------------------------------


// ---------------------------- Start Field: memo -------------------------------------- 

	/** 
	* Sets a value to `memo` variable
	* @access public
	*/

		public function setMemo($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('memo', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `memo` variable
	* @access public
	*/

		public function getMemo() {
			return $this->memo;
		}
	
// ------------------------------ End Field: memo --------------------------------------


// ---------------------------- Start Field: number -------------------------------------- 

	/** 
	* Sets a value to `number` variable
	* @access public
	*/

		public function setNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('number', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `number` variable
	* @access public
	*/

		public function getNumber() {
			return $this->number;
		}
	
// ------------------------------ End Field: number --------------------------------------


// ---------------------------- Start Field: lines -------------------------------------- 

	/** 
	* Sets a value to `lines` variable
	* @access public
	*/

		public function setLines($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('lines', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `lines` variable
	* @access public
	*/

		public function getLines() {
			return $this->lines;
		}
	
// ------------------------------ End Field: lines --------------------------------------


// ---------------------------- Start Field: name_id -------------------------------------- 

	/** 
	* Sets a value to `name_id` variable
	* @access public
	*/

		public function setNameId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('name_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `name_id` variable
	* @access public
	*/

		public function getNameId() {
			return $this->name_id;
		}
	
// ------------------------------ End Field: name_id --------------------------------------


// ---------------------------- Start Field: amount -------------------------------------- 

	/** 
	* Sets a value to `amount` variable
	* @access public
	*/

		public function setAmount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('amount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `amount` variable
	* @access public
	*/

		public function getAmount() {
			return $this->amount;
		}
	
// ------------------------------ End Field: amount --------------------------------------


// ---------------------------- Start Field: item_id -------------------------------------- 

	/** 
	* Sets a value to `item_id` variable
	* @access public
	*/

		public function setItemId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('item_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `item_id` variable
	* @access public
	*/

		public function getItemId() {
			return $this->item_id;
		}
	
// ------------------------------ End Field: item_id --------------------------------------




}

/* End of file Accounting_transactions_model.php */
/* Location: ./application/models/Accounting_transactions_model.php */
