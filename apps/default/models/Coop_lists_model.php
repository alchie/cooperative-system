<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Coop_lists_model Class
 *
 * Manipulates `coop_lists` table on database

CREATE TABLE `coop_lists` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `dept` varchar(50) NOT NULL,
  `type` varchar(50) NOT NULL,
  `parent` int(20) NOT NULL,
  `value` text NOT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
);

 ALTER TABLE  `coop_lists` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
 ALTER TABLE  `coop_lists` ADD  `dept` varchar(50) NOT NULL   ;
 ALTER TABLE  `coop_lists` ADD  `type` varchar(50) NOT NULL   ;
 ALTER TABLE  `coop_lists` ADD  `parent` int(20) NOT NULL   ;
 ALTER TABLE  `coop_lists` ADD  `value` text NOT NULL   ;
 ALTER TABLE  `coop_lists` ADD  `active` int(1) NOT NULL   DEFAULT '1';


 * @package			        Model
 * @version_number	        4.0.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG)
 */
 
class Coop_lists_model extends MY_Model {

	protected $id;
	protected $dept;
	protected $type;
	protected $parent;
	protected $value;
	protected $active;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'coop_lists';
		$this->_short_name = 'coop_lists';
		$this->_fields = array("id","dept","type","parent","value","active");
		$this->_required = array("dept","type","parent","value","active");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

		public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

		public function getId() {
			return $this->id;
		}
	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: dept -------------------------------------- 

	/** 
	* Sets a value to `dept` variable
	* @access public
	*/

		public function setDept($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('dept', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `dept` variable
	* @access public
	*/

		public function getDept() {
			return $this->dept;
		}
	
// ------------------------------ End Field: dept --------------------------------------


// ---------------------------- Start Field: type -------------------------------------- 

	/** 
	* Sets a value to `type` variable
	* @access public
	*/

		public function setType($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('type', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `type` variable
	* @access public
	*/

		public function getType() {
			return $this->type;
		}
	
// ------------------------------ End Field: type --------------------------------------


// ---------------------------- Start Field: parent -------------------------------------- 

	/** 
	* Sets a value to `parent` variable
	* @access public
	*/

		public function setParent($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('parent', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `parent` variable
	* @access public
	*/

		public function getParent() {
			return $this->parent;
		}
	
// ------------------------------ End Field: parent --------------------------------------


// ---------------------------- Start Field: value -------------------------------------- 

	/** 
	* Sets a value to `value` variable
	* @access public
	*/

		public function setValue($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('value', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `value` variable
	* @access public
	*/

		public function getValue() {
			return $this->value;
		}
	
// ------------------------------ End Field: value --------------------------------------


// ---------------------------- Start Field: active -------------------------------------- 

	/** 
	* Sets a value to `active` variable
	* @access public
	*/

		public function setActive($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('active', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `active` variable
	* @access public
	*/

		public function getActive() {
			return $this->active;
		}
	
// ------------------------------ End Field: active --------------------------------------




}

/* End of file Coop_lists_model.php */
/* Location: ./application/models/Coop_lists_model.php */
