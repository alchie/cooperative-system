<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Companies_members_model Class
 *
 * Manipulates `companies_members` table on database

CREATE TABLE `companies_members` (
  `company_id` int(20) NOT NULL,
  `member_id` int(20) NOT NULL,
  KEY `company_id` (`company_id`,`member_id`),
  KEY `member_id` (`member_id`)
);

 ALTER TABLE  `companies_members` ADD  `company_id` int(20) NOT NULL   ;
 ALTER TABLE  `companies_members` ADD  `member_id` int(20) NOT NULL   ;


 * @package			        Model
 * @version_number	        4.0.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG)
 */
 
class Companies_members_model extends MY_Model {

	protected $company_id;
	protected $member_id;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'companies_members';
		$this->_short_name = 'companies_members';
		$this->_fields = array("company_id","member_id");
		$this->_required = array("company_id","member_id");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: company_id -------------------------------------- 

	/** 
	* Sets a value to `company_id` variable
	* @access public
	*/

		public function setCompanyId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('company_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `company_id` variable
	* @access public
	*/

		public function getCompanyId() {
			return $this->company_id;
		}
	
// ------------------------------ End Field: company_id --------------------------------------


// ---------------------------- Start Field: member_id -------------------------------------- 

	/** 
	* Sets a value to `member_id` variable
	* @access public
	*/

		public function setMemberId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('member_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `member_id` variable
	* @access public
	*/

		public function getMemberId() {
			return $this->member_id;
		}
	
// ------------------------------ End Field: member_id --------------------------------------




}

/* End of file Companies_members_model.php */
/* Location: ./application/models/Companies_members_model.php */
