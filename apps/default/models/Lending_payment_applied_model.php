<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Lending_payment_applied_model Class
 *
 * Manipulates `lending_payment_applied` table on database

CREATE TABLE `lending_payment_applied` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `payment_id` int(20) NOT NULL,
  `invoice_id` int(20) NOT NULL,
  `amount` decimal(20,5) NOT NULL,
  PRIMARY KEY (`id`)
);

 ALTER TABLE  `lending_payment_applied` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
 ALTER TABLE  `lending_payment_applied` ADD  `payment_id` int(20) NOT NULL   ;
 ALTER TABLE  `lending_payment_applied` ADD  `invoice_id` int(20) NOT NULL   ;
 ALTER TABLE  `lending_payment_applied` ADD  `amount` decimal(20,5) NOT NULL   ;


 * @package			        Model
 * @version_number	        4.0.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG)
 */
 
class Lending_payment_applied_model extends MY_Model {

	protected $id;
	protected $payment_id;
	protected $invoice_id;
	protected $amount;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'lending_payment_applied';
		$this->_short_name = 'lending_payment_applied';
		$this->_fields = array("id","payment_id","invoice_id","amount");
		$this->_required = array("payment_id","invoice_id","amount");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

		public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

		public function getId() {
			return $this->id;
		}
	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: payment_id -------------------------------------- 

	/** 
	* Sets a value to `payment_id` variable
	* @access public
	*/

		public function setPaymentId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('payment_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `payment_id` variable
	* @access public
	*/

		public function getPaymentId() {
			return $this->payment_id;
		}
	
// ------------------------------ End Field: payment_id --------------------------------------


// ---------------------------- Start Field: invoice_id -------------------------------------- 

	/** 
	* Sets a value to `invoice_id` variable
	* @access public
	*/

		public function setInvoiceId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('invoice_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `invoice_id` variable
	* @access public
	*/

		public function getInvoiceId() {
			return $this->invoice_id;
		}
	
// ------------------------------ End Field: invoice_id --------------------------------------


// ---------------------------- Start Field: amount -------------------------------------- 

	/** 
	* Sets a value to `amount` variable
	* @access public
	*/

		public function setAmount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('amount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `amount` variable
	* @access public
	*/

		public function getAmount() {
			return $this->amount;
		}
	
// ------------------------------ End Field: amount --------------------------------------




}

/* End of file Lending_payment_applied_model.php */
/* Location: ./application/models/Lending_payment_applied_model.php */
