<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Lending_invoices_model Class
 *
 * Manipulates `lending_invoices` table on database

CREATE TABLE `lending_invoices` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `loan_id` int(20) NOT NULL,
  `due_date` date NOT NULL,
  `principal_due` decimal(20,5) NOT NULL,
  `interest_due` decimal(20,5) NOT NULL,
  `number` int(2) DEFAULT NULL,
  `trn_id` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

 ALTER TABLE  `lending_invoices` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
 ALTER TABLE  `lending_invoices` ADD  `loan_id` int(20) NOT NULL   ;
 ALTER TABLE  `lending_invoices` ADD  `due_date` date NOT NULL   ;
 ALTER TABLE  `lending_invoices` ADD  `principal_due` decimal(20,5) NOT NULL   ;
 ALTER TABLE  `lending_invoices` ADD  `interest_due` decimal(20,5) NOT NULL   ;
 ALTER TABLE  `lending_invoices` ADD  `number` int(2) NULL   ;
 ALTER TABLE  `lending_invoices` ADD  `trn_id` int(20) NULL   ;


 * @package			        Model
 * @version_number	        4.0.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG)
 */
 
class Lending_invoices_model extends MY_Model {

	protected $id;
	protected $loan_id;
	protected $due_date;
	protected $principal_due;
	protected $interest_due;
	protected $number;
	protected $trn_id;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'lending_invoices';
		$this->_short_name = 'lending_invoices';
		$this->_fields = array("id","loan_id","due_date","principal_due","interest_due","number","trn_id");
		$this->_required = array("loan_id","due_date","principal_due","interest_due");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

		public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

		public function getId() {
			return $this->id;
		}
	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: loan_id -------------------------------------- 

	/** 
	* Sets a value to `loan_id` variable
	* @access public
	*/

		public function setLoanId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('loan_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `loan_id` variable
	* @access public
	*/

		public function getLoanId() {
			return $this->loan_id;
		}
	
// ------------------------------ End Field: loan_id --------------------------------------


// ---------------------------- Start Field: due_date -------------------------------------- 

	/** 
	* Sets a value to `due_date` variable
	* @access public
	*/

		public function setDueDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('due_date', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `due_date` variable
	* @access public
	*/

		public function getDueDate() {
			return $this->due_date;
		}
	
// ------------------------------ End Field: due_date --------------------------------------


// ---------------------------- Start Field: principal_due -------------------------------------- 

	/** 
	* Sets a value to `principal_due` variable
	* @access public
	*/

		public function setPrincipalDue($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('principal_due', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `principal_due` variable
	* @access public
	*/

		public function getPrincipalDue() {
			return $this->principal_due;
		}
	
// ------------------------------ End Field: principal_due --------------------------------------


// ---------------------------- Start Field: interest_due -------------------------------------- 

	/** 
	* Sets a value to `interest_due` variable
	* @access public
	*/

		public function setInterestDue($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('interest_due', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `interest_due` variable
	* @access public
	*/

		public function getInterestDue() {
			return $this->interest_due;
		}
	
// ------------------------------ End Field: interest_due --------------------------------------


// ---------------------------- Start Field: number -------------------------------------- 

	/** 
	* Sets a value to `number` variable
	* @access public
	*/

		public function setNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('number', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `number` variable
	* @access public
	*/

		public function getNumber() {
			return $this->number;
		}
	
// ------------------------------ End Field: number --------------------------------------


// ---------------------------- Start Field: trn_id -------------------------------------- 

	/** 
	* Sets a value to `trn_id` variable
	* @access public
	*/

		public function setTrnId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('trn_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `trn_id` variable
	* @access public
	*/

		public function getTrnId() {
			return $this->trn_id;
		}
	
// ------------------------------ End Field: trn_id --------------------------------------




}

/* End of file Lending_invoices_model.php */
/* Location: ./application/models/Lending_invoices_model.php */
