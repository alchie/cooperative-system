<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Coop_lists_options_model Class
 *
 * Manipulates `coop_lists_options` table on database

CREATE TABLE `coop_lists_options` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `list_id` int(20) NOT NULL,
  `key` varchar(200) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `list_id` (`list_id`)
);

 ALTER TABLE  `coop_lists_options` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
 ALTER TABLE  `coop_lists_options` ADD  `list_id` int(20) NOT NULL   ;
 ALTER TABLE  `coop_lists_options` ADD  `key` varchar(200) NOT NULL   ;
 ALTER TABLE  `coop_lists_options` ADD  `value` text NOT NULL   ;


 * @package			        Model
 * @version_number	        4.0.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG)
 */
 
class Coop_lists_options_model extends MY_Model {

	protected $id;
	protected $list_id;
	protected $key;
	protected $value;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'coop_lists_options';
		$this->_short_name = 'coop_lists_options';
		$this->_fields = array("id","list_id","key","value");
		$this->_required = array("list_id","key","value");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

		public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

		public function getId() {
			return $this->id;
		}
	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: list_id -------------------------------------- 

	/** 
	* Sets a value to `list_id` variable
	* @access public
	*/

		public function setListId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('list_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `list_id` variable
	* @access public
	*/

		public function getListId() {
			return $this->list_id;
		}
	
// ------------------------------ End Field: list_id --------------------------------------


// ---------------------------- Start Field: key -------------------------------------- 

	/** 
	* Sets a value to `key` variable
	* @access public
	*/

		public function setKey($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('key', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `key` variable
	* @access public
	*/

		public function getKey() {
			return $this->key;
		}
	
// ------------------------------ End Field: key --------------------------------------


// ---------------------------- Start Field: value -------------------------------------- 

	/** 
	* Sets a value to `value` variable
	* @access public
	*/

		public function setValue($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('value', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `value` variable
	* @access public
	*/

		public function getValue() {
			return $this->value;
		}
	
// ------------------------------ End Field: value --------------------------------------




}

/* End of file Coop_lists_options_model.php */
/* Location: ./application/models/Coop_lists_options_model.php */
