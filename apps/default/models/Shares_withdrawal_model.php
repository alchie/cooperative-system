<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Shares_withdrawal_model Class
 *
 * Manipulates `shares_withdrawal` table on database

CREATE TABLE `shares_withdrawal` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `member_id` int(20) NOT NULL,
  `bank_id` int(20) NOT NULL,
  `check_number` varchar(100) NOT NULL,
  `check_date` date NOT NULL,
  `amount` decimal(20,5) NOT NULL,
  `memo` text,
  `trn_id` int(20) NOT NULL,
  `entry_id` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `check_number` (`check_number`),
  UNIQUE KEY `entry_id` (`entry_id`)
);

 ALTER TABLE  `shares_withdrawal` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
 ALTER TABLE  `shares_withdrawal` ADD  `member_id` int(20) NOT NULL   ;
 ALTER TABLE  `shares_withdrawal` ADD  `bank_id` int(20) NOT NULL   ;
 ALTER TABLE  `shares_withdrawal` ADD  `check_number` varchar(100) NOT NULL   UNIQUE KEY;
 ALTER TABLE  `shares_withdrawal` ADD  `check_date` date NOT NULL   ;
 ALTER TABLE  `shares_withdrawal` ADD  `amount` decimal(20,5) NOT NULL   ;
 ALTER TABLE  `shares_withdrawal` ADD  `memo` text NULL   ;
 ALTER TABLE  `shares_withdrawal` ADD  `trn_id` int(20) NOT NULL   ;
 ALTER TABLE  `shares_withdrawal` ADD  `entry_id` int(20) NULL   UNIQUE KEY;


 * @package			        Model
 * @version_number	        4.0.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG)
 */
 
class Shares_withdrawal_model extends MY_Model {

	protected $id;
	protected $member_id;
	protected $bank_id;
	protected $check_number;
	protected $check_date;
	protected $amount;
	protected $memo;
	protected $trn_id;
	protected $entry_id;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'shares_withdrawal';
		$this->_short_name = 'shares_withdrawal';
		$this->_fields = array("id","member_id","bank_id","check_number","check_date","amount","memo","trn_id","entry_id");
		$this->_required = array("member_id","bank_id","check_number","check_date","amount","trn_id");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

		public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

		public function getId() {
			return $this->id;
		}
	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: member_id -------------------------------------- 

	/** 
	* Sets a value to `member_id` variable
	* @access public
	*/

		public function setMemberId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('member_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `member_id` variable
	* @access public
	*/

		public function getMemberId() {
			return $this->member_id;
		}
	
// ------------------------------ End Field: member_id --------------------------------------


// ---------------------------- Start Field: bank_id -------------------------------------- 

	/** 
	* Sets a value to `bank_id` variable
	* @access public
	*/

		public function setBankId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('bank_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `bank_id` variable
	* @access public
	*/

		public function getBankId() {
			return $this->bank_id;
		}
	
// ------------------------------ End Field: bank_id --------------------------------------


// ---------------------------- Start Field: check_number -------------------------------------- 

	/** 
	* Sets a value to `check_number` variable
	* @access public
	*/

		public function setCheckNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('check_number', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `check_number` variable
	* @access public
	*/

		public function getCheckNumber() {
			return $this->check_number;
		}
	
// ------------------------------ End Field: check_number --------------------------------------


// ---------------------------- Start Field: check_date -------------------------------------- 

	/** 
	* Sets a value to `check_date` variable
	* @access public
	*/

		public function setCheckDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('check_date', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `check_date` variable
	* @access public
	*/

		public function getCheckDate() {
			return $this->check_date;
		}
	
// ------------------------------ End Field: check_date --------------------------------------


// ---------------------------- Start Field: amount -------------------------------------- 

	/** 
	* Sets a value to `amount` variable
	* @access public
	*/

		public function setAmount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('amount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `amount` variable
	* @access public
	*/

		public function getAmount() {
			return $this->amount;
		}
	
// ------------------------------ End Field: amount --------------------------------------


// ---------------------------- Start Field: memo -------------------------------------- 

	/** 
	* Sets a value to `memo` variable
	* @access public
	*/

		public function setMemo($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('memo', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `memo` variable
	* @access public
	*/

		public function getMemo() {
			return $this->memo;
		}
	
// ------------------------------ End Field: memo --------------------------------------


// ---------------------------- Start Field: trn_id -------------------------------------- 

	/** 
	* Sets a value to `trn_id` variable
	* @access public
	*/

		public function setTrnId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('trn_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `trn_id` variable
	* @access public
	*/

		public function getTrnId() {
			return $this->trn_id;
		}
	
// ------------------------------ End Field: trn_id --------------------------------------


// ---------------------------- Start Field: entry_id -------------------------------------- 

	/** 
	* Sets a value to `entry_id` variable
	* @access public
	*/

		public function setEntryId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('entry_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `entry_id` variable
	* @access public
	*/

		public function getEntryId() {
			return $this->entry_id;
		}
	
// ------------------------------ End Field: entry_id --------------------------------------




}

/* End of file Shares_withdrawal_model.php */
/* Location: ./application/models/Shares_withdrawal_model.php */
