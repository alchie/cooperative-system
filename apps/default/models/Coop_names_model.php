<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Coop_names_model Class
 *
 * Manipulates `coop_names` table on database

CREATE TABLE `coop_names` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(200) NOT NULL,
  `address` varchar(200) DEFAULT NULL,
  `contact_number` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `full_name` (`full_name`)
);

 ALTER TABLE  `coop_names` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
 ALTER TABLE  `coop_names` ADD  `full_name` varchar(200) NOT NULL   UNIQUE KEY;
 ALTER TABLE  `coop_names` ADD  `address` varchar(200) NULL   ;
 ALTER TABLE  `coop_names` ADD  `contact_number` varchar(200) NULL   ;


 * @package			        Model
 * @version_number	        4.0.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG)
 */
 
class Coop_names_model extends MY_Model {

	protected $id;
	protected $full_name;
	protected $address;
	protected $contact_number;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'coop_names';
		$this->_short_name = 'coop_names';
		$this->_fields = array("id","full_name","address","contact_number");
		$this->_required = array("full_name");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

		public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

		public function getId() {
			return $this->id;
		}
	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: full_name -------------------------------------- 

	/** 
	* Sets a value to `full_name` variable
	* @access public
	*/

		public function setFullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('full_name', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `full_name` variable
	* @access public
	*/

		public function getFullName() {
			return $this->full_name;
		}
	
// ------------------------------ End Field: full_name --------------------------------------


// ---------------------------- Start Field: address -------------------------------------- 

	/** 
	* Sets a value to `address` variable
	* @access public
	*/

		public function setAddress($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('address', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `address` variable
	* @access public
	*/

		public function getAddress() {
			return $this->address;
		}
	
// ------------------------------ End Field: address --------------------------------------


// ---------------------------- Start Field: contact_number -------------------------------------- 

	/** 
	* Sets a value to `contact_number` variable
	* @access public
	*/

		public function setContactNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('contact_number', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `contact_number` variable
	* @access public
	*/

		public function getContactNumber() {
			return $this->contact_number;
		}
	
// ------------------------------ End Field: contact_number --------------------------------------




}

/* End of file Coop_names_model.php */
/* Location: ./application/models/Coop_names_model.php */
