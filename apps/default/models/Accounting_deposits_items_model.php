<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Accounting_deposits_items_model Class
 *
 * Manipulates `accounting_deposits_items` table on database

CREATE TABLE `accounting_deposits_items` (
  `entry_id` int(20) NOT NULL,
  `deposit_id` int(20) NOT NULL,
  UNIQUE KEY `entry_id_2` (`entry_id`),
  KEY `entry_id` (`entry_id`)
);

 ALTER TABLE  `accounting_deposits_items` ADD  `entry_id` int(20) NOT NULL   PRIMARY KEY;
 ALTER TABLE  `accounting_deposits_items` ADD  `deposit_id` int(20) NOT NULL   ;


 * @package			        Model
 * @version_number	        4.0.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG)
 */
 
class Accounting_deposits_items_model extends MY_Model {

	protected $entry_id;
	protected $deposit_id;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'accounting_deposits_items';
		$this->_short_name = 'accounting_deposits_items';
		$this->_fields = array("entry_id","deposit_id");
		$this->_required = array("deposit_id");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: entry_id -------------------------------------- 

	/** 
	* Sets a value to `entry_id` variable
	* @access public
	*/

		public function setEntryId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('entry_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `entry_id` variable
	* @access public
	*/

		public function getEntryId() {
			return $this->entry_id;
		}
	
// ------------------------------ End Field: entry_id --------------------------------------


// ---------------------------- Start Field: deposit_id -------------------------------------- 

	/** 
	* Sets a value to `deposit_id` variable
	* @access public
	*/

		public function setDepositId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('deposit_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `deposit_id` variable
	* @access public
	*/

		public function getDepositId() {
			return $this->deposit_id;
		}
	
// ------------------------------ End Field: deposit_id --------------------------------------




}

/* End of file Accounting_deposits_items_model.php */
/* Location: ./application/models/Accounting_deposits_items_model.php */
