<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Accounting_checks_model Class
 *
 * Manipulates `accounting_checks` table on database

CREATE TABLE `accounting_checks` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `chart_id` int(20) NOT NULL,
  `check_number` int(20) NOT NULL,
  `check_date` date NOT NULL,
  `name_id` int(20) NOT NULL,
  `amount` decimal(20,5) NOT NULL,
  `memo` text,
  `lines` int(2) DEFAULT '10',
  `trn_id` int(20) DEFAULT NULL,
  `cancelled` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
);

 ALTER TABLE  `accounting_checks` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
 ALTER TABLE  `accounting_checks` ADD  `chart_id` int(20) NOT NULL   ;
 ALTER TABLE  `accounting_checks` ADD  `check_number` int(20) NOT NULL   ;
 ALTER TABLE  `accounting_checks` ADD  `check_date` date NOT NULL   ;
 ALTER TABLE  `accounting_checks` ADD  `name_id` int(20) NOT NULL   ;
 ALTER TABLE  `accounting_checks` ADD  `amount` decimal(20,5) NOT NULL   ;
 ALTER TABLE  `accounting_checks` ADD  `memo` text NULL   ;
 ALTER TABLE  `accounting_checks` ADD  `lines` int(2) NULL   DEFAULT '10';
 ALTER TABLE  `accounting_checks` ADD  `trn_id` int(20) NULL   ;
 ALTER TABLE  `accounting_checks` ADD  `cancelled` int(1) NULL   DEFAULT '0';


 * @package			        Model
 * @version_number	        4.0.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG)
 */
 
class Accounting_checks_model extends MY_Model {

	protected $id;
	protected $chart_id;
	protected $check_number;
	protected $check_date;
	protected $name_id;
	protected $amount;
	protected $memo;
	protected $lines;
	protected $trn_id;
	protected $cancelled;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'accounting_checks';
		$this->_short_name = 'accounting_checks';
		$this->_fields = array("id","chart_id","check_number","check_date","name_id","amount","memo","lines","trn_id","cancelled");
		$this->_required = array("chart_id","check_number","check_date","name_id","amount");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

		public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

		public function getId() {
			return $this->id;
		}
	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: chart_id -------------------------------------- 

	/** 
	* Sets a value to `chart_id` variable
	* @access public
	*/

		public function setChartId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('chart_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `chart_id` variable
	* @access public
	*/

		public function getChartId() {
			return $this->chart_id;
		}
	
// ------------------------------ End Field: chart_id --------------------------------------


// ---------------------------- Start Field: check_number -------------------------------------- 

	/** 
	* Sets a value to `check_number` variable
	* @access public
	*/

		public function setCheckNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('check_number', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `check_number` variable
	* @access public
	*/

		public function getCheckNumber() {
			return $this->check_number;
		}
	
// ------------------------------ End Field: check_number --------------------------------------


// ---------------------------- Start Field: check_date -------------------------------------- 

	/** 
	* Sets a value to `check_date` variable
	* @access public
	*/

		public function setCheckDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('check_date', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `check_date` variable
	* @access public
	*/

		public function getCheckDate() {
			return $this->check_date;
		}
	
// ------------------------------ End Field: check_date --------------------------------------


// ---------------------------- Start Field: name_id -------------------------------------- 

	/** 
	* Sets a value to `name_id` variable
	* @access public
	*/

		public function setNameId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('name_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `name_id` variable
	* @access public
	*/

		public function getNameId() {
			return $this->name_id;
		}
	
// ------------------------------ End Field: name_id --------------------------------------


// ---------------------------- Start Field: amount -------------------------------------- 

	/** 
	* Sets a value to `amount` variable
	* @access public
	*/

		public function setAmount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('amount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `amount` variable
	* @access public
	*/

		public function getAmount() {
			return $this->amount;
		}
	
// ------------------------------ End Field: amount --------------------------------------


// ---------------------------- Start Field: memo -------------------------------------- 

	/** 
	* Sets a value to `memo` variable
	* @access public
	*/

		public function setMemo($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('memo', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `memo` variable
	* @access public
	*/

		public function getMemo() {
			return $this->memo;
		}
	
// ------------------------------ End Field: memo --------------------------------------


// ---------------------------- Start Field: lines -------------------------------------- 

	/** 
	* Sets a value to `lines` variable
	* @access public
	*/

		public function setLines($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('lines', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `lines` variable
	* @access public
	*/

		public function getLines() {
			return $this->lines;
		}
	
// ------------------------------ End Field: lines --------------------------------------


// ---------------------------- Start Field: trn_id -------------------------------------- 

	/** 
	* Sets a value to `trn_id` variable
	* @access public
	*/

		public function setTrnId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('trn_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `trn_id` variable
	* @access public
	*/

		public function getTrnId() {
			return $this->trn_id;
		}
	
// ------------------------------ End Field: trn_id --------------------------------------


// ---------------------------- Start Field: cancelled -------------------------------------- 

	/** 
	* Sets a value to `cancelled` variable
	* @access public
	*/

		public function setCancelled($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('cancelled', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `cancelled` variable
	* @access public
	*/

		public function getCancelled() {
			return $this->cancelled;
		}
	
// ------------------------------ End Field: cancelled --------------------------------------




}

/* End of file Accounting_checks_model.php */
/* Location: ./application/models/Accounting_checks_model.php */
