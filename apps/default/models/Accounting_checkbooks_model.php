<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Accounting_checkbooks_model Class
 *
 * Manipulates `accounting_checkbooks` table on database

CREATE TABLE `accounting_checkbooks` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `chart_id` int(20) NOT NULL,
  `num_start` int(20) NOT NULL,
  `num_end` int(20) NOT NULL,
  PRIMARY KEY (`id`)
);

 ALTER TABLE  `accounting_checkbooks` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
 ALTER TABLE  `accounting_checkbooks` ADD  `chart_id` int(20) NOT NULL   ;
 ALTER TABLE  `accounting_checkbooks` ADD  `num_start` int(20) NOT NULL   ;
 ALTER TABLE  `accounting_checkbooks` ADD  `num_end` int(20) NOT NULL   ;


 * @package			        Model
 * @version_number	        4.0.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG)
 */
 
class Accounting_checkbooks_model extends MY_Model {

	protected $id;
	protected $chart_id;
	protected $num_start;
	protected $num_end;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'accounting_checkbooks';
		$this->_short_name = 'accounting_checkbooks';
		$this->_fields = array("id","chart_id","num_start","num_end");
		$this->_required = array("chart_id","num_start","num_end");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

		public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

		public function getId() {
			return $this->id;
		}
	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: chart_id -------------------------------------- 

	/** 
	* Sets a value to `chart_id` variable
	* @access public
	*/

		public function setChartId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('chart_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `chart_id` variable
	* @access public
	*/

		public function getChartId() {
			return $this->chart_id;
		}
	
// ------------------------------ End Field: chart_id --------------------------------------


// ---------------------------- Start Field: num_start -------------------------------------- 

	/** 
	* Sets a value to `num_start` variable
	* @access public
	*/

		public function setNumStart($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('num_start', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `num_start` variable
	* @access public
	*/

		public function getNumStart() {
			return $this->num_start;
		}
	
// ------------------------------ End Field: num_start --------------------------------------


// ---------------------------- Start Field: num_end -------------------------------------- 

	/** 
	* Sets a value to `num_end` variable
	* @access public
	*/

		public function setNumEnd($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('num_end', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `num_end` variable
	* @access public
	*/

		public function getNumEnd() {
			return $this->num_end;
		}
	
// ------------------------------ End Field: num_end --------------------------------------




}

/* End of file Accounting_checkbooks_model.php */
/* Location: ./application/models/Accounting_checkbooks_model.php */
