<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Reports_saved_model Class
 *
 * Manipulates `reports_saved` table on database

CREATE TABLE `reports_saved` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `type` varchar(100) NOT NULL,
  `filters` text NOT NULL,
  PRIMARY KEY (`id`)
);

 ALTER TABLE  `reports_saved` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
 ALTER TABLE  `reports_saved` ADD  `name` varchar(100) NOT NULL   ;
 ALTER TABLE  `reports_saved` ADD  `type` varchar(100) NOT NULL   ;
 ALTER TABLE  `reports_saved` ADD  `filters` text NOT NULL   ;


 * @package			        Model
 * @version_number	        4.0.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG)
 */
 
class Reports_saved_model extends MY_Model {

	protected $id;
	protected $name;
	protected $type;
	protected $filters;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'reports_saved';
		$this->_short_name = 'reports_saved';
		$this->_fields = array("id","name","type","filters");
		$this->_required = array("name","type","filters");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

		public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

		public function getId() {
			return $this->id;
		}
	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: name -------------------------------------- 

	/** 
	* Sets a value to `name` variable
	* @access public
	*/

		public function setName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('name', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `name` variable
	* @access public
	*/

		public function getName() {
			return $this->name;
		}
	
// ------------------------------ End Field: name --------------------------------------


// ---------------------------- Start Field: type -------------------------------------- 

	/** 
	* Sets a value to `type` variable
	* @access public
	*/

		public function setType($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('type', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `type` variable
	* @access public
	*/

		public function getType() {
			return $this->type;
		}
	
// ------------------------------ End Field: type --------------------------------------


// ---------------------------- Start Field: filters -------------------------------------- 

	/** 
	* Sets a value to `filters` variable
	* @access public
	*/

		public function setFilters($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('filters', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `filters` variable
	* @access public
	*/

		public function getFilters() {
			return $this->filters;
		}
	
// ------------------------------ End Field: filters --------------------------------------




}

/* End of file Reports_saved_model.php */
/* Location: ./application/models/Reports_saved_model.php */
