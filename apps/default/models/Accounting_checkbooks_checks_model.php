<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Accounting_checkbooks_checks_model Class
 *
 * Manipulates `accounting_checkbooks_checks` table on database

CREATE TABLE `accounting_checkbooks_checks` (
  `checkbook_id` int(20) NOT NULL,
  `check_number` int(20) NOT NULL,
  KEY `checkbook_id` (`checkbook_id`)
);

 ALTER TABLE  `accounting_checkbooks_checks` ADD  `checkbook_id` int(20) NOT NULL   ;
 ALTER TABLE  `accounting_checkbooks_checks` ADD  `check_number` int(20) NOT NULL   ;


 * @package			        Model
 * @version_number	        4.0.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG)
 */
 
class Accounting_checkbooks_checks_model extends MY_Model {

	protected $checkbook_id;
	protected $check_number;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'accounting_checkbooks_checks';
		$this->_short_name = 'accounting_checkbooks_checks';
		$this->_fields = array("checkbook_id","check_number");
		$this->_required = array("checkbook_id","check_number");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: checkbook_id -------------------------------------- 

	/** 
	* Sets a value to `checkbook_id` variable
	* @access public
	*/

		public function setCheckbookId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('checkbook_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `checkbook_id` variable
	* @access public
	*/

		public function getCheckbookId() {
			return $this->checkbook_id;
		}
	
// ------------------------------ End Field: checkbook_id --------------------------------------


// ---------------------------- Start Field: check_number -------------------------------------- 

	/** 
	* Sets a value to `check_number` variable
	* @access public
	*/

		public function setCheckNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('check_number', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `check_number` variable
	* @access public
	*/

		public function getCheckNumber() {
			return $this->check_number;
		}
	
// ------------------------------ End Field: check_number --------------------------------------




}

/* End of file Accounting_checkbooks_checks_model.php */
/* Location: ./application/models/Accounting_checkbooks_checks_model.php */
