<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Accounting_charts_model Class
 *
 * Manipulates `accounting_charts` table on database

CREATE TABLE `accounting_charts` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `number` int(7) DEFAULT NULL,
  `type` varchar(100) NOT NULL,
  `title` varchar(200) NOT NULL,
  `parent` int(20) DEFAULT '0',
  `active` int(1) DEFAULT '1',
  `reconcile` int(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `number` (`number`)
);

 ALTER TABLE  `accounting_charts` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
 ALTER TABLE  `accounting_charts` ADD  `number` int(7) NULL   UNIQUE KEY;
 ALTER TABLE  `accounting_charts` ADD  `type` varchar(100) NOT NULL   ;
 ALTER TABLE  `accounting_charts` ADD  `title` varchar(200) NOT NULL   ;
 ALTER TABLE  `accounting_charts` ADD  `parent` int(20) NULL   DEFAULT '0';
 ALTER TABLE  `accounting_charts` ADD  `active` int(1) NULL   DEFAULT '1';
 ALTER TABLE  `accounting_charts` ADD  `reconcile` int(1) NULL   DEFAULT '0';


 * @package			        Model
 * @version_number	        4.0.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG)
 */
 
class Accounting_charts_model extends MY_Model {

	protected $id;
	protected $number;
	protected $type;
	protected $title;
	protected $parent;
	protected $active;
	protected $reconcile;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'accounting_charts';
		$this->_short_name = 'accounting_charts';
		$this->_fields = array("id","number","type","title","parent","active","reconcile");
		$this->_required = array("type","title");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

		public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

		public function getId() {
			return $this->id;
		}
	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: number -------------------------------------- 

	/** 
	* Sets a value to `number` variable
	* @access public
	*/

		public function setNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('number', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `number` variable
	* @access public
	*/

		public function getNumber() {
			return $this->number;
		}
	
// ------------------------------ End Field: number --------------------------------------


// ---------------------------- Start Field: type -------------------------------------- 

	/** 
	* Sets a value to `type` variable
	* @access public
	*/

		public function setType($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('type', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `type` variable
	* @access public
	*/

		public function getType() {
			return $this->type;
		}
	
// ------------------------------ End Field: type --------------------------------------


// ---------------------------- Start Field: title -------------------------------------- 

	/** 
	* Sets a value to `title` variable
	* @access public
	*/

		public function setTitle($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('title', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `title` variable
	* @access public
	*/

		public function getTitle() {
			return $this->title;
		}
	
// ------------------------------ End Field: title --------------------------------------


// ---------------------------- Start Field: parent -------------------------------------- 

	/** 
	* Sets a value to `parent` variable
	* @access public
	*/

		public function setParent($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('parent', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `parent` variable
	* @access public
	*/

		public function getParent() {
			return $this->parent;
		}
	
// ------------------------------ End Field: parent --------------------------------------


// ---------------------------- Start Field: active -------------------------------------- 

	/** 
	* Sets a value to `active` variable
	* @access public
	*/

		public function setActive($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('active', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `active` variable
	* @access public
	*/

		public function getActive() {
			return $this->active;
		}
	
// ------------------------------ End Field: active --------------------------------------


// ---------------------------- Start Field: reconcile -------------------------------------- 

	/** 
	* Sets a value to `reconcile` variable
	* @access public
	*/

		public function setReconcile($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('reconcile', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `reconcile` variable
	* @access public
	*/

		public function getReconcile() {
			return $this->reconcile;
		}
	
// ------------------------------ End Field: reconcile --------------------------------------




}

/* End of file Accounting_charts_model.php */
/* Location: ./application/models/Accounting_charts_model.php */
