<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Accounting_bankrecon_items_model Class
 *
 * Manipulates `accounting_bankrecon_items` table on database

CREATE TABLE `accounting_bankrecon_items` (
  `entry_id` int(20) NOT NULL,
  `recon_id` int(20) NOT NULL,
  KEY `entry_id` (`entry_id`)
);

 ALTER TABLE  `accounting_bankrecon_items` ADD  `entry_id` int(20) NOT NULL   ;
 ALTER TABLE  `accounting_bankrecon_items` ADD  `recon_id` int(20) NOT NULL   ;


 * @package			        Model
 * @version_number	        4.0.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG)
 */
 
class Accounting_bankrecon_items_model extends MY_Model {

	protected $entry_id;
	protected $recon_id;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'accounting_bankrecon_items';
		$this->_short_name = 'accounting_bankrecon_items';
		$this->_fields = array("entry_id","recon_id");
		$this->_required = array("entry_id","recon_id");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: entry_id -------------------------------------- 

	/** 
	* Sets a value to `entry_id` variable
	* @access public
	*/

		public function setEntryId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('entry_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `entry_id` variable
	* @access public
	*/

		public function getEntryId() {
			return $this->entry_id;
		}
	
// ------------------------------ End Field: entry_id --------------------------------------


// ---------------------------- Start Field: recon_id -------------------------------------- 

	/** 
	* Sets a value to `recon_id` variable
	* @access public
	*/

		public function setReconId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('recon_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `recon_id` variable
	* @access public
	*/

		public function getReconId() {
			return $this->recon_id;
		}
	
// ------------------------------ End Field: recon_id --------------------------------------




}

/* End of file Accounting_bankrecon_items_model.php */
/* Location: ./application/models/Accounting_bankrecon_items_model.php */
