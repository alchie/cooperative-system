<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Services_shares extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->template_data->set('page_title', 'COOP - Share Capital');
		$this->template_data->set('current_page', 'Share Capital');
		$this->template_data->set('current_uri', 'services_shares');
		$this->template_data->set('current_sub_uri', 'services_shares');

		$this->_isAuth('services', 'shares', 'view');
		
		$this->load->model('Accounting_charts_model');
		$this->load->model('Accounting_transactions_model');
		$this->load->model('Accounting_entries_model');
		$this->load->model('Members_model');
		$this->load->model('Coop_lists_model');
		$this->load->model('Coop_settings_model');
		$this->load->model('Shares_capital_model');
		$this->load->model('Shares_withdrawal_model');
		$this->load->model('Shares_dividend_model');
		$this->load->model('Shares_dividend_item_model');

	}

	private function _member_data($mid) {
		$members = new $this->Members_model;
		$members->setId($mid,true);
		$members->set_select("members.*");
		$members->set_select("(SELECT c.name FROM companies_members cm JOIN companies c ON cm.company_id=c.id WHERE cm.member_id=members.id) as company_name");
		$members->set_select("(SELECT c.id FROM companies_members cm JOIN companies c ON cm.company_id=c.id WHERE cm.member_id=members.id) as company_id");
		return $members->get();
	}

	private function _settings() {

		$fund_surplus = new $this->Coop_settings_model;
		$fund_surplus->setDepartment('ACCOUNTING',true);
		$fund_surplus->setSection('CONFIG',true);
		$fund_surplus->setKey('fund_surplus',true);
		$fs_setting = $fund_surplus->get();		

		$undeposited = new $this->Coop_settings_model;
		$undeposited->setDepartment('ACCOUNTING',true);
		$undeposited->setSection('CONFIG',true);
		$undeposited->setKey('undeposited',true);
		$udf_setting = $undeposited->get();

		$class_name = new $this->Coop_settings_model;
		$class_name->setDepartment('SERVICES',true);
		$class_name->setSection('SHARES',true);
		$class_name->setKey('class_name',true);
		$cls_setting = $class_name->get();

		$capital = new $this->Coop_settings_model;
		$capital->setDepartment('SERVICES',true);
		$capital->setSection('SHARES',true);
		$capital->setKey('capital',true);
		$capital_setting = $capital->get();

		$settings = (object) array(
			'undeposited' => ($udf_setting) ? $udf_setting->value : false,
			'fund_surplus' => ($fs_setting) ? $fs_setting->value : false,
			'class_name' => ($cls_setting) ? $cls_setting->value : false,
			'capital' => ($capital_setting) ? $capital_setting->value : false,
		);

		foreach($settings as $key=>$value) {
			if(!$value) {
				if( $key=='undeposited') {
					redirect(site_url("accounting_settings/configuration") . "?error_code=202&next=" . uri_string() );
				}
				if( $key=='fund_surplus') {
					redirect(site_url("accounting_settings/configuration") . "?error_code=207&next=" . uri_string() );
				}
				if( $key=='capital') {
					redirect(site_url("services_shares/configuration") . "?error_code=203&next=" . uri_string() );
				} 
				if( $key=='class_name') {
					redirect(site_url("services_shares/configuration") . "?error_code=201&next=" . uri_string() );
				}
			}
		}

		return $settings;
	}

	public function index() { 
		$this->overview();
	}

	public function search_name() {

		$members = new $this->Members_model('c');
		if( $this->input->get('q') ) {
			$members->set_where('lastname LIKE "%' . $this->input->get('q') . '%"');
			$members->set_where_or('firstname LIKE "%' . $this->input->get('q') . '%"');
			$members->set_where_or('middlename LIKE "%' . $this->input->get('q') . '%"');
		}
		$this->template_data->set('names', $members->populate());

		$this->load->view('services/shares/shares_search_name', $this->template_data->get_data());
	}

	public function balance($start=0) {
		
		$setting = $this->_settings();
		
		$members = new $this->Members_model;
		$members->set_start($start);
		$members->set_limit(10);
		$members->set_select('members.*');
		$members->set_select("(SELECT SUM(sc.amount) FROM shares_capital sc WHERE sc.member_id=members.id) as shares_capital");
		$members->set_select("(SELECT SUM(sdi.amount) FROM shares_dividend_item sdi WHERE sdi.member_id=members.id) as share_dividend");
		$members->set_select("(SELECT SUM(sw.amount) FROM shares_withdrawal sw WHERE sw.member_id=members.id) as share_withdrawal");
		$members->set_select("((IF((SELECT shares_capital),(SELECT shares_capital),0)) + (IF((SELECT share_dividend),(SELECT share_dividend),0)) - (IF((SELECT share_withdrawal),(SELECT share_withdrawal),0))) as balance");
		$members->set_where('((IF((SELECT SUM(sc.amount) FROM shares_capital sc WHERE sc.member_id=members.id),(SELECT SUM(sc.amount) FROM shares_capital sc WHERE sc.member_id=members.id),0)) + (IF((SELECT SUM(sdi.amount) FROM shares_dividend_item sdi WHERE sdi.member_id=members.id),(SELECT SUM(sdi.amount) FROM shares_dividend_item sdi WHERE sdi.member_id=members.id),0)) - (IF((SELECT SUM(sw.amount) FROM shares_withdrawal sw WHERE sw.member_id=members.id),(SELECT SUM(sw.amount) FROM shares_withdrawal sw WHERE sw.member_id=members.id),0))) > 0');
		$this->template_data->set('balances', $members->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'uri_segment' => 4,
			'base_url' => base_url( $this->config->item('index_page') . "/services_shares/balance"),
			'total_rows' => $members->count_all_results(),
			'per_page' => $members->get_limit(),
		)));

		$this->template_data->set('current_page', 'Share Capital');
		$this->template_data->set('current_sub_uri', 'services_shares_overview');

		$this->load->view( 'services/shares/shares_overview_balances', $this->template_data->get_data() );
	}

	public function overview($mid=null) {
		
		$setting = $this->_settings();
		
		$overview = new $this->Shares_capital_model('sc');
		
		if( $mid ) { 
			$overview->setMemberId( $mid, true );
			$overview->set_select("SUM(sc.amount) as share_capital");
			$overview->set_select("(SELECT SUM(sdi.amount) FROM shares_dividend_item sdi WHERE sdi.member_id={$mid}) as share_dividend");
			$overview->set_select("(SELECT SUM(sw.amount) FROM shares_withdrawal sw WHERE sw.member_id={$mid}) as share_withdrawal");

			$this->template_data->set('member', $this->_member_data($mid));
		} else {
			$overview->set_select("SUM(sc.amount) as share_capital");
			$overview->set_select("(SELECT SUM(sdi.amount) FROM shares_dividend_item sdi) as share_dividend");
			$overview->set_select("(SELECT SUM(sw.amount) FROM shares_withdrawal sw) as share_withdrawal");
		}

		$this->template_data->set('overview', $overview->get());

		$new_capitals = new $this->Accounting_entries_model('ae');
		$new_capitals->setItemType('SHACAP', true);
		$new_capitals->setChartId($setting->capital, true);
		if( $mid ) {
			$new_capitals->setNameId($mid,true);
		}
		$new_capitals->set_join('accounting_transactions at', 'at.id=ae.trn_id');
		$new_capitals->set_join('accounting_receipts ar', 'ar.trn_id=ae.trn_id');
		$new_capitals->set_select('ae.*');
		$new_capitals->set_select('at.*');
		$new_capitals->set_select('ar.*');
		$new_capitals->set_select('ae.id as ae_id');
		$new_capitals->set_select('ae.name_id as ae_name_id');
		$new_capitals->set_select('at.date as share_date');
		$new_capitals->set_select('at.number as share_number');
		$new_capitals->set_select('at.type as entry_type');
		$new_capitals->set_where('((SELECT entry_id FROM  `shares_capital` WHERE entry_id=ae.id) IS NULL)');
		$this->template_data->set('new_capitals', $new_capitals->populate());
		
		$this->template_data->set('current_page', 'Share Capital');
		$this->template_data->set('current_sub_uri', 'services_shares_overview');

		$this->load->view( 'services/shares/shares_overview', $this->template_data->get_data() );
	}

	public function share_capital($mid=0,$start=0) {
		
		$setting = $this->_settings();

		if(( $this->input->post('output') == 'body_wrapper' )||( $this->input->post('output') == 'inner_page' )) {
		
			$capital = new $this->Shares_capital_model;
			$capital->set_start($start);
			$capital->set_limit(10);
			$capital->set_join('coop_names cn', 'cn.id=shares_capital.member_id');
			if( $mid ) { 
				$capital->setMemberId( $mid, true );
				$this->template_data->set('member', $this->_member_data($mid));
			}
			$capital->set_select('cn.full_name');
			$capital->set_select('shares_capital.*');
			$capital->set_select('(SELECT adi.deposit_id FROM accounting_deposits_items adi JOIN accounting_entries ae ON adi.entry_id=ae.id JOIN accounting_transactions at ON ae.trn_id=at.id WHERE ae.trn_id=shares_capital.trn_id LIMIT 1) as deposit_id');
			$capital->set_order('payment_date', 'DESC');
			
			$this->template_data->set('shares_capital', $capital->populate());

			$this->template_data->set('pagination', bootstrap_pagination(array(
				'uri_segment' => 4,
				'base_url' => base_url( $this->config->item('index_page') . "/services_shares/share_capital/{$mid}"),
				'total_rows' => $capital->count_all_results(),
				'per_page' => $capital->get_limit(),
				'ajax'=>true,
			)));

		}
		$new_capitals = new $this->Accounting_entries_model('ae');
		$new_capitals->setItemType('SHACAP', true);
		$new_capitals->setChartId($setting->capital, true);
		if( $mid ) {
			$new_capitals->setNameId($mid,true);
		}
		$new_capitals->set_join('accounting_transactions at', 'at.id=ae.trn_id');
		$new_capitals->set_join('accounting_receipts ar', 'ar.trn_id=ae.trn_id');
		$new_capitals->set_select('ae.*');
		$new_capitals->set_select('at.*');
		$new_capitals->set_select('ar.*');
		$new_capitals->set_select('ae.id as ae_id');
		$new_capitals->set_select('ae.name_id as ae_name_id');
		$new_capitals->set_select('at.date as share_date');
		$new_capitals->set_select('at.number as share_number');
		$new_capitals->set_select('at.type as entry_type');
		$new_capitals->set_where('((SELECT entry_id FROM  `shares_capital` WHERE entry_id=ae.id) IS NULL)');
		$this->template_data->set('new_capitals', $new_capitals->populate());

		$this->template_data->set('current_page', 'Share Capital');
		$this->template_data->set('current_sub_uri', 'services_shares_share_capital');

		$this->load->view( 'services/shares/shares_share_capital', $this->template_data->get_data() );
	}

	public function new_capitals($mid=0, $output='normal', $start=0) {

		$this->template_data->set('member', $this->_member_data( $mid ) );

		$setting = $this->_settings();

		$new_capitals = new $this->Accounting_entries_model('ae');
		$new_capitals->setItemType('SHACAP', true);
		$new_capitals->setChartId($setting->capital, true);
		if( $mid ) {
			$new_capitals->setNameId($mid, true);
		}
		$new_capitals->set_join('accounting_transactions at', 'at.id=ae.trn_id');
		$new_capitals->set_join('accounting_receipts ar', 'ar.trn_id=ae.trn_id');
		$new_capitals->set_join('members m', 'm.id=ae.name_id');
		$new_capitals->set_select('ae.*');
		$new_capitals->set_select('at.*');
		$new_capitals->set_select('ar.*');
		$new_capitals->set_select('m.lastname, m.firstname, m.middlename');
		$new_capitals->set_select('ae.id as ae_id');
		$new_capitals->set_select('ae.name_id as ae_name_id');
		$new_capitals->set_select('at.date as share_date');
		$new_capitals->set_select('at.number as share_number');
		$new_capitals->set_select('at.type as entry_type');
		$new_capitals->set_where('((SELECT entry_id FROM  `shares_capital` WHERE entry_id=ae.id) IS NULL)');
		$new_capitals->set_where('((SELECT id FROM  `members` WHERE id=ae.name_id) IS NOT NULL)');
		$new_capitals->set_limit(5);
		$new_capitals->set_start($start);
		$this->template_data->set('new_capitals', $new_capitals->populate()); 

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'uri_segment' => 5,
			'base_url' => base_url( $this->config->item('index_page') . "/services_shares/new_capitals/{$mid}/{$output}"),
			'total_rows' => $new_capitals->count_all_results(),
			'per_page' => $new_capitals->get_limit(),
			'attributes' => array(
				'class' => 'btn btn-default ajax-modal-inner',
				'data-hide_footer' => 1
				)
		)));

		$this->template_data->set('output', $output);
		$this->load->view('services/shares/shares_new_capitals', $this->template_data->get_data());
	}

	public function add_capital($mid, $aeid, $output='') {

		$this->_isAuth('services', 'shares', 'add');

		$setting = $this->_settings();
		$this->template_data->set('setting', $setting);

		$new_capital = new $this->Accounting_entries_model('ae');
		$new_capital->setId($aeid,true);
		$new_capital->setItemType('SHACAP', true);
		$new_capital->setChartId($setting->capital, true);
		$new_capital->setNameId($mid, true);
		$new_capital->set_join('accounting_transactions at', 'at.id=ae.trn_id');
		$new_capital->set_join('accounting_receipts ar', 'ar.trn_id=ae.trn_id');
		$new_capital->set_join('members m', 'm.id=ae.name_id');
		$new_capital->set_select('ae.*');
		$new_capital->set_select('at.*');
		$new_capital->set_select('ar.*');
		$new_capital->set_select('at.date as receipt_date');
		$new_capital->set_select('at.number as receipt_number');
		$new_capital->set_select('at.id as trn_id');
		$new_capital->set_select('m.lastname, m.firstname, m.middlename');
		$new_capital->set_select('ae.id as ae_id');
		$new_capital->set_select('ae.name_id as ae_name_id');
		$new_capital->set_where('((SELECT entry_id FROM  `lending_payments` WHERE entry_id=ae.id) IS NULL)');
		$new_capital->set_where('((SELECT id FROM  `members` WHERE id=ae.name_id) IS NOT NULL)');

		if( $new_capital->nonEmpty() ) {

			$entry_data = $new_capital->get();
			$this->template_data->set('new_capital', $entry_data); 

			if( $this->input->post() ) {

						$capital = new $this->Shares_capital_model;
						$capital->setMemberId( $mid );
						$capital->setPaymentDate( $entry_data->receipt_date );
						$capital->setReceiptNumber( $entry_data->receipt_number );
						$capital->setAmount( $entry_data->credit );
						$capital->setTrnId( $entry_data->trn_id );
						$capital->setEntryId( $entry_data->ae_id ); 
						if( $capital->insert() ) {
							redirect("services_shares/share_capital/{$mid}");
						}

				$this->postNext("error_code=204");
			}
		}

		$this->template_data->set('output', $output);
		
		$this->template_data->set('member', $this->_member_data($mid));

		$this->template_data->set('current_page', 'Share Capital');
		$this->template_data->set('current_sub_uri', 'services_shares_share_capital');

		$this->load->view( 'services/shares/shares_add_capital', $this->template_data->get_data() );
	}

	public function delete_capital($capital_id) {

		$this->_isAuth('services', 'shares', 'delete');

		$capital = new $this->Shares_capital_model;
		$capital->setId($capital_id, true);
		$capital_data = $capital->get();
		$capital->delete();
		
		redirect("services_shares/share_capital/{$capital_data->member_id}");
	}

	public function withdrawals($mid=0,$start=0) {
		
		$setting = $this->_settings();
		
		if(( $this->input->post('output') == 'body_wrapper' )||( $this->input->post('output') == 'inner_page' )) {

		$withdrawal = new $this->Shares_withdrawal_model;
		$withdrawal->set_start($start);
		$withdrawal->set_limit(10);
		$withdrawal->set_join('coop_names cn', 'cn.id=shares_withdrawal.member_id');
		$withdrawal->set_join('accounting_checks ak', 'ak.trn_id=shares_withdrawal.trn_id');
		$withdrawal->set_select("shares_withdrawal.*");
		$withdrawal->set_select("ak.id as check_id");
		$withdrawal->set_select("(SELECT title FROM accounting_charts WHERE id=shares_withdrawal.bank_id) as bank_account");
		$withdrawal->set_select("cn.full_name");

		if( $mid ) { 
			$withdrawal->setMemberId( $mid, true );
			$this->template_data->set('member', $this->_member_data($mid));
		}
		$withdrawal->set_order('check_date', 'DESC');
		$withdrawal->set_order('check_number', 'DESC');
		$this->template_data->set('shares_withdrawal', $withdrawal->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'uri_segment' => 4,
			'base_url' => base_url( $this->config->item('index_page') . "/services_shares/withdrawals/{$mid}"),
			'total_rows' => $withdrawal->count_all_results(),
			'per_page' => $withdrawal->get_limit(),
		)));

		}

		if( $mid ) { 
			$overview = new $this->Shares_capital_model('sc');
			$overview->setMemberId( $mid, true );
			$overview->set_select("SUM(sc.amount) as share_capital");
			$overview->set_select("(SELECT SUM(sdi.amount) FROM shares_dividend_item sdi WHERE sdi.member_id={$mid}) as share_dividend");
			$overview->set_select("(SELECT SUM(sw.amount) FROM shares_withdrawal sw WHERE sw.member_id={$mid}) as share_withdrawal");
			$overview_data = $overview->get();
			$this->template_data->set('overview',  $overview_data ); 
		}


		$new_withdrawals = new $this->Accounting_entries_model('ae');
		$new_withdrawals->setItemType('SHAWIT', true);
		$new_withdrawals->setChartId($setting->capital, true);
		if( $mid ) {
			$new_withdrawals->setNameId($mid, true);
		}
		$new_withdrawals->set_join('accounting_transactions at', 'at.id=ae.trn_id');
		$new_withdrawals->set_join('accounting_checks ak', 'ak.trn_id=ae.trn_id');
		$new_withdrawals->set_join('members m', 'm.id=ae.name_id');
		$new_withdrawals->set_join('accounting_charts ac', 'ac.id=ak.chart_id');
		$new_withdrawals->set_select('ae.*');
		$new_withdrawals->set_select('at.*');
		$new_withdrawals->set_select('ak.*');
		$new_withdrawals->set_select('at.date as check_date');
		$new_withdrawals->set_select('at.number as check_number');
		$new_withdrawals->set_select('ak.chart_id as bank_id');
		$new_withdrawals->set_select('at.id as trn_id');
		$new_withdrawals->set_select('m.lastname, m.firstname, m.middlename');
		$new_withdrawals->set_select('ae.id as ae_id');
		$new_withdrawals->set_select('ae.name_id as ae_name_id');
		$new_withdrawals->set_select('ae.memo as ae_memo');
		$new_withdrawals->set_select('ac.title as bank_name');
		$new_withdrawals->set_where('((SELECT entry_id FROM  `shares_withdrawal` WHERE entry_id=ae.id) IS NULL)');
		$new_withdrawals->set_where('((SELECT id FROM  `members` WHERE id=ae.name_id) IS NOT NULL)');
		$this->template_data->set('new_withdrawals', $new_withdrawals->populate()); 

		$this->template_data->set('current_page', 'Share Withdrawal');
		$this->template_data->set('current_sub_uri', 'services_shares_withdrawals');

		$this->load->view( 'services/shares/shares_withdrawal', $this->template_data->get_data() );
	}

	public function new_withdrawals($mid=0, $output='') {

		$this->template_data->set('member', $this->_member_data( $mid ) );

		$setting = $this->_settings();

		$new_withdrawals = new $this->Accounting_entries_model('ae');
		$new_withdrawals->setItemType('SHAWIT', true);
		$new_withdrawals->setChartId($setting->capital, true);
		if( $mid ) {
			$new_withdrawals->setNameId($mid, true);
		}
		$new_withdrawals->set_join('accounting_transactions at', 'at.id=ae.trn_id');
		$new_withdrawals->set_join('accounting_checks ak', 'ak.trn_id=ae.trn_id');
		$new_withdrawals->set_join('members m', 'm.id=ae.name_id');
		$new_withdrawals->set_join('accounting_charts ac', 'ac.id=ak.chart_id');
		$new_withdrawals->set_select('ae.*');
		$new_withdrawals->set_select('at.*');
		$new_withdrawals->set_select('ak.*');
		$new_withdrawals->set_select('at.date as check_date');
		$new_withdrawals->set_select('at.number as check_number');
		$new_withdrawals->set_select('ae.chart_id as bank_id');
		$new_withdrawals->set_select('at.id as trn_id');
		$new_withdrawals->set_select('m.lastname, m.firstname, m.middlename');
		$new_withdrawals->set_select('ae.id as ae_id');
		$new_withdrawals->set_select('ae.name_id as ae_name_id');
		$new_withdrawals->set_select('ae.memo as ae_memo');
		$new_withdrawals->set_select('at.type as entry_type');
		$new_withdrawals->set_select('ac.title as bank_name');
		$new_withdrawals->set_where('((SELECT entry_id FROM  `shares_withdrawal` WHERE entry_id=ae.id) IS NULL)');
		$new_withdrawals->set_where('((SELECT id FROM  `members` WHERE id=ae.name_id) IS NOT NULL)');
		$this->template_data->set('new_withdrawals', $new_withdrawals->populate()); 

		$this->template_data->set('output', $output);
		$this->load->view('services/shares/shares_new_withdrawals', $this->template_data->get_data());
	}


	public function add_withdrawal($mid, $aeid, $output='') {

		$this->_isAuth('services', 'shares', 'edit');

		$setting = $this->_settings();
		$this->template_data->set('setting', $setting);

		$new_withdrawal = new $this->Accounting_entries_model('ae');
		$new_withdrawal->setId($aeid,true);
		$new_withdrawal->setItemType('SHAWIT', true);
		$new_withdrawal->setChartId($setting->capital, true);
		$new_withdrawal->setNameId($mid, true);
		$new_withdrawal->set_join('accounting_transactions at', 'at.id=ae.trn_id');
		$new_withdrawal->set_join('accounting_checks ak', 'ak.trn_id=ae.trn_id');
		$new_withdrawal->set_join('members m', 'm.id=ae.name_id');
		$new_withdrawal->set_join('accounting_charts ac', 'ac.id=ak.chart_id');
		$new_withdrawal->set_select('ae.*');
		$new_withdrawal->set_select('at.*');
		$new_withdrawal->set_select('ak.*');
		$new_withdrawal->set_select('at.date as check_date');
		$new_withdrawal->set_select('at.number as check_number');
		$new_withdrawal->set_select('ak.chart_id as bank_id');
		$new_withdrawal->set_select('at.id as trn_id');
		$new_withdrawal->set_select('m.lastname, m.firstname, m.middlename');
		$new_withdrawal->set_select('ae.id as ae_id');
		$new_withdrawal->set_select('ae.name_id as ae_name_id');
		$new_withdrawal->set_select('ae.memo as ae_memo');
		$new_withdrawal->set_select('ac.title as bank_name');
		$new_withdrawal->set_where('((SELECT entry_id FROM  `shares_withdrawal` WHERE entry_id=ae.id) IS NULL)');
		$new_withdrawal->set_where('((SELECT id FROM  `members` WHERE id=ae.name_id) IS NOT NULL)');

		if( $new_withdrawal->nonEmpty() ) {

			$entry_data = $new_withdrawal->get();
			$this->template_data->set('new_withdrawal', $entry_data); 

			if( $this->input->post() ) {

						$withdrawal = new $this->Shares_withdrawal_model;
						$withdrawal->setMemberId( $mid );
						$withdrawal->setBankId( $entry_data->bank_id );
						$withdrawal->setCheckDate( $entry_data->check_date );
						$withdrawal->setCheckNumber( $entry_data->check_number );
						$withdrawal->setAmount( $entry_data->debit );
						$withdrawal->setMemo( $entry_data->ae_memo );
						$withdrawal->setTrnId( $entry_data->trn_id );
						$withdrawal->setEntryId( $entry_data->ae_id );
						if( $withdrawal->insert() ) {
							redirect("services_shares/withdrawals/{$mid}");
						}

				$this->postNext("error_code=204");
			}
		}

		$this->template_data->set('output', $output);
		
		$this->template_data->set('member', $this->_member_data($mid));

		$this->template_data->set('current_page', 'Share Capital');
		$this->template_data->set('current_sub_uri', 'services_shares_share_capital');

		$this->load->view( 'services/shares/shares_add_withdrawal', $this->template_data->get_data() );
	}

	public function delete_withdrawal($withdrawal_id) {

		$this->_isAuth('services', 'shares', 'edit');

		$withdrawal = new $this->Shares_withdrawal_model;
		$withdrawal->setId($withdrawal_id, true);
		$withdrawal_data = $withdrawal->get();
		$withdrawal->delete();

		redirect("services_shares/withdrawals/{$withdrawal_data->member_id}");
	}

	public function dividends($mid=0,$start=0) {

		if(( $this->input->post('output') == 'body_wrapper' )||( $this->input->post('output') == 'inner_page' )) {

			if( $mid ) {
				$this->template_data->set('member', $this->_member_data($mid));
				$div = new $this->Shares_dividend_item_model('sdi');
				$div->setMemberId($mid,true);
				$div->set_start($start);
				$div->set_limit(10);
				$div->set_join('shares_dividend sd', 'sdi.dividend_id=sd.id');
				$div->set_select("sd.id");
				$div->set_select("sd.trn_id");
				$div->set_select("sd.res_date");
				$div->set_select("sd.res_number");
				$div->set_select("sdi.amount as amount");
				$div->set_order('sd.res_number', 'DESC');
				$div_data = $div->populate();
			} else {
				$div = new $this->Shares_dividend_model;
				$div->set_start($start);
				$div->set_limit(10);
				$div->set_select("*");
				$div->set_select('(SELECT SUM(amount) FROM shares_dividend_item WHERE shares_dividend_item.dividend_id=shares_dividend.id) as total_recipients');
				$div->set_order('res_number', 'DESC');
				$div_data = $div->populate();
			}
			$this->template_data->set('dividends', $div_data);

			$this->template_data->set('pagination', bootstrap_pagination(array(
				'uri_segment' => 4,
				'base_url' => base_url( $this->config->item('index_page') . "/services_shares/dividends/{$mid}"),
				'total_rows' => $div->count_all_results(),
				'per_page' => $div->get_limit(),
			)));

		}

		$this->template_data->set('current_page', 'Dividends');
		$this->template_data->set('current_sub_uri', 'services_shares_dividends');

		$this->load->view( 'services/shares/shares_dividends', $this->template_data->get_data() );
	}

	public function add_dividend($output='') {

		$this->_isAuth('services', 'shares', 'edit');

		if($this->input->post()) {
			$this->form_validation->set_rules('res_date', 'Resolution Date', 'trim|required');
			$this->form_validation->set_rules('res_number', 'Resolution Number', 'trim|required|is_unique[shares_dividend.res_number]');
			$this->form_validation->set_rules('amount', 'Amount', 'trim|required');

			if($this->form_validation->run()) {

				$date = date('Y-m-d', strtotime($this->input->post('res_date')));
				$amount = str_replace(',', '', $this->input->post('amount') );

				$div = new $this->Shares_dividend_model;
				$div->setResDate( $date );
				$div->setResNumber($this->input->post('res_number'));
				$div->setAmount($amount);
				$div->insert();

				redirect("services_shares/dividend_recipients/{$div->getId()}");
			}
			$this->postNext();
		}

		$this->template_data->set('output', $output);
		$this->template_data->set('current_page', 'Add Dividends');
		$this->template_data->set('current_sub_uri', 'services_shares_dividends');

		$this->load->view( 'services/shares/shares_add_dividend', $this->template_data->get_data() );
	}

	public function edit_dividend($id, $output='') {

		$this->_isAuth('services', 'shares', 'edit');

		$div = new $this->Shares_dividend_model;
		$div->setId($id,true);

		if($this->input->post()) {

			$this->form_validation->set_rules('res_date', 'Resolution Date', 'trim|required');
			$this->form_validation->set_rules('res_number', 'Resolution Number', 'trim|required');
			$this->form_validation->set_rules('amount', 'Amount', 'trim|required');

			if($this->form_validation->run()) {
				
				$date = date('Y-m-d', strtotime($this->input->post('res_date')));
				$amount = str_replace(',', '', $this->input->post('amount') );

				$div->setResDate( $date );
				$div->setResNumber($this->input->post('res_number'));
				$div->setAmount($amount);

				$div->update();
			}
			$this->postNext();
		}

		$this->template_data->set('dividend', $div->get());

		$this->template_data->set('output', $output);
		$this->template_data->set('current_page', 'Add Dividends');
		$this->template_data->set('current_sub_uri', 'services_shares_dividends');

		$this->load->view( 'services/shares/shares_edit_dividend', $this->template_data->get_data() );
	}

	public function delete_dividend($id, $output='') {

		$this->_isAuth('services', 'shares', 'edit');

		$div = new $this->Shares_dividend_model;
		$div->setId($id,true);
		$div_data = $div->get();

		$trn = new $this->Accounting_transactions_model;
		$trn->setId($div_data->trn_id,true);
		$trn->delete();

		$entries = new $this->Accounting_entries_model;
		$entries->setTrnId($div_data->trn_id,true);
		$entries->delete();

		$div_items = new $this->Shares_dividend_item_model;
		$div_items->setDividendId( $id, true );
		$div_items->delete();

		$div->delete();
		
		redirect("services_shares/dividends");

	}

	public function dividend_recipients($id) {

		$setting = $this->_settings();

		$div = new $this->Shares_dividend_model;
		$div->setId($id,true);
		$div->set_select('*');
		$div->set_select('(SELECT SUM(amount) FROM shares_dividend_item WHERE shares_dividend_item.dividend_id=shares_dividend.id) as total_recipients');
		$this->template_data->set('dividend', $div->get());

		$div_items = new $this->Shares_dividend_item_model('sdi');
		$div_items->setDividendId($id,true);
		$div_items->set_join('coop_names cn', 'cn.id=sdi.member_id');
		$div_items->set_join('shares_dividend sd', 'sd.id=sdi.dividend_id');
		$div_items->set_select('sdi.*');
		$div_items->set_select('cn.full_name');
		$div_items->set_select('sd.trn_id');
		$div_items->set_select("(SELECT COUNT(*) FROM accounting_entries ae WHERE ae.trn_id = sd.trn_id AND ae.chart_id={$setting->capital} AND ae.name_id=sdi.member_id) as capital");
		$div_items->set_select("(SELECT COUNT(*) FROM accounting_entries ae WHERE ae.trn_id = sd.trn_id AND ae.chart_id={$setting->fund_surplus} AND ae.name_id=sdi.member_id) as fund_surplus");
		$this->template_data->set('dividend_items', $div_items->populate());

		$this->template_data->set('current_sub_uri', 'services_shares_dividends');

		$this->load->view( 'services/shares/shares_dividend_recipients', $this->template_data->get_data() );
	}

	public function add_recipient($id,$output='') {

		$this->_isAuth('services', 'shares', 'edit');

		$div = new $this->Shares_dividend_model('sd');
		$div->setId($id,true);
		$div->set_select('sd.*');
		$div->set_select("( (SELECT sd.amount) - (IF( (SELECT SUM(sdi.amount) FROM shares_dividend_item sdi WHERE sdi.dividend_id=sd.id), (SELECT SUM(sdi.amount) FROM shares_dividend_item sdi WHERE sdi.dividend_id=sd.id), 0)) ) as balance");
		$div_data = $div->get();

		if($this->input->post()) {
			
			$this->form_validation->set_rules('member_id', 'Member', 'trim|required');
			$this->form_validation->set_rules('amount', 'Amount', 'trim|required');

			if($this->form_validation->run()) {
				$amount = ($this->input->post('amount') > number_format($div_data->balance,2,'.','')) ? number_format($div_data->balance,2,'.','') : $this->input->post('amount');
				$div_item = new $this->Shares_dividend_item_model;
				$div_item->setDividendId( $id, true );
				$div_item->setMemberId($this->input->post('member_id'),true);
				$div_item->setAmount( $amount );
				if( !$div_item->nonEmpty() ) {
					$div_item->insert();

					if( $div_data->trn_id ) {

						$setting = $this->_settings();

						// debit
						$debit = new $this->Accounting_entries_model;
						$debit->setTrnId( $div_data->trn_id,true );
						$debit->setChartId( $setting->fund_surplus,true );
						$debit->setDebit( $amount );
						$debit->setClass( $setting->class_name );
						$debit->setNameId( $this->input->post('member_id'), true );
						$debit->setItemNumber(0);

						// credit
						$credit = new $this->Accounting_entries_model;
						$credit->setTrnId( $div_data->trn_id,true );
						$credit->setChartId( $setting->capital,true );
						$credit->setCredit( $amount );
						$credit->setClass( $setting->class_name );
						$credit->setNameId( $this->input->post('member_id'), true );
						$credit->setItemNumber(1);

						if( !$debit->nonEmpty() && !$credit->nonEmpty() ) {
							$debit->insert();
							$credit->insert();
						}
						
					}
				}
			}
			$this->postNext();
		}

		$this->template_data->set('dividend', $div_data);

		$this->template_data->set('output', $output);
		$this->template_data->set('current_sub_uri', 'services_shares_dividends');

		$this->load->view( 'services/shares/shares_add_recipient', $this->template_data->get_data() );
	}

	public function edit_recipient($id,$output='') {

		$this->_isAuth('services', 'shares', 'edit');

		$div_item = new $this->Shares_dividend_item_model('sdi');
		$div_item->setId($id,true);
		$div_item->set_join('members m', 'm.id=sdi.member_id');
		$div_item->set_join('shares_dividend sd', 'sd.id=sdi.dividend_id');
		$div_item->set_select('sdi.*');
		$div_item->set_select('sd.trn_id');
		$div_item->set_select('m.lastname, m.firstname, m.middlename');
		$div_item->set_select("((SELECT sd.amount FROM shares_dividend sd WHERE sd.id=sdi.dividend_id) - (IF((SELECT SUM(sdi2.amount) FROM shares_dividend_item sdi2 WHERE sdi2.dividend_id=sdi.dividend_id AND sdi2.member_id != sdi.member_id), (SELECT SUM(sdi2.amount) FROM shares_dividend_item sdi2 WHERE sdi2.dividend_id=sdi.dividend_id AND sdi2.member_id != sdi.member_id), 0))) as balance");
		
		$div_data = $div_item->get();

		if($this->input->post()) {

			$this->form_validation->set_rules('member_id', 'Member', 'trim|required');
			$this->form_validation->set_rules('amount', 'Amount', 'trim|required');

			if($this->form_validation->run()) {
				$amount = ($this->input->post('amount') > number_format($div_data->balance,2,'.','')) ? number_format($div_data->balance,2,'.','') : $this->input->post('amount');
				
				$div_item = new $this->Shares_dividend_item_model('sdi');
				$div_item->setDividendId( $div_data->dividend_id, true, false);
				$div_item->setMemberId( $this->input->post('member_id'), true);
				$div_item->setAmount( $amount );
				if( $div_item->nonEmpty() ) {
					$div_item->set_exclude('member_id');
				}
				$div_item->clear_where();
				$div_item->setId($id,true,false);
				
					if( $div_data->trn_id ) {

						$setting = $this->_settings();

						// debit
						$debit = new $this->Accounting_entries_model;
						$debit->setTrnId( $div_data->trn_id,true,false );
						$debit->setChartId( $setting->fund_surplus, true,false );
						$debit->setDebit( $amount,false, true );
						$debit->setNameId( $this->input->post('member_id'), false, true );
						$debit->set_where('name_id', $div_data->member_id);

						// credit
						$credit = new $this->Accounting_entries_model;
						$credit->setTrnId( $div_data->trn_id,true,false );
						$credit->setChartId( $setting->capital,true,false );
						$credit->setCredit( $amount, false, true );
						$credit->setNameId( $this->input->post('member_id'),false,true );
						$credit->set_where('name_id', $div_data->member_id);

						$debit->update();
						$credit->update();
						
					}
					$div_item->update();

			}
			$this->postNext();
		}
		
		$this->template_data->set('dividend', $div_data);
		
		$this->template_data->set('output', $output);
		$this->template_data->set('current_sub_uri', 'services_shares_dividends');

		$this->load->view( 'services/shares/shares_edit_recipient', $this->template_data->get_data() );
	}

	public function delete_recipient($id,$output='') {

		$this->_isAuth('services', 'shares', 'edit');

		$div = new $this->Shares_dividend_item_model;
		$div->setId($id,true);
		$div->set_join('shares_dividend sd', 'sd.id=shares_dividend_item.dividend_id');
		$div_data = $div->get(); 

		$setting = $this->_settings();

		// debit
		$debit = new $this->Accounting_entries_model;
		$debit->setTrnId( $div_data->trn_id,true );
		$debit->setChartId( $setting->fund_surplus, true );
		$debit->setNameId( $div_data->member_id, true );

		// credit
		$credit = new $this->Accounting_entries_model;
		$credit->setTrnId( $div_data->trn_id,true );
		$credit->setChartId( $setting->capital,true );
		$credit->setNameId( $div_data->member_id,true );

		//if( $debit->nonEmpty() && $credit->nonEmpty() ) {
			$debit->delete();
			$credit->delete();
		//}

		$div->delete();
		redirect("services_shares/dividend_recipients/{$div_data->dividend_id}");
	}

	public function declare_dividend($id,$output='') {

		$this->_isAuth('services', 'shares', 'edit');
		
		$div = new $this->Shares_dividend_model('sd');
		$div->setId($id,true);
		$div->set_select('sd.*');
		$div->set_select("( (SELECT sd.amount) - (IF( (SELECT SUM(sdi.amount) FROM shares_dividend_item sdi WHERE sdi.dividend_id=sd.id), (SELECT SUM(sdi.amount) FROM shares_dividend_item sdi WHERE sdi.dividend_id=sd.id), 0)) ) as balance");
		$div_data = $div->get();

		if( round($div_data->balance) == 0 ) {
				$trn = new $this->Accounting_transactions_model;
				$trn->setDept('SERVICES');
				$trn->setSect('SHARES');
				$trn->setType('DIVIDEND');
				$trn->setDate( $div_data->res_date );
				$trn->setNumber( $div_data->res_number );
				$trn->setAmount( $div_data->amount );

				if( $div_data->trn_id == NULL || $div_data->trn_id == '') {
					if( $trn->insert() ) {
						$div->setTrnId($trn->getId(),false,true);
						$div->update();	

						$setting = $this->_settings();

						$div_item = new $this->Shares_dividend_item_model('sdi');
						$div_item->setDividendId($id,true);

						foreach($div_item->populate() as $dItem) {

							// debit
							$debit = new $this->Accounting_entries_model;
							$debit->setTrnId( $trn->getId() );
							$debit->setChartId( $setting->fund_surplus );
							$debit->setDebit( $dItem->amount );
							$debit->setClass( $setting->class_name );
							$debit->setNameId( $dItem->member_id  );
							$debit->setItemNumber(0);
							$debit->insert();

							// credit
							$credit = new $this->Accounting_entries_model;
							$credit->setTrnId( $trn->getId() );
							$credit->setChartId( $setting->capital );
							$credit->setCredit( $dItem->amount );
							$credit->setClass( $setting->class_name );
							$credit->setNameId( $dItem->member_id  );
							$credit->setItemNumber(1);
							$credit->insert();

						}
					}
				} 
		}

		redirect("services_shares/dividend_recipients/{$id}");

	}
	
	public function save_configuration() {

		$this->_isAuth('system', 'settings', 'add');
		$this->_isAuth('system', 'settings', 'edit');

		if($this->input->post()) {
			$old_values = array();
			$new_values = array();

			foreach( $this->input->post() as $key=>$value ) {
				$setting = new $this->Coop_settings_model;
				$setting->setDepartment('SERVICES',true);
				$setting->setSection('SHARES',true);
				$setting->setKey($key,true);
				$setting->setValue($value);
				if($setting->nonEmpty()) {
					$setting->set_exclude('id');
					$results = $setting->getResults();
					$old_values[$results->key] = $results->value;
					$new_values[$key] = $value;
					$setting->update();
				} else {
					$setting->insert();
				}
			}

			if( $old_values['capital'] != $new_values['capital'] ) {
				$entries = new $this->Accounting_entries_model;
				$entries->setChartId($new_values['capital'],false,true);
				$entries->set_where( "accounting_entries.chart_id", $old_values['capital'] );
				$entries->set_where("(((SELECT id FROM accounting_transactions at WHERE at.dept='SERVICES' AND at.sect='SHARES' AND at.type='CAPITAL' AND at.id=accounting_entries.trn_id) = accounting_entries.trn_id)");
				$entries->set_where_or("((SELECT id FROM accounting_transactions at WHERE at.dept='SERVICES' AND at.sect='SHARES' AND at.type='DIVIDEND' AND at.id=accounting_entries.trn_id) = accounting_entries.trn_id))");
				$entries->update();
			}

			if( $old_values['class_name'] != $new_values['class_name'] ) {
				$entries = new $this->Accounting_entries_model;
				$entries->setClass($new_values['class_name'],false,true);
				$entries->set_where( "accounting_entries.class", $old_values['class_name'] );
				$entries->set_where("(((SELECT id FROM accounting_transactions at WHERE at.dept='SERVICES' AND at.sect='SHARES' AND at.type='CAPITAL' AND at.id=accounting_entries.trn_id) = accounting_entries.trn_id)");
				$entries->set_where_or("((SELECT id FROM accounting_transactions at WHERE at.dept='SERVICES' AND at.sect='SHARES' AND at.type='DIVIDEND' AND at.id=accounting_entries.trn_id) = accounting_entries.trn_id))");
				$entries->update();
			}

			if( $this->input->get('next') ) {
				redirect( $this->input->get('next') );
			} else {
				redirect( "services_shares/configuration" );
			}
		}
	}

	public function configuration() {

		$this->_isAuth('system', 'settings', 'view');
		$this->_isAuth('system', 'settings', 'add');
		$this->_isAuth('system', 'settings', 'edit');
		$this->_isAuth('services', 'shares', 'edit');

		$settings = new $this->Coop_settings_model;
		$settings->setDepartment('SERVICES',true);
		$settings->setSection('SHARES',true);
		$this->template_data->set('settings', $settings->populate());

		$main_charts = new $this->Accounting_charts_model('ac');
		$main_charts->set_order('ac.type', 'ASC');
		$main_charts->set_order('ac.number', 'ASC');
		$main_charts->set_select('ac.id,ac.title,ac.type');
		$main_charts->set_limit(0);
		$main_charts->set_where('ac.type', '090_BS_EQU_CAP');
		$main_charts->set_where('((SELECT value FROM coop_settings cs WHERE `cs`.`department`="ACCOUNTING" AND `cs`.`section`="CONFIG" AND `cs`.`key`="fund_surplus") <> ac.id)');
		$charts_data = $main_charts->recursive('ac.parent', 0);
		$this->template_data->set('account_titles', $charts_data);

		$list = new $this->Coop_lists_model;
		$list->setDept('ACCOUNTING',true);
		$list->setType('CLASS',true);
		$list->setActive(1,true);
		$list->set_select('id,value');
		$list->set_limit(0);
		$list_data = $list->recursive('parent', 0);
		$this->template_data->set('class_list', $list_data);

		$this->load->view('services/shares/shares_configuration', $this->template_data->get_data());
	}

	public function ajax($action='') {
		$results = array();
		switch($action) {
			case 'add_recipient':
				$members = new $this->Members_model;
				if( $this->input->get('term') ) {
					$members->set_where('lastname LIKE "%' . $this->input->get('term') . '%"');
					$members->set_where_or('firstname LIKE "%' . $this->input->get('term') . '%"');
					$members->set_where_or('middlename LIKE "%' . $this->input->get('term') . '%"');
				}
				$data = array();
				foreach($members->populate() as $member) {
					$data[] = array(
						'label' => $member->lastname . ", " . $member->firstname . " " .$member->middlename,
						'id' => $member->id
						);
				}
				$results = $data;
			break;

			case 'member_data':
				$members = new $this->Members_model;
				if( $this->input->get('term') ) {
					$members->set_where('lastname LIKE "%' . $this->input->get('term') . '%"');
					$members->set_where_or('firstname LIKE "%' . $this->input->get('term') . '%"');
					$members->set_where_or('middlename LIKE "%' . $this->input->get('term') . '%"');
				}
				$data = array();
				foreach($members->populate() as $member) {
					$data[] = array(
						'label' => $member->lastname . ", " . $member->firstname . " " .$member->middlename,
						'id' => $member->id,
						'redirect'=> site_url("services_shares/overview/{$member->id}"),
						);
				}
				$results = $data;
			break;

			case 'change_member':
				$members = new $this->Members_model;
				if( $this->input->get('term') ) {
					$members->set_where('lastname LIKE "%' . $this->input->get('term') . '%"');
					$members->set_where_or('firstname LIKE "%' . $this->input->get('term') . '%"');
					$members->set_where_or('middlename LIKE "%' . $this->input->get('term') . '%"');
				}
				$data = array();
				switch( $this->input->get('sub_uri') ) {
					case 'services_shares_share_capital':
						$redirect_uri = "services_shares/share_capital/";
					break;
					case 'services_shares_dividends':
						$redirect_uri = "services_shares/dividends/";
					break;
					case 'services_shares_withdrawals':
						$redirect_uri = "services_shares/withdrawals/";
					break;
					case 'services_shares_overview':
					default:
						$redirect_uri = "services_shares/overview/";
					break;

				}
				$members->set_order('lastname', 'ASC');
				$members->set_order('firstname', 'ASC');
				$members->set_order('middlename', 'ASC');
				foreach($members->populate() as $member) {
					$data[] = array(
						'label' => $member->lastname . ", " . $member->firstname . " " .$member->middlename,
						'id' => $member->id,
						'redirect'=> site_url($redirect_uri . $member->id),
						);
				}
				$results = $data;
			break;
		}
		$this->output
        ->set_content_type('application/json')
        ->set_output(json_encode( $results ));
	}

}
