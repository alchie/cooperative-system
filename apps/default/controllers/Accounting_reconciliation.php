<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Accounting_reconciliation extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->template_data->set('page_title', 'COOP - Bank Reconciliation');
		$this->template_data->set('current_page', 'Bank Reconciliation');
		$this->template_data->set('current_uri', 'accounting_reconciliation');
		
		$this->_isAuth('accounting', 'reconciliation', 'view');
		
		$this->template_data->set('coa_types', unserialize(CHART_ACCOUNT_TYPES));

		$this->load->model('Accounting_charts_model');
		$this->load->model('Accounting_entries_model');
		$this->load->model('Accounting_bankrecon_model');
		$this->load->model('Accounting_bankrecon_items_model');
	}

	public function index($start=0) {

		if(( $this->input->post('output') == 'body_wrapper' )||( $this->input->post('output') == 'inner_page' )) {

		$recons = new $this->Accounting_bankrecon_model('ab');
		$recons->set_join('accounting_charts ac', 'ac.id=ab.chart_id');
		$recons->set_select('ab.*');
		$recons->set_select('ac.title as bank_name');
		$recons->set_order('ab.date_end', 'DESC');
		$recons->set_select("(SELECT COUNT(*) FROM `accounting_bankrecon_items` abi WHERE abi.recon_id=ab.id) as recon_items");
		$recons->set_start($start);

		$this->template_data->set('recons', $recons->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'base_url' => base_url( $this->config->item('index_page') . "/accounting_reconciliation/index"),
			'total_rows' => $recons->count_all_results(),
			'per_page' => $recons->get_limit(),
			'ajax' => true,
		)));

		}

		$bank_accounts = new $this->Accounting_charts_model;
		$bank_accounts->setReconcile(1,true);
		$bank_accounts->set_order('type', 'ASC');
		$bank_accounts->set_order('number', 'ASC');
		$bank_accounts->set_where('type LIKE "001_BS_ASS_CSB"');
		$bank_accounts_data = $bank_accounts->populate();
		$this->template_data->set('bank_accounts', $bank_accounts_data);

		$this->load->view('accounting/reconciliation/reconciliation', $this->template_data->get_data());
	}

	public function add_recon($bank_id, $output='') {

		$this->_isAuth('accounting', 'reconciliation', 'add');

		$bank_account = new $this->Accounting_charts_model;
		$bank_account->setReconcile(1,true);
		$bank_account->setId($bank_id,true);
		$bank_account->set_where('type LIKE "001_BS_ASS_CSB"');
		$bank_account_data = $bank_account->get();

		if( !$bank_account_data ) {
			redirect("accounting_reconciliation");
		}

		$this->template_data->set('bank_account', $bank_account_data);

		if($this->input->post()) {
			$this->form_validation->set_rules('date_end', 'Statement End', 'trim|required');
			$this->form_validation->set_rules('balance_end', 'Ending balance', 'trim|required');
			if($this->form_validation->run()) {

				$date_end = date('Y-m-d', strtotime($this->input->post('date_end')));
				$balance_end = str_replace(',', '', $this->input->post('balance_end'));

				$recon_exist = new $this->Accounting_bankrecon_model('ab');
				$recon_exist->setChartId( $bank_account_data->id, true );
				$recon_exist->set_select("(DATE_FORMAT(ab.date_end, '%c')) as month");
				$recon_exist->set_select("(DATE_FORMAT(ab.date_end, '%e')) as day");
				$recon_exist->set_select("(DATE_FORMAT(ab.date_end, '%Y')) as year");
				$recon_exist->set_select("(DATE_FORMAT(ab.date_end, '%j')) as day_of_year");
				$recon_exist->set_where("(DATE_FORMAT(ab.date_end, '%c') = DATE_FORMAT('{$date_end}', '%c'))");
				$recon_exist->set_where("(DATE_FORMAT(ab.date_end, '%e') >= DATE_FORMAT('{$date_end}', '%e'))");
				$recon_exist->set_where("(DATE_FORMAT(ab.date_end, '%Y') = DATE_FORMAT('{$date_end}', '%Y'))");
				
				if( $recon_exist->nonEmpty() ) {
					$_existing = $recon_exist->getResults();
					redirect("accounting_reconciliation/recon_items/{$_existing->id}");
				}

				$recon = new $this->Accounting_bankrecon_model;
				$recon->setChartId( $bank_account_data->id );
				$recon->setMemo($this->input->post('memo'));
				$recon->setDateEnd($date_end);
				$recon->setBalanceEnd($balance_end);

				if( $recon->insert() ) {
					redirect("accounting_reconciliation/recon_items/{$recon->getId()}");
				}
			} else {
				redirect( site_url("accounting_reconciliation") . "?error_code=604");
			}
		}

		$last_recon = new $this->Accounting_bankrecon_model;
		$last_recon->setChartId( $bank_account_data->id );
		$last_recon->set_order('date_end', 'DESC');
		$this->template_data->set('last_recon', $last_recon->get());

		$this->template_data->set('output', $output);
		$this->load->view('accounting/reconciliation/reconciliation_add', $this->template_data->get_data());
	}

	public function edit_recon($id, $output="") {

		$this->_isAuth('accounting', 'reconciliation', 'edit');

		$recon = new $this->Accounting_bankrecon_model;
		$recon->setId($id,true);
		$recon->set_select("accounting_bankrecon.*");
		$recon->set_select("(SELECT COUNT(*) FROM `accounting_bankrecon_items` abi WHERE abi.recon_id=accounting_bankrecon.id) as recon_items");
		$recon_data = $recon->get();

		if($this->input->post()) {
			if( $recon_data->recon_items == 0 ) {
				$this->form_validation->set_rules('date_end', 'Statement End', 'trim|required');
				$this->form_validation->set_rules('balance_end', 'Ending balance', 'trim|required');
			}
			$this->form_validation->set_rules('memo', 'Memo', 'trim');
			if($this->form_validation->run()) {

				$date_end = date('Y-m-d', strtotime($this->input->post('date_end')));
				$balance_end = str_replace(',', '', $this->input->post('balance_end'));

				if( $recon_data->recon_items == 0 ) {
					$recon->setDateEnd($date_end, false, true);
					$recon->setBalanceEnd($balance_end, false, true);
				}
				$recon->setMemo($this->input->post('memo'), false, true);
				$recon->update();
				
			}

			$this->postNext();
		}

		$recon_data = $recon->get();
		$this->template_data->set('recon', $recon_data);

		$last_recon = new $this->Accounting_bankrecon_model;
		$last_recon->setChartId( $recon_data->chart_id );
		$last_recon->set_where('date_end <', $recon_data->date_end);
		$last_recon->set_order('date_end', 'DESC');
		$this->template_data->set('last_recon', $last_recon->get());

		$adv_recon = new $this->Accounting_bankrecon_model;
		$adv_recon->setChartId( $recon_data->chart_id );
		$adv_recon->set_where('date_end >', $recon_data->date_end);
		$adv_recon->set_order('date_end', 'ASC');
		$adv_recon_data = $adv_recon->get();
		$this->template_data->set('adv_recon', $adv_recon_data);

		$this->template_data->set('output', $output);

		$this->load->view('accounting/reconciliation/reconciliation_edit', $this->template_data->get_data());
	}

	public function delete_recon($id) {

		$this->_isAuth('accounting', 'reconciliation', 'delete');

		$recon = new $this->Accounting_bankrecon_model;
		$recon->setId($id,true);
		$recon_data = $recon->get();

		$adv_recon = new $this->Accounting_bankrecon_model;
		$adv_recon->setChartId( $recon_data->chart_id );
		$adv_recon->set_where('date_end >', $recon_data->date_end);
		$adv_recon->set_order('date_end', 'ASC');
		$adv_recon_data = $adv_recon->get();	

		$recon_item = new $this->Accounting_bankrecon_items_model;
		$recon_item->setReconId($id,true);

		if( !$adv_recon_data ) {
			$recon->delete();
			$recon_item->delete();
			redirect("accounting_reconciliation");
		} else {
			redirect(site_url("accounting_reconciliation/recon_items/{$id}") . "?error_code=601");
		}

	}

	public function reset_recon($id) {

		$this->_isAuth('accounting', 'reconciliation', 'edit');

		$recon = new $this->Accounting_bankrecon_model;
		$recon->setId($id,true);
		$recon_data = $recon->get();

		$adv_recon = new $this->Accounting_bankrecon_model;
		$adv_recon->setChartId( $recon_data->chart_id );
		$adv_recon->set_where('date_end >', $recon_data->date_end);
		$adv_recon->set_order('date_end', 'ASC');
		$adv_recon_data = $adv_recon->get();	

		if( !$adv_recon_data ) {
			$recon_item = new $this->Accounting_bankrecon_items_model;
			$recon_item->setReconId($recon_data->id,true);
			$recon_item->delete();
			redirect(site_url("accounting_reconciliation/recon_items/{$id}") . "?error_code=602");
		} 
		redirect(site_url("accounting_reconciliation/recon_items/{$id}") . "?error_code=603");

	}

	public function recon_items($id) {

		$recon = new $this->Accounting_bankrecon_model('ab');
		$recon->setId($id,true);
		$recon->set_select("ab.*");
		$recon->set_select("(SELECT SUM(ae.debit) FROM accounting_bankrecon_items abi JOIN accounting_entries ae ON abi.entry_id=ae.id WHERE abi.recon_id=ab.id) as total_deposit");
		$recon->set_select("(SELECT SUM(ae.credit) FROM accounting_bankrecon_items abi JOIN accounting_entries ae ON abi.entry_id=ae.id WHERE abi.recon_id=ab.id) as total_disburse");
		$recon->set_select("(SELECT COUNT(*) FROM `accounting_bankrecon_items` abi WHERE abi.recon_id=ab.id) as recon_items");
		$recon_data = $recon->get(); 

		$this->template_data->set('recon', $recon_data); 

		$last_recon = new $this->Accounting_bankrecon_model;
		$last_recon->setChartId( $recon_data->chart_id );
		$last_recon->set_where('date_end <', $recon_data->date_end);
		$last_recon->set_order('date_end', 'DESC');
		$last_recon_data = $last_recon->get();
		$this->template_data->set('last_recon', $last_recon_data);

		if( $recon_data->recon_items == 0 ) {

			$this->_isAuth('accounting', 'reconciliation', 'add');

			if($this->input->post()) {
				$this->form_validation->set_rules('item[]', 'Items', 'trim|required');
				if($this->form_validation->run()) {
					$recon_item = new $this->Accounting_bankrecon_items_model;
					$recon_item->setReconId($id,true);

					if( $recon_item->nonEmpty() ) {
						$_existing = $recon_items->getResults();
						redirect("accounting_reconciliation/recon_items/{$id}");
					}

					$entries2 = new $this->Accounting_entries_model('ae');
					$entries2->setChartId($recon_data->chart_id,true);
					$entries2->set_select("(SUM(IF(ae.credit,ae.credit,0))) as balance_credit");
					$entries2->set_select("(SUM(IF(ae.debit,ae.debit,0))) as balance_debit");
					$entries2->set_where_in('trn_id', $this->input->post('item'));
					$entries2_data = $entries2->get();

					$balane_beg = ($last_recon_data) ? $last_recon_data->balance_end : 0;
					$total_difference = (($recon_data->balance_end - $balane_beg) + $entries2_data->balance_credit) - $entries2_data->balance_debit;
					if( intval( number_format( $total_difference ) ) != 0 ) {
						redirect("accounting_reconciliation/recon_items/{$id}");
					}

					foreach( $this->input->post('item') as $trn_id) {
						$entries = new $this->Accounting_entries_model('ae');
						$entries->setChartId($recon_data->chart_id,true);
						$entries->setTrnId($trn_id,true);
						$entries->set_limit(0);
						foreach($entries->populate() as $item) {
							$recon_item = new $this->Accounting_bankrecon_items_model;
							$recon_item->setEntryId($item->id);
							$recon_item->setReconId($id);
							$recon_item->insert();
						}
					}

					redirect("accounting_reconciliation/recon_items/{$id}");
					
				}
			}

			$recon_data = $recon->get(); 
			$this->template_data->set('recon', $recon_data); 


				$disbursements = new $this->Accounting_entries_model('ae');
				$disbursements->setChartId($recon_data->chart_id,true);
				$disbursements->set_select("ae.*");
				$disbursements->set_select("SUM(ae.credit) as total_amount");
				$disbursements->set_limit(0);
				$disbursements->set_where('ae.credit > 0');
				$disbursements->set_group_by('ae.trn_id');

				$disbursements->set_join('accounting_transactions at', 'at.id=ae.trn_id');
				$disbursements->set_select("at.number");
				$disbursements->set_select("at.type as trn_type");
				$disbursements->set_select("at.date as date");
				$disbursements->set_order('at.date', 'ASC');

				$disbursements->set_join('accounting_bankrecon_items abi', 'abi.entry_id=ae.id');
				$disbursements->set_where('(abi.recon_id IS NULL');
				$disbursements->set_where_or("abi.recon_id='$id')");
				$disbursements->set_select("abi.recon_id");
				$this->template_data->set('disbursements', $disbursements->populate());

				$deposits = new $this->Accounting_entries_model('ae');
				$deposits->setChartId($recon_data->chart_id,true);
				$deposits->set_select("ae.*");
				$deposits->set_select("SUM(ae.debit) as total_amount");
				$deposits->set_limit(0);
				$deposits->set_where('ae.debit > 0');
				$deposits->set_group_by('ae.trn_id');

				$deposits->set_join('accounting_transactions at', 'at.id=ae.trn_id');
				$deposits->set_select("at.number");
				$deposits->set_select("at.type as trn_type");
				$deposits->set_select("at.date as date");
				$deposits->set_order('at.date', 'ASC');

				$deposits->set_join('accounting_bankrecon_items abi', 'abi.entry_id=ae.id');
				$deposits->set_where('(abi.recon_id IS NULL');
				$deposits->set_where_or("abi.recon_id='$id')");
				$deposits->set_select("abi.recon_id");

				$this->template_data->set('deposits', $deposits->populate());

				$this->load->view('accounting/reconciliation/reconciliation_items', $this->template_data->get_data());
		} else {

				$deposits = new $this->Accounting_bankrecon_items_model('abi');
				$deposits->setReconId($id,true);
				$deposits->set_join('accounting_entries ae', 'abi.entry_id=ae.id');
				$deposits->set_join('accounting_transactions at', 'at.id=ae.trn_id');
				$deposits->set_limit(0);
				$deposits->set_where('ae.debit > 0');
				$deposits->set_select("ae.*");
				$deposits->set_select("SUM(ae.debit) as total_amount");
				$deposits->set_select("at.number");
				$deposits->set_select("at.memo");
				$deposits->set_select("at.type as trn_type");
				$deposits->set_select("at.date as date");
				$deposits->set_order('at.date', 'ASC');
				$deposits->set_group_by('ae.trn_id');
				$this->template_data->set('deposits', $deposits->populate());

				$disbursements = new $this->Accounting_bankrecon_items_model('abi');
				$disbursements->setReconId($id,true);
				$disbursements->set_join('accounting_entries ae', 'abi.entry_id=ae.id');
				$disbursements->set_join('accounting_transactions at', 'at.id=ae.trn_id');
				$disbursements->set_limit(0);
				$disbursements->set_where('ae.credit > 0');
				$disbursements->set_select("ae.*");
				$disbursements->set_select("SUM(ae.credit) as total_amount");
				$disbursements->set_select("at.number");
				$disbursements->set_select("at.memo");
				$disbursements->set_select("at.type as trn_type");
				$disbursements->set_select("at.date as date");
				$disbursements->set_order('at.date', 'ASC');
				$disbursements->set_group_by('ae.trn_id');
				$this->template_data->set('disbursements', $disbursements->populate());

				$adv_recon = new $this->Accounting_bankrecon_model;
				$adv_recon->setChartId( $recon_data->chart_id );
				$adv_recon->set_where('date_end >', $recon_data->date_end);
				$adv_recon->set_order('date_end', 'ASC');
				$adv_recon_data = $adv_recon->get();
				$this->template_data->set('adv_recon', $adv_recon_data);

				$this->load->view('accounting/reconciliation/reconciliation_report', $this->template_data->get_data());
		}
	}

	public function print_summary($id) {

		$recon = new $this->Accounting_bankrecon_model('ab');
		$recon->setId($id,true);
		$recon->set_select("ab.*");
		$recon->set_select("(SELECT SUM(ae.debit) FROM accounting_bankrecon_items abi JOIN accounting_entries ae ON abi.entry_id=ae.id WHERE abi.recon_id=ab.id) as total_deposit");
		$recon->set_select("(SELECT SUM(ae.credit) FROM accounting_bankrecon_items abi JOIN accounting_entries ae ON abi.entry_id=ae.id WHERE abi.recon_id=ab.id) as total_disburse");
		$recon->set_select("(SELECT COUNT(*) FROM `accounting_bankrecon_items` abi WHERE abi.recon_id=ab.id) as recon_items");
		$recon_data = $recon->get(); 

		$this->template_data->set('recon', $recon_data); 

		$last_recon = new $this->Accounting_bankrecon_model;
		$last_recon->setChartId( $recon_data->chart_id );
		$last_recon->set_where('date_end <', $recon_data->date_end);
		$last_recon->set_order('date_end', 'DESC');
		$last_recon_data = $last_recon->get();
		$this->template_data->set('last_recon', $last_recon_data);

		if( $recon_data->recon_items == 0 ) {
			redirect("accounting_reconciliation/recon_items/{$id}");
		} else {

			$deposits = new $this->Accounting_bankrecon_items_model('abi');
				$deposits->setReconId($id,true);
				$deposits->set_join('accounting_entries ae', 'abi.entry_id=ae.id');
				$deposits->set_join('accounting_transactions at', 'at.id=ae.trn_id');
				$deposits->set_limit(0);
				$deposits->set_where('ae.debit > 0');
				$deposits->set_select("ae.*");
				$deposits->set_select("SUM(ae.debit) as total_amount");
				$deposits->set_select("at.number");
				$deposits->set_select("at.memo");
				$deposits->set_select("at.type as trn_type");
				$deposits->set_select("at.date as date");
				$deposits->set_order('at.date', 'ASC');
				$deposits->set_group_by('ae.trn_id');
				$this->template_data->set('deposits', $deposits->populate());

				$disbursements = new $this->Accounting_bankrecon_items_model('abi');
				$disbursements->setReconId($id,true);
				$disbursements->set_join('accounting_entries ae', 'abi.entry_id=ae.id');
				$disbursements->set_join('accounting_transactions at', 'at.id=ae.trn_id');
				$disbursements->set_limit(0);
				$disbursements->set_where('ae.credit > 0');
				$disbursements->set_select("ae.*");
				$disbursements->set_select("SUM(ae.credit) as total_amount");
				$disbursements->set_select("at.number");
				$disbursements->set_select("at.memo");
				$disbursements->set_select("at.type as trn_type");
				$disbursements->set_select("at.date as date");
				$disbursements->set_order('at.date', 'ASC');
				$disbursements->set_group_by('ae.trn_id');
				$this->template_data->set('disbursements', $disbursements->populate());

				$adv_recon = new $this->Accounting_bankrecon_model;
				$adv_recon->setChartId( $recon_data->chart_id );
				$adv_recon->set_where('date_end >', $recon_data->date_end);
				$adv_recon->set_order('date_end', 'ASC');
				$adv_recon_data = $adv_recon->get();
				$this->template_data->set('adv_recon', $adv_recon_data);

				$this->load->view('accounting/reconciliation/reconciliation_print', $this->template_data->get_data());

		}

	}


}
