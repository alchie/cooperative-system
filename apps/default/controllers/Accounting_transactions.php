<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Accounting_transactions extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->template_data->set('page_title', 'COOP - Transactions');
		$this->template_data->set('current_page', 'Transactions');
		$this->template_data->set('current_uri', 'accounting_transactions');
		
		$this->_isAuth('accounting', 'transactions', 'view');
		
		$this->template_data->set('coa_types', unserialize(CHART_ACCOUNT_TYPES));

		$this->load->model('Accounting_receipts_model');
		$this->load->model('Accounting_checks_model');
		$this->load->model('Accounting_deposits_model');
		$this->load->model('Accounting_transactions_model');
		$this->load->model('Lending_loans_model');
		$this->load->model('Lending_invoices_model');
		$this->load->model('Lending_payments_model');
		$this->load->model('Shares_withdrawal_model');
		$this->load->model('Shares_capital_model');
	}

	public function index($start=0) {
		$trns = new $this->Accounting_transactions_model;

		$trns->set_join('coop_names', 'coop_names.id=accounting_transactions.name_id');

		$trns->set_select('accounting_transactions.*');
		$trns->set_select('coop_names.full_name');

		$trns->set_order('date', 'DESC');
		$trns->set_order('id', 'DESC');
		$trns->set_where('date <=', date('Y-m-d'));
		$trns->set_start($start);
		$this->template_data->set('transactions', $trns->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
					'base_url' => base_url( $this->config->item('index_page') . "/accounting_transactions/index"),
					'total_rows' => $trns->count_all_results(),
					'per_page' => $trns->get_limit(),
				)));

		$this->load->view('accounting/transactions/accounting_transactions', $this->template_data->get_data());
	}

}
