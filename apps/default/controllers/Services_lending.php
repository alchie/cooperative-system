<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Services_lending extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->template_data->set('page_title', 'COOP - Lending');
		$this->template_data->set('current_page', 'Lending');
		$this->template_data->set('current_uri', 'services_lending');
		$this->template_data->set('current_sub_uri', 'services_lending_loans');
		$this->template_data->set('enable_search', false);

		$this->_isAuth('services', 'lending', 'view');
		
		$this->load->model('Members_model');
		$this->load->model('Lending_loans_model');
		$this->load->model('Lending_invoices_model');
		$this->load->model('Lending_payments_model');
		$this->load->model('Lending_payment_applied_model');
		$this->load->model('Accounting_charts_model');
		$this->load->model('Accounting_transactions_model');
		$this->load->model('Accounting_entries_model');
		$this->load->model('Coop_lists_model');
		$this->load->model('Coop_settings_model');
		$this->load->model('Coop_names_model');
		$this->load->model('User_accounts_model');

	}

	private function _settings() {
		$class_name = new $this->Coop_settings_model;
		$class_name->setDepartment('SERVICES',true);
		$class_name->setSection('LENDING',true);
		$class_name->setKey('class_name',true);
		$cls_setting = $class_name->get();

		$receivable = new $this->Coop_settings_model;
		$receivable->setDepartment('SERVICES',true);
		$receivable->setSection('LENDING',true);
		$receivable->setKey('receivable',true);
		$at_setting = $receivable->get();

		$interest_income = new $this->Coop_settings_model;
		$interest_income->setDepartment('SERVICES',true);
		$interest_income->setSection('LENDING',true);
		$interest_income->setKey('interest_income',true);
		$ii_setting = $interest_income->get();

		$undeposited = new $this->Coop_settings_model;
		$undeposited->setDepartment('ACCOUNTING',true);
		$undeposited->setSection('CONFIG',true);
		$undeposited->setKey('undeposited',true);
		$udf_setting = $undeposited->get();

		$releaser = new $this->Coop_settings_model;
		$releaser->setDepartment('SERVICES',true);
		$releaser->setSection('LENDING',true);
		$releaser->setKey('releaser',true);
		$r_setting = $releaser->get();

		$settings = (object) array(
			'class_name' => ($cls_setting) ? $cls_setting->value : false,
			'receivable' => ($at_setting) ? $at_setting->value : false,
			'interest_income' => ($ii_setting) ? $ii_setting->value : false,
			'undeposited' => ($udf_setting) ? $udf_setting->value : false,
			'releaser' => ($r_setting) ? $r_setting->value : false,
		);

		foreach($settings as $key=>$value) {
			if(!$value) {
				if( $key=='undeposited') {
					redirect(site_url("accounting_settings/configuration") . "?error_code=102&next=" . uri_string() );
				} else {
					redirect(site_url("services_lending/configuration") . "?error_code=101&next=" . uri_string() );
				}
			}
		}

		return $settings;
	}

	private function _member_data($mid) {

		$members = new $this->Members_model;
		$members->setId($mid,true);
		$members->set_select("members.*");
		$members->set_select("(SELECT c.name FROM companies_members cm JOIN companies c ON cm.company_id=c.id WHERE cm.member_id=members.id) as company_name");
		$members->set_select("(SELECT c.id FROM companies_members cm JOIN companies c ON cm.company_id=c.id WHERE cm.member_id=members.id) as company_id");
		$members->set_join('members_address', 'members_address.mid=members.id');
		$members->set_select("members_address.*");
		return $members->get();
		
	}

	public function index($type='overview', $status='outstanding', $start=0) {

		$setting = $this->_settings();

		$this->template_data->set('enable_search', false);
		switch($type) {
			case 'payments':
				$this->template_data->set('current_sub_uri', 'services_lending_payments');

				if(( $this->input->post('output') == 'body_wrapper' )||( $this->input->post('output') == 'inner_page' )) {

				$payments = new $this->Lending_payments_model;
				//$payments->set_order('receipt_number', 'DESC');
				$payments->set_order('payment_date', 'DESC');
				$payments->set_join('coop_names cn', 'cn.id=lending_payments.member_id');

				$payments->set_select('lending_payments.*');
				$payments->set_select('(SELECT SUM(amount) FROM lending_payment_applied lpa WHERE lpa.payment_id=lending_payments.id) as total_applied');
				$payments->set_select('cn.full_name');
				//$payments->set_select('(SELECT ROUND(lending_payments.amount - (IF((SELECT SUM(amount) FROM lending_payment_applied lpa WHERE lpa.payment_id=lending_payments.id) IS NULL, 0, (SELECT SUM(amount) FROM lending_payment_applied lpa WHERE lpa.payment_id=lending_payments.id))))) as balance');

				$payments->set_select('(SELECT COUNT(*) FROM members WHERE members.id=lending_payments.member_id) as member');
				$payments->set_select("(SELECT COUNT(*) FROM companies WHERE companies.id=lending_payments.id) as company");

				switch($status) {
					case 'applied':
						$this->template_data->set('loan_page_title', 'Applied Payments');
						$payments->set_where("(SELECT ROUND(lending_payments.amount - (IF((SELECT SUM(amount) FROM lending_payment_applied lpa WHERE lpa.payment_id=lending_payments.id) IS NULL, 0, (SELECT SUM(amount) FROM lending_payment_applied lpa WHERE lpa.payment_id=lending_payments.id))))) = 0");
					break;
					case 'unapplied':
						$this->template_data->set('loan_page_title', 'Un-applied Payments');
						$payments->set_where("(SELECT ROUND(lending_payments.amount - (IF((SELECT SUM(amount) FROM lending_payment_applied lpa WHERE lpa.payment_id=lending_payments.id) IS NULL, 0, (SELECT SUM(amount) FROM lending_payment_applied lpa WHERE lpa.payment_id=lending_payments.id))))) > 0");
						$payments->set_order('receipt_number', 'ASC');
						$payments->set_order('payment_date', 'ASC');
					break;
					default:
						$this->template_data->set('loan_page_title', 'All Payments');
					break;
				}

				$payments->set_start( $start );
				$this->template_data->set('payments', $payments->populate());

				$this->template_data->set('pagination', bootstrap_pagination(array(
					'base_url' => base_url( $this->config->item('index_page') . "/services_lending/index/{$type}/{$status}"),
					'total_rows' => $payments->count_all_results(),
					'per_page' => $payments->get_limit(),
					'ajax' => true,
				)));

			}

				$new_payments = new $this->Accounting_entries_model('ae');
				//$new_payments->setItemType('LOANPY', true);
				$new_payments->setChartId($setting->receivable, true);
				$new_payments->set_join('accounting_transactions at', 'at.id=ae.trn_id');
				$new_payments->set_join('accounting_receipts ar', 'ar.trn_id=ae.trn_id');
				$new_payments->set_select('ae.*');
				$new_payments->set_select('at.*');
				$new_payments->set_select('ar.*');
				$new_payments->set_select('ae.id as ae_id');
				$new_payments->set_select('ae.name_id as ae_name_id');
				$new_payments->set_where('((SELECT entry_id FROM  `lending_payments` WHERE entry_id=ae.id) IS NULL)');
				$new_payments->set_where('((SELECT id FROM `members` WHERE id=ae.name_id) IS NOT NULL)');
				$new_payments->set_where_in('ae.item_type', array('LOANPY', 'LOANADJ'));
				$this->template_data->set('new_payments', $new_payments->populate());

				$this->load->view('services/lending/lending_index_payments', $this->template_data->get_data());


			break;
			case 'loans':

				if(( $this->input->post('output') == 'body_wrapper' )||( $this->input->post('output') == 'inner_page' )) {

				$loans = new $this->Lending_loans_model;
				$loans->set_order('lending_loans.loan_date', 'DESC');
				$loans->set_select('lending_loans.*');
				$loans->set_select('(SELECT sum(lpa.amount) FROM lending_payment_applied lpa JOIN lending_invoices li ON lpa.invoice_id=li.id WHERE li.loan_id=lending_loans.id) as payments');
				$loans->set_join('members m', 'm.id=lending_loans.member_id');
				$loans->set_select('m.lastname, m.firstname, m.middlename');

				$loans->set_select('(SELECT COUNT(*) FROM accounting_entries ae WHERE ae.id=lending_loans.entry_id) as entry_exist');
	
				$loans->set_start($start);

				switch($status) {
					case 'archived':
						$this->template_data->set('loan_page_title', 'Archived');
						$loans->set_where("(SELECT COUNT(*) FROM lending_invoices li WHERE li.loan_id=lending_loans.id) > 0");
						$loans->set_where('ROUND((SELECT ((lending_loans.principal + (IF(((SELECT IF( (lending_loans.interest_type LIKE "fixed"), (lending_loans.principal * (((lending_loans.interest_rate / CEIL(30 / lending_loans.skip_days)) * (CEIL((30 / lending_loans.skip_days) * lending_loans.months))) / 100)), ((((lending_loans.principal * (lending_loans.interest_rate / 100) ) / 2 ) * ((CEIL((30 / lending_loans.skip_days) * lending_loans.months)) + 1) * (1/CEIL(30 / lending_loans.skip_days) ))) ) ) IS NULL), 0, (SELECT IF( (lending_loans.interest_type LIKE "fixed"), (lending_loans.principal * (((lending_loans.interest_rate / CEIL(30 / lending_loans.skip_days)) * (CEIL((30 / lending_loans.skip_days) * lending_loans.months))) / 100)), ((((lending_loans.principal * (lending_loans.interest_rate / 100) ) / 2 ) * ((CEIL((30 / lending_loans.skip_days) * lending_loans.months)) + 1) * (1/CEIL(30 / lending_loans.skip_days) ))) ) ))) ) - (IF(((SELECT sum(lpa.amount) FROM lending_payment_applied lpa JOIN lending_invoices li ON lpa.invoice_id=li.id WHERE li.loan_id=lending_loans.id) IS NULL), 0, (SELECT sum(lpa.amount) FROM lending_payment_applied lpa JOIN lending_invoices li ON lpa.invoice_id=li.id WHERE li.loan_id=lending_loans.id))) ) )) = 0');
					break;
					case 'inactive':
						$this->template_data->set('loan_page_title', 'Inactive');
						$loans->set_where("lending_loans.inactive = 1");
					break;
					case 'pending':
						$this->template_data->set('loan_page_title', 'Pending');
						$loans->set_where('(SELECT COUNT(*) FROM lending_invoices li WHERE li.loan_id=lending_loans.id) = 0');
					break;
					case 'outstanding':
					default:
						$this->template_data->set('loan_page_title', 'Outstanding');
						$loans->set_where('(SELECT COUNT(*) FROM lending_invoices li WHERE li.loan_id=lending_loans.id) > 0');
						$loans->set_where('ROUND((SELECT ((lending_loans.principal + (IF(((SELECT IF( (lending_loans.interest_type LIKE "fixed"), (lending_loans.principal * (((lending_loans.interest_rate / CEIL(30 / lending_loans.skip_days)) * (CEIL((30 / lending_loans.skip_days) * lending_loans.months))) / 100)), ((((lending_loans.principal * (lending_loans.interest_rate / 100) ) / 2 ) * ((CEIL((30 / lending_loans.skip_days) * lending_loans.months)) + 1) * (1/CEIL(30 / lending_loans.skip_days) ))) ) ) IS NULL), 0, (SELECT IF( (lending_loans.interest_type LIKE "fixed"), (lending_loans.principal * (((lending_loans.interest_rate / CEIL(30 / lending_loans.skip_days)) * (CEIL((30 / lending_loans.skip_days) * lending_loans.months))) / 100)), ((((lending_loans.principal * (lending_loans.interest_rate / 100) ) / 2 ) * ((CEIL((30 / lending_loans.skip_days) * lending_loans.months)) + 1) * (1/CEIL(30 / lending_loans.skip_days) ))) ) ))) ) - (IF(((SELECT sum(lpa.amount) FROM lending_payment_applied lpa JOIN lending_invoices li ON lpa.invoice_id=li.id WHERE li.loan_id=lending_loans.id) IS NULL), 0, (SELECT sum(lpa.amount) FROM lending_payment_applied lpa JOIN lending_invoices li ON lpa.invoice_id=li.id WHERE li.loan_id=lending_loans.id))) ) )) > 0');
					break;
				}

				$this->template_data->set('loans', $loans->populate());

				$this->template_data->set('pagination', bootstrap_pagination(array(
					'base_url' => base_url( $this->config->item('index_page') . "/services_lending/index/{$type}/{$status}"),
					'total_rows' => $loans->count_all_results(),
					'per_page' => $loans->get_limit(),
					'ajax' => true,
				)));

			}

					$new_loan = new $this->Accounting_entries_model('ae');
					$new_loan->setItemType('LOANRE', true);
					$new_loan->setChartId($setting->receivable, true);
					$new_loan->set_join('accounting_transactions at', 'at.id=ae.trn_id');
					$new_loan->set_join('accounting_checks ac', 'ac.trn_id=ae.trn_id');
					$new_loan->set_select('ae.*');
					$new_loan->set_select('at.*');
					$new_loan->set_select('ac.*');
					$new_loan->set_select('ae.id as ae_id');
					$new_loan->set_select('ae.name_id as ae_name_id');
					$new_loan->set_where('((SELECT entry_id FROM  `lending_loans` WHERE entry_id=ae.id) IS NULL)');
					$new_loan->set_where('((SELECT id FROM  `members` WHERE id=ae.name_id) IS NOT NULL)');
					$this->template_data->set('new_loans', $new_loan->populate());

				$this->load->view('services/lending/lending_index_loans', $this->template_data->get_data());

			break;
			case 'open_invoices':
				$this->template_data->set('current_sub_uri', 'services_lending_open_invoices');

					if(( $this->input->post('output') == 'body_wrapper' )||( $this->input->post('output') == 'inner_page' )) {

					$invoices = new $this->Lending_invoices_model('li');
					$invoices->set_limit(10);

					$invoices->set_join('lending_loans ll', 'll.id=li.loan_id');
					$invoices->set_join('members m', 'm.id=ll.member_id');
					
					$invoices->set_select('li.*');
					$invoices->set_select('m.id as member_id, m.lastname, m.firstname, m.middlename');
					$invoices->set_select('(li.principal_due + li.interest_due) as gross_amount');

					$invoices->set_select('(SELECT SUM(amount) FROM lending_payment_applied lpa WHERE lpa.invoice_id=li.id) as total_paid');

					$invoices->set_select('(SELECT (gross_amount - (IF((total_paid IS NULL),0,total_paid) )) ) as balance');

					$invoices->set_order('li.due_date', 'DESC');
					$invoices->set_start( $start );
					$invoices->set_where( 'li.due_date <=', date('Y-m-d') );
					$invoices->set_where( 'ROUND((SELECT ((li.principal_due + li.interest_due) - (IF(((SELECT SUM(amount) FROM lending_payment_applied lpa WHERE lpa.invoice_id=li.id) IS NULL),0,(SELECT SUM(amount) FROM lending_payment_applied lpa WHERE lpa.invoice_id=li.id)) )) )) > 0' );
					
					$this->template_data->set('invoices', $invoices->populate());
				
					$this->template_data->set('pagination', bootstrap_pagination(array(
						'base_url' => base_url( $this->config->item('index_page') . "/services_lending/index/{$type}/{$status}"),
						'total_rows' => $invoices->count_all_results(),
						'per_page' => $invoices->get_limit(),
						'ajax' => true,
					)));

				}

				$this->load->view('services/lending/lending_index_open_invoices', $this->template_data->get_data());
				
			break;
			case 'overview':
			default:
				$this->overview();
			break;
		}

	}

	public function overview($mid=null) {
		
		$setting = $this->_settings();
		
		$overview = new $this->Lending_loans_model;

		if( $mid ) { 
			$overview->setMemberId( $mid, true );
			$overview->set_select("(SUM(principal)) as total_principal");
			$overview->set_select('(SUM((IF(((SELECT IF( (lending_loans.interest_type LIKE "fixed"), (lending_loans.principal * (((lending_loans.interest_rate / CEIL(30 / lending_loans.skip_days)) * (CEIL((30 / lending_loans.skip_days) * lending_loans.months))) / 100)), ((((lending_loans.principal * (lending_loans.interest_rate / 100) ) / 2 ) * ((CEIL((30 / lending_loans.skip_days) * lending_loans.months)) + 1) * (1/CEIL(30 / lending_loans.skip_days) ))) ) ) IS NULL), 0, (SELECT IF( (lending_loans.interest_type LIKE "fixed"), (lending_loans.principal * (((lending_loans.interest_rate / CEIL(30 / lending_loans.skip_days)) * (CEIL((30 / lending_loans.skip_days) * lending_loans.months))) / 100)), ((((lending_loans.principal * (lending_loans.interest_rate / 100) ) / 2 ) * ((CEIL((30 / lending_loans.skip_days) * lending_loans.months)) + 1) * (1/CEIL(30 / lending_loans.skip_days) ))) ) ))))) as total_interest');
			$overview->set_select("(SELECT SUM(amount) FROM `lending_payments` WHERE member_id=lending_loans.member_id) as total_payments");
			$overview->set_select("(SELECT SUM(lpa.amount) FROM `lending_payment_applied` lpa JOIN `lending_payments` lp ON lp.id=lpa.payment_id WHERE lp.member_id=lending_loans.member_id) as total_applied");
			$overview->set_select('((SELECT SUM(principal) FROM `lending_loans` WHERE member_id='.$mid.') + (SUM((IF(((SELECT IF( (lending_loans.interest_type LIKE "fixed"), (lending_loans.principal * (((lending_loans.interest_rate / CEIL(30 / lending_loans.skip_days)) * (CEIL((30 / lending_loans.skip_days) * lending_loans.months))) / 100)), ((((lending_loans.principal * (lending_loans.interest_rate / 100) ) / 2 ) * ((CEIL((30 / lending_loans.skip_days) * lending_loans.months)) + 1) * (1/CEIL(30 / lending_loans.skip_days) ))) ) ) IS NULL), 0, (SELECT IF( (lending_loans.interest_type LIKE "fixed"), (lending_loans.principal * (((lending_loans.interest_rate / CEIL(30 / lending_loans.skip_days)) * (CEIL((30 / lending_loans.skip_days) * lending_loans.months))) / 100)), ((((lending_loans.principal * (lending_loans.interest_rate / 100) ) / 2 ) * ((CEIL((30 / lending_loans.skip_days) * lending_loans.months)) + 1) * (1/CEIL(30 / lending_loans.skip_days) ))) ) )))))  - (IF((SELECT SUM(amount) FROM `lending_payments` WHERE member_id='.$mid.'), (SELECT SUM(amount) FROM `lending_payments` WHERE member_id='.$mid.'), 0)) ) as total_unpaid');
			$overview->set_select("(SELECT SUM(principal) FROM `lending_loans` WHERE member_id={$mid} AND inactive=1) as total_inactive");
			$overview->set_select("(SELECT SUM(principal) FROM `lending_loans` WHERE member_id={$mid} AND ((SELECT COUNT(*) FROM lending_invoices li WHERE li.loan_id=lending_loans.id) = 0)) as total_pending");
			$overview->set_select("(SELECT SUM(ae.debit) FROM `accounting_entries` `ae` WHERE `ae`.`item_type` = 'LOANRE' AND `ae`.`chart_id` = '{$setting->receivable}' AND ((SELECT ll.entry_id FROM `lending_loans` ll WHERE ll.entry_id = ae.id) IS NULL) AND `ae`.`name_id`={$mid}) as new_loans");
			$overview->set_select("(SELECT SUM(ae.credit) FROM `accounting_entries` `ae` WHERE `ae`.`item_type` IN ('LOANPY', 'LOANADJ') AND `ae`.`chart_id` = '{$setting->receivable}' AND ((SELECT lp.entry_id FROM `lending_payments` lp WHERE lp.entry_id = ae.id) IS NULL) AND `ae`.`name_id`={$mid}) as new_payments");
			$overview->set_select("(SELECT SUM( lpa.amount * ( li.principal_due / (li.interest_due + li.principal_due) )) FROM lending_payment_applied lpa JOIN lending_payments lp ON lpa.payment_id=lp.id JOIN lending_invoices li ON lpa.invoice_id=li.id WHERE lp.member_id=lending_loans.member_id) as total_applied_principal");
			$overview->set_select("(SELECT SUM( lpa.amount * ( li.interest_due / (li.interest_due + li.principal_due) )) FROM lending_payment_applied lpa JOIN lending_payments lp ON lpa.payment_id=lp.id JOIN lending_invoices li ON lpa.invoice_id=li.id WHERE lp.member_id=lending_loans.member_id) as total_applied_interest");

			$this->template_data->set('member', $this->_member_data($mid));
		} else {
			$overview->set_select("(SUM(principal)) as total_principal");
			$overview->set_select('(SUM((IF(((SELECT IF( (lending_loans.interest_type LIKE "fixed"), (lending_loans.principal * (((lending_loans.interest_rate / CEIL(30 / lending_loans.skip_days)) * (CEIL((30 / lending_loans.skip_days) * lending_loans.months))) / 100)), ((((lending_loans.principal * (lending_loans.interest_rate / 100) ) / 2 ) * ((CEIL((30 / lending_loans.skip_days) * lending_loans.months)) + 1) * (1/CEIL(30 / lending_loans.skip_days) ))) ) ) IS NULL), 0, (SELECT IF( (lending_loans.interest_type LIKE "fixed"), (lending_loans.principal * (((lending_loans.interest_rate / CEIL(30 / lending_loans.skip_days)) * (CEIL((30 / lending_loans.skip_days) * lending_loans.months))) / 100)), ((((lending_loans.principal * (lending_loans.interest_rate / 100) ) / 2 ) * ((CEIL((30 / lending_loans.skip_days) * lending_loans.months)) + 1) * (1/CEIL(30 / lending_loans.skip_days) ))) ) ))))) as total_interest');
			$overview->set_select("(SELECT SUM(amount) FROM `lending_payments`) as total_payments");
			$overview->set_select("(SELECT SUM(lpa.amount) FROM `lending_payment_applied` lpa) as total_applied");
			$overview->set_select('((SELECT SUM(principal) FROM `lending_loans`) + (SUM((IF(((SELECT IF( (lending_loans.interest_type LIKE "fixed"), (lending_loans.principal * (((lending_loans.interest_rate / CEIL(30 / lending_loans.skip_days)) * (CEIL((30 / lending_loans.skip_days) * lending_loans.months))) / 100)), ((((lending_loans.principal * (lending_loans.interest_rate / 100) ) / 2 ) * ((CEIL((30 / lending_loans.skip_days) * lending_loans.months)) + 1) * (1/CEIL(30 / lending_loans.skip_days) ))) ) ) IS NULL), 0, (SELECT IF( (lending_loans.interest_type LIKE "fixed"), (lending_loans.principal * (((lending_loans.interest_rate / CEIL(30 / lending_loans.skip_days)) * (CEIL((30 / lending_loans.skip_days) * lending_loans.months))) / 100)), ((((lending_loans.principal * (lending_loans.interest_rate / 100) ) / 2 ) * ((CEIL((30 / lending_loans.skip_days) * lending_loans.months)) + 1) * (1/CEIL(30 / lending_loans.skip_days) ))) ) ))))) - (IF((SELECT SUM(amount) FROM `lending_payments`), (SELECT SUM(amount) FROM `lending_payments`), 0)) ) as total_unpaid');
			$overview->set_select("(SELECT SUM(principal) FROM `lending_loans` WHERE inactive=1) as total_inactive");
			$overview->set_select("(SELECT SUM(principal) FROM `lending_loans` WHERE (SELECT COUNT(*) FROM lending_invoices li WHERE li.loan_id=lending_loans.id) = 0) as total_pending");
			$overview->set_select("(SELECT SUM(ae.debit) FROM `accounting_entries` `ae` WHERE `ae`.`item_type` = 'LOANRE' AND `ae`.`chart_id` = '{$setting->receivable}' AND ((SELECT ll.entry_id FROM `lending_loans` ll WHERE ll.entry_id = ae.id) IS NULL) AND ((SELECT id FROM  `members` WHERE id=ae.name_id) IS NOT NULL)) as new_loans");
			$overview->set_select("(SELECT SUM(ae.credit) FROM `accounting_entries` `ae` WHERE `ae`.`item_type` IN ('LOANPY','LOANADJ') AND `ae`.`chart_id` = '{$setting->receivable}' AND ((SELECT lp.entry_id FROM `lending_payments` lp WHERE lp.entry_id = ae.id) IS NULL) AND ((SELECT id FROM  `members` WHERE id=ae.name_id) IS NOT NULL)) as new_payments");
			$overview->set_select("(SELECT SUM( lpa.amount * ( li.principal_due / (li.interest_due + li.principal_due) )) FROM lending_payment_applied lpa JOIN lending_payments lp ON lpa.payment_id=lp.id JOIN lending_invoices li ON lpa.invoice_id=li.id) as total_applied_principal");
			$overview->set_select("(SELECT SUM( lpa.amount * ( li.interest_due / (li.interest_due + li.principal_due) )) FROM lending_payment_applied lpa JOIN lending_payments lp ON lpa.payment_id=lp.id JOIN lending_invoices li ON lpa.invoice_id=li.id) as total_applied_interest");

		}

		$this->template_data->set('overview', $overview->get());

		$this->template_data->set('current_page', 'Lending');
		$this->template_data->set('current_sub_uri', 'services_lending_overview');

		$this->load->view( 'services/lending/lending_overview', $this->template_data->get_data() );
	}

	public function loans($mid, $status='all', $start=0) {
		

		$setting = $this->_settings();
		$this->template_data->set('setting', $setting);
		
		$this->template_data->set('member', $this->_member_data( $mid ) );

		if(( $this->input->post('output') == 'body_wrapper' )||( $this->input->post('output') == 'inner_page' )) {

		$loans = new $this->Lending_loans_model;
		$loans->setMemberId($mid,true);
		//$loans->set_order('id', 'DESC');
		$loans->set_order('loan_date', 'DESC');
		$loans->set_select('*');
		$loans->set_select('(SELECT COUNT(*) FROM lending_invoices li WHERE li.loan_id=lending_loans.id) as invoices');
		$loans->set_select('(SELECT sum(lpa.amount) FROM lending_payment_applied lpa JOIN lending_invoices li ON lpa.invoice_id=li.id WHERE li.loan_id=lending_loans.id) as payments');
		$loans->set_select('(SELECT COUNT(*) FROM accounting_entries ae WHERE ae.id=lending_loans.entry_id) as entry_exist');
		
		//$loans->set_select('(SELECT IF( (lending_loans.interest_type LIKE "fixed"), (lending_loans.principal * (((lending_loans.interest_rate / CEIL(30 / lending_loans.skip_days)) * (CEIL((30 / lending_loans.skip_days) * lending_loans.months))) / 100)), ((((lending_loans.principal * (lending_loans.interest_rate / 100) ) / 2 ) * ((CEIL((30 / lending_loans.skip_days) * lending_loans.months)) + 1) * (1/CEIL(30 / lending_loans.skip_days) ))) ) ) as total_interest');


		//$loans->set_select('(SELECT(lending_loans.principal + total_interest)) as gross_amount');

		//$loans->set_select('(SELECT ((lending_loans.principal + (IF((total_interest IS NULL), 0, total_interest)) ) - (IF((payments IS NULL), 0, payments)) ) ) as balance');

		$loans->set_start($start);
		//$loans->set_limit(1);

		switch($status) {
			case 'archived':
				$this->template_data->set('loan_page_title', 'Archived');
				$loans->set_where("(SELECT COUNT(*) FROM lending_invoices li WHERE li.loan_id=lending_loans.id) > 0");
				$loans->set_where('ROUND((SELECT ((lending_loans.principal + (IF(((SELECT IF( (lending_loans.interest_type LIKE "fixed"), (lending_loans.principal * (((lending_loans.interest_rate / CEIL(30 / lending_loans.skip_days)) * (CEIL((30 / lending_loans.skip_days) * lending_loans.months))) / 100)), ((((lending_loans.principal * (lending_loans.interest_rate / 100) ) / 2 ) * ((CEIL((30 / lending_loans.skip_days) * lending_loans.months)) + 1) * (1/CEIL(30 / lending_loans.skip_days) ))) ) ) IS NULL), 0, (SELECT IF( (lending_loans.interest_type LIKE "fixed"), (lending_loans.principal * (((lending_loans.interest_rate / CEIL(30 / lending_loans.skip_days)) * (CEIL((30 / lending_loans.skip_days) * lending_loans.months))) / 100)), ((((lending_loans.principal * (lending_loans.interest_rate / 100) ) / 2 ) * ((CEIL((30 / lending_loans.skip_days) * lending_loans.months)) + 1) * (1/CEIL(30 / lending_loans.skip_days) ))) ) ))) ) - (IF(((SELECT sum(lpa.amount) FROM lending_payment_applied lpa JOIN lending_invoices li ON lpa.invoice_id=li.id WHERE li.loan_id=lending_loans.id) IS NULL), 0, (SELECT sum(lpa.amount) FROM lending_payment_applied lpa JOIN lending_invoices li ON lpa.invoice_id=li.id WHERE li.loan_id=lending_loans.id))) ) )) = 0');
			break;
			case 'pending':
				$this->template_data->set('loan_page_title', 'Pending');
				$loans->set_where("(SELECT COUNT(*) FROM lending_invoices li WHERE li.loan_id=lending_loans.id) = 0");
				//$loans->set_where('ROUND((SELECT ((lending_loans.principal + (IF(((SELECT IF( (lending_loans.interest_type LIKE "fixed"), (lending_loans.principal * (((lending_loans.interest_rate / CEIL(30 / lending_loans.skip_days)) * (CEIL((30 / lending_loans.skip_days) * lending_loans.months))) / 100)), ((((lending_loans.principal * (lending_loans.interest_rate / 100) ) / 2 ) * ((CEIL((30 / lending_loans.skip_days) * lending_loans.months)) + 1) * (1/CEIL(30 / lending_loans.skip_days) ))) ) ) IS NULL), 0, (SELECT IF( (lending_loans.interest_type LIKE "fixed"), (lending_loans.principal * (((lending_loans.interest_rate / CEIL(30 / lending_loans.skip_days)) * (CEIL((30 / lending_loans.skip_days) * lending_loans.months))) / 100)), ((((lending_loans.principal * (lending_loans.interest_rate / 100) ) / 2 ) * ((CEIL((30 / lending_loans.skip_days) * lending_loans.months)) + 1) * (1/CEIL(30 / lending_loans.skip_days) ))) ) ))) ) - (IF(((SELECT sum(lpa.amount) FROM lending_payment_applied lpa JOIN lending_invoices li ON lpa.invoice_id=li.id WHERE li.loan_id=lending_loans.id) IS NULL), 0, (SELECT sum(lpa.amount) FROM lending_payment_applied lpa JOIN lending_invoices li ON lpa.invoice_id=li.id WHERE li.loan_id=lending_loans.id))) ) )) = 0');
			break;
			case 'active':
				$this->template_data->set('loan_page_title', 'Active');
				$loans->set_where("(SELECT COUNT(*) FROM lending_invoices li WHERE li.loan_id=lending_loans.id) > 0");
				$loans->set_where('ROUND((SELECT ((lending_loans.principal + (IF(((SELECT IF( (lending_loans.interest_type LIKE "fixed"), (lending_loans.principal * (((lending_loans.interest_rate / CEIL(30 / lending_loans.skip_days)) * (CEIL((30 / lending_loans.skip_days) * lending_loans.months))) / 100)), ((((lending_loans.principal * (lending_loans.interest_rate / 100) ) / 2 ) * ((CEIL((30 / lending_loans.skip_days) * lending_loans.months)) + 1) * (1/CEIL(30 / lending_loans.skip_days) ))) ) ) IS NULL), 0, (SELECT IF( (lending_loans.interest_type LIKE "fixed"), (lending_loans.principal * (((lending_loans.interest_rate / CEIL(30 / lending_loans.skip_days)) * (CEIL((30 / lending_loans.skip_days) * lending_loans.months))) / 100)), ((((lending_loans.principal * (lending_loans.interest_rate / 100) ) / 2 ) * ((CEIL((30 / lending_loans.skip_days) * lending_loans.months)) + 1) * (1/CEIL(30 / lending_loans.skip_days) ))) ) ))) ) - (IF(((SELECT sum(lpa.amount) FROM lending_payment_applied lpa JOIN lending_invoices li ON lpa.invoice_id=li.id WHERE li.loan_id=lending_loans.id) IS NULL), 0, (SELECT sum(lpa.amount) FROM lending_payment_applied lpa JOIN lending_invoices li ON lpa.invoice_id=li.id WHERE li.loan_id=lending_loans.id))) ) )) > 0');
			break;
			case 'inactive':
				$this->template_data->set('loan_page_title', 'Inactive');
				$loans->set_where("(SELECT COUNT(*) FROM lending_invoices li WHERE li.loan_id=lending_loans.id) > 0");
				$loans->set_where('lending_loans.inactive=1');
			break;
			default:
				$this->template_data->set('loan_page_title', 'All');
				//$loans->set_where('ROUND((SELECT ((lending_loans.principal + (IF(((SELECT IF( (lending_loans.interest_type LIKE "fixed"), (lending_loans.principal * (((lending_loans.interest_rate / CEIL(30 / lending_loans.skip_days)) * (CEIL((30 / lending_loans.skip_days) * lending_loans.months))) / 100)), ((((lending_loans.principal * (lending_loans.interest_rate / 100) ) / 2 ) * ((CEIL((30 / lending_loans.skip_days) * lending_loans.months)) + 1) * (1/CEIL(30 / lending_loans.skip_days) ))) ) ) IS NULL), 0, (SELECT IF( (lending_loans.interest_type LIKE "fixed"), (lending_loans.principal * (((lending_loans.interest_rate / CEIL(30 / lending_loans.skip_days)) * (CEIL((30 / lending_loans.skip_days) * lending_loans.months))) / 100)), ((((lending_loans.principal * (lending_loans.interest_rate / 100) ) / 2 ) * ((CEIL((30 / lending_loans.skip_days) * lending_loans.months)) + 1) * (1/CEIL(30 / lending_loans.skip_days) ))) ) ))) ) - (IF(((SELECT sum(lpa.amount) FROM lending_payment_applied lpa JOIN lending_invoices li ON lpa.invoice_id=li.id WHERE li.loan_id=lending_loans.id) IS NULL), 0, (SELECT sum(lpa.amount) FROM lending_payment_applied lpa JOIN lending_invoices li ON lpa.invoice_id=li.id WHERE li.loan_id=lending_loans.id))) ) )) > 0');
			break;
		}
		
		$this->template_data->set('member_loans', $loans->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
					'uri_segment' => 5,
					'base_url' => base_url( $this->config->item('index_page') . "/services_lending/loans/{$mid}/{$status}"),
					'total_rows' => $loans->count_all_results(),
					'per_page' => $loans->get_limit(),
					//'ajax'=>true,
				)));

		}

		$new_loan = new $this->Accounting_entries_model('ae');
		$new_loan->setItemType('LOANRE', true);
		$new_loan->setChartId($setting->receivable, true);
		$new_loan->setNameId($mid, true);
		$new_loan->set_join('accounting_transactions at', 'at.id=ae.trn_id');
		$new_loan->set_join('accounting_checks ac', 'ac.trn_id=ae.trn_id');
		$new_loan->set_select('ae.*');
		$new_loan->set_select('at.*');
		$new_loan->set_select('ac.*');
		$new_loan->set_select('ae.id as ae_id');
		$new_loan->set_select('ae.name_id as ae_name_id');
		$new_loan->set_where('((SELECT entry_id FROM  `lending_loans` WHERE entry_id=ae.id) IS NULL)');
		$this->template_data->set('new_loans', $new_loan->populate()); 

		$this->template_data->set('current_page', 'Loans');
		$this->template_data->set('current_sub_uri', 'services_lending_loans');

		$this->load->view('services/lending/lending_loans', $this->template_data->get_data());
	}

	public function add($mid=0, $output='', $start=0) {

		$this->template_data->set('member', $this->_member_data( $mid ) );

		$setting = $this->_settings();

		$new_loan = new $this->Accounting_entries_model('ae');
		$new_loan->setItemType('LOANRE', true);
		$new_loan->setChartId($setting->receivable, true);
		if( $mid ) {
			$new_loan->setNameId($mid, true);
		}
		$new_loan->set_join('accounting_transactions at', 'at.id=ae.trn_id');
		$new_loan->set_join('accounting_checks ac', 'ac.trn_id=ae.trn_id');
		$new_loan->set_join('members m', 'm.id=ae.name_id');
		$new_loan->set_select('ae.*');
		$new_loan->set_select('at.*');
		$new_loan->set_select('ac.*');
		$new_loan->set_select('m.lastname, m.firstname, m.middlename');
		$new_loan->set_select('ae.id as ae_id');
		$new_loan->set_select('ae.name_id as ae_name_id');
		$new_loan->set_select('at.date as loan_date');
		$new_loan->set_select('at.number as loan_number');
		$new_loan->set_select('at.type as entry_type');
		$new_loan->set_where('((SELECT entry_id FROM  `lending_loans` WHERE entry_id=ae.id) IS NULL)');
		$new_loan->set_where('((SELECT id FROM  `members` WHERE id=ae.name_id) IS NOT NULL)');
		$new_loan->set_start($start);
		$new_loan->set_limit(5);
		$this->template_data->set('new_loans', $new_loan->populate()); 

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'uri_segment' => 5,
			'base_url' => base_url( $this->config->item('index_page') . "/services_lending/add/{$mid}/{$output}"),
			'total_rows' => $new_loan->count_all_results(),
			'per_page' => $new_loan->get_limit(),
			'attributes' => array('class' => 'btn btn-default ajax-modal-inner')
		), '?next=' . $this->input->get('next') ));

		$this->template_data->set('output', $output);
		$this->load->view('services/lending/lending_add', $this->template_data->get_data());
	}

	public function add_loan($mid, $aeid, $output='') {
		
		$this->_isAuth('services', 'lending', 'add');

		$setting = $this->_settings();
		$this->template_data->set('setting', $setting);

		$new_loan = new $this->Accounting_entries_model('ae');
		$new_loan->setId($aeid,true);
		$new_loan->setItemType('LOANRE', true);
		$new_loan->setChartId($setting->receivable, true);
		$new_loan->setNameId($mid, true);
		$new_loan->set_join('accounting_transactions at', 'at.id=ae.trn_id');
		$new_loan->set_join('accounting_checks ac', 'ac.trn_id=ae.trn_id');
		$new_loan->set_join('coop_names cn', 'cn.id=ae.name_id');
		$new_loan->set_select('ae.*');
		$new_loan->set_select('at.*');
		$new_loan->set_select('ac.*');
		$new_loan->set_select('cn.full_name');
		$new_loan->set_select('ae.id as ae_id');
		$new_loan->set_select('ae.name_id as ae_name_id');
		$new_loan->set_select('ac.chart_id as bank_id');
		$new_loan->set_where('((SELECT entry_id FROM  `lending_loans` WHERE entry_id=ae.id) IS NULL)');

		if( $new_loan->nonEmpty() ) {

			$entry_data = $new_loan->get();
			$this->template_data->set('new_loan', $entry_data); 
		
			$this->template_data->set('member', $this->_member_data( $mid ) );

				if( $this->input->post() ) {

					$this->form_validation->set_rules('payment_start', 'Payment Start Date', 'trim|required');
					$this->form_validation->set_rules('interest_rate', 'Interest Rate', 'trim|required');
					$this->form_validation->set_rules('months', 'Number of Months', 'trim|required');
					$this->form_validation->set_rules('skip_days', 'Skip Days', 'trim|required');
					$this->form_validation->set_rules('interest_type', 'Interest Type', 'trim|required');

					if($this->form_validation->run()) {
												
							$loan = new $this->Lending_loans_model;
							$loan->setEntryId( $entry_data->ae_id );
							$loan->setMemberId( $entry_data->ae_name_id );
							$loan->setLoanDate( $entry_data->date );
							$loan->setPrincipal( $entry_data->debit );
							$loan->setPaymentStart( date( "Y-m-d", strtotime( $this->input->post('payment_start') ) ) );
							$loan->setInterestRate($this->input->post('interest_rate'));
							$loan->setMonths($this->input->post('months'));
							$loan->setSkipDays($this->input->post('skip_days'));
							$loan->setInterestType($this->input->post('interest_type'));
							$loan->setDeleted(0);
							$loan->setCheckNumber($entry_data->check_number);
							$loan->setBankId( $entry_data->bank_id );
							$loan->setMemo($this->input->post('memo'));
							$loan->setTrnId( $entry_data->trn_id );
								
							if( $loan->insert() ) {
								redirect("services_lending/schedule/{$loan->getId()}?success=true");
							}						
						
					}
				} // input:post

		} // entry data

		$this->template_data->set('output', $output);
		$this->load->view('services/lending/lending_add_loan', $this->template_data->get_data());
	}

	public function edit($id, $output='') {
		
		$this->_isAuth('services', 'lending', 'edit');

		$this->template_data->set('output', $output);

		$loan = new $this->Lending_loans_model('ll');
		$loan->setId($id,true);
		$loan_data = $loan->get();

		if( !$loan_data ) {
			redirect("services_lending");
		}
		$setting = $this->_settings();
		$this->template_data->set('setting', $setting);

		$invoice = new $this->Lending_invoices_model('li');
		$invoice->setLoanId($id,true);
		$invoice->set_order('li.id', 'ASC');
		$invoice->set_limit(0);
		$invoice->set_select('li.*');
		$invoices_data = $invoice->populate(); 
		$this->template_data->set('invoices', $invoices_data);

		$this->template_data->set('member', $this->_member_data( $loan_data->member_id ) );

		if( $this->input->post() ) {
			
			if( !$invoices_data ) {
				$this->form_validation->set_rules('interest_rate', 'Interest Rate', 'trim');
				$this->form_validation->set_rules('months', 'Number of Months', 'trim|required');
				$this->form_validation->set_rules('skip_days', 'Skip Days', 'trim|required');
				$this->form_validation->set_rules('interest_type', 'Interest Type', 'trim|required');
			}
				$this->form_validation->set_rules('memo', 'Memo', 'trim');
				$this->form_validation->set_rules('inactive', 'Inactive', 'trim');

				if($this->form_validation->run()) {
					
					if( !$invoices_data ) {
						$loan->setInterestRate($this->input->post('interest_rate'), false, true);
						$loan->setMonths($this->input->post('months'), false, true);
						$loan->setSkipDays($this->input->post('skip_days'), false, true);
						$loan->setInterestType($this->input->post('interest_type'), false, true);
					}

					$loan->setPaymentStart( date( "Y-m-d", strtotime( $this->input->post('payment_start') ) ), false, true );
					$loan->setMemo($this->input->post('memo'), false, true);
					$loan->setInactive(($this->input->post('inactive') ? 1 : 0), false, true);	

					$loan->update();
	
				}

			$this->postNext();
			
		}

		$this->template_data->set('current_loan',  $loan->get());

		$main_charts = new $this->Accounting_charts_model;
		$main_charts->set_order('type', 'ASC');
		$main_charts->set_order('number', 'ASC');
		$main_charts->set_select('id,title,reconcile');
		$main_charts->set_limit(0);
		$main_charts->setType('001_BS_ASS_CSB',true);
		$charts_data = $main_charts->recursive('parent', 0);
		$this->template_data->set('account_titles', $charts_data);

		$this->load->view('services/lending/lending_edit_loan', $this->template_data->get_data());
	}

	public function delete($id) {
		$loan = new $this->Lending_loans_model('ll');
		$loan->setId($id,true);
		$loan->set_select('(SELECT sum(lpa.amount) FROM lending_payment_applied lpa JOIN lending_invoices li ON lpa.invoice_id=li.id WHERE li.loan_id=ll.id) as payments');
		$member_id = false;
		
		if( $loan->nonEmpty() ) {

			$results = $loan->getResults();

			$payments = $loan->get();
			
			if( $payments->payments ) {

				redirect(site_url("services_lending/schedule/{$id}") . "?error_code=103");

			}

			$member_id = $results->member_id;
			
			$loan2 = new $this->Lending_loans_model;
			$loan2->setId($id,true);
			$loan2->delete();
		}
		
		if( $member_id ) {
			redirect("services_lending/loans/{$member_id}");
		} else {
			redirect("services_lending");
		}
		
	}

	public function schedule($id) {

		$setting = $this->_settings();

		$loan = new $this->Lending_loans_model('ll');
		$loan->setId($id,true);
		$loan->set_select('ll.*');
		$loan->set_select('(SELECT sum(lpa.amount) FROM lending_payment_applied lpa JOIN lending_invoices li ON lpa.invoice_id=li.id WHERE li.loan_id=ll.id) as total_payments');
		$loan_data = $loan->get();
		$this->template_data->set('current_loan', $loan_data);

		if( ($this->input->post('create_invoices')) && ($this->input->post('create_invoices')==$id) ) {

			$this->_isAuth('services', 'lending', 'edit');

			$this->_create_invoices( $loan_data );
			redirect("services_lending/schedule/{$id}?create_invoices=true");
		}

		if( ($this->input->post('delete_invoice')) && (count($this->input->post('delete_invoice')) > 0) ) {

			$this->_isAuth('services', 'lending', 'edit');
			
			foreach( $this->input->post('delete_invoice') as $inv_id) {
				$invoice = new $this->Lending_invoices_model;
				$invoice->setId($inv_id, true);
				if( $invoice->nonEmpty() ) {
					$results = $invoice->getResults();
					
					$applied = new $this->Lending_payment_applied_model;
					$applied->setInvoiceId($results->id,true);

					$trn = new $this->Accounting_transactions_model;
					$trn->setId( $results->trn_id, true);

					$entries = new $this->Accounting_entries_model;
					$entries->setTrnId($results->trn_id,true);

					$applied->delete();
					$entries->delete();
					$trn->delete();
					$invoice->delete();
				}
			}
		}

		$this->template_data->set('member', $this->_member_data( $loan_data->member_id ) );

		$invoice = new $this->Lending_invoices_model('li');
		$invoice->setLoanId($loan_data->id,true);
		$invoice->set_order('li.id', 'ASC');
		$invoice->set_limit(0);
		$invoice->set_select('li.*');
		$invoice->set_select('(SELECT sum(lpa.amount) FROM `lending_payment_applied` lpa WHERE lpa.invoice_id=li.id) as total_applied');
		$this->template_data->set('invoices', $invoice->populate());

		$this->template_data->set('current_page', 'Loans');
		$this->template_data->set('current_sub_uri', 'services_lending_loans');

		$this->load->view('services/lending/lending_schedule', $this->template_data->get_data());
	}

	public function check_voucher($id) {

		$prepared = new $this->User_accounts_model;
		$prepared->setId($this->session->user_id,true);
		$this->template_data->set('prepared', $prepared->get());

		$releaser = new $this->Coop_settings_model;
		$releaser->setDepartment('SERVICES',true);
		$releaser->setSection('LENDING',true);
		$releaser->setKey('releaser',true);
		$releaser->set_join('user_accounts u', 'u.id=coop_settings.value');
		$releaser->set_select('u.name, coop_settings.*');
		$r_setting = $releaser->get();
		$this->template_data->set('releaser', $r_setting);

		$approver = new $this->Coop_settings_model;
		$approver->setDepartment('SERVICES',true);
		$approver->setSection('LENDING',true);
		$approver->setKey('approver',true);
		$approver->set_join('user_accounts u', 'u.id=coop_settings.value');
		$approver->set_select('u.name, coop_settings.*');
		$a_setting = $approver->get();
		$this->template_data->set('approver', $a_setting);

		$loan = new $this->Lending_loans_model;
		$loan->setId($id,true);
		$loan_data = $loan->get();
		$this->template_data->set('current_loan', $loan_data);

		$this->template_data->set('member', $this->_member_data( $loan_data->member_id ) );

		$invoice = new $this->Lending_invoices_model;
		$invoice->setLoanId($loan_data->id,true);
		$invoice->set_order('id', 'ASC');
		$invoice->set_limit(0);
		$this->template_data->set('invoices', $invoice->populate());

		$this->load->view('services/lending/lending_check_voucher', $this->template_data->get_data());
	}

	public function print_schedule($id) {

		$releaser = new $this->Coop_settings_model;
		$releaser->setDepartment('SERVICES',true);
		$releaser->setSection('LENDING',true);
		$releaser->setKey('releaser',true);
		$releaser->set_join('user_accounts u', 'u.id=coop_settings.value');
		$releaser->set_select('u.name, coop_settings.*');
		$r_setting = $releaser->get();
		$this->template_data->set('releaser', $r_setting);

		$loan = new $this->Lending_loans_model;
		$loan->setId($id,true);
		$loan_data = $loan->get();
		$this->template_data->set('current_loan', $loan_data);

		$this->template_data->set('member', $this->_member_data( $loan_data->member_id ) );

		$invoice = new $this->Lending_invoices_model;
		$invoice->setLoanId($loan_data->id,true);
		$invoice->set_order('id', 'ASC');
		$invoice->set_limit(0);
		$this->template_data->set('invoices', $invoice->populate());

		$this->load->view('services/lending/lending_print_schedule', $this->template_data->get_data());
	}

	public function print_soa($id) {

		$releaser = new $this->Coop_settings_model;
		$releaser->setDepartment('SERVICES',true);
		$releaser->setSection('LENDING',true);
		$releaser->setKey('releaser',true);
		$releaser->set_join('user_accounts u', 'u.id=coop_settings.value');
		$releaser->set_select('u.name, coop_settings.*');
		$r_setting = $releaser->get();
		$this->template_data->set('releaser', $r_setting);

		$loan = new $this->Lending_loans_model;
		$loan->setId($id,true);
		$loan_data = $loan->get();
		$this->template_data->set('current_loan', $loan_data);

		$this->template_data->set('member', $this->_member_data( $loan_data->member_id ) );

		$invoice = new $this->Lending_invoices_model;
		$invoice->setLoanId($loan_data->id,true);
		$invoice->set_order('id', 'ASC');
		$invoice->set_limit(0);
		$this->template_data->set('invoices', $invoice->populate());

		$this->load->view('services/lending/lending_print_soa', $this->template_data->get_data());
	}

	public function sample_computation($id) {

		$releaser = new $this->Coop_settings_model;
		$releaser->setDepartment('SERVICES',true);
		$releaser->setSection('LENDING',true);
		$releaser->setKey('releaser',true);
		$releaser->set_join('user_accounts u', 'u.id=coop_settings.value');
		$releaser->set_select('u.name, coop_settings.*');
		$r_setting = $releaser->get();
		$this->template_data->set('releaser', $r_setting);

		$loan = new $this->Lending_loans_model;
		$loan->setId($id,true);
		$current_loan = $loan->get();
		$this->template_data->set('current_loan', $current_loan);

		$this->template_data->set('member', $this->_member_data( $current_loan->member_id ) );

		$invoices = array();
		$installments = ceil((30 / $current_loan->skip_days) * $current_loan->months);
		$due_date = date('Y-m-d', strtotime($current_loan->payment_start));
		$principal_amount = ($current_loan->principal/$installments);
		$interest = (($current_loan->principal * $current_loan->interest_rate) / 100) / ceil(30 / $current_loan->skip_days);
		$principal_diminishing = $current_loan->principal;

		for($i=1;$i<=$installments;$i++) { 

			if( $current_loan->interest_type == 'diminishing' ) {
				$interest = ((($principal_diminishing * $current_loan->interest_rate) / 100) * (1/ceil((30 / $current_loan->skip_days))));
			}

			$principal_diminishing -= $principal_amount;
			
			$invoices[$i] = (object) array(
					'principal_due' => $principal_amount,
					'due_date' => $due_date,
					'interest_due' => $interest,
				); 

			$due_date = $this->_nextPaymentDate($due_date, $current_loan->skip_days); 
		}
		$this->template_data->set('invoices', $invoices);

		$this->load->view('services/lending/lending_sample', $this->template_data->get_data());
	}

	public function print_sample() {
		
		if( $this->input->get('name_id') ) {
			$this->template_data->set('member', $this->_member_data( $this->input->get('name_id') ) );
		}

		$this->template_data->set('skip_days', ($this->input->get('skip_days')) ? $this->input->get('skip_days') : 15);
		$this->template_data->set('months', ($this->input->get('months')) ? $this->input->get('months') : 6);
		$this->template_data->set('payment_start', ($this->input->get('payment_start')) ? $this->input->get('payment_start') : date("m/d/Y"));
		$this->template_data->set('principal', ($this->input->get('principal')) ? $this->input->get('principal') : 5000);
		$this->template_data->set('interest_type', ($this->input->get('interest_type')) ? $this->input->get('interest_type') : 'fixed');
		$this->template_data->set('interest_rate', ($this->input->get('interest_rate')) ? $this->input->get('interest_rate') : 0);

		$invoices = array();
		$installments = ceil((30 / $this->template_data->get('skip_days')) * $this->template_data->get('months'));
		$due_date = date('Y-m-d', strtotime($this->template_data->get('payment_start') ));
		$principal_amount = ($this->template_data->get('principal')/$installments);
		$interest = (($this->template_data->get('principal') * $this->template_data->get('interest_rate')) / 100) / ceil(30 / $this->template_data->get('skip_days'));
		$principal_diminishing = $this->template_data->get('principal');

		for($i=1;$i<=$installments;$i++) { 

			if( $this->template_data->get('interest_type') == 'diminishing' ) {
				$interest = ((($principal_diminishing * $this->template_data->get('interest_rate')) / 100) * (1/ceil((30 / $this->template_data->get('skip_days')))));
			}

			$principal_diminishing -= $principal_amount;
			
			$invoices[$i] = (object) array(
					'principal_due' => $principal_amount,
					'due_date' => $due_date,
					'interest_due' => $interest,
				); 

			$due_date = $this->_nextPaymentDate($due_date, $this->template_data->get('skip_days')); 
		}
		$this->template_data->set('invoices', $invoices);

		$this->template_data->set('invoices', $invoices);
		$this->load->view('services/lending/lending_print_sample', $this->template_data->get_data());
	}

	private function _create_invoices($current_loan) {

		$setting = $this->_settings();

		$installments = ceil((30 / $current_loan->skip_days) * $current_loan->months);
		$due_date = date('Y-m-d', strtotime($current_loan->payment_start));
		$principal_amount = ($current_loan->principal/$installments);
		$interest = (($current_loan->principal * $current_loan->interest_rate) / 100) / ceil(30 / $current_loan->skip_days);
		$principal_diminishing = $current_loan->principal;

		for($i=1;$i<=$installments;$i++) { 

			if( $current_loan->interest_type == 'diminishing' ) {
				$interest = ((($principal_diminishing * $current_loan->interest_rate) / 100) * (1/ceil((30 / $current_loan->skip_days))));
			}

			$principal_diminishing -= $principal_amount;

				$trn = new $this->Accounting_transactions_model;
				$trn->setDept('SERVICES', true);
				$trn->setSect('LENDING', true);
				$trn->setType('INTEREST', true);
				$trn->setDate( $due_date );
				$trn->setNameId( $current_loan->member_id, true );
				$trn->setAmount( $interest );
				$trn->setLines($i, true);
				$trn->setItemId($current_loan->id, true);
				
			if( $trn->nonEmpty() === false ) {
				if( $trn->insert() ) {
					
					// debit
					$debit = new $this->Accounting_entries_model;
					$debit->setTrnId( $trn->getId() );
					$debit->setChartId( $setting->receivable );
					$debit->setDebit( $interest );
					$debit->setClass( $setting->class_name );
					$debit->setNameId( $current_loan->member_id );
					$debit->setItemNumber(0);

					// credit
					$credit = new $this->Accounting_entries_model;
					$credit->setTrnId( $trn->getId() );
					$credit->setChartId( $setting->interest_income );
					$credit->setCredit( $interest );
					$credit->setClass( $setting->class_name );
					$credit->setNameId( $current_loan->member_id );
					$credit->setItemNumber(1);

					$invoice = new $this->Lending_invoices_model;
					$invoice->setLoanId($current_loan->id);
					$invoice->setPrincipalDue($principal_amount);
					$invoice->setInterestDue($interest);
					$invoice->setDueDate($due_date);
					$invoice->setTrnId( $trn->getId() );

					if( $invoice->insert() ) {

						$debit->setItemId($invoice->getId());
						$debit->insert();

						$credit->setItemId($invoice->getId());
						$credit->insert();
						
						$trn->setId( $trn->getId(), true);
						$trn->setNumber($invoice->getId(), false, true);
						$trn->update();
						
					}


				}
			}

			
			$due_date = $this->_nextPaymentDate($due_date, $current_loan->skip_days); 
		}

	}

	private function _nextPaymentDate($today, $skip=15) {
		if( $skip == 15) {
			$todayStamp = strtotime($today);
			$numOfDays = date('t', $todayStamp);
			if( date('d', $todayStamp) > $skip) {
				$base = strtotime('+'.$numOfDays.' days', strtotime(date('Y-m-01', $todayStamp)));
				return date('Y-m-15', $base);
			} else {
				$base = strtotime(date('Y-m-01', $todayStamp));
				return date('Y-m-'.min(date('t', $base), 30), $base);
			}
		} else {
			return date('Y-m-d', (strtotime($today . " + " . $skip ." days")) );
		}
	}

	public function add_invoice($id, $output='') {

		$this->_isAuth('services', 'lending', 'edit');

		$this->template_data->set('output', $output);

		$loan = new $this->Lending_loans_model('ll');
		$loan->setId($id,true);
		$loan->set_select('*');
		$loan->set_select('(SELECT IF( (ll.interest_type LIKE "fixed"), (ll.principal * (((ll.interest_rate / CEIL(30 / ll.skip_days)) * (CEIL((30 / ll.skip_days) * ll.months))) / 100)), ((((ll.principal * (ll.interest_rate / 100) ) / 2 ) * ((CEIL((30 / ll.skip_days) * ll.months)) + 1) * (1/CEIL(30 / ll.skip_days) ))) ) ) as total_interest');
		$loan->set_select('(SELECT SUM(li.principal_due) FROM lending_invoices li WHERE li.loan_id=ll.id) as total_invoiced_principal');
		$loan->set_select('(SELECT SUM(li.interest_due) FROM lending_invoices li WHERE li.loan_id=ll.id) as total_invoiced_interest');
		$loan_data = $loan->get();

		if( !$loan_data ) {
			redirect("services_lending");
		}

		$difference_principal = $loan_data->principal - $loan_data->total_invoiced_principal;
 		$difference_interest = $loan_data->total_interest - $loan_data->total_invoiced_interest;

		if( (round($difference_principal) == 0) && (round($difference_interest) == 0)) {
			redirect("services_lending/schedule/{$loan_data->id}");
		}

		$setting = $this->_settings();

		if( $this->input->post() ) {
			$this->form_validation->set_rules('due_date', 'Due Date', 'trim|required');
			$this->form_validation->set_rules('principal_due', 'Payment Due', 'trim|required|decimal');
			$this->form_validation->set_rules('interest_due', 'Interst Due', 'trim|required|decimal');
			if($this->form_validation->run()) { 
				
				$due_date = date('Y-m-d', strtotime($this->input->post('due_date')));
				$principal_due = str_replace(',', '', $this->input->post('principal_due') );
				$interest_due = str_replace(',', '', $this->input->post('interest_due') );

				$trn = new $this->Accounting_transactions_model;
				$trn->setDept('SERVICES');
				$trn->setSect('LENDING');
				$trn->setType('INTEREST');
				$trn->setDate( $due_date );
				$trn->setNameId( $loan_data->member_id );
				$trn->setAmount( $interest_due );
				
				if( $trn->insert() ) {
					
					// debit
					$debit = new $this->Accounting_entries_model;
					$debit->setTrnId( $trn->getId() );
					$debit->setChartId( $setting->receivable );
					$debit->setDebit( $interest_due );
					$debit->setClass( $setting->class_name );
					$debit->setNameId( $loan_data->member_id );
					$debit->setItemNumber(0);
					$debit->insert();

					// credit
					$credit = new $this->Accounting_entries_model;
					$credit->setTrnId( $trn->getId() );
					$credit->setChartId( $setting->interest_income );
					$credit->setCredit( $interest_due );
					$credit->setClass( $setting->class_name );
					$credit->setNameId( $loan_data->member_id );
					$credit->setItemNumber(1);
					$credit->insert();

					$invoice = new $this->Lending_invoices_model;
					$invoice->setLoanId($loan_data->id,true);
					$invoice->setPrincipalDue($principal_due);
					$invoice->setInterestDue($interest_due);
					$invoice->setDueDate($due_date);
					$invoice->setTrnId( $trn->getId() );
					$invoice->insert();

					redirect("services_lending/schedule/{$loan_data->id}");

				}

			}
		}

		$this->template_data->set('current_loan', $loan_data);

		$this->template_data->set('member', $this->_member_data( $loan_data->member_id ) );

		$this->load->view('services/lending/lending_add_invoice', $this->template_data->get_data());
	}

	public function edit_invoice($id, $output='') {

		$this->_isAuth('services', 'lending', 'edit');

		$this->template_data->set('output', $output);

		$setting = $this->_settings();

		$this->template_data->set('current_sub_uri', 'services_lending_loans');

		$invoice = new $this->Lending_invoices_model('li');
		$invoice->setId($id,true);
		$invoice->set_select('li.*');
		$invoice->set_select('(SELECT sum(lpa.amount) FROM `lending_payment_applied` lpa WHERE lpa.invoice_id=li.id) as total_applied');
		$invoice->set_select('(SELECT member_id FROM `lending_loans` ll WHERE li.loan_id=ll.id) as member_id');
		$invoice_data = $invoice->get();

		if( !is_null( $invoice_data->total_applied ) ) {
			redirect("services_lending/schedule/{$invoice_data->loan_id}");
		}

		if( $this->input->post() ) {
			$this->form_validation->set_rules('due_date', 'Due Date', 'trim|required');
			$this->form_validation->set_rules('principal_due', 'Payment Due', 'trim|required|decimal');
			$this->form_validation->set_rules('interest_due', 'Interst Due', 'trim|required|decimal');
			if($this->form_validation->run()) { 

				$principal_due = str_replace(',', '', $this->input->post('principal_due') );
				$interest_due = str_replace(',', '', $this->input->post('interest_due') );
				$date = date('Y-m-d', strtotime($this->input->post('due_date')));

				$invoice->setDueDate($date);
				$invoice->setPrincipalDue( $principal_due );
				$invoice->setInterestDue( $interest_due );
				$invoice->set_exclude(array('id','loan_id', 'trn_id'));

				if( $invoice->update() ) {
					
					// debit
					$debit = new $this->Accounting_entries_model;
					$debit->setTrnId( $invoice_data->trn_id, true,false );
					$debit->setChartId( $setting->receivable );
					$debit->setDebit( $interest_due );
					$debit->setClass( $setting->class_name );
					$debit->setItemNumber(0,true,false);
					$debit->set_exclude(array('id','name_id'));
					$debit->update();

					// credit
					$credit = new $this->Accounting_entries_model;
					$credit->setTrnId( $invoice_data->trn_id, true,false );
					$credit->setChartId( $setting->interest_income );
					$credit->setCredit( $interest_due );
					$credit->setClass( $setting->class_name );
					$credit->setItemNumber(1,true,false);
					$credit->set_exclude(array('id','name_id'));
					$credit->update();

					$trn = new $this->Accounting_transactions_model;
					$trn->setId( $invoice_data->trn_id, true );
					$trn->setDate( $date, false, true );
					$trn->setNameId( $invoice_data->member_id, false, true);
					$trn->setAmount( $interest_due , false, true);
					$trn->update();

					//redirect(uri_string() . "?updated=true");
				}
			}
			$this->postNext();
		}

		$this->template_data->set('current_invoice', $invoice->get());

		$loan = new $this->Lending_loans_model;
		$loan->setId($invoice_data->loan_id,true);
		$loan_data = $loan->get();
		$this->template_data->set('current_loan', $loan_data);

		$this->template_data->set('member', $this->_member_data( $loan_data->member_id ) );


		$this->load->view('services/lending/lending_edit_invoice', $this->template_data->get_data());
	}

	public function delete_invoice($id) {
		
		$invoice = new $this->Lending_invoices_model('li');
		$invoice->setId($id,true);
		$invoice->set_select('li.*');
		$invoice->set_select('(SELECT sum(lpa.amount) FROM `lending_payment_applied` lpa WHERE lpa.invoice_id=li.id) as total_applied');
		$invoice_data = $invoice->get();

		if( !is_null( $invoice_data->total_applied ) ) {
			redirect("services_lending/schedule/{$invoice_data->loan_id}");
		}

		if( $invoice->nonEmpty() ) {
			$invoice2 = new $this->Lending_invoices_model;
			$invoice2->setId($id,true);
			$invoice2->delete();
			redirect("services_lending/schedule/{$invoice_data->loan_id}");
		}
	}

	public function unapplied_payments($mid=0, $output='', $start=0) { 

		$this->template_data->set('member', $this->_member_data( $mid ) );

		$setting = $this->_settings();

		$payments = new $this->Lending_payments_model('lp');
		if( $mid ) {
			$payments->setMemberId($mid,true);
		}
		$payments->set_join('members m', 'm.id=lp.member_id');
		$payments->set_order('receipt_number', 'DESC');
		$payments->set_order('payment_date', 'DESC');
		$payments->set_select('m.lastname, m.firstname, m.middlename');
		$payments->set_select('lp.*');
		$payments->set_select('(SELECT SUM(amount) FROM lending_payment_applied lpa WHERE lpa.payment_id=lp.id) as total_applied');

		$payments->set_where('(SELECT SUM(lpa.amount) FROM lending_payment_applied lpa WHERE lpa.payment_id=lp.id) IS NULL');
		$payments->set_order('receipt_number', 'ASC');
		$payments->set_order('payment_date', 'ASC');

		$payments->set_start($start);
		$payments->set_limit(5);

		$this->template_data->set('payments', $payments->populate());  

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'uri_segment' => 5,
			'base_url' => base_url( $this->config->item('index_page') . "/services_lending/new_payments/{$mid}/{$output}"),
			'total_rows' => $payments->count_all_results(),
			'per_page' => $payments->get_limit(),
			'attributes' => array('class' => 'btn btn-default ajax-modal-inner')
		), '?next=' . $this->input->get('next') ));

		$this->template_data->set('output', $output);
		$this->load->view('services/lending/lending_unapplied_payments', $this->template_data->get_data());
	}

	public function new_payments($mid=0, $output='normal', $start=0) { 

		$this->template_data->set('member', $this->_member_data( $mid ) );

		$setting = $this->_settings();

		$new_payments = new $this->Accounting_entries_model('ae');
		//$new_payments->setItemType('LOANPY', true);
		$new_payments->setChartId($setting->receivable, true);
		if( $mid ) {
			$new_payments->setNameId($mid, true);
		}
		$new_payments->set_join('accounting_transactions at', 'at.id=ae.trn_id');
		$new_payments->set_join('accounting_receipts ar', 'ar.trn_id=ae.trn_id');
		$new_payments->set_join('members m', 'm.id=ae.name_id');
		$new_payments->set_select('ae.*');
		$new_payments->set_select('at.*');
		$new_payments->set_select('ar.*');
		$new_payments->set_select('m.lastname, m.firstname, m.middlename');
		$new_payments->set_select('ae.id as ae_id');
		$new_payments->set_select('ae.name_id as ae_name_id');
		$new_payments->set_select('at.date as trn_date');
		$new_payments->set_select('at.number as trn_number');
		$new_payments->set_where('((SELECT entry_id FROM  `lending_payments` WHERE entry_id=ae.id) IS NULL)');
		$new_payments->set_where('((SELECT id FROM  `members` WHERE id=ae.name_id) IS NOT NULL)');
		$new_payments->set_where_in('ae.item_type', array('LOANPY', 'LOANADJ'));
		$new_payments->set_start($start);
		$new_payments->set_order('at.number', 'ASC');
		$new_payments->set_order('at.date', 'ASC');
		$new_payments->set_limit(5);
		$this->template_data->set('new_payments', $new_payments->populate()); 

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'uri_segment' => 5,
			'base_url' => base_url( $this->config->item('index_page') . "/services_lending/new_payments/{$mid}/{$output}"),
			'total_rows' => $new_payments->count_all_results(),
			'per_page' => $new_payments->get_limit(),
			'attributes' => array(
				'class' => 'btn btn-default ajax-modal-inner',
				'data-hide_footer' => 1
				)
		), '?next=' . $this->input->get('next') ));

		$this->template_data->set('output', $output);
		$this->load->view('services/lending/lending_new_payments', $this->template_data->get_data());
	}

	public function applied_payments($id, $output='') {
		$this->template_data->set('output', $output);

		$invoice = new $this->Lending_invoices_model;
		$invoice->setId($id,true);
		$invoice_data = $invoice->get();

		$loan = new $this->Lending_loans_model;
		$loan->setId($invoice_data->loan_id,true);
		$loan_data = $loan->get();
		$this->template_data->set('current_loan', $loan_data);

		$this->template_data->set('member', $this->_member_data( $loan_data->member_id ) );

		$payments = new $this->Lending_payment_applied_model('lpa');
		$payments->set_join('lending_payments lp', 'lp.id=lpa.payment_id');
		$payments->set_join('lending_invoices li', 'li.id=lpa.invoice_id');
		$payments->set_join('lending_loans ll', 'li.loan_id=ll.id');
		$payments->set_join('coop_names cn', 'cn.id=lp.member_id');

		$payments->set_select('lp.*');
		$payments->set_select('lp.id as payment_id');
		$payments->set_select('li.*');
		$payments->set_select('lpa.*');
		$payments->set_select('cn.full_name');
		$payments->set_select('ll.member_id as lender_id');
		$payments->set_select('lp.member_id as payor_id');

		$payments->set_where('lpa.invoice_id', $id);
		$payments->set_order('lp.receipt_number', 'DESC');
		$payments->set_order('lp.payment_date', 'DESC');
		$payments->set_limit(0);
		$this->template_data->set('payments', $payments->populate());

		$this->load->view('services/lending/lending_applied_payments', $this->template_data->get_data());
	}

	public function loan_payments($id) {

		$loan = new $this->Lending_loans_model;
		$loan->setId($id,true);
		$loan_data = $loan->get();
		$this->template_data->set('current_loan', $loan_data);

		$this->template_data->set('member', $this->_member_data( $loan_data->member_id ) );

		$payments = new $this->Lending_payment_applied_model('lpa');
		$payments->set_join('lending_payments lp', 'lp.id=lpa.payment_id');
		$payments->set_join('lending_invoices li', 'li.id=lpa.invoice_id');
		$payments->set_join('lending_loans ll', 'li.loan_id=ll.id');
		$payments->set_join('coop_names cn', 'cn.id=lp.member_id');

		$payments->set_select('SUM(lpa.amount) as lp_amount');
		$payments->set_select('lpa.payment_id');
		$payments->set_select('lp.receipt_number');
		$payments->set_select('lp.payment_date');
		$payments->set_select('cn.full_name');
		$payments->set_select('ll.member_id as lender_id');
		$payments->set_select('lp.member_id as payor_id');

		$payments->set_where('li.loan_id', $id);
		$payments->set_order('lp.receipt_number', 'DESC');
		$payments->set_order('lp.payment_date', 'DESC');
		$payments->set_limit(0);
		$payments->set_group_by('lp.id');
		$this->template_data->set('payments', $payments->populate());

		$setting = $this->_settings();

		$new_payments = new $this->Accounting_entries_model('ae');
		//$new_payments->setItemType('LOANPY', true);
		$new_payments->setChartId($setting->receivable, true);
		$new_payments->setNameId($loan_data->member_id,true);
		$new_payments->set_join('accounting_transactions at', 'at.id=ae.trn_id');
		$new_payments->set_join('accounting_receipts ar', 'ar.trn_id=ae.trn_id');
		$new_payments->set_select('ae.*');
		$new_payments->set_select('at.*');
		$new_payments->set_select('ar.*');
		$new_payments->set_select('ae.id as ae_id');
		$new_payments->set_select('ae.name_id as ae_name_id');
		$new_payments->set_where('((SELECT entry_id FROM  `lending_payments` WHERE entry_id=ae.id) IS NULL)');
		$new_payments->set_where_in('ae.item_type', array('LOANPY', 'LOANADJ'));
		$this->template_data->set('new_payments', $new_payments->populate());

		$this->template_data->set('current_page', 'Loans');
		$this->template_data->set('current_sub_uri', 'services_lending_loans');

		$this->load->view('services/lending/lending_loan_payments', $this->template_data->get_data());
	}

	public function payments($mid, $status='all', $start=0) {

		$this->template_data->set('member', $this->_member_data( $mid ) );

		if(( $this->input->post('output') == 'body_wrapper' )||( $this->input->post('output') == 'inner_page' )) {

		$payments = new $this->Lending_payments_model('lp');
		$payments->setMemberId($mid,true);
		$payments->set_order('receipt_number', 'DESC');
		$payments->set_order('payment_date', 'DESC');

		$payments->set_select('lp.*');
		$payments->set_select('(SELECT SUM(amount) FROM lending_payment_applied lpa WHERE lpa.payment_id=lp.id) as total_applied');

		$this->template_data->set('payment_page_title', "All Payments"); 

		switch($status) {
			case 'applied':
				$this->template_data->set('payment_page_title', "Applied Payments");
				$payments->set_where('(SELECT SUM(lpa.amount) FROM lending_payment_applied lpa WHERE lpa.payment_id=lp.id) > 0');
			break;
			case 'unapplied':
				$this->template_data->set('payment_page_title', "Unapplied Payments");
				$payments->set_where('(SELECT SUM(lpa.amount) FROM lending_payment_applied lpa WHERE lpa.payment_id=lp.id) IS NULL');
				$payments->set_order('receipt_number', 'ASC');
				$payments->set_order('payment_date', 'ASC');
			break;
		}

		$payments->set_start($start);
		$payments->set_limit(10);
		$this->template_data->set('payments', $payments->populate()); 

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'uri_segment' => 5,
			'base_url' => base_url( $this->config->item('index_page') . "/services_lending/payments/{$mid}/{$status}"),
			'total_rows' => $payments->count_all_results(),
			'per_page' => $payments->get_limit(),
			'ajax' => true,
		)));

		}

		$setting = $this->_settings();

		$new_payments = new $this->Accounting_entries_model('ae');
		//$new_payments->setItemType('LOANPY', true);
		$new_payments->setChartId($setting->receivable, true);
		$new_payments->setNameId($mid,true);
		$new_payments->set_join('accounting_transactions at', 'at.id=ae.trn_id');
		$new_payments->set_join('accounting_receipts ar', 'ar.trn_id=ae.trn_id');
		$new_payments->set_select('ae.*');
		$new_payments->set_select('at.*');
		$new_payments->set_select('ar.*');
		$new_payments->set_select('ae.id as ae_id');
		$new_payments->set_select('ae.name_id as ae_name_id');
		$new_payments->set_where('((SELECT entry_id FROM  `lending_payments` WHERE entry_id=ae.id) IS NULL)');
		$new_payments->set_where_in('ae.item_type', array('LOANPY', 'LOANADJ'));
		$this->template_data->set('new_payments', $new_payments->populate());

		$this->template_data->set('current_page', 'Payments');
		$this->template_data->set('current_sub_uri', 'services_lending_payments');

		$this->load->view('services/lending/lending_payments', $this->template_data->get_data());
	}

	public function open_invoices($mid, $start=0) {

			$this->template_data->set('member', $this->_member_data( $mid ) );

			if(( $this->input->post('output') == 'body_wrapper' )||( $this->input->post('output') == 'inner_page' )) {

			$invoices = new $this->Lending_invoices_model('li');
			$invoices->set_limit(10);

			$invoices->set_join('lending_loans ll', 'll.id=li.loan_id');
			$invoices->set_join('members m', 'm.id=ll.member_id');
			
			$invoices->set_select('li.*');
			$invoices->set_select('m.id as member_id, m.lastname, m.firstname, m.middlename');
			$invoices->set_select('(li.principal_due + li.interest_due) as gross_amount');

			$invoices->set_select('(SELECT SUM(amount) FROM lending_payment_applied lpa WHERE lpa.invoice_id=li.id) as total_paid');

			$invoices->set_select('(SELECT (gross_amount - (IF((total_paid IS NULL),0,total_paid) )) ) as balance');

			$invoices->set_order('li.due_date', 'DESC');
			$invoices->set_start( $start );
			$invoices->set_where( 'm.id =', $mid );
			$invoices->set_where( 'li.due_date <=', date('Y-m-d') );
			$invoices->set_where( 'ROUND((SELECT ((li.principal_due + li.interest_due) - (IF(((SELECT SUM(amount) FROM lending_payment_applied lpa WHERE lpa.invoice_id=li.id) IS NULL),0,(SELECT SUM(amount) FROM lending_payment_applied lpa WHERE lpa.invoice_id=li.id)) )) )) > 0' );
			
			$this->template_data->set('invoices', $invoices->populate());
		
			$this->template_data->set('pagination', bootstrap_pagination(array(
				'uri_segment' => 4,
				'base_url' => base_url( $this->config->item('index_page') . "/services_lending/open_invoices/{$mid}"),
				'total_rows' => $invoices->count_all_results(),
				'per_page' => $invoices->get_limit(),
				'ajax' => true,
			)));

			}

			$this->template_data->set('current_page', 'Open Invoices');
			$this->template_data->set('current_sub_uri', 'services_lending_open_invoices');

			$this->load->view('services/lending/lending_open_invoices', $this->template_data->get_data());
	}

	public function print_open_invoices() {

		$invoices = new $this->Lending_invoices_model('li');
		$invoices->set_limit(10);

		$invoices->set_join('lending_loans ll', 'll.id=li.loan_id');
		$invoices->set_join('members m', 'm.id=ll.member_id');
		$invoices->set_group_by('ll.member_id');
		
		$invoices->set_select('li.*');
		$invoices->set_select('m.id as member_id, m.lastname, m.firstname, m.middlename, m.phone_mobile, m.phone_home');
		$invoices->set_select('(li.principal_due +li.interest_due) as gross_amount');
		$invoices->set_select('(SUM(li.principal_due)) as total_principal_due');
		$invoices->set_select('(SUM(li.interest_due)) as total_interest_due');
		$invoices->set_order('m.lastname', 'ASC');
		$invoices->set_limit( 0 );
		$invoices->set_where( 'li.due_date <=', date('Y-m-d') );
		$invoices->set_where( 'ROUND((SELECT ((li.principal_due + li.interest_due) - (IF(((SELECT SUM(amount) FROM lending_payment_applied lpa WHERE lpa.invoice_id=li.id) IS NULL),0,(SELECT SUM(amount) FROM lending_payment_applied lpa WHERE lpa.invoice_id=li.id)) )) )) > 0' );
		
		$this->template_data->set('invoices', $invoices->populate());
	
		$this->load->view('services/lending/lending_print_open_invoices', $this->template_data->get_data());

	}

	public function payment($pid, $output='') {

		$this->template_data->set('output', $output);
		
		$this->template_data->set('current_sub_uri', 'services_lending_payments');

		$setting = $this->_settings();
		$this->template_data->set('setting', $setting);

		$payment = new $this->Lending_payments_model('lp');
		$payment->setId($pid,true);
		$payment->set_select('lp.*');
		$payment->set_select('(SELECT SUM(amount) FROM lending_payment_applied lpa WHERE lp.id=lpa.payment_id) as applied'); 
		$payment->set_select('(SELECT adi.deposit_id FROM accounting_deposits_items adi JOIN accounting_entries ae ON adi.entry_id=ae.id JOIN accounting_transactions at ON ae.trn_id=at.id WHERE ae.trn_id=lp.trn_id) as deposit_id');
		$payment_data = $payment->get();

		$this->template_data->set('payment', $payment_data );
		$this->template_data->set('member', $this->_member_data( $payment_data->member_id ) );

		$applied = new $this->Lending_payment_applied_model;
		$applied->setPaymentId($pid,true);
		$this->template_data->set('applied', $applied->count_all_results() );

		$this->load->view('services/lending/lending_payment_update', $this->template_data->get_data());
	}

	public function pay($mid,$aeid,$output='') {

		$this->_isAuth('services', 'lending', 'edit');

		$this->template_data->set('output', $output);

		$this->template_data->set('current_sub_uri', 'services_lending_payments');

		$setting = $this->_settings();
		$this->template_data->set('setting', $setting);

		$new_payment = new $this->Accounting_entries_model('ae');
		$new_payment->setId($aeid,true);
		//$new_payment->setItemType('LOANPY', true);
		$new_payment->setChartId($setting->receivable, true);
		$new_payment->setNameId($mid, true);
		$new_payment->set_join('accounting_transactions at', 'at.id=ae.trn_id');
		$new_payment->set_join('accounting_receipts ar', 'ar.trn_id=ae.trn_id');
		$new_payment->set_join('coop_names cn', 'cn.id=ae.name_id');
		$new_payment->set_select('ae.*');
		$new_payment->set_select('at.*');
		$new_payment->set_select('ar.*');
		$new_payment->set_select('cn.full_name');
		$new_payment->set_select('ae.id as ae_id');
		$new_payment->set_select('ae.name_id as ae_name_id');
		$new_payment->set_select('at.date as trn_date');
		$new_payment->set_select('at.number as trn_number');
		$new_payment->set_select('at.id as trn_id');
		$new_payment->set_where('((SELECT entry_id FROM  `lending_payments` WHERE entry_id=ae.id) IS NULL)');
		$new_payment->set_where_in('ae.item_type', array('LOANPY', 'LOANADJ'));
		if( $new_payment->nonEmpty() ) {

			$entry_data = $new_payment->get();
			$this->template_data->set('new_payment', $entry_data); 

			if( $this->input->post() ) {

				$payment = new $this->Lending_payments_model;
				$payment->setMemberId($mid);
				$payment->setPaymentDate( $entry_data->trn_date );
				$payment->setReceiptNumber( $entry_data->trn_number );
				$payment->setAmount( $entry_data->credit );
				$payment->setTrnId( $entry_data->trn_id );
				$payment->setEntryId( $entry_data->ae_id );
			
				if( $payment->insert() ) {
						redirect( site_url("services_lending/payment_apply/{$payment->getId()}") . "?next=" . urlencode($this->input->get('next')) );
				}
					
				$this->postNext("error_code=104");
			}
		}

		$this->template_data->set('member', $this->_member_data( $mid ) );

		$this->load->view('services/lending/lending_pay', $this->template_data->get_data());
	}

	public function payment_apply($pid,$output='') {

		$this->_isAuth('services', 'lending', 'edit');

		$setting = $this->_settings();

		$payment = new $this->Lending_payments_model('lp');
		$payment->setId($pid,true);
		$payment->set_select('lp.*');
		$payment->set_select('(SELECT sum(lpa.amount) FROM `lending_payment_applied` lpa WHERE lpa.payment_id=lp.id) as total_applied');
		$payment_data = $payment->get();

		$this->template_data->set('payment', $payment_data);

		$applied = $payment_data->amount;
		$total_applied = 0;
		if( $this->input->post('apply_invoice') && $this->input->post('amount_due') ) {
			
			$amount_due = $this->input->post('amount_due');
			foreach( $this->input->post('apply_invoice') as $invoice_id => $amount2ba ) {
							
				$a_due = $amount_due[$invoice_id];
				$amount2ba = str_replace(",", "", $amount2ba);
				$amount = (floatval($amount2ba) > floatval($a_due)) ? floatval($a_due) : floatval($amount2ba);
				$amount = (floatval($amount) > floatval($applied)) ? floatval($applied) : floatval($amount);
				$total_applied += $amount;
				$applied -= $amount;

				$apply = new $this->Lending_payment_applied_model;
				$apply->setPaymentId($pid,true);
				$apply->setInvoiceId($invoice_id,true);
				$apply->setAmount( $amount );

				if( floatval($total_applied) > floatval($payment_data->amount) ) {
					$apply->delete();
					continue;
				}

				if( $amount ) {
					if( $apply->nonEmpty() ) {
						$apply->set_exclude('id');
						$apply->update();
					} else {
						$apply->insert();
					}
				} else {
					$apply->delete();
				}
			}

			if( $this->input->get('next') ) {
				redirect( urldecode( $this->input->get('next') ) );
			} else {
				redirect( uri_string() );
			}
		}

		$payment = new $this->Lending_payments_model;
		$payment->setId($pid,true);
		$payment_data = $payment->get();
		$this->template_data->set('member', $this->_member_data( $payment_data->member_id ) );

		$invoice = new $this->Lending_invoices_model('li');
		$invoice->set_order('li.due_date', 'ASC');
		$invoice->set_select('li.*');
		$invoice->set_select('(SELECT lpa.amount FROM `lending_payment_applied` lpa WHERE lpa.invoice_id=li.id AND lpa.payment_id='.$pid.') as applied');
		$invoice->set_select('(SELECT sum(lpa.amount) FROM `lending_payment_applied` lpa WHERE lpa.invoice_id=li.id) as total_applied');
		$invoice->set_select('(SELECT((li.principal_due + li.interest_due) - (IF(total_applied IS NULL, 0, total_applied)) + (IF(applied IS NULL, 0, applied)))) as amount_due');
		$invoice->set_join('lending_loans ll', 'll.id=li.loan_id');
		$invoice->set_where('ll.member_id', $payment_data->member_id);
		$invoice->set_having('amount_due > 0');
		$invoice->set_limit(0);
		$open_invoices = $invoice->populate();
		$this->template_data->set('open_invoices', $open_invoices);


		$this->template_data->set('current_page', 'Payments');
		$this->template_data->set('current_sub_uri', 'services_lending_payments');

		$this->template_data->set('output', $output);
		$this->load->view('services/lending/lending_payment_apply', $this->template_data->get_data());
	}

	public function reset_payment_apply($pid) {
		$apply = new $this->Lending_payment_applied_model;
		$apply->setPaymentId($pid,true);
		$apply->delete();

		redirect("services_lending/payment_apply/{$pid}");
	}

	public function payment_delete($pid, $mid) {	

		$payment = new $this->Lending_payments_model('lp');
		$payment->setId($pid,true);
		$payment->set_select('(SELECT SUM(amount) FROM lending_payment_applied lpa WHERE lp.id=lpa.payment_id) as applied'); 
		$payment_data = $payment->get();
		
		if( $payment_data->applied ) {
			redirect("services_lending/payment_apply/{$pid}");
		}

		$payment2 = new $this->Lending_payments_model;
		$payment2->setId($pid,true);
		$payment2->delete();

		redirect("services_lending/payments/{$mid}");

	}

	public function save_configuration() {

		$this->_isAuth('system', 'settings', 'add');
		$this->_isAuth('system', 'settings', 'edit');

		if($this->input->post()) {

			$old_values = array();
			$new_values = array();

			foreach( $this->input->post() as $key=>$value ) {
				$setting = new $this->Coop_settings_model;
				$setting->setDepartment('SERVICES',true);
				$setting->setSection('LENDING',true);
				$setting->setKey($key,true);
				$setting->setValue($value);
				if($setting->nonEmpty()) {
					$setting->set_exclude('id');
					$results = $setting->getResults();
					$old_values[$results->key] = $results->value;
					$new_values[$key] = $value;
					$setting->update();
				} else {
					$setting->insert();
				}
			}

			if( $old_values['receivable'] != $new_values['receivable'] ) {
				$entries = new $this->Accounting_entries_model;
				$entries->setChartId($new_values['receivable'],false,true);
				$entries->set_where( "accounting_entries.chart_id", $old_values['receivable'], 99);
				$entries->set_where("(((SELECT id FROM accounting_transactions at WHERE at.dept='SERVICES' AND at.sect='LENDING' AND at.type='LOAN' AND at.id=accounting_entries.trn_id) = accounting_entries.trn_id)" );
				$entries->set_where_or("((SELECT id FROM accounting_transactions at WHERE at.dept='SERVICES' AND at.sect='LENDING' AND at.type='INTEREST' AND at.id=accounting_entries.trn_id) = accounting_entries.trn_id))");
				$entries->update();
			}

			if( $old_values['interest_income'] != $new_values['interest_income'] ) {
				$entries = new $this->Accounting_entries_model;
				$entries->setChartId($new_values['interest_income'],false,true);
				$entries->set_where("((SELECT id FROM accounting_transactions at WHERE at.dept='SERVICES' AND at.sect='LENDING' AND at.type='INTEREST' AND at.id=accounting_entries.trn_id) = accounting_entries.trn_id)");
				$entries->set_where( "accounting_entries.chart_id", $old_values['interest_income'] );
				$entries->update();
			}

			if( $old_values['class_name'] != $new_values['class_name'] ) {
				$entries = new $this->Accounting_entries_model;
				$entries->setClass($new_values['class_name'],false,true);
				$entries->set_where("(((SELECT id FROM accounting_transactions at WHERE at.dept='SERVICES' AND at.sect='LENDING' AND at.type='LOAN' AND at.id=accounting_entries.trn_id) = accounting_entries.trn_id)" );
				$entries->set_where_or("((SELECT id FROM accounting_transactions at WHERE at.dept='SERVICES' AND at.sect='LENDING' AND at.type='INTEREST' AND at.id=accounting_entries.trn_id) = accounting_entries.trn_id))");
				$entries->set_where( "accounting_entries.class", $old_values['class_name'] );
				$entries->update();
			}

			if( $this->input->get('next') ) {
				redirect( $this->input->get('next') );
			} else {
				redirect( "services_lending/configuration" );
			}
		}
	}
	
	public function configuration() {

		$this->_isAuth('system', 'settings', 'view');
		$this->_isAuth('system', 'settings', 'add');
		$this->_isAuth('system', 'settings', 'edit');
		$this->_isAuth('services', 'lending', 'edit');
		
		$settings = new $this->Coop_settings_model;
		$settings->setDepartment('SERVICES',true);
		$settings->setSection('LENDING',true);
		$this->template_data->set('settings', $settings->populate());

		$main_charts = new $this->Accounting_charts_model;
		$main_charts->set_order('type', 'ASC');
		$main_charts->set_order('number', 'ASC');
		$main_charts->set_select('id,title,type');
		$main_charts->set_limit(0);
		$charts_data = $main_charts->recursive('parent', 0);
		$this->template_data->set('account_titles', $charts_data);

		$list = new $this->Coop_lists_model;
		$list->setDept('ACCOUNTING',true);
		$list->setType('CLASS',true);
		$list->setActive(1,true);
		$list->set_select('id,value');
		$list->set_limit(0);
		$list_data = $list->recursive('parent', 0);
		$this->template_data->set('class_list', $list_data);

		$users = new $this->User_accounts_model;
		$this->template_data->set( 'users', $users->populate() );

		$this->load->view('services/lending/lending_configuration', $this->template_data->get_data());
	}

	public function search_name() {

		$members = new $this->Members_model('c');
		if( $this->input->get('q') ) {
			$members->set_where('lastname LIKE "%' . $this->input->get('q') . '%"');
			$members->set_where_or('firstname LIKE "%' . $this->input->get('q') . '%"');
			$members->set_where_or('middlename LIKE "%' . $this->input->get('q') . '%"');
		}
		$this->template_data->set('names', $members->populate());

		$this->load->view('services/lending/lending_search_name', $this->template_data->get_data());
	}

	public function ajax($action='') {
		$results = array();
		switch($action) {
			case 'lender':
				$members = new $this->Members_model;
				if( $this->input->get('term') ) {
					$members->set_where('lastname LIKE "%' . $this->input->get('term') . '%"');
					$members->set_where_or('firstname LIKE "%' . $this->input->get('term') . '%"');
					$members->set_where_or('middlename LIKE "%' . $this->input->get('term') . '%"');
				}
				$data = array();
				foreach($members->populate() as $member) {
					$data[] = array(
						'label' => $member->lastname . ", " . $member->firstname . " " .$member->middlename,
						'id' => $member->id,
						'redirect'=> site_url("services_lending/loans/{$member->id}"),
						);
				}
				$results = $data;
			break;

			case 'change_member':
				$members = new $this->Members_model;
				if( $this->input->get('term') ) {
					$members->set_where('lastname LIKE "%' . $this->input->get('term') . '%"');
					$members->set_where_or('firstname LIKE "%' . $this->input->get('term') . '%"');
					$members->set_where_or('middlename LIKE "%' . $this->input->get('term') . '%"');
				}
				$data = array();
				switch( $this->input->get('sub_uri') ) {
					case 'services_lending_overview':
						$redirect_uri = "services_lending/overview/";
					break;
					case 'services_lending_payments':
						$redirect_uri = "services_lending/payments/";
					break;
					case 'services_lending_loans':
					default:
						$redirect_uri = "services_lending/loans/";
					break;
				}
				
				$members->set_order('lastname', 'ASC');
				$members->set_order('firstname', 'ASC');
				$members->set_order('middlename', 'ASC');
				foreach($members->populate() as $member) {
					$data[] = array(
						'label' => $member->lastname . ", " . $member->firstname . " " .$member->middlename,
						'id' => $member->id,
						'redirect'=> site_url($redirect_uri . $member->id),
						);
				}
				$results = $data;
			break;
		}
		$this->output
        ->set_content_type('application/json')
        ->set_output(json_encode( $results ));
	}

}
