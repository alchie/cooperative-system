<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Accounting_entries extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->template_data->set('page_title', 'COOP - Entries');
		$this->template_data->set('current_page', 'Entries');
		$this->template_data->set('current_uri', 'accounting_entries');
		
		$this->_isAuth('accounting', 'entries', 'view');
		
		$this->template_data->set('coa_types', unserialize(CHART_ACCOUNT_TYPES));

		$this->load->model('Accounting_entries_model');
	}

	public function index($start=0) {
		
		$entries = new $this->Accounting_entries_model;

		$entries->set_join('accounting_transactions', 'accounting_transactions.id=accounting_entries.trn_id');
		$entries->set_join('coop_names', 'coop_names.id=accounting_entries.name_id');

		$entries->set_select('accounting_entries.*');
		$entries->set_select('accounting_transactions.*');
		$entries->set_select('coop_names.full_name');

		$entries->set_order('accounting_transactions.date', 'DESC');
		$entries->set_order('accounting_transactions.id', 'DESC');
		$entries->set_where('accounting_transactions.date <=', date('Y-m-d'));
		$entries->set_start($start);
		$this->template_data->set('transactions', $entries->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
					'base_url' => base_url( $this->config->item('index_page') . "/accounting_entries/index"),
					'total_rows' => $entries->count_all_results(),
					'per_page' => $entries->get_limit(),
				)));

		$this->load->view('accounting/entries/accounting_entries', $this->template_data->get_data());
	}

}
