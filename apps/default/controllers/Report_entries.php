<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report_entries extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->template_data->set('page_title', 'COOP - Entries');
		$this->template_data->set('current_page', 'Entries');
		$this->template_data->set('current_uri', 'report_entries');
		
		$this->_isAuth('report', 'entries', 'view');
		
		$this->template_data->set('coa_types', unserialize(CHART_ACCOUNT_TYPES));

		$this->load->model('Accounting_entries_model');
	}

	public function index($start=0) {

		$filters = $this->input->get('filter');
		$filters_uri = '';
		$filters_arr = array();
		if( $this->input->get('filter') ) {
			foreach($filters as $key=>$value) {
				$filters_arr[] = "filter[{$key}]=" . urlencode($value);
			}
			$filters_uri = implode("&", $filters_arr);
		}

		$this->template_data->set('filters', $this->input->get('filter'));
		$this->template_data->set('filters_uri', $filters_uri);

		if(( $this->input->post('output') == 'body_wrapper' )||( $this->input->post('output') == 'inner_page' )) {

		$entries = new $this->Accounting_entries_model;

		$entries->set_join('accounting_transactions', 'accounting_transactions.id=accounting_entries.trn_id');
		$entries->set_join('coop_names', 'coop_names.id=accounting_entries.name_id');

		$entries->set_select('accounting_entries.*');
		$entries->set_select('accounting_transactions.*');
		$entries->set_select('coop_names.full_name');
		$entries->set_select('(SELECT COUNT(*) FROM accounting_transactions WHERE id=accounting_entries.trn_id) as trn_exist');

		$entries->set_order('accounting_transactions.date', 'DESC');
		$entries->set_order('accounting_transactions.id', 'DESC');
		$entries->set_where('accounting_transactions.date <=', date('Y-m-d'));
		$entries->set_start($start);
		$this->template_data->set('entries', $entries->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
					'base_url' => base_url( $this->config->item('index_page') . "/report_entries/index"),
					'total_rows' => $entries->count_all_results(),
					'per_page' => $entries->get_limit(),
					'ajax' => true,
				)));

		}

		$this->load->view('reports/entries/report_entries', $this->template_data->get_data());
	}

	public function transaction($trn_id, $start=0) {
		
		$entries = new $this->Accounting_entries_model;

		$entries->set_join('accounting_transactions', 'accounting_transactions.id=accounting_entries.trn_id');
		$entries->set_join('coop_names', 'coop_names.id=accounting_entries.name_id');

		$entries->set_select('accounting_entries.*');
		$entries->set_select('accounting_transactions.*');
		$entries->set_select('coop_names.full_name');

		$entries->set_order('accounting_transactions.date', 'DESC');
		$entries->set_order('accounting_transactions.id', 'DESC');
		
		$entries->set_where('accounting_transactions.id', $trn_id);
		$entries->set_where('accounting_transactions.date <=', date('Y-m-d'));
		$entries->set_group_by('accounting_entries.name_id');
		$entries->set_select('SUM(accounting_entries.debit) as total_debit');
		$entries->set_select('SUM(accounting_entries.credit) as total_credit');
		$entries->set_order('accounting_entries.debit', 'DESC');
		$entries->set_start($start);
		$entries->set_limit(0);
$entries->set_limit(2);
		$this->template_data->set('transactions', $entries->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
					'uri_segment' => 4,
					'base_url' => base_url( $this->config->item('index_page') . "/report_entries/transaction/{$trn_id}"),
					'total_rows' => $entries->count_all_results(),
					'per_page' => $entries->get_limit(),
					'ajax' => true,
				)));

		$this->load->view('reports/entries/report_entries_transaction', $this->template_data->get_data());
	}

}
