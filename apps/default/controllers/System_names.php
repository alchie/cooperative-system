<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class System_names extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->template_data->set('page_title', 'COOP - Names List');
		$this->template_data->set('current_page', 'Names List');
		$this->template_data->set('current_uri', 'system_names');
		$this->template_data->set('navbar_search', true);

		$this->_isAuth('system', 'names', 'view');

		$this->load->model('Coop_names_model');

	}

	private function _searchRedirect() {
		if( $this->input->get('q') ) {
			redirect(site_url("system_names?q=" . $this->input->get('q') ));
		}
	}

	public function index($start=0) {
		
		if( $start > 0 ) {
			$this->_searchRedirect();
		}

		if(( $this->input->post('output') == 'body_wrapper' )||( $this->input->post('output') == 'inner_page' )) {

		$names = new $this->Coop_names_model;

		if( $this->input->get('q') ) {
			$names->set_where('full_name LIKE "%' . $this->input->get('q') . '%"');
			$names->set_where_or('address LIKE "%' . $this->input->get('q') . '%"');
		}

		$names->set_select("coop_names.*");
		$names->set_select("(SELECT COUNT(*) FROM members WHERE members.id=coop_names.id) as members");
		$names->set_select("(SELECT COUNT(*) FROM companies WHERE companies.id=coop_names.id) as companies");

		$names->set_order('full_name', 'ASC');
		$names->set_start($start);

		$this->template_data->set('names', $names->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'base_url' => base_url( $this->config->item('index_page') . '/system_names/index/'),
			'total_rows' => $names->count_all_results(),
			'per_page' => $names->get_limit(),
			'ajax'=>true,
		)));

		}

		$this->load->view('system/names/names_list', $this->template_data->get_data());
	}

	public function add($output='') {

		$this->_isAuth('system', 'names', 'add');

		$this->template_data->set('output', $output);
		$name_id = false;
		if( $this->input->post() ) {
			$this->form_validation->set_rules('full_name', 'Full Name', 'trim|required|is_unique[coop_names.full_name]');
			$this->form_validation->set_rules('address', 'Address', 'trim');
			$this->form_validation->set_rules('contact_number', 'Contact Number', 'trim');
			if( $this->form_validation->run() ) {
				$name = new $this->Coop_names_model;
				$name->setFullName($this->input->post('full_name'));
				$name->setAddress($this->input->post('address'));
				$name->setContactNumber($this->input->post('contact_number'));
				if( ! $name->nonEmpty() ) {
					$name->insert();
					$name_id = $name->getId();
				}
			}
			if( $this->input->get('next') ) {
					$url = $this->input->get('next');
					if( $name_id ) {
						$url = str_replace('$new_id', $name_id, $url);
					}
                    
                    if( $name_id ) {
                    	$url = site_url($url) . "?error_code=340&new_name=" . $name_id;
                    } else {
                    	$url = site_url($url) . "?error_code=341";
                    }
                    redirect( $url );
            } else {
            	if( $name_id ) {
            		redirect( "system_names/edit/" . $name_id );
            	}
            }
		}
		$this->load->view('system/names/names_list_add', $this->template_data->get_data());
	}

	public function edit($id,$output='') {

		$this->_isAuth('system', 'names', 'edit');

		$this->template_data->set('output', $output);

		$name = new $this->Coop_names_model;
		$name->setId($id, true);

		if( $this->input->post() ) {
			$this->form_validation->set_rules('full_name', 'Full Name', 'trim|required');
			$this->form_validation->set_rules('address', 'Address', 'trim');
			$this->form_validation->set_rules('contact_number', 'Contact Number', 'trim');
			if( $this->form_validation->run() ) {
				$name->setFullName($this->input->post('full_name'));
				$name->setAddress($this->input->post('address'));
				$name->setContactNumber($this->input->post('contact_number'));
				if( $name->nonEmpty() ) {
					$name->set_exclude('id');
					$name->update();
				} 
			}
			$this->postNext();
		}

		$name->set_select("coop_names.*");
		$name->set_select("(SELECT COUNT(*) FROM members WHERE members.id=coop_names.id) as members");
		$name->set_select("(SELECT COUNT(*) FROM companies WHERE companies.id=coop_names.id) as companies");
		$name->set_select("(SELECT COUNT(*) FROM accounting_transactions WHERE accounting_transactions.name_id=coop_names.id) as transactions");
		$name->set_select("(SELECT COUNT(*) FROM accounting_entries WHERE accounting_entries.name_id=coop_names.id) as entries");
		$name->set_select("(SELECT COUNT(*) FROM accounting_checks WHERE accounting_checks.name_id=coop_names.id) as checks");
		$name->set_select("(SELECT COUNT(*) FROM accounting_checks_items WHERE accounting_checks_items.name_id=coop_names.id) as checks_items");
		$name->set_select("(SELECT COUNT(*) FROM accounting_receipts WHERE accounting_receipts.name_id=coop_names.id) as receipts");
		$name->set_select("(SELECT COUNT(*) FROM accounting_receipts_items WHERE accounting_receipts_items.name_id=coop_names.id) as receipts_items");

		$this->template_data->set('name', $name->get());
		
		$this->load->view('system/names/names_list_edit', $this->template_data->get_data());
	}

	public function delete($id) {
		
		$this->_isAuth('system', 'names', 'delete');

		$name = new $this->Coop_names_model;
		$name->setId($id, true);
		$name->set_select("coop_names.*");
		$name->set_select("(SELECT COUNT(*) FROM members WHERE members.id=coop_names.id) as members");
		$name->set_select("(SELECT COUNT(*) FROM companies WHERE companies.id=coop_names.id) as companies");
		$name->set_select("(SELECT COUNT(*) FROM accounting_transactions WHERE accounting_transactions.name_id=coop_names.id) as transactions");
		$name->set_select("(SELECT COUNT(*) FROM accounting_entries WHERE accounting_entries.name_id=coop_names.id) as entries");
		$name->set_select("(SELECT COUNT(*) FROM accounting_checks WHERE accounting_checks.name_id=coop_names.id) as checks");
		$name->set_select("(SELECT COUNT(*) FROM accounting_checks_items WHERE accounting_checks_items.name_id=coop_names.id) as checks_items");
		$name->set_select("(SELECT COUNT(*) FROM accounting_receipts WHERE accounting_receipts.name_id=coop_names.id) as receipts");
		$name->set_select("(SELECT COUNT(*) FROM accounting_receipts_items WHERE accounting_receipts_items.name_id=coop_names.id) as receipts_items");
		$name_data = $name->get();

		if( ($name_data->members == 0) && 
			($name_data->companies == 0) &&
			($name_data->transactions == 0) &&
			($name_data->entries == 0) &&
			($name_data->checks == 0) &&
			($name_data->checks_items == 0) &&
			($name_data->receipts == 0) &&
			($name_data->receipts_items == 0)
		) { 
			$name->delete();
		}

		redirect( "system_names" );
	}

}
