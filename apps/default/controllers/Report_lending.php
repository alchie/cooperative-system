<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report_lending extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->template_data->set('page_title', 'COOP - Lending');
		$this->template_data->set('current_page', 'Lending');
		$this->template_data->set('current_uri', 'report_lending');

		$this->_isAuth('report', 'lending', 'view');

		$this->template_data->set( 'coa_types', unserialize(CHART_ACCOUNT_TYPES) );

		$this->load->model('Coop_settings_model');
		$this->load->model('Coop_names_model');
		$this->load->model('Accounting_charts_model');
		$this->load->model('Accounting_entries_model');
		$this->load->model('Lending_loans_model');
		$this->load->model('Lending_payments_model');
		$this->load->model('Lending_payment_applied_model');
		$this->load->model('Lending_invoices_model');
		$this->load->model('Reports_saved_model');
	}

	public function index() {
		$this->outstanding_loans();
	}

	public function outstanding_loans() {

		$this->template_data->set('current_sub_page', 'Outstanding Loans');
		$this->template_data->set('current_sub_uri', 'report_lending_outstanding_loans');

		$report_type = 'outstanding_loans';
		$filters = $this->input->get('filter');
		$iframe_url = site_url("report_lending/generate_report/{$report_type}/iframe");
		$filters_uri = '';
		$filters_arr = array();
		if( $filters ) {
			foreach($filters as $key=>$value) {
				$filters_arr[] = "filter[{$key}]=" . urlencode($value);
			}
			$filters_uri = implode("&", $filters_arr);
			$iframe_url .= "?" . $filters_uri;
		}

		$this->template_data->set('iframe_url', $iframe_url);
		$this->template_data->set('filters', $filters);
		$this->template_data->set('filters_uri', $filters_uri);
		$this->template_data->set('report_type', $report_type);

		$reports = new $this->Reports_saved_model;
		$reports->setType($report_type, true);
		$reports->set_order('name', 'ASC');
		$this->template_data->set('reports', $reports->populate());

		$this->load->view('reports/lending/report_lending', $this->template_data->get_data());

	}

	public function pending_loans() {
		
		$this->template_data->set('current_sub_page', 'Pending Loans');
		$this->template_data->set('current_sub_uri', 'report_lending_pending_loans');

		$report_type = 'pending_loans';
		$filters = $this->input->get('filter');
		$iframe_url = site_url("report_lending/generate_report/{$report_type}/iframe");
		$filters_uri = '';
		$filters_arr = array();
		if( $filters ) {
			foreach($filters as $key=>$value) {
				$filters_arr[] = "filter[{$key}]=" . urlencode($value);
			}
			$filters_uri = implode("&", $filters_arr);
			$iframe_url .= "?" . $filters_uri;
		}

		$this->template_data->set('iframe_url', $iframe_url);
		$this->template_data->set('filters', $filters);
		$this->template_data->set('filters_uri', $filters_uri);
		$this->template_data->set('report_type', $report_type);

		$reports = new $this->Reports_saved_model;
		$reports->setType($report_type, true);
		$reports->set_order('name', 'ASC');
		$this->template_data->set('reports', $reports->populate());

		$this->load->view('reports/lending/report_lending', $this->template_data->get_data());

	}


	public function archived_loans() {
		
		$this->template_data->set('current_sub_page', 'Archived Loans');
		$this->template_data->set('current_sub_uri', 'report_lending_archived_loans');

		$report_type = 'archived_loans';
		$filters = $this->input->get('filter');
		$iframe_url = site_url("report_lending/generate_report/{$report_type}/iframe");
		$filters_uri = '';
		$filters_arr = array();
		if( $filters ) {
			foreach($filters as $key=>$value) {
				$filters_arr[] = "filter[{$key}]=" . urlencode($value);
			}
			$filters_uri = implode("&", $filters_arr);
			$iframe_url .= "?" . $filters_uri;
		}

		$this->template_data->set('iframe_url', $iframe_url);
		$this->template_data->set('filters', $filters);
		$this->template_data->set('filters_uri', $filters_uri);
		$this->template_data->set('report_type', $report_type);

		$reports = new $this->Reports_saved_model;
		$reports->setType($report_type, true);
		$reports->set_order('name', 'ASC');
		$this->template_data->set('reports', $reports->populate());

		$this->load->view('reports/lending/report_lending', $this->template_data->get_data());

	}

	public function payments() {
		
		$this->template_data->set('current_sub_page', 'Payments');
		$this->template_data->set('current_sub_uri', 'report_lending_payments');

		$report_type = 'payments';
		$filters = $this->input->get('filter');
		$iframe_url = site_url("report_lending/generate_report/{$report_type}/iframe");
		$filters_uri = '';
		$filters_arr = array();
		if( $filters ) {
			foreach($filters as $key=>$value) {
				$filters_arr[] = "filter[{$key}]=" . urlencode($value);
			}
			$filters_uri = implode("&", $filters_arr);
			$iframe_url .= "?" . $filters_uri;
		}

		$this->template_data->set('iframe_url', $iframe_url);
		$this->template_data->set('filters', $filters);
		$this->template_data->set('filters_uri', $filters_uri);
		$this->template_data->set('report_type', $report_type);

		$reports = new $this->Reports_saved_model;
		$reports->setType($report_type, true);
		$reports->set_order('name', 'ASC');
		$this->template_data->set('reports', $reports->populate());

		$this->load->view('reports/lending/report_lending', $this->template_data->get_data());

	}

	public function applied_payments() {
		
		$this->template_data->set('current_sub_page', 'Applied Payments');
		$this->template_data->set('current_sub_uri', 'report_lending_applied_payments');

		$report_type = 'applied_payments';
		$filters = $this->input->get('filter');
		$iframe_url = site_url("report_lending/generate_report/{$report_type}/iframe");
		$filters_uri = '';
		$filters_arr = array();
		if( $filters ) {
			foreach($filters as $key=>$value) {
				$filters_arr[] = "filter[{$key}]=" . urlencode($value);
			}
			$filters_uri = implode("&", $filters_arr);
			$iframe_url .= "?" . $filters_uri;
		}

		$this->template_data->set('iframe_url', $iframe_url);
		$this->template_data->set('filters', $filters);
		$this->template_data->set('filters_uri', $filters_uri);
		$this->template_data->set('report_type', $report_type);

		$reports = new $this->Reports_saved_model;
		$reports->setType($report_type, true);
		$reports->set_order('name', 'ASC');
		$this->template_data->set('reports', $reports->populate());

		$this->load->view('reports/lending/report_lending', $this->template_data->get_data());

	}

	public function open_invoices() {
		
		$this->template_data->set('current_sub_page', 'Open Invoices');
		$this->template_data->set('current_sub_uri', 'report_lending_open_invoices');

		$report_type = 'open_invoices';
		$filters = $this->input->get('filter');
		$iframe_url = site_url("report_lending/generate_report/{$report_type}/iframe");
		$filters_uri = '';
		$filters_arr = array();
		if( $filters ) {
			foreach($filters as $key=>$value) {
				$filters_arr[] = "filter[{$key}]=" . urlencode($value);
			}
			$filters_uri = implode("&", $filters_arr);
			$iframe_url .= "?" . $filters_uri;
		}

		$this->template_data->set('iframe_url', $iframe_url);
		$this->template_data->set('filters', $filters);
		$this->template_data->set('filters_uri', $filters_uri);
		$this->template_data->set('report_type', $report_type);

		$reports = new $this->Reports_saved_model;
		$reports->setType($report_type, true);
		$reports->set_order('name', 'ASC');
		$this->template_data->set('reports', $reports->populate());

		$this->load->view('reports/lending/report_lending', $this->template_data->get_data());

	}

	public function modify($report_type="outstanding_loans", $output='') {

		if( $this->input->post() ) {
			$url = site_url(($this->input->get('next')) ? $this->input->get('next') : "report_lending/{$report_type}") . "?";
			$filters = array();
			foreach($this->input->post('filter') as $key=>$value) {
				$value = (is_array($value)) ? implode('|', $value) : $value;
				$filters[] = "filter[{$key}]=" . urlencode($value);
			}
			$url .= implode('&', $filters);
			redirect( $url );
		}

		$this->template_data->set('report_type', $report_type);
		$this->template_data->set('filters', $this->input->get('filter'));
		
		$filters = $this->input->get('filter');
		if(isset($filters['name_id']) && (!empty($filters['name_id']))) { 
			$filter_name = new $this->Coop_names_model;
			$filter_name->setId($filters['name_id'],true);
			$this->template_data->set('filter_name', $filter_name->get());
		}

		$this->template_data->set('output', $output);
		$this->load->view('reports/lending/report_lending_modify', $this->template_data->get_data());

	}

	public function save_report($report_type='outstanding_loans', $report_id=NULL) {

		$this->_isAuth('report', 'lending', 'add');
		$this->_isAuth('report', 'lending', 'edit');

		if( $this->input->get('filter') ) {
			$filters = $this->input->get('filter');
			$saved = new $this->Reports_saved_model;
			if( $report_id ) {
				$saved->setId($report_id, true);
			}
			$saved->setName( $filters['report_title'] );
			$saved->setType($report_type);
			$saved->setFilters( json_encode($filters) );

			if( $report_id && $saved->nonEmpty() ) {
				$saved->update();
				redirect( site_url("report_lending/view_report/{$report_id}")  );
			} else {
				if( $saved->insert() ) {
					redirect( site_url("report_lending/view_report/{$saved->getId()}")  );
				}
			}

			$filters_uri = '';
			$filters_arr = array();
			if( $this->input->get('filter') ) {
				foreach($filters as $key=>$value) {
					$filters_arr[] = "filter[{$key}]=" . urlencode($value);
				}
				$filters_uri = implode("&", $filters_arr);
			}
			redirect( site_url("report_lending/{$report_type}") . "?" . $filters_uri );
		}
		redirect( "report_lending/{$report_type}" );
	}

	public function view_report($report_id, $start=0) {

		$report = new $this->Reports_saved_model;
		$report->setId($report_id, true);
		$report_filters = $report->get();
		$this->template_data->set('current_report', $report_filters);

		$current_sub_page = 'Outstanding Loans';
		switch( $report_filters->type ) {
			case 'pending_loans':
				$current_sub_page = 'Pending Loans';
			break;
			case 'archived_loans':
				$current_sub_page = 'Archived Loans';
			break;
			case 'payments':
				$current_sub_page = 'Payments';
			break;
			case 'applied_payments':
				$current_sub_page = 'Applied Payments';
			break;
			case 'open_invoices':
				$current_sub_page = 'Open Invoices';
			break;
			default:
				$current_sub_page = 'Outstanding Loans';
			break;
		}
		$this->template_data->set('current_sub_page', $current_sub_page);
		$this->template_data->set('current_sub_uri', 'report_lending_' . $report_filters->type);

		$filters = ($this->input->get('filter')) ? $this->input->get('filter') : (array) json_decode( $report_filters->filters );
		
		$iframe_url = site_url("report_lending/generate_report/{$report_filters->type}/iframe");
		$filters_uri = '';
		$filters_arr = array();
		if( $filters ) {
			foreach($filters as $key=>$value) {
				$filters_arr[] = "filter[{$key}]=" . urlencode($value);
			}
			$filters_uri = implode("&", $filters_arr);
			$iframe_url .= "?" . $filters_uri;
		}

		$this->template_data->set('iframe_url', $iframe_url);
		$this->template_data->set('filters', $filters);
		$this->template_data->set('filters_uri', $filters_uri);
		$this->template_data->set('report_type', $report_filters->type);

		$reports = new $this->Reports_saved_model;
		$reports->setType($report_filters->type, true);
		$reports->set_order('name', 'ASC');
		$this->template_data->set('reports', $reports->populate());

		$this->load->view('reports/lending/report_lending', $this->template_data->get_data());

	}

	public function delete_report($report_id) {

		$this->_isAuth('report', 'lending', 'delete');

		$report = new $this->Reports_saved_model;
		$report->setId($report_id, true);
		$report_data = $report->get();
		$report->delete();
		redirect("report_lending/{$report_data->type}");

	}
	
	public function generate_report($report_type='', $output='') {

		$this->template_data->set('output', $output);

		$filters = $this->input->get('filter'); 
		$this->template_data->set('filters', $filters);


		$date_range_beg = (isset( $filters['date_range_beg'] ) ) ? date('Y-m-d', strtotime($filters['date_range_beg'])) : date('Y-m-01');
		$date_range_end = (isset( $filters['date_range_end'] ) ) ? date('Y-m-d', strtotime($filters['date_range_end'])) : date('Y-m-d');

			switch( $report_type ) {
				case 'pending_loans':

					$loans = new $this->Lending_loans_model('ll');
					$loans->set_limit(0);

					$loans->set_join('members m', 'm.id=ll.member_id');

					$loans->set_select('ll.*');
					$loans->set_select('(SELECT sum(lpa.amount) FROM lending_payment_applied lpa JOIN lending_invoices li ON lpa.invoice_id=li.id WHERE li.loan_id=ll.id) as payments');
					
					$loans->set_select('m.lastname, m.firstname, m.middlename');

					// total_interest
					$loans->set_select('(SELECT IF( (ll.interest_type LIKE "fixed"), (ll.principal * (((ll.interest_rate / CEIL(30 / ll.skip_days)) * (CEIL((30 / ll.skip_days) * ll.months))) / 100)), ((((ll.principal * (ll.interest_rate / 100) ) / 2 ) * ((CEIL((30 / ll.skip_days) * ll.months)) + 1) * (1/CEIL(30 / ll.skip_days) ))) ) ) as total_interest');

					/*
					// fixed
					$loans->set_select('(SELECT (ll.principal * (((ll.interest_rate / CEIL(30 / ll.skip_days)) * (CEIL((30 / ll.skip_days) * ll.months))) / 100))) as fixed_interest');
		
					// diminishing
					$loans->set_select('(SELECT (((ll.principal * (ll.interest_rate / 100) ) / 2 ) * ((CEIL((30 / ll.skip_days) * ll.months)) + 1) * (1/CEIL(30 / ll.skip_days) ))) diminishing_interest');

					$loans->set_select('(SELECT IF( (ll.interest_type LIKE "fixed"), ((ll.principal + fixed_interest) - payments), ((ll.principal + diminishing_interest) - payments)))  as balance');
					*/
					
					$loans->set_select('(SELECT(ll.principal + total_interest)) as gross_amount');

					$loans->set_select('(SELECT ((ll.principal + (IF((total_interest IS NULL), 0, total_interest)) ) - (IF((payments IS NULL), 0, payments)) ) ) as balance');
					
					
					$loans->set_where('(SELECT COUNT(*) FROM lending_invoices li WHERE li.loan_id=ll.id) = 0');
					
					$loans->set_where('ll.loan_date >=', $date_range_beg);
					$loans->set_where('ll.loan_date <=', $date_range_end );

					$sort_by = ($filters['sort']) ? $filters['sort'] : 'DESC'; 
					switch($filters['order_by']) {
						case 'balance':
							$loans->set_order('balance', $sort_by);
						break;
						case 'total_interest':
							$loans->set_order('total_interest', $sort_by);
						break;
						case 'gross_amount':
							$loans->set_order('gross_amount', $sort_by);
						break;
						case 'payments':
							$loans->set_order('payments', $sort_by);
						break;
						case 'principal':
							$loans->set_order('ll.principal', $sort_by);
						break;
						case 'interest_type':
							$loans->set_order('ll.interest_type', $sort_by);
						break;
						case 'lastname':
							$loans->set_order('m.lastname', $sort_by);
						break;
						case 'firstname':
							$loans->set_order('m.firstname', $sort_by);
						break;
						case 'middlename':
							$loans->set_order('m.middlename', $sort_by);
						break;
						default:
							$loans->set_order('ll.loan_date', $sort_by);
						break;
					}
					$this->template_data->set('loans', $loans->populate());

					$this->load->view('reports/lending/report_lending_pending_loans', $this->template_data->get_data());
				break;

				case 'payments':
					$payments = new $this->Lending_payments_model('lp');
					$payments->set_limit(0);
					$payments->set_select('lp.*');
					$payments->set_select('(SELECT cn.full_name FROM coop_names cn WHERE cn.id=lp.member_id) as full_name');
					$payments->set_select('(SELECT SUM(amount) FROM lending_payment_applied lpa WHERE lpa.payment_id=lp.id) as total_applied');

					$sort_by = ($filters['sort']) ? $filters['sort'] : 'DESC'; 
					switch($filters['order_by']) {
						case 'payment_date':
							$payments->set_order('lp.payment_date', $sort_by);
						break;
						case 'amount_paid':
							$payments->set_order('lp.amount', $sort_by);
						break;
						case 'amount_applied':
							$payments->set_order('total_applied', $sort_by);
						break;
						case 'receipt_number':
						default:
							$payments->set_order('lp.receipt_number', $sort_by);
						break;
					}

					$this->template_data->set('payments', $payments->populate());
					$this->load->view('reports/lending/report_lending_payments', $this->template_data->get_data());
				break;
				case 'applied_payments':
					
					$payments = new $this->Lending_payment_applied_model('lpa');
					$payments->set_limit(0);

					$payments->set_join('lending_payments lp', 'lpa.payment_id=lp.id');
					$payments->set_join('lending_invoices li', 'lpa.invoice_id=li.id');
					$payments->set_join('lending_loans ll', 'll.id=li.loan_id');
					$payments->set_join('members m', 'm.id=lp.member_id');

					$payments->set_select('lp.*');
					$payments->set_select('m.lastname, m.firstname, m.middlename');
					$payments->set_select('li.principal_due as principal_due');
					$payments->set_select('li.interest_due as interest_due');
					$payments->set_select('lpa.amount as amount_applied');
					$payments->set_select('ll.loan_date');
					$payments->set_select('ll.principal as loan_principal');
					$payments->set_select('ll.interest_rate as loan_interest_rate');
					$payments->set_select('ll.interest_type as loan_interest_type');

					$payments->set_select('(lpa.amount * ( li.principal_due / (li.interest_due + li.principal_due) )) as applied_principal');
					$payments->set_select('(lpa.amount * ( li.interest_due / (li.interest_due + li.principal_due) )) as applied_interest');
					
					$payments->set_select('SUM(lpa.amount) as total_applied');
					$payments->set_select('SUM(li.principal_due) as total_principal');
					$payments->set_select('SUM(li.interest_due) as total_interest');
					$payments->set_select('SUM((lpa.amount * ( li.principal_due / (li.interest_due + li.principal_due) ))) as total_applied_principal');
					$payments->set_select('SUM((lpa.amount * ( li.interest_due / (li.interest_due + li.principal_due) ))) as total_applied_interest');
					$payments->set_group_by('lp.id');

					$payments->set_where('lp.payment_date >=', $date_range_beg);
					$payments->set_where('lp.payment_date <=', $date_range_end);

					$sort_by = ($filters['sort']) ? $filters['sort'] : 'DESC'; 

					$loan_date_beg = (isset( $filters['loan_date_beg'] ) ) ? date('Y-m-d', strtotime($filters['loan_date_beg'])) : date('Y-m-01');
					$loan_date_end = (isset( $filters['loan_date_end'] ) ) ? date('Y-m-d', strtotime($filters['loan_date_end'])) : date('Y-m-d');

					$payments->set_where('ll.loan_date >=', $loan_date_beg);
					$payments->set_where('ll.loan_date <=', $loan_date_end);

					switch($filters['order_by']) {
						case 'payment_date':
							$payments->set_order('lp.payment_date', $sort_by);
						break;
						case 'amount_paid':
							$payments->set_order('lp.amount', $sort_by);
						break;
						case 'amount_applied':
							$payments->set_order('amount_applied', $sort_by);
						break;
						case 'applied_principal':
							$payments->set_order('applied_principal', $sort_by);
						break;
						case 'applied_interest':
							$payments->set_order('applied_interest', $sort_by);
						break;
						case 'receipt_number':
						default:
							$payments->set_order('lp.receipt_number', $sort_by);
						break;
					}

					//$payments->set_group_by('lp.receipt_number');
					$payments_data = $payments->populate();
					//print_r( $payments_data );
					$this->template_data->set('payments', $payments_data);

					$this->load->view('reports/lending/report_lending_applied_payments', $this->template_data->get_data());
				break;
				case 'open_invoices':

					$invoices = new $this->Lending_invoices_model('li');
					$invoices->set_limit(0);

					$invoices->set_join('lending_loans ll', 'll.id=li.loan_id');
					$invoices->set_join('members m', 'm.id=ll.member_id');
					
					$invoices->set_select('li.*');
					$invoices->set_select('m.lastname, m.firstname, m.middlename');
					$invoices->set_select('(li.principal_due + li.interest_due) as gross_amount');

					$invoices->set_select('(SELECT SUM(amount) FROM lending_payment_applied lpa WHERE lpa.invoice_id=li.id) as total_paid');

					$invoices->set_select('(SELECT (gross_amount - (IF((total_paid IS NULL),0,total_paid) ))  ) as balance');

					$sort_by = ($filters['sort']) ? $filters['sort'] : 'DESC'; 
					switch($filters['order_by']) {
						case 'total_paid':
							$invoices->set_order('total_paid', $sort_by);
						break;
						case 'lastname':
							$invoices->set_order('m.lastname', $sort_by);
						break;
						case 'firstname':
							$invoices->set_order('m.firstname', $sort_by);
						break;
						case 'middlename':
							$invoices->set_order('m.middlename', $sort_by);
						break;
						case 'due_date':
						default:
							$invoices->set_order('li.due_date', $sort_by);
						break;
					}

					$invoices->set_having('round(balance) > 0');

					$invoices->set_where('li.due_date >=', $date_range_beg);
					$invoices->set_where('li.due_date <=', $date_range_end);

					$this->template_data->set('invoices', $invoices->populate());

					$this->load->view('reports/lending/report_lending_open_invoices', $this->template_data->get_data());
				break;
				case 'outstanding_loans':
				default:

					$loans = new $this->Lending_loans_model('ll');
					$loans->set_limit(0);

					$loans->set_join('members m', 'm.id=ll.member_id');

					$loans->set_select('ll.*');

					$loans->set_select('(SELECT sum(lpa.amount) FROM lending_payment_applied lpa JOIN lending_invoices li ON lpa.invoice_id=li.id WHERE li.loan_id=ll.id) as payments');

					//$loans->set_select('(SELECT SUM(lpa.amount) FROM lending_payment_applied lpa JOIN lending_invoices li ON lpa.invoice_id=li.id JOIN lending_payments lp ON lp.id=lpa.payment_id JOIN accounting_entries ae ON ae.id=lp.entry_id WHERE (ae.item_type NOT LIKE \'LOANADJ\' OR ae.item_type IS NULL) AND li.loan_id=ll.id) as payments');
					
					$loans->set_select('m.lastname, m.firstname, m.middlename');

					// total_interest
					$loans->set_select('(SELECT IF( (ll.interest_type LIKE "fixed"), (ll.principal * (((ll.interest_rate / CEIL(30 / ll.skip_days)) * (CEIL((30 / ll.skip_days) * ll.months))) / 100)), ((((ll.principal * (ll.interest_rate / 100) ) / 2 ) * ((CEIL((30 / ll.skip_days) * ll.months)) + 1) * (1/CEIL(30 / ll.skip_days) ))) ) ) as total_interest');


					/*
					// fixed
					$loans->set_select('(SELECT (ll.principal * (((ll.interest_rate / CEIL(30 / ll.skip_days)) * (CEIL((30 / ll.skip_days) * ll.months))) / 100))) as fixed_interest');
		
					// diminishing
					$loans->set_select('(SELECT (((ll.principal * (ll.interest_rate / 100) ) / 2 ) * ((CEIL((30 / ll.skip_days) * ll.months)) + 1) * (1/CEIL(30 / ll.skip_days) ))) diminishing_interest');

					$loans->set_select('(SELECT IF( (ll.interest_type LIKE "fixed"), ((ll.principal + fixed_interest) - payments), ((ll.principal + diminishing_interest) - payments)))  as balance');
					*/

					$loans->set_select('(SELECT(ll.principal + total_interest)) as gross_amount');

					$loans->set_select('(SELECT ((ll.principal + (IF((total_interest IS NULL), 0, total_interest)) ) - (IF((payments IS NULL), 0, payments)) ) ) as balance');
					
					$loans->set_select('(SELECT(ll.principal - payments)) as principal_balance');

					$loans->set_select('(SELECT(payments - ll.principal)) as interest_collected');

					$loans->set_where('(SELECT COUNT(*) FROM lending_invoices li WHERE li.loan_id=ll.id) > 0');
					
					

					$loans->set_where('ll.loan_date >=',  $date_range_beg);
					$loans->set_where('ll.loan_date <=', $date_range_end);
					

					if(isset($filters['name_id']) && (!empty($filters['name_id']))) { 
						$loans->set_where('ll.member_id', $filters['name_id']);
					}

					$sort_by = ($filters['sort']) ? $filters['sort'] : 'DESC'; 
					switch($filters['order_by']) {
						case 'balance':
							$loans->set_order('balance', $sort_by);
						break;
						case 'total_interest':
							$loans->set_order('total_interest', $sort_by);
						break;
						case 'gross_amount':
							$loans->set_order('gross_amount', $sort_by);
						break;
						case 'payments':
							$loans->set_order('payments', $sort_by);
						break;
						case 'principal':
							$loans->set_order('ll.principal', $sort_by);
						break;
						case 'interest_type':
							$loans->set_order('ll.interest_type', $sort_by);
						break;
						case 'lastname':
							$loans->set_order('m.lastname', $sort_by);
						break;
						case 'firstname':
							$loans->set_order('m.firstname', $sort_by);
						break;
						case 'middlename':
							$loans->set_order('m.middlename', $sort_by);
						break;
						default:
							$loans->set_order('ll.loan_date', $sort_by);
						break;
					}
					
					$this->template_data->set('loans', $loans->populate());

					$this->load->view('reports/lending/report_lending_outstanding_loans', $this->template_data->get_data());
				break;
			}
		
	}

	public function ajax($action='') {
		$results = array();
		switch($action) {
			case 'name':
				$names = new $this->Coop_names_model;
				if( $this->input->get('term') ) {
					$names->set_where('full_name LIKE "%' . $this->input->get('term') . '%"');
				}
				$data = array();
				foreach($names->populate() as $name) {
					$data[] = array(
						'label' => $name->full_name,
						'id' => $name->id,
						'desc' => $name->address . " - " . $name->contact_number,
						);
				}
				$results = $data;
			break;
		}
		$this->output
        ->set_content_type('application/json')
        ->set_output(json_encode( $results ));
	}

}
