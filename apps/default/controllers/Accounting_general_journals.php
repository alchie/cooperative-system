<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Accounting_general_journals extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->template_data->set('page_title', 'COOP - General Journals');
		$this->template_data->set('current_page', 'General Journals');
		$this->template_data->set('current_uri', 'accounting_general_journals');
		
		$this->_isAuth('accounting', 'general_journals', 'view');
		
		$this->template_data->set('dept_sect', unserialize(COOP_DEPT_SECT));
		$this->template_data->set('check_item_types', unserialize(CHECK_ITEM_TYPES));
		$this->template_data->set('receipt_item_types', unserialize(RECEIPT_ITEM_TYPES));

		$this->load->model('Accounting_charts_model');
		$this->load->model('Accounting_entries_model');
		$this->load->model('Accounting_transactions_model');
		$this->load->model('Coop_lists_model');
		$this->load->model('Coop_names_model');
	}

	public function index($start=0) {

		if(( $this->input->post('output') == 'body_wrapper' )||( $this->input->post('output') == 'inner_page' )) {

		$main_charts = new $this->Accounting_charts_model;
		$main_charts->setParent(0,true);
		$main_charts->set_order('type', 'ASC');
		$main_charts->set_order('number', 'ASC');
		$charts_data = $main_charts->populate();
		foreach($charts_data as $main_id=>$main_chart) {
			$sub_charts = new $this->Accounting_charts_model;
			$sub_charts->setParent($main_chart->id,true);
			$sub_charts->set_order('type', 'ASC');
			$sub_charts->set_order('number', 'ASC');
			$charts_data[$main_id]->sub_accounts = $sub_charts->populate();
		}
		$this->template_data->set('account_titles', $charts_data);

		$journals = new $this->Accounting_transactions_model('trn');
		$journals->setType('GENERAL_JOURNAL',true);
		$journals->set_select('*');
		$journals->set_select('(SELECT SUM(debit) FROM accounting_entries WHERE trn_id=trn.id) as amount');
		$journals->set_start($start);
		$journals->set_order('date','DESC');
		$journals->set_order('number','DESC');

		$this->template_data->set('journals', $journals->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'base_url' => base_url( $this->config->item('index_page') . "/accounting_general_journals/index"),
			'total_rows' => $journals->count_all_results(),
			'per_page' => $journals->get_limit(),
			'ajax' => true,
		)));

		}

		$this->load->view('accounting/general_journals/general_journals', $this->template_data->get_data());
	}

	public function add($output="") {
		
		$trans = new $this->Accounting_transactions_model;
		$trans->setType('GENERAL_JOURNAL', true);
		$trans->set_order('number', 'DESC');
		$trans->set_select("*"); 
		$this->template_data->set('journal', $trans->get());

		if( $this->input->post() ) {
			$this->form_validation->set_rules('date', 'Date', 'trim|required');
			$this->form_validation->set_rules('memo', 'Memo', 'trim|required');
			if($this->form_validation->run()) {
				$dept_sect = explode("|", $this->input->post("section"));
				$trans = new $this->Accounting_transactions_model;
				$trans->setDate( date('Y-m-d', strtotime( $this->input->post('date') ) ) );
				$trans->setDept('ACCOUNTING');
				$trans->setSect('BOOKKEEPING');
				$trans->setType('GENERAL_JOURNAL');
				$trans->setMemo($this->input->post('memo'));
				$trans->setNumber($this->input->post('number'));
				if( $trans->insert() ) {
					redirect( "accounting_general_journals/entries/" . $trans->getId() );
				}
			}
		}

		$this->template_data->set('output', $output);

		$this->load->view('accounting/general_journals/general_journals_add', $this->template_data->get_data());
	}

	public function edit($trn_id, $output='') {
		
		$trans = new $this->Accounting_transactions_model;
		$trans->setType('GENERAL_JOURNAL', true);
		$trans->setId($trn_id,true);

		if( $this->input->post() ) {
			$this->form_validation->set_rules('date', 'Date', 'trim|required');
			$this->form_validation->set_rules('memo', 'Memo', 'trim|required');
			if($this->form_validation->run()) {
				$dept_sect = explode("|", $this->input->post("section"));
				$trans->setDate( date('Y-m-d', strtotime( $this->input->post('date') ) ) );
				$trans->setMemo($this->input->post('memo'));
				$trans->setNumber($this->input->post('number'));
				$trans->set_exclude(array('id', 'type', 'dept', 'sect'));
				if( $trans->update() ) {
					if( $this->input->get('next') ) {
						redirect( $this->input->get('next') );
					} else {
						redirect( "accounting_general_journals" );
					}
				}
			}
		}

		$this->template_data->set('journal', $trans->get());

		$this->template_data->set('output', $output);

		$this->load->view('accounting/general_journals/general_journals_edit', $this->template_data->get_data());
	}

	public function delete($trn_id) {
		$trans = new $this->Accounting_transactions_model;
		$trans->setType('GENERAL_JOURNAL');
		$trans->setId($trn_id,true);
		$trans->delete();

		$entry = new $this->Accounting_entries_model;
		$entry->setTrnId($trn_id,true);
		$entry->delete();

		redirect( "accounting_general_journals" );
	}

	public function save_entries($trn_id) {

		if( $this->input->post() ) {
			
			$account_titles = $this->input->post('account_title');
			$item_types = $this->input->post('item_type');
			$debits = $this->input->post('debit');
			$credits = $this->input->post('credit');
			$class_names = $this->input->post('class_name');
			$name_id = $this->input->post('name_id');
			$memos = $this->input->post('memo');

			$total_debits = 0;
			$total_credits = 0;

			foreach($account_titles as $key=>$at) {
				if( $at ) {
					$debit = str_replace(",", "", $debits[$key]);
					$credit = str_replace(",", "", $credits[$key]);
					$total_debits += $debit;
					$total_credits += $credit;
				}
			}

			$total_debits = number_format($total_debits,2,".","");
			$total_credits = number_format($total_credits,2,".","");

			if( (($total_debits > 0) && ($total_credits > 0)) && ($total_credits === $total_debits) ) {
					
					$item_max = 0;

					foreach($account_titles as $key=>$at) {
							$item_max = $key;
							$entry = new $this->Accounting_entries_model;
							$entry->setTrnId($trn_id,true);
							$entry->setItemNumber($key,true);
							$entry->setChartId($at);
							$entry->setItemType($item_types[$key]);
							$entry->setDebit(str_replace(",", "", $debits[$key]));
							$entry->setCredit(str_replace(",", "", $credits[$key]));
							$entry->setClass($class_names[$key]);
							$entry->setNameId($name_id[$key]);
							$entry->setMemo($memos[$key]);
							$entry->set_select("(SELECT COUNT(*) FROM lending_loans ll WHERE ll.entry_id=accounting_entries.id) as loan_id");
							$entry->set_select("(SELECT COUNT(*) FROM shares_withdrawal sw WHERE sw.entry_id=accounting_entries.id) as withdrawal_id");
							$entry->set_select("(SELECT COUNT(*) FROM lending_payments lp WHERE lp.entry_id=accounting_entries.id) as payments");
							$entry->set_select("(SELECT COUNT(*) FROM shares_capital sc WHERE sc.entry_id=accounting_entries.id) as capitals");

							if( $entry->nonEmpty() ) {

								$entry_data = $entry->getResults();

								$entry->set_exclude('id');
								if($at) {
									$entry->update();
								} else {
									$entry->delete();
								}

							} else {

								if($at) {
									$entry->insert();
								}

							}
					}
				if( $item_max ) {
						$entry_delete = new $this->Accounting_entries_model;
						$entry_delete->setTrnId($trn_id,true);
						$entry_delete->setItemNumber($item_max,true, false, '>');
						$entry_delete->delete();
				}
			}
		}

		redirect("accounting_general_journals/entries/{$trn_id}");

	}
	public function entries($trn_id) {

		$trans = new $this->Accounting_transactions_model;
		$trans->setType('GENERAL_JOURNAL');
		$trans->setId($trn_id,true);
		$trans->set_select("*");
		$trans->set_select("(SELECT MAX(item_number)  FROM `accounting_entries` WHERE `trn_id` = accounting_transactions.id) as max_items");
		$trans_data = $trans->get();
		$this->template_data->set('journal', $trans_data);
		
		$entries = new $this->Accounting_entries_model('ae');
		$entries->setTrnId($trn_id,true);
		$entries->set_select('ae.*');
		$entries->set_select('(SELECT full_name FROM coop_names WHERE coop_names.id=ae.name_id) as full_name');
		$entries->set_order('ae.item_number', 'ASC');
		$entries->set_limit(0);
		$entries->set_select("(SELECT COUNT(*) FROM lending_loans ll WHERE ll.entry_id=ae.id) as loan_id");
		$entries->set_select("(SELECT COUNT(*) FROM shares_withdrawal sw WHERE sw.entry_id=ae.id) as withdrawal_id");
		$entries->set_select("(SELECT COUNT(*) FROM lending_payments lp WHERE lp.entry_id=ae.id) as payments");
		$entries->set_select("(SELECT COUNT(*) FROM shares_capital sc WHERE sc.entry_id=ae.id) as capitals");
		$this->template_data->set('journal_entries', $entries->populate());

		$main_charts = new $this->Accounting_charts_model;
		$main_charts->set_order('type', 'ASC');
		$main_charts->set_order('number', 'ASC');
		$main_charts->set_select('id,title,type');
		$main_charts->set_limit(0);
		$charts_data = $main_charts->recursive('parent', 0);
		$this->template_data->set('account_titles', $charts_data);
		
		$list = new $this->Coop_lists_model;
		$list->setDept('ACCOUNTING',true);
		$list->setType('CLASS',true);
		$list->setActive(1,true);
		$list->set_select('id,value');
		$list->set_limit(0);
		$list_data = $list->recursive('parent', 0);
		$this->template_data->set('class_list', $list_data);

		$this->load->view('accounting/general_journals/general_journals_entries', $this->template_data->get_data());
	}

	public function ajax($action='') {
		$results = array();
		switch($action) {
			case 'name':
				$names = new $this->Coop_names_model;
				if( $this->input->get('term') ) {
					$names->set_where('full_name LIKE "%' . $this->input->get('term') . '%"');
				}
				$data = array();
				foreach($names->populate() as $name) {
					$data[] = array(
						'label' => $name->full_name,
						'id' => $name->id,
						'desc' => $name->address . " - " . $name->contact_number,
						);
				}
				$results = $data;
			break;
		}
		$this->output
        ->set_content_type('application/json')
        ->set_output(json_encode( $results ));
	}

}
