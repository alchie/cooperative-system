<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Marketing_mlm extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->template_data->set('page_title', 'COOP - Multi-level Marketing');
		$this->template_data->set('current_page', 'Multi-level Marketing');
		$this->template_data->set('current_uri', 'marketing_mlm');
	}

	public function index() {
		$this->load->view('marketing/marketing', $this->template_data->get_data());
	}

}
