<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report_income_statement extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->template_data->set('page_title', 'COOP - Income Statement');
		$this->template_data->set('current_page', 'Income Statement');
		$this->template_data->set('current_uri', 'report_income_statement');
		
		$this->_isAuth('report', 'income_statement', 'view');

		$this->template_data->set('coa_types', unserialize(CHART_ACCOUNT_TYPES));

		$this->load->model('Accounting_charts_model');
		$this->load->model('Coop_settings_model');
		$this->load->model('Accounting_entries_model');

	}

	private function _settings() {
		$coop_settings = new $this->Coop_settings_model;
		$coop_settings->setDepartment('ACCOUNTING',true);
		$coop_settings->setSection('CONFIG',true);

		$settings = array();
		
		foreach( $coop_settings->populate() as $cSet) {
			$settings[$cSet->key] = $cSet->value;
		}

		$important = array('year_start', 'year_end');
		foreach( $important as $imp) {
			if( (!isset($settings[$imp])) || ((isset($settings[$imp])) && ($settings[$imp]=='')) ) {
				redirect(site_url("accounting_settings/configuration") . "?error_code=102&next=" . uri_string() );
			}
		}
		return $settings;

	}

	private function _filters() {
		$filters = $this->input->get('filter');
		
		$filters_uri = '';
		$filters_arr = array();
		
		if( $filters ) {
			foreach($filters as $key=>$value) {
				$filters_arr[] = "filter[{$key}]=" . urlencode($value);
			}
			$filters_uri = implode("&", $filters_arr);
		}

		$this->template_data->set('filters', $filters);
		$this->template_data->set('filters_uri', $filters_uri);

		return $filters;
	}

	public function index() {

		$settings = $this->_settings();
		$this->template_data->set('settings', $settings);

		$filters = $this->_filters();

		// start date beg
		$date_range_beg = (isset($filters['date_range_beg'])) ? date('Y-m-d', strtotime($filters['date_range_beg'])) : date('Y-m-01'); 
		$this->template_data->set('date_from', $date_range_beg);

		// start date end
		$date_range_end = (isset($filters['date_range_end'])) ? date('Y-m-d', strtotime($filters['date_range_end'])) : date('Y-m-d');
		$this->template_data->set('date_to', $date_range_end);

		if(( $this->input->post('output') == 'body_wrapper' )||( $this->input->post('output') == 'inner_page' )) {

			$main_charts = new $this->Accounting_charts_model('ac');
			$main_charts->set_order('type', 'ASC');
			$main_charts->set_order('number', 'ASC');
			$main_charts->set_where('type LIKE "%_IS_%"');
			$main_charts->set_select('ac.*');
			$main_charts->set_select('(SELECT SUM(ae.debit) FROM accounting_entries ae JOIN accounting_transactions at ON ae.trn_id=at.id WHERE ae.chart_id=ac.id AND at.date >= "'.$date_range_beg.'" AND at.date <= "'.$date_range_end.'") as debit_balance');
			$main_charts->set_select('(SELECT SUM(ae.credit) FROM accounting_entries ae JOIN accounting_transactions at ON ae.trn_id=at.id WHERE ae.chart_id=ac.id AND at.date >= "'.$date_range_beg.'" AND at.date <= "'.$date_range_end.'") as credit_balance');
			$main_charts->set_limit(0);
			$charts_data = $main_charts->recursive('parent', 0);
			$this->template_data->set('accounts', $charts_data);

		}

		$this->load->view('reports/income_statement/report_income_statement', $this->template_data->get_data());
	}

	public function print_report() {

		$settings = $this->_settings();
		$this->template_data->set('settings', $settings);

		$filters = $this->_filters();

		// start date beg
		$date_range_beg = (isset($filters['date_range_beg'])) ? date('Y-m-d', strtotime($filters['date_range_beg'])) : date('Y-m-01'); 
		$this->template_data->set('date_from', $date_range_beg);

		// start date end
		$date_range_end = (isset($filters['date_range_end'])) ? date('Y-m-d', strtotime($filters['date_range_end'])) : date('Y-m-d');
		$this->template_data->set('date_to', $date_range_end);

		$main_charts = new $this->Accounting_charts_model('ac');
		$main_charts->set_order('type', 'ASC');
		$main_charts->set_order('number', 'ASC');
		$main_charts->set_where('type LIKE "%_IS_%"');
		$main_charts->set_select('ac.*');
		$main_charts->set_select('(SELECT SUM(ae.debit) FROM accounting_entries ae JOIN accounting_transactions at ON ae.trn_id=at.id WHERE ae.chart_id=ac.id AND at.date >= "'.$date_range_beg.'" AND at.date <= "'.$date_range_end.'") as debit_balance');
		$main_charts->set_select('(SELECT SUM(ae.credit) FROM accounting_entries ae JOIN accounting_transactions at ON ae.trn_id=at.id WHERE ae.chart_id=ac.id AND at.date >= "'.$date_range_beg.'" AND at.date <= "'.$date_range_end.'") as credit_balance');
		$main_charts->set_limit(0);
		$charts_data = $main_charts->recursive('parent', 0);
		$this->template_data->set('accounts', $charts_data);
		$this->load->view('reports/income_statement/report_income_statement_print', $this->template_data->get_data());
	}

	public function details($id, $start=0) {
		$charts = new $this->Accounting_charts_model;
		$charts->setId($id,true);
		$this->template_data->set('charts_data', $charts->get());

		$children = new $this->Accounting_charts_model('ac');
		$children->set_limit(0);
		$children_data = $children->recursive_one('parent', $id);

		$entries = new $this->Accounting_entries_model('ae');
		//$entries->setChartId($id,true);
		$entries->set_join('accounting_transactions at', 'at.id=ae.trn_id');
		$entries->set_join('accounting_charts ac', 'ac.id=ae.chart_id');
		$entries->set_join('coop_names cn', 'cn.id=ae.name_id');
		$entries->set_select('ae.*');
		$entries->set_select('at.dept, at.sect, at.type, at.date, , at.memo');
		$entries->set_select('cn.full_name');
		$entries->set_start($start);
		$entries->set_limit(10);
		$entries->set_order('at.date', 'DESC');
		$entries->set_order('at.id', 'DESC');

		$entries->set_group_by('ae.trn_id');
		$entries->set_select('SUM(ae.debit) as total_debit');
		$entries->set_select('SUM(ae.credit) as total_credit');

		$entries->set_where_in('ae.chart_id', array_merge( array($id), $children_data) );

		$entries_data = $entries->populate();
		$this->template_data->set('entries_data', $entries_data);

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'uri_segment' => 4,
			'base_url' => base_url( $this->config->item('index_page') . "/report_income_statement/details/{$id}"),
			'total_rows' => $entries->count_all_results(),
			'per_page' => $entries->get_limit(),
		)));

		$this->load->view('reports/income_statement/report_income_statement_details', $this->template_data->get_data());
	}

	public function modify($output='') {

		if( $this->input->post() ) {
			$url = site_url(($this->input->get('next')) ? $this->input->get('next') : "report_income_statement") . "?";
			$filters = array();
			foreach($this->input->post('filter') as $key=>$value) {
				$value = (is_array($value)) ? implode('|', $value) : $value;
				$filters[] = "filter[{$key}]=" . urlencode($value);
			}
			$url .= implode('&', $filters);
			redirect( $url );
		}

		$filters = $this->input->get('filter');
		if(isset($filters['name_id']) && (!empty($filters['name_id']))) { 
			$filter_name = new $this->Coop_names_model;
			$filter_name->setId($filters['name_id'],true);
			$this->template_data->set('filter_name', $filter_name->get());
		}
		$this->template_data->set('filters', $filters);

		$this->template_data->set('output', $output);
		$this->load->view('reports/income_statement/report_income_statement_modify', $this->template_data->get_data());
	}

}
