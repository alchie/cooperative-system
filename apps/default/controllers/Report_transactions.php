<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report_transactions extends MY_Controller {
	
	public function __construct() {

		parent::__construct();
		$this->template_data->set('page_title', 'COOP - Transactions');
		$this->template_data->set('current_page', 'Transactions');
		$this->template_data->set('current_uri', 'report_transactions');
		
		$this->_isAuth('report', 'transactions', 'view');

		$this->template_data->set('coa_types', unserialize(CHART_ACCOUNT_TYPES));

		$this->load->model('Accounting_receipts_model');
		$this->load->model('Accounting_checks_model');
		$this->load->model('Accounting_deposits_model');
		$this->load->model('Accounting_transactions_model');
		$this->load->model('Accounting_entries_model');
		$this->load->model('Lending_loans_model');
		$this->load->model('Lending_invoices_model');
		$this->load->model('Lending_payments_model');
		$this->load->model('Shares_withdrawal_model');
		$this->load->model('Shares_capital_model');
		$this->load->model('Shares_dividend_model');
		$this->load->model('Coop_names_model');
		$this->load->model('Reports_saved_model');
		
	}

	private function _report($start=0, $limit=10, $filters=array()) {

		$trns = new $this->Accounting_transactions_model;

		$trns->set_join('coop_names', 'coop_names.id=accounting_transactions.name_id');

		$trns->set_select('accounting_transactions.*');
		$trns->set_select('coop_names.full_name as full_name');
		
		$trns->set_select('(SELECT COUNT(*) FROM  accounting_entries ae WHERE ae.trn_id=accounting_transactions.id) as entries');

		$trns->set_select('(SELECT SUM(debit) FROM  accounting_entries ae WHERE ae.trn_id=accounting_transactions.id) as debit');
		$trns->set_select('(SELECT SUM(credit) FROM  accounting_entries ae WHERE ae.trn_id=accounting_transactions.id) as credit');

		// start pagination
		$trns->set_start($start);
		$trns->set_limit($limit);

		// start order
		$order_by = (isset($filters['order_by'])) ? $filters['order_by'] : 'date';
		$order_sort = (isset($filters['sort'])) ? $filters['sort'] : 'DESC';
		$trns->set_order($order_by, $order_sort);
		// end order

				// start date beg
		$date_range_beg = (isset($filters['date_range_beg'])) ? date('Y-m-d', strtotime($filters['date_range_beg'])) : date('Y-m-01');
		$trns->set_where('date >=', $date_range_beg); 
		// end date beg

		// start date end
		$date_range_end = (isset($filters['date_range_end'])) ? date('Y-m-d', strtotime($filters['date_range_end'])) : date('Y-m-d');
		$trns->set_where('date <=', $date_range_end); // end
		// start date end

		// start departments filter
		if(isset($filters['departments'])) { 
			$trns->set_where_in('dept', explode("|", $filters['departments']));
		}
		// start departments filter

		// start sections filter
		if(isset($filters['sections'])) { 
			$trns->set_where_in('sect', explode("|", $filters['sections']));
		}
		// start sections filter

		// start types filter
		if(isset($filters['types'])) { 
			$trns->set_where_in('type', explode("|", $filters['types']));
		}
		// start types filter

		// start memo filter
		if(isset($filters['memo']) && (!empty($filters['memo']))) { 
			$trns->set_where("memo LIKE '%{$filters['memo']}%'");
		}
		// start memo filter

		// start number filter
		if(isset($filters['number']) && (!empty($filters['number']))) { 
			$trns->set_where("number LIKE '%{$filters['number']}%'");
		}
		// start number filter

		// start name_id filter
		if(isset($filters['name_id']) && (!empty($filters['name_id']))) { 
			$trns->set_where("name_id", $filters['name_id']);
		}
		// start name_id filter
		return $trns;


		
	}

	public function index($start=0) {

		$filters = $this->input->get('filter');
		
		$filters_uri = '';
		$filters_arr = array();
		
		if( $filters ) {
			foreach($filters as $key=>$value) {
				$filters_arr[] = "filter[{$key}]=" . urlencode($value);
			}
			$filters_uri = implode("&", $filters_arr);
		}

		$this->template_data->set('filters', $filters);
		$this->template_data->set('filters_uri', $filters_uri);

		if(( $this->input->post('output') == 'body_wrapper' )||( $this->input->post('output') == 'inner_page' )) {

		$trns = $this->_report( $start, 10, $filters );

		$this->template_data->set('transactions', $trns->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
					'base_url' => base_url( $this->config->item('index_page') . "/report_transactions/index"),
					'total_rows' => $trns->count_all_results(),
					'per_page' => $trns->get_limit(),
					'ajax' => true,
				), "?" . $filters_uri ));

		$reports = new $this->Reports_saved_model;
		$reports->setType('transactions', true);
		$reports->set_order('name', 'ASC');
		$this->template_data->set('reports', $reports->populate());

		}
		
		$this->load->view('reports/transactions/report_transactions', $this->template_data->get_data());
	}

	public function view_report($report_id, $start=0) {

		$report = new $this->Reports_saved_model;
		$report->setId($report_id, true);
		$report_filters = $report->get();
		$this->template_data->set('current_report', $report_filters);

		$filters = ($this->input->get('filter')) ? $this->input->get('filter') : (array) json_decode( $report_filters->filters );
		
		$filters_uri = '';
		$filters_arr = array();
		
		if( $filters ) {
			foreach($filters as $key=>$value) {
				$filters_arr[] = "filter[{$key}]=" . urlencode($value);
			}
			$filters_uri = implode("&", $filters_arr);
		}

		$this->template_data->set('filters', $filters);
		$this->template_data->set('filters_uri', $filters_uri);

		$trns = $this->_report( $start, 10, $filters );

		$this->template_data->set('transactions', $trns->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
					'base_url' => base_url( $this->config->item('index_page') . "/report_transactions/view_report/{$report_id}"),
					'total_rows' => $trns->count_all_results(),
					'per_page' => $trns->get_limit(),
					'ajax' => true,
				), "?" . $filters_uri ));

		$reports = new $this->Reports_saved_model;
		$reports->setType('transactions', true);
		$reports->set_order('name', 'ASC');
		$this->template_data->set('reports', $reports->populate());

		$this->load->view('reports/transactions/report_transactions', $this->template_data->get_data());

	}

	public function delete_report($id) {

		$this->_isAuth('report', 'transactions', 'delete');

		$reports = new $this->Reports_saved_model;
		$reports->setId($id,true);
		$reports->delete();
		redirect( site_url("report_transactions")  );
	}
	
	public function modify($output='') {

		if( $this->input->post() ) {
			$url = site_url(($this->input->get('next')) ? $this->input->get('next') : "report_transactions") . "?";
			$filters = array();
			foreach($this->input->post('filter') as $key=>$value) {
				$value = (is_array($value)) ? implode('|', $value) : $value;
				$filters[] = "filter[{$key}]=" . urlencode($value);
			}
			$url .= implode('&', $filters);
			redirect( $url );
		}

		$departments = new $this->Accounting_transactions_model;
		$departments->set_select('dept');
		$departments->is_distinct();
		$this->template_data->set('departments', $departments->populate());

		$sections = new $this->Accounting_transactions_model;
		$sections->set_select('sect');
		$sections->is_distinct();
		$this->template_data->set('sections', $sections->populate());

		$types = new $this->Accounting_transactions_model;
		$types->set_select('type');
		$types->is_distinct();
		$this->template_data->set('types', $types->populate());

		$filters = $this->input->get('filter');
		if(isset($filters['name_id']) && (!empty($filters['name_id']))) { 
			$filter_name = new $this->Coop_names_model;
			$filter_name->setId($filters['name_id'],true);
			$this->template_data->set('filter_name', $filter_name->get());
		}
		$this->template_data->set('filters', $filters);
		
		$this->template_data->set('output', $output);
		$this->load->view('reports/transactions/report_transactions_modify', $this->template_data->get_data());

	}

	public function generate_report($report_id=NULL, $output='') {

		$report = new $this->Reports_saved_model;
		$report->setId($report_id, true);
		$report_filters = $report->get();
		$this->template_data->set('current_report', $report_filters);

		$filters = ($this->input->get('filter')) ? $this->input->get('filter') : (array) json_decode( $report_filters->filters );

		$filters_uri = '';
		$filters_arr = array();
		if( $this->input->get('filter') ) {
			foreach($filters as $key=>$value) {
				$filters_arr[] = "filter[{$key}]=" . urlencode($value);
			}
			$filters_uri = implode("&", $filters_arr);
		}

		$this->template_data->set('filters', $filters);
		$this->template_data->set('filters_uri', $filters_uri);

		$trns = $this->_report( 0, 0, $filters );

		$this->template_data->set('transactions', $trns->populate());

		$this->template_data->set('output', $output);
		$this->load->view('reports/transactions/report_transactions_print', $this->template_data->get_data());
	}

	public function save_report($report_id=NULL) {

		$this->_isAuth('report', 'transactions', 'add');
		$this->_isAuth('report', 'transactions', 'edit');

		if( $this->input->get('filter') ) {
			$filters = $this->input->get('filter');
			$saved = new $this->Reports_saved_model;
			if( $report_id ) {
				$saved->setId($report_id, true);
			}
			$saved->setName( $filters['report_title'] );
			$saved->setType('transactions');
			$saved->setFilters( json_encode($filters) );

			if( $saved->nonEmpty() ) {
				$saved->update();
				if( $report_id ) {
					redirect( site_url("report_transactions/view_report/{$report_id}")  );
				}
			} else {
				if( $saved->insert() ) {
					redirect( site_url("report_transactions/view_report/{$saved->getId()}")  );
				}
			}

			$filters_uri = '';
			$filters_arr = array();
			if( $this->input->get('filter') ) {
				foreach($filters as $key=>$value) {
					$filters_arr[] = "filter[{$key}]=" . urlencode($value);
				}
				$filters_uri = implode("&", $filters_arr);
			}
			redirect( site_url("report_transactions") . "?" . $filters_uri );
		}
		redirect( "report_transactions" );
	}

	public function delete($id) {

		$this->_isAuth('report', 'transactions', 'delete');

		$trns = new $this->Accounting_transactions_model;
		$trns->setId($id,true);
		$trns->set_select('*');
		$trns->set_select('(SELECT COUNT(*) FROM  accounting_entries ae WHERE ae.trn_id=accounting_transactions.id) as entries');
		$trn_data = $trns->get();

		if( $trn_data->entries == 0) {
			$trns->delete();
			redirect( site_url($this->input->get('next')) . '?error_code=701' );
		}

		redirect( site_url($this->input->get('next')) );
	}

	public function redirect($id, $output='', $no_delete=0) {

			$trns = new $this->Accounting_transactions_model;
			$trns->setId($id,true);
			$trn = $trns->get();

			if( $trn ) {

			$delete_trn = false;
			$url = 'welcome';

			switch($trn->dept) {
				case 'ACCOUNTING':
					switch ($trn->sect) {
						case 'RECEIVING':
							switch ($trn->type) {
								case 'RECEIPT':
									$receipt = new $this->Accounting_receipts_model;
									$receipt->setTrnId($id,true);
									$receipt_data = $receipt->get();
									if( $receipt_data ) {
										$url = site_url('accounting_sales_receipts/receipt_items/' . $receipt_data->id . '/' . $output . '/' . $no_delete);
									} else {
										$delete_trn = true;
									}
								break;
								case 'DEPOSIT':
									$deposit = new $this->Accounting_deposits_model;
									$deposit->setTrnId($id,true);
									$deposit_data = $deposit->get();
									if( $deposit_data ) {
										$url = site_url('accounting_deposits/edit_deposit/' . $deposit_data->id. '/' . $output . '/' . $no_delete);
									} else {
										$delete_trn = true;
									}
								break;
							}	
						break;
						case 'DISBURSING':
							switch ($trn->type) {
								case 'CHECK':
									$check = new $this->Accounting_checks_model;
									$check->setTrnId($id,true);
									$check_data = $check->get();
									if( $check_data ) {
										$url = site_url('accounting_write_checks/check_items/' . $check_data->id. '/' . $output . '/' . $no_delete);
									} else {
										$delete_trn = true;
									}
								break;
							}	
						break;
						case 'BOOKKEEPING':
							switch ($trn->type) {
								case 'GENERAL_JOURNAL':
									$trn = new $this->Accounting_transactions_model;
									$trn->setId($id,true);
									$trn_data = $trn->get();

									if( $trn_data ) {
										$url = site_url('accounting_general_journals/entries/' . $trn_data->id. '/' . $output . '/' . $no_delete);
									} else {
										$delete_trn = true;
									}

								break;
							}	
						break;
					}
				break;
				case 'SERVICES':
					switch ($trn->sect) {
						case 'LENDING':
							switch ($trn->type) {
								case 'LOAN':
									$loan = new $this->Lending_loans_model;
									$loan->setTrnId($id,true);
									$loan_data = $loan->get();
									
									if( $loan_data ) {
										$url = site_url('services_lending/schedule/' . $loan_data->id. '/' . $output . '/' . $no_delete);
									} else {
										$delete_trn = true;
									}

								break;
								case 'INTEREST':
									$invoice = new $this->Lending_invoices_model;
									$invoice->setTrnId($id,true);
									$invoice_data = $invoice->get();
									
									if( $invoice_data ) {
										$url = site_url('services_lending/schedule/' . $invoice_data->loan_id. '/' . $output . '/' . $no_delete) . "?highlight=" . $id;
									} else {
										$delete_trn = true;
									}
								break;
								case 'PAYMENT':
									$payment = new $this->Lending_payments_model;
									$payment->setTrnId($id,true);
									$payment_data = $payment->get(); 
									
									if( $payment_data ) {
										$url = site_url('services_lending/payment_apply/' . $payment_data->id. '/' . $output . '/' . $no_delete);
									} else {
										$delete_trn = true;
									}
								break;
							}	
						break;
						case 'SHARES':
							switch ($trn->type) {
								case 'CAPITAL':
									$capital = new $this->Shares_capital_model;
									$capital->setTrnId($id,true);
									$capital_data = $capital->get();
									
									if( $capital_data ) {
										$url = site_url('services_shares/edit_capital/' . $capital_data->id . '/' . $output . '/' . $no_delete);
									} else {
										$delete_trn = true;
									}
								break;
								case 'WITHDRAWAL':
									$withdrawal = new $this->Shares_withdrawal_model;
									$withdrawal->setTrnId($id,true);
									$withdrawal_data = $withdrawal->get();
									
									if( $withdrawal_data ) {
										$url = site_url('services_shares/edit_withdrawal/' . $withdrawal_data->id . '/' . $output . '/' . $no_delete);
									} else {
										$delete_trn = true;
									}
								break;
								case 'DIVIDEND':
									$dividend = new $this->Shares_dividend_model;
									$dividend->setTrnId($id,true);
									$dividend_data = $dividend->get();
									
									if( $dividend_data ) {
										$url = site_url('services_shares/edit_dividend/' . $dividend_data->id . '/' . $output . '/' . $no_delete);
									} else {
										$delete_trn = true;
									}
								break;
							}	
						break;
					}
				break;
				case 'MEMBERSHIP':
					switch ($trn->sect) {
						case 'COMPANY':
							switch ($trn->type) {
								case 'PAYMENT':
									$payment = new $this->Lending_payments_model;
									$payment->setTrnId($id,true);
									$payment_data = $payment->get();
									
									if( $payment_data ) {
										$url = site_url('membership_companies/payment/' . $payment_data->id . '/' . $output . '/' . $no_delete);
									} else {
										$delete_trn = true;
									}
								break;
							}
						break;
					}
				break;
			}


				if( $delete_trn ) {

					$trns->delete();

					$entries = new $this->Accounting_entries_model;
					$entries->setTrnId($id, true);
					$entries->delete();

					redirect( site_url( $this->input->get('next') ) . "?error_code=330" );

				}

				if( $this->input->get('next') ) {
					if( strpos($url, '?') ) {
						$url .= '&';
					} else {
						$url .= '?';
					}
					$url .= 'next=' . $this->input->get('next');
				}

				redirect($url);

			} else {

				$entries = new $this->Accounting_entries_model;
				$entries->setTrnId($id, true);
				$entries->delete();

				if( $this->input->get('next') ) {
					$url = $this->input->get('next');
				} 

				redirect( site_url( $url ) . "?error_code=331" );
			}

			
	}

	public function ajax($action='') {
		$results = array();
		switch($action) {
			case 'name':
				$names = new $this->Coop_names_model;
				if( $this->input->get('term') ) {
					$names->set_where('full_name LIKE "%' . $this->input->get('term') . '%"');
				}
				$data = array();
				foreach($names->populate() as $name) {
					$data[] = array(
						'label' => $name->full_name,
						'id' => $name->id,
						'desc' => $name->address . " - " . $name->contact_number,
						);
				}
				$results = $data;
			break;
		}
		$this->output
        ->set_content_type('application/json')
        ->set_output(json_encode( $results ));
	}

}
