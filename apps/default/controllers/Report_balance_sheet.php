<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report_balance_sheet extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->template_data->set('page_title', 'COOP - Balance Sheet');
		$this->template_data->set('current_page', 'Balance Sheet');
		$this->template_data->set('current_uri', 'report_balance_sheet');

		$this->_isAuth('report', 'balance_sheet', 'view');
		
		$this->template_data->set('coa_types', unserialize(CHART_ACCOUNT_TYPES));

		$this->load->model('Coop_settings_model');
		$this->load->model('Accounting_charts_model');
		$this->load->model('Accounting_entries_model');
	}

	private function _settings() {
		
		$coop_settings = new $this->Coop_settings_model;
		$coop_settings->setDepartment('ACCOUNTING',true);
		$coop_settings->setSection('CONFIG',true);

		$settings = array();
		
		foreach( $coop_settings->populate() as $cSet) {

			$settings[$cSet->key] = $cSet->value;

		}

		$important = array('fund_surplus');

		foreach( $important as $imp) {

			if( (!isset($settings[$imp])) || ((isset($settings[$imp])) && ($settings[$imp]=='')) ) {

				redirect(site_url("accounting_settings/configuration") . "?error_code=102&next=" . uri_string() );

			}

		}

		return (object) $settings;

	}

	private function _filters() {
		$filters = $this->input->get('filter');
		
		$filters_uri = '';
		$filters_arr = array();
		
		if( $filters ) {
			foreach($filters as $key=>$value) {
				$filters_arr[] = "filter[{$key}]=" . urlencode($value);
			}
			$filters_uri = implode("&", $filters_arr);
		}

		$this->template_data->set('filters', $filters);
		$this->template_data->set('filters_uri', $filters_uri);

		return $filters;
	}

	public function index() { 

		$settings = $this->_settings();
		$this->template_data->set('settings', $settings);

		$filters = $this->_filters();

		$as_of = (isset($filters['date_range_end'])) ? date('Y-m-d', strtotime($filters['date_range_end'])) : date('Y-m-d');

		if(( $this->input->post('output') == 'body_wrapper' )||( $this->input->post('output') == 'inner_page' )) {

			$main_charts = new $this->Accounting_charts_model('ac');
			$main_charts->set_order('type', 'ASC');
			$main_charts->set_order('number', 'ASC');
			$main_charts->set_where('type LIKE "%_BS_%"');
			$main_charts->set_select('ac.*');
			$main_charts->set_select('(SELECT SUM(ae.debit) FROM accounting_entries ae JOIN accounting_transactions at ON ae.trn_id=at.id WHERE ae.chart_id=ac.id AND at.date <= "'.$as_of.'") as debit_balance');
			$main_charts->set_select('(SELECT SUM(ae.credit) FROM accounting_entries ae JOIN accounting_transactions at ON ae.trn_id=at.id WHERE ae.chart_id=ac.id AND at.date <= "'.$as_of.'") as credit_balance');
			$main_charts->set_limit(0);
			$charts_data = $main_charts->recursive('parent', 0);
			$this->template_data->set('accounts', $charts_data);

			$surplus = new $this->Accounting_entries_model('ae');
			$surplus->set_select("SUM(ae.debit) as debit_balance");
			$surplus->set_select("SUM(ae.credit) as credit_balance");
			$surplus->set_join("accounting_charts ac", "ac.id=ae.chart_id");
			$surplus->set_join("accounting_transactions at", "at.id=ae.trn_id");
			$surplus->set_where('ac.type LIKE "%_IS_%"');
			$surplus->set_where('at.date <= "'.$as_of.'"');
			$this->template_data->set('surplus', $surplus->get());

		}

		$this->load->view('reports/balance_sheet/report_balance_sheet', $this->template_data->get_data());

	}

	public function print_report() {

		$settings = $this->_settings();
		$this->template_data->set('settings', $settings);

		$filters = $this->_filters();
		
		$as_of = (isset($filters['date_range_end'])) ? date('Y-m-d', strtotime($filters['date_range_end'])) : date('Y-m-d');

		$main_charts = new $this->Accounting_charts_model('ac');
		$main_charts->set_order('type', 'ASC');
		$main_charts->set_order('number', 'ASC');
		$main_charts->set_where('type LIKE "%_BS_%"');
		$main_charts->set_select('ac.*');
		$main_charts->set_select('(SELECT SUM(ae.debit) FROM accounting_entries ae JOIN accounting_transactions at ON ae.trn_id=at.id WHERE ae.chart_id=ac.id AND at.date <= "'.$as_of.'") as debit_balance');
		$main_charts->set_select('(SELECT SUM(ae.credit) FROM accounting_entries ae JOIN accounting_transactions at ON ae.trn_id=at.id WHERE ae.chart_id=ac.id AND at.date <= "'.$as_of.'") as credit_balance');
		$main_charts->set_limit(0);
		$charts_data = $main_charts->recursive('parent', 0);
		$this->template_data->set('accounts', $charts_data);

		$surplus = new $this->Accounting_entries_model('ae');
		$surplus->set_select("SUM(ae.debit) as debit_balance");
		$surplus->set_select("SUM(ae.credit) as credit_balance");
		$surplus->set_join("accounting_charts ac", "ac.id=ae.chart_id");
		$surplus->set_join("accounting_transactions at", "at.id=ae.trn_id");
		$surplus->set_where('ac.type LIKE "%_IS_%"');
		$surplus->set_where('at.date <= "'.$as_of.'"');
		$this->template_data->set('surplus', $surplus->get());

		$this->load->view('reports/balance_sheet/report_balance_sheet_print', $this->template_data->get_data());

	}

	public function details($id, $start=0) {
		$charts = new $this->Accounting_charts_model;
		$charts->setId($id,true);
		$this->template_data->set('charts_data', $charts->get());

		$children = new $this->Accounting_charts_model('ac');
		$children->set_limit(0);
		$children_data = $children->recursive_one('parent', $id);

		$entries = new $this->Accounting_entries_model('ae');
		//$entries->setChartId($id,true);
		$entries->set_join('accounting_transactions at', 'at.id=ae.trn_id');
		$entries->set_join('accounting_charts ac', 'ac.id=ae.chart_id');
		$entries->set_join('coop_names cn', 'cn.id=ae.name_id');
		$entries->set_select('ae.*');
		$entries->set_select('at.dept, at.sect, at.type, at.date, , at.memo');
		$entries->set_select('cn.full_name');
		$entries->set_start($start);
		$entries->set_limit(10);
		$entries->set_order('at.date', 'DESC');
		$entries->set_order('at.id', 'DESC');

		$entries->set_group_by('ae.trn_id');
		$entries->set_select('SUM(ae.debit) as total_debit');
		$entries->set_select('SUM(ae.credit) as total_credit');

		$entries->set_where_in('ae.chart_id', array_merge( array($id), $children_data) );

		$entries_data = $entries->populate();
		$this->template_data->set('entries_data', $entries_data);

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'uri_segment' => 4,
			'base_url' => base_url( $this->config->item('index_page') . "/report_balance_sheet/details/{$id}"),
			'total_rows' => $entries->count_all_results(),
			'per_page' => $entries->get_limit(),
		)));

		$this->load->view('reports/balance_sheet/report_balance_sheet_details', $this->template_data->get_data());
	}

	public function modify($output='') {

		if( $this->input->post() ) {
			$url = site_url(($this->input->get('next')) ? $this->input->get('next') : "report_balance_sheet") . "?";
			$filters = array();
			foreach($this->input->post('filter') as $key=>$value) {
				$value = (is_array($value)) ? implode('|', $value) : $value;
				$filters[] = "filter[{$key}]=" . urlencode($value);
			}
			$url .= implode('&', $filters);
			redirect( $url );
		}

		$filters = $this->input->get('filter');
		if(isset($filters['name_id']) && (!empty($filters['name_id']))) { 
			$filter_name = new $this->Coop_names_model;
			$filter_name->setId($filters['name_id'],true);
			$this->template_data->set('filter_name', $filter_name->get());
		}
		$this->template_data->set('filters', $filters);

		$this->template_data->set('output', $output);
		$this->load->view('reports/balance_sheet/report_balance_sheet_modify', $this->template_data->get_data());
	}

}
