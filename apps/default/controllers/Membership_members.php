<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Membership_members extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->template_data->set('page_title', 'COOP - Members');
		$this->template_data->set('current_page', 'Members');
		$this->template_data->set('current_uri', 'membership_members');
		$this->template_data->set('current_sub_uri', 'membership_members_personal_info');
		$this->template_data->set('enable_search', false);

		$this->_isAuth('membership', 'members', 'view');
		
		$this->load->model('Members_model');
		$this->load->model('Members_address_model');
		$this->load->model('Coop_names_model');

	}

	private function _searchRedirect() {
		if( $this->input->get('q') ) {
			redirect( site_url("membership_members?q=" . $this->input->get('q') ) );
		}
	}


	public function index($start=0) {

		if( $start > 0) {
			$this->_searchRedirect();
		}
		
		if(( $this->input->post('output') == 'body_wrapper' )||( $this->input->post('output') == 'inner_page' )) {
			$members = new $this->Members_model;

			if( $this->input->get('q') ) {
				$members->set_where('lastname LIKE "%' . $this->input->get('q') . '%"');
				$members->set_where_or('firstname LIKE "%' . $this->input->get('q') . '%"');
				$members->set_where_or('middlename LIKE "%' . $this->input->get('q') . '%"');
			}

			$members->set_select("members.*");
			$members->set_select("(SELECT c.name FROM companies_members m JOIN companies c ON m.company_id=c.id WHERE m.member_id=members.id) as company_name");
			$members->set_select("(SELECT c.id FROM companies_members cm JOIN companies c ON cm.company_id=c.id WHERE cm.member_id=members.id) as company_id");
			$members->set_start($start);
			$members->set_order('lastname', 'ASC');
			$members->set_order('firstname', 'ASC');
			$members->set_order('middlename', 'ASC');
			$this->template_data->set('members', $members->populate());
			$this->template_data->set('members_count', $members->count_all_results());

			$this->template_data->set('pagination', bootstrap_pagination(array(
				'base_url' => base_url( $this->config->item('index_page') . '/membership_members/index/'),
				'total_rows' => $members->count_all_results(),
				'per_page' => $members->get_limit(),
				'ajax' => true,
			)));
		}

		$this->template_data->set('enable_search', true);

		$this->load->view('membership/members/members', $this->template_data->get_data());
	}

	public function add($name_id) {

		$this->_isAuth('membership', 'members', 'add'); 

		$name = new $this->Coop_names_model;
		$name->setId($name_id,true);
		$name_data = $name->get();

		if(!$name_data) {
			redirect("membership_members/search_name");
		}

		$this->template_data->set( 'name', $name_data );

		if( $this->input->post('firstname') && $this->input->post('lastname') ) {
			$this->form_validation->set_rules('firstname', 'Firstname', 'trim|required');
			$this->form_validation->set_rules('lastname', 'Lastname', 'trim|required');
			if($this->form_validation->run()) {
				$members = new $this->Members_model;
				$members->setId($name_id,true);
				$members->setFirstname($this->input->post('firstname'));
				$members->setLastname($this->input->post('lastname'));
				$members->setMiddlename($this->input->post('middlename'));
				$members->setActive(1);
				$members->setBirthdate(date('Y-m-d', strtotime($this->input->post('birthdate'))));
				$members->setBirthplace($this->input->post('birthplace'));
				$members->setGender($this->input->post('gender'));
				$members->setMaritalStatus($this->input->post('marital_status'));
				$members->setLastmodBy($this->session->user_id);
				$members->setAddedBy($this->session->user_id);
				if( $members->insert() ) {
					redirect(site_url("membership_members/member_data/{$name_id}"));
				}
			}
		}
		$this->load->view('membership/members/members_add', $this->template_data->get_data());
	}

	public function edit($id=false, $output=false) {

		$this->_searchRedirect();
		
		$this->template_data->set('output', $output);

		$members = new $this->Members_model;
		$members->setId($id,true);

		if( !$members->nonEmpty() ) {
			redirect("membership_members");
		}
		if( $this->input->post() ) {
			$this->form_validation->set_rules('firstname', 'Firstname', 'trim|required');
			$this->form_validation->set_rules('lastname', 'Lastname', 'trim|required');
			if($this->form_validation->run()) {
				$members->setFirstname($this->input->post('firstname'));
				$members->setLastname($this->input->post('lastname'));
				$members->setMiddlename($this->input->post('middlename'));
				$members->setActive(1);
				$members->setBirthdate(date('Y-m-d', strtotime($this->input->post('birthdate'))));
				$members->setBirthplace($this->input->post('birthplace'));
				$members->setGender($this->input->post('gender'));
				$members->setMaritalStatus($this->input->post('marital_status'));
				$members->setLastmodBy($this->session->user_id);
				$members->set_exclude(array('id', 'added_by'));
				$members->update();
			}
			if( $this->input->get('next') ) {
				redirect($this->input->get('next'));
			}
		}

		$members->set_select("members.*");
		$members->set_select("(SELECT c.name FROM companies_members cm JOIN companies c ON cm.company_id=c.id WHERE cm.member_id=members.id) as company_name");
		$members->set_select("(SELECT c.id FROM companies_members cm JOIN companies c ON cm.company_id=c.id WHERE cm.member_id=members.id) as company_id");
		$this->template_data->set('member', $members->get());

		$this->load->view('membership/members/members_edit', $this->template_data->get_data());
	}

	public function member_data($id) {

		$this->_searchRedirect();
		
		$members = new $this->Members_model;
		$members->setId($id,true);
		if( $this->input->post() ) {
			switch($this->input->post('action')) {
				case 'personal_info':
					$this->_updateProfileInfo($members);
				break;
			}
		}

		$members->set_select("members.*");
		$members->set_select("(SELECT c.name FROM companies_members cm JOIN companies c ON cm.company_id=c.id WHERE cm.member_id=members.id) as company_name");
		$members->set_select("(SELECT c.id FROM companies_members cm JOIN companies c ON cm.company_id=c.id WHERE cm.member_id=members.id) as company_id");
		$members->set_join('members_address', 'members_address.mid=members.id');
		$members->set_select("members_address.*");
		$this->template_data->set('member', $members->get());

		$this->template_data->set('current_page', 'Personal Information');
		$this->template_data->set('current_sub_uri', 'membership_members_personal_info');

		$this->load->view('membership/members/members_member_data', $this->template_data->get_data());
	}

	public function delete($id, $output='') {

		$this->_searchRedirect();
		
		$this->template_data->set('output', $output);

		$members = new $this->Members_model;
		$members->setId($id,true);
		
		$members->set_select("members.*");
		$members->set_select("(SELECT c.name FROM companies_members cm JOIN companies c ON cm.company_id=c.id WHERE cm.member_id=members.id) as company_name");
		
		$members->set_select("(SELECT c.id FROM companies_members cm JOIN companies c ON cm.company_id=c.id WHERE cm.member_id=members.id) as company_id");

		$members->set_select("(SELECT COUNT(*) FROM lending_loans ll WHERE ll.member_id=members.id) as loans");

		$members->set_select("(SELECT COUNT(*) FROM lending_payments lp WHERE lp.member_id=members.id) as payments");

		$members->set_select("(SELECT COUNT(*) FROM shares_capital sc WHERE sc.member_id=members.id) as capital");

		$members->set_select("(SELECT COUNT(*) FROM shares_dividend_item sdi WHERE sdi.member_id=members.id) as dividends");

		$members->set_select("(SELECT COUNT(*) FROM shares_withdrawal sw WHERE sw.member_id=members.id) as withdrawal");
		
		$members_data = $members->get();
		$this->template_data->set('member', $members_data);

		if( $this->input->post() ) {

			if( $members_data->loans != 0) {
				$this->postNext("error_code=501");
			}

			$this->form_validation->set_rules('time', 'Time', 'trim|required');
			$this->form_validation->set_rules('confirm_code', 'Confirm Code', 'trim|required');
			if($this->form_validation->run()) {
				$code = strtoupper( substr( sha1( $this->input->post('time') ),0,4 ) );
				if( $code == $this->input->post('confirm_code') ) {
					$members->delete();
					redirect('membership_members');
				}
			}

			$this->postNext();
		}

		$this->load->view('membership/members/members_delete', $this->template_data->get_data());

	}

	private function _updateProfileInfo($member) {
		$this->form_validation->set_rules('lastname', 'Last Name', 'trim|required');
		$this->form_validation->set_rules('firstname', 'First Name', 'trim|required');
		if( $this->form_validation->run() ) {
			$member->setLastname($this->input->post('lastname'), false, true);
			$member->setFirstname($this->input->post('firstname'), false, true);
			$member->setMiddlename($this->input->post('middlename'), false, true);
			$member->setMiddlename($this->input->post('middlename'), false, true);
			$member->setBirthdate(date('Y-m-d', strtotime($this->input->post('birthdate'))), false, true);
			$member->setBirthplace($this->input->post('birthplace'), false, true);
			$member->setGender($this->input->post('gender'), false, true);
			$member->setMaritalStatus($this->input->post('marital_status'), false, true);
			$member->setLastmodBy($this->session->user_id, false, true);
			$member->update();
		}
	}

	public function search_name() {

		$this->_isAuth('membership', 'members', 'add');

		$names = new $this->Coop_names_model('c');
		$names->set_select('c.*');
		$names->set_join('members m', 'c.id=m.id');
		$names->set_where('c.full_name LIKE "%'.$this->input->get('q').'%"');
		$names->set_where('m.id IS NULL');
		$this->template_data->set('names', $names->populate());

		$this->load->view('membership/members/members_search_name', $this->template_data->get_data());
	}

	public function address($id, $output=false) {

		$this->_searchRedirect();
		
		$this->template_data->set('output', $output);

		if( $this->input->post() ) {
			
				$address = new $this->Members_address_model;
				$address->setMid($id,true);
				$address->setUnit($this->input->post('unit'));
				$address->setBuilding($this->input->post('building'));
				$address->setLotBlock($this->input->post('lot_block'));
				$address->setStreet($this->input->post('street'));
				$address->setSubdivision($this->input->post('subdivision'));
				$address->setBarangay($this->input->post('barangay'));
				$address->setCity($this->input->post('city'));
				$address->setProvince($this->input->post('province'));
				$address->setZip($this->input->post('zip'));
				if($address->nonEmpty()) {
					$address->update();
				} else {
					$address->insert();
				}
			
			if( $this->input->get('next') ) {
				redirect($this->input->get('next'));
			}
		}

		$members = new $this->Members_model;
		$members->setId($id,true);
		$members->set_select("members.*");
		$members->set_select("(SELECT c.name FROM companies_members cm JOIN companies c ON cm.company_id=c.id WHERE cm.member_id=members.id) as company_name");
		$members->set_select("(SELECT c.id FROM companies_members cm JOIN companies c ON cm.company_id=c.id WHERE cm.member_id=members.id) as company_id");
		$members->set_join('members_address', 'members_address.mid=members.id');
		$members->set_select("members_address.*");
		$this->template_data->set('member', $members->get());

		$this->load->view('membership/members/members_address', $this->template_data->get_data());
	}

	public function contacts($id, $output=false) {

		$this->_searchRedirect();
		
		$this->template_data->set('output', $output);

		$members = new $this->Members_model;
		$members->setId($id,true);
		if( $this->input->post() ) {
			$members->setPhoneMobile($this->input->post('phone_mobile'), false, true);
			$members->setPhoneHome($this->input->post('phone_home'), false, true);
			$members->setEmail($this->input->post('email'), false, true);
			$members->update();
			$this->postNext();
		}
		
		$members = new $this->Members_model;
		$members->setId($id,true);
		$members->set_select("members.*");
		$members->set_select("(SELECT c.name FROM companies_members cm JOIN companies c ON cm.company_id=c.id WHERE cm.member_id=members.id) as company_name");
		$members->set_select("(SELECT c.id FROM companies_members cm JOIN companies c ON cm.company_id=c.id WHERE cm.member_id=members.id) as company_id");
		$members->set_join('members_address', 'members_address.mid=members.id');
		$members->set_select("members_address.*");
		$this->template_data->set('member', $members->get());

		$this->load->view('membership/members/members_contacts', $this->template_data->get_data());
	}

	public function employment($id) {

		$this->_searchRedirect();
		
		$members = new $this->Members_model;
		$members->setId($id,true);
		if( $this->input->post() ) {
			switch($this->input->post('action')) {
				case 'personal_info':
					$this->_updateProfileInfo($members);
				break;
			}
		}
		$members->set_select("members.*");
		$members->set_select("(SELECT c.name FROM companies_members cm JOIN companies c ON cm.company_id=c.id WHERE cm.member_id=members.id) as company_name");
		$members->set_select("(SELECT c.id FROM companies_members cm JOIN companies c ON cm.company_id=c.id WHERE cm.member_id=members.id) as company_id");
		$members->set_join('members_address', 'members_address.mid=members.id');
		$members->set_select("members_address.*");
		$this->template_data->set('member', $members->get());

		$this->template_data->set('current_page', 'Employment Records');
		$this->template_data->set('current_sub_uri', 'membership_members_employment');

		$this->load->view('membership/members/members_employment_records', $this->template_data->get_data());
	}

	public function add_employment($id, $output=false) {

		$this->template_data->set('output', $output);
		
		$members = new $this->Members_model;
		$members->setId($id,true);
		$this->template_data->set('member', $members->get());

		$this->template_data->set('current_page', 'Employment Records');
		$this->template_data->set('current_sub_uri', 'membership_members_employment');

		$this->load->view('membership/members/members_employment_records_add', $this->template_data->get_data());
	}

	public function ajax($action='') {
		$results = array();
		switch($action) {
			case 'add_member':
				$names = new $this->Coop_names_model;
				if( $this->input->get('term') ) {
					$names->set_where('full_name LIKE "%' . $this->input->get('term') . '%"');
				}
				$names->set_where("(SELECT COUNT(*) FROM members WHERE members.id=coop_names.id) = 0");
				$names->set_where("(SELECT COUNT(*) FROM companies WHERE companies.id=coop_names.id) = 0");
				$data = array();
				foreach($names->populate() as $name) {
					$data[] = array(
						'label' => $name->full_name,
						'id' => $name->id,
						'redirect'=> site_url("membership_members/add/" . $name->id),
						);
				}
				$results = $data;
			break;
			case 'member_data':
				$members = new $this->Members_model;
				if( $this->input->get('term') ) {
					$members->set_where('lastname LIKE "%' . $this->input->get('term') . '%"');
					$members->set_where_or('firstname LIKE "%' . $this->input->get('term') . '%"');
					$members->set_where_or('middlename LIKE "%' . $this->input->get('term') . '%"');
				}
				$members->set_order('lastname', 'ASC');
				$members->set_order('firstname', 'ASC');
				$members->set_order('middlename', 'ASC');
				$data = array();
				foreach($members->populate() as $member) {
					$data[] = array(
						'label' => $member->lastname . ", " . $member->firstname . " " .$member->middlename,
						'id' => $member->id,
						'redirect'=> site_url("membership_members/member_data/{$member->id}"),
						);
				}
				$results = $data;
			break;

			case 'change_member':
				$members = new $this->Members_model;
				if( $this->input->get('term') ) {
					$members->set_where('lastname LIKE "%' . $this->input->get('term') . '%"');
					$members->set_where_or('firstname LIKE "%' . $this->input->get('term') . '%"');
					$members->set_where_or('middlename LIKE "%' . $this->input->get('term') . '%"');
				}
				$members->set_order('lastname', 'ASC');
				$members->set_order('firstname', 'ASC');
				$members->set_order('middlename', 'ASC');
				$data = array();
				switch( $this->input->get('sub_uri') ) {
					case 'membership_members_employment':
						$redirect_uri = "membership_members/employment/";
					break;
					case 'membership_members_personal_info':
					default:
						$redirect_uri = "membership_members/member_data/";
					break;

				}
				foreach($members->populate() as $member) {
					$data[] = array(
						'label' => $member->lastname . ", " . $member->firstname . " " .$member->middlename,
						'id' => $member->id,
						'redirect'=> site_url($redirect_uri . $member->id),
						);
				}
				$results = $data;
			break;
		}
		$this->output
        ->set_content_type('application/json')
        ->set_output(json_encode( $results ));
	}

}
