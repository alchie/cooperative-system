<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Accounting_deposits extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->template_data->set('page_title', 'COOP - Deposits');
		$this->template_data->set('current_page', 'Deposits');
		$this->template_data->set('current_uri', 'accounting_deposits');
		
		$this->_isAuth('accounting', 'deposits', 'view');
		
		$this->template_data->set('coa_types', unserialize(CHART_ACCOUNT_TYPES));

		$this->load->model('Accounting_charts_model');
		$this->load->model('Accounting_entries_model');
		$this->load->model('Accounting_deposits_model');
		$this->load->model('Accounting_deposits_items_model');
		$this->load->model('Accounting_transactions_model');
		$this->load->model('Reports_saved_model');
		$this->load->model('Coop_settings_model');

	}

	private function _filters() {
		$filters = $this->input->get('filter');
		
		$filters_uri = '';
		$filters_arr = array();
		
		if( $filters ) {
			foreach($filters as $key=>$value) {
				$filters_arr[] = "filter[{$key}]=" . urlencode($value);
			}
			$filters_uri = implode("&", $filters_arr);
		}

		$this->template_data->set('filters', $filters);
		$this->template_data->set('filters_uri', $filters_uri);

		return $filters;
	}

	private function _index($start=0, $filters=NULL) {

		$filters_uri = '';
		$filters_arr = array();
		if( $filters ) {
			foreach($filters as $key=>$value) {
				$filters_arr[] = "filter[{$key}]=" . urlencode($value);
			}
		}
		$filters_uri = implode("&", $filters_arr);
		$this->template_data->set('filters_uri', $filters_uri);

		if(( $this->input->post('output') == 'body_wrapper' )||( $this->input->post('output') == 'inner_page' )) {

			$deposits = new $this->Accounting_deposits_model;
			$deposits->set_select('*');
			$deposits->set_select('(SELECT ac.title FROM accounting_charts ac WHERE ac.id=accounting_deposits.bank_id) as bank_name');
			$deposits->set_select('(SELECT COUNT(*) FROM accounting_bankrecon_items abi JOIN accounting_entries ae ON ae.id = abi.entry_id WHERE ae.chart_id=accounting_deposits.bank_id AND ae.trn_id=accounting_deposits.trn_id) as recon');
			$deposits->set_select('(SELECT abi.recon_id FROM accounting_bankrecon_items abi JOIN accounting_entries ae ON ae.id = abi.entry_id WHERE ae.chart_id=accounting_deposits.bank_id AND ae.trn_id=accounting_deposits.trn_id LIMIT 1) as recon_id');
			$deposits->set_select('(SELECT SUM(ae.debit) FROM `accounting_deposits_items` adi JOIN accounting_entries ae ON adi.entry_id=ae.id WHERE adi.deposit_id=accounting_deposits.id) as total_entries');
			$deposits->set_order('date_deposited', 'DESC');
			$deposits->set_start($start);
			
			if( isset($filters['bank_account']) && ($filters['bank_account'] != '') ) {
				$deposits->setBankId($filters['bank_account'], true);
			}
			if( isset($filters['date_range_beg']) && ($filters['date_range_beg'] != '') ) {
				$deposits->set_where("date_deposited >=", date("Y-m-d", strtotime($filters['date_range_beg'])));
			}
			if( isset($filters['date_range_end']) && ($filters['date_range_end'] != '') ) {
				$deposits->set_where("date_deposited <=", date("Y-m-d", strtotime($filters['date_range_end'])));
			}

			$this->template_data->set('deposits', $deposits->populate());

			$this->template_data->set('pagination', bootstrap_pagination(array(
				'uri_segment' => 3,
				'base_url' => base_url( $this->config->item('index_page') . "/accounting_deposits/index"),
				'total_rows' => $deposits->count_all_results(),
				'per_page' => $deposits->get_limit(),
				"ajax" => true,
			), "?" . $filters_uri));

		}

		$bank_accounts = new $this->Accounting_charts_model;
		$bank_accounts->setReconcile(1,true);
		$bank_accounts->set_order('type', 'ASC');
		$bank_accounts->set_order('number', 'ASC');
		$bank_accounts->set_where('type LIKE "001_BS_ASS_CSB"');
		$bank_accounts_data = $bank_accounts->populate();
		$this->template_data->set('bank_accounts', $bank_accounts_data);

		$reports = new $this->Reports_saved_model;
		$reports->setType('accounting_deposits', true);
		$reports->set_order('name', 'ASC');
		$this->template_data->set('reports', $reports->populate());

		$this->load->view('accounting/deposits/deposits', $this->template_data->get_data());
	}

	public function index($start=0) {

		$filters = $this->_filters(); //$this->input->get('filter');
		$this->template_data->set('filters', $filters);

		$this->_index($start, $filters);
	}

	public function select_receipts($bank_id) {

		$this->_isAuth('accounting', 'deposits', 'add');

		$bank = new $this->Accounting_charts_model;
		$bank->setId($bank_id,true);
		$this->template_data->set('bank', $bank->get());

		$undeposited = new $this->Coop_settings_model;
		$undeposited->setDepartment('ACCOUNTING',true);
		$undeposited->setSection('CONFIG',true);
		$undeposited->setKey('undeposited',true);
		$undeposited_data = $undeposited->get();
		$this->template_data->set('undeposited', $undeposited_data);
		
		if(( $this->input->post('output') == 'body_wrapper' )||( $this->input->post('output') == 'inner_page' )) {

		$entries = new $this->Accounting_entries_model('ae');
		$entries->setChartId($undeposited_data->value,true);
		$entries->set_join('accounting_transactions at', 'at.id=ae.trn_id');
		$entries->set_join('accounting_charts ac', 'ac.id=ae.chart_id');
		$entries->set_join('coop_names cn', 'cn.id=ae.name_id');

		$entries->set_select('ae.*');
		$entries->set_select('SUM(ae.debit) as total_amount');
		$entries->set_select('at.dept, at.sect, at.type, at.date, , at.memo, at.number');
		$entries->set_select('cn.full_name');
		$entries->set_select('ac.type as chart_type');
		$entries->set_select('(SELECT cl.value FROM coop_lists cl WHERE cl.dept=\'ACCOUNTING\' AND cl.type=\'CLASS\' AND cl.id=ae.class) as class_name');
		
		$entries->set_where('(SELECT COUNT(*) FROM accounting_deposits_items adi WHERE adi.entry_id=ae.id) = 0');
		$entries->set_where('at.type != "DEPOSIT"');

		$entries->set_start(0);
		$entries->set_limit(0);
		$entries->set_order('at.number', 'ASC');
		$entries->set_order('at.date', 'ASC');
		$entries->set_group_by('ae.trn_id');

		$entries_data = $entries->populate();
		$this->template_data->set('entries_data', $entries_data); 

		}
		
		$this->load->view('accounting/deposits/select_receipts', $this->template_data->get_data());
	}

	public function add_deposit($bank_id) {

		$this->_isAuth('accounting', 'deposits', 'add');

		if( count($this->input->get('receipts')) == 0) {
			redirect("accounting_deposits/select_receipts/{$bank_id}");
		}

		$bank = new $this->Accounting_charts_model;
		$bank->setId($bank_id,true);
		$this->template_data->set('bank', $bank->get());

		$undeposited = new $this->Coop_settings_model;
		$undeposited->setDepartment('ACCOUNTING',true);
		$undeposited->setSection('CONFIG',true);
		$undeposited->setKey('undeposited',true);
		$undeposited_data = $undeposited->get();
		$this->template_data->set('undeposited', $undeposited_data);

		$entries = new $this->Accounting_entries_model('ae');
		$entries->setChartId($undeposited_data->value,true);
		$entries->set_join('accounting_transactions at', 'at.id=ae.trn_id');
		$entries->set_join('accounting_charts ac', 'ac.id=ae.chart_id');
		$entries->set_join('coop_names cn', 'cn.id=ae.name_id');

		$entries->set_select('ae.*');
		$entries->set_select('at.dept, at.sect, at.type, at.date, , at.memo, at.number');
		$entries->set_select('cn.full_name');
		$entries->set_select('ac.type as chart_type');
		$entries->set_select('(SELECT cl.value FROM coop_lists cl WHERE cl.dept=\'ACCOUNTING\' AND cl.type=\'CLASS\' AND cl.id=ae.class) as class_name');
		
		$entries->set_start(0);
		$entries->set_limit(0);
		$entries->set_order('at.number', 'ASC');
		$entries->set_order('at.date', 'ASC');
		$entries->set_where('ae.trn_id IN (' . implode(",", $this->input->get('receipts')) . ')');
		$entries_data = $entries->populate(); 
		
		if( $this->input->post() ) {

			$this->form_validation->set_rules('deposit_date', 'Date Deposited', 'trim|required');
			$this->form_validation->set_rules('deposit_amount', 'Amount Deposited', 'trim|required');
			if($this->form_validation->run()) {

				$total_amount = 0;
				foreach($entries_data as $ent_data) {
					$total_amount += $ent_data->debit;
				}

				if( round( $this->input->post('deposit_amount') ) != round( $total_amount )  ) {
					redirect( site_url("accounting_deposits/select_receipts/{$bank_id}") . "?error_code=401" );
				}

				$trans = new $this->Accounting_transactions_model;
				$trans->setDate( date('Y-m-d', strtotime( $this->input->post('deposit_date') ) ) );
				$trans->setDept('ACCOUNTING');
				$trans->setSect('RECEIVING');
				$trans->setType('DEPOSIT');
				$trans->setMemo($this->input->post('deposit_memo'));
				$trans->setAmount( $this->input->post('deposit_amount') );
				$trans->setLines(0);

				if( $trans->insert() ) {

					foreach( $entries_data as $entry ) {
						$entryU = new $this->Accounting_entries_model;
						$entryU->setTrnId( $trans->getId());
						$entryU->setItemNumber(0);
						$entryU->setChartId($entry->chart_id);
						$entryU->setDebit(0);
						$entryU->setCredit($entry->debit);
						$entryU->setClass($entry->class);
						$entryU->setNameId($entry->name_id);
						$entryU->setMemo($entry->memo);
						$entryU->insert();

						$entryB = new $this->Accounting_entries_model;
						$entryB->setTrnId( $trans->getId() );
						$entryB->setItemNumber(0);
						$entryB->setChartId($bank_id);
						$entryB->setDebit($entry->debit);
						$entryB->setCredit(0);
						$entryB->setClass($entry->class);
						$entryB->setNameId($entry->name_id);
						$entryB->setMemo($entry->memo);
						$entryB->insert();
					}

					$deposit = new $this->Accounting_deposits_model;
					$deposit->setBankId($bank_id);
					$deposit->setDateDeposited(date('Y-m-d', strtotime($this->input->post('deposit_date'))));
					$deposit->setAmount($this->input->post('deposit_amount'));
					$deposit->setMemo($this->input->post('deposit_memo'));
					$deposit->setTrnId( $trans->getId() );

					if( $deposit->insert() ) {

						foreach( $entries_data as $entry ) {
							$deposit_item = new $this->Accounting_deposits_items_model;
							$deposit_item->setEntryId( $entry->id, true);
							$deposit_item->setDepositId( $deposit->getId(), true);
							if( $deposit_item->nonEmpty() == false ) {
								$deposit_item->insert();
							}
						}
						//redirect("accounting_deposits/edit_deposit/" . $deposit->getId());
						redirect("accounting_deposits/select_receipts/{$bank_id}");
					}

				}				
			}
		}

		$entries->set_select('SUM(ae.debit) as total_amount');
		$entries->set_group_by('ae.trn_id');
		$entries_data = $entries->populate(); 
		$this->template_data->set('entries_data', $entries_data); 

		$this->load->view('accounting/deposits/add_deposit', $this->template_data->get_data());
	}

	public function edit_deposit($deposit_id, $output='') {

		$this->_isAuth('accounting', 'deposits', 'edit');

		$deposit = new $this->Accounting_deposits_model;
		$deposit->setId( $deposit_id, true );
		$deposit->set_select('*');
		$deposit->set_select('(SELECT ac.title FROM accounting_charts ac WHERE ac.id=accounting_deposits.bank_id) as bank_name');
		$deposit->set_select('(SELECT COUNT(*) FROM accounting_bankrecon_items abi JOIN accounting_entries ae ON ae.id = abi.entry_id WHERE ae.chart_id=accounting_deposits.bank_id AND ae.trn_id=accounting_deposits.trn_id) as recon');
		$deposit->set_select('(SELECT abi.recon_id FROM accounting_bankrecon_items abi JOIN accounting_entries ae ON ae.id = abi.entry_id WHERE ae.chart_id=accounting_deposits.bank_id AND ae.trn_id=accounting_deposits.trn_id LIMIT 1) as recon_id');
		$deposit_data = $deposit->get();

		if($this->input->post()) {
			$this->form_validation->set_rules('deposit_date', 'Date Deposited', 'trim');
			$this->form_validation->set_rules('memo', 'Memo', 'trim');
			if($this->form_validation->run()) { 
				if( $deposit_data->recon == 0 ) {
					$deposit->setDateDeposited(date('Y-m-d', strtotime($this->input->post('deposit_date'))), false, true);
				}
				$deposit->setMemo($this->input->post('memo'), false, true);
				$deposit->update();

				$trans = new $this->Accounting_transactions_model;
				$trans->setId($deposit_data->trn_id,true);
				if( $deposit_data->recon == 0 ) {
					$trans->setDate( date('Y-m-d', strtotime($this->input->post('deposit_date'))), false, true );
				}
				$trans->setMemo($this->input->post('memo'), false, true);
				$trans->update();
			}
			$this->postNext();
		}

		$this->template_data->set( 'deposit', $deposit_data );

		$undeposited = new $this->Coop_settings_model;
		$undeposited->setDepartment('ACCOUNTING',true);
		$undeposited->setSection('CONFIG',true);
		$undeposited->setKey('undeposited',true);
		$undeposited_data = $undeposited->get();
		$this->template_data->set('undeposited', $undeposited_data);


		$deposit_item = new $this->Accounting_deposits_items_model('adi');
		$deposit_item->setDepositId( $deposit_id, true);

		$deposit_item->set_join('accounting_entries ae', 'ae.id=adi.entry_id');
		$deposit_item->set_join('accounting_transactions at', 'at.id=ae.trn_id');
		$deposit_item->set_join('accounting_charts ac', 'ac.id=ae.chart_id');
		$deposit_item->set_join('coop_names cn', 'cn.id=ae.name_id');

		$deposit_item->set_select('ae.*');
		$deposit_item->set_select('at.dept, at.sect, at.type, at.date, , at.memo, at.number');
		$deposit_item->set_select('cn.full_name');
		$deposit_item->set_select('ac.type as chart_type');
		$deposit_item->set_select('(SELECT cl.value FROM coop_lists cl WHERE cl.dept=\'ACCOUNTING\' AND cl.type=\'CLASS\' AND cl.id=ae.class) as class_name');
		$deposit_item->set_limit(0);
		$deposit_item->set_order('at.number');
		$deposit_item->set_order('at.date');
		$deposit_item->set_select('SUM(ae.debit) as total_amount');
		$deposit_item->set_group_by('ae.trn_id');
		$deposit_items = $deposit_item->populate();
		$this->template_data->set('entries_data', $deposit_items);

		$this->template_data->set('output', $output);
		$this->load->view('accounting/deposits/edit_deposit', $this->template_data->get_data());
	}

	public function deposit_items($deposit_id, $output='') {

		$this->_isAuth('accounting', 'deposits', 'edit');

		$deposit = new $this->Accounting_deposits_model;
		$deposit->setId( $deposit_id, true );
		$deposit->set_select('*');
		$deposit->set_select('(SELECT ac.title FROM accounting_charts ac WHERE ac.id=accounting_deposits.bank_id) as bank_name');
		$deposit_data = $deposit->get();
		$this->template_data->set( 'deposit', $deposit_data );

		$undeposited = new $this->Coop_settings_model;
		$undeposited->setDepartment('ACCOUNTING',true);
		$undeposited->setSection('CONFIG',true);
		$undeposited->setKey('undeposited',true);
		$undeposited_data = $undeposited->get();
		$this->template_data->set('undeposited', $undeposited_data);


		$deposit_item = new $this->Accounting_deposits_items_model('adi');
		$deposit_item->setDepositId( $deposit_id, true);

		$deposit_item->set_join('accounting_entries ae', 'ae.id=adi.entry_id');
		$deposit_item->set_join('accounting_transactions at', 'at.id=ae.trn_id');
		$deposit_item->set_join('accounting_charts ac', 'ac.id=ae.chart_id');
		$deposit_item->set_join('coop_names cn', 'cn.id=ae.name_id');

		$deposit_item->set_select('ae.*');
		$deposit_item->set_select('SUM(ae.debit) as total_amount');
		$deposit_item->set_select('at.dept, at.sect, at.type, at.date, , at.memo, at.number');
		$deposit_item->set_select('cn.full_name');
		$deposit_item->set_select('ac.type as chart_type');
		$deposit_item->set_select('(SELECT cl.value FROM coop_lists cl WHERE cl.dept=\'ACCOUNTING\' AND cl.type=\'CLASS\' AND cl.id=ae.class) as class_name');
		$deposit_item->set_limit(0);
		$deposit_item->set_order('at.number');
		$deposit_item->set_order('at.date');
		$deposit_item->set_group_by('ae.trn_id');
		$deposit_items = $deposit_item->populate();
		$this->template_data->set('entries_data', $deposit_items);

		$this->template_data->set('output', $output);
		$this->load->view('accounting/deposits/deposit_items', $this->template_data->get_data());
	}

	public function delete_deposit($deposit_id) {

		$this->_isAuth('accounting', 'deposits', 'delete');

		$deposit = new $this->Accounting_deposits_model;
		$deposit->setId( $deposit_id, true );
		$deposit->set_select('*');
		$deposit->set_select('(SELECT COUNT(*) FROM accounting_bankrecon_items abi JOIN accounting_entries ae ON ae.id = abi.entry_id WHERE ae.chart_id=accounting_deposits.bank_id AND ae.trn_id=accounting_deposits.trn_id) as recon');
		$deposit_data = $deposit->get();

		if( $deposit_data->recon > 0 ) {
			redirect( site_url("accounting_deposits") . "?error_code=402" );
		}

		$deposit_item = new $this->Accounting_deposits_items_model;
		$deposit_item->setDepositId( $deposit_id, true);
		$deposit_item->delete();
		//$deposit_items = $deposit_item->populate();
		
		$entries = new $this->Accounting_entries_model;
		$entries->setTrnId( $deposit_data->trn_id,true );
		$entries->delete();
		//$entries_data = $entries->populate();

		$trans = new $this->Accounting_transactions_model;
		$trans->setId( $deposit_data->trn_id,true );
		$trans->delete();

		$deposit->delete();
		//$transaction = $trans->get();
		redirect("accounting_deposits");
	}

	public function filters($output='') {

		if( $this->input->post() ) {
			$url = site_url(($this->input->get('next')) ? $this->input->get('next') : "accounting_deposits/index") . "?";
			$filters_arr = array();
			foreach($this->input->post('filter') as $key=>$value) {
				$value = (is_array($value)) ? implode('|', $value) : $value;
				$filters_arr[] = "filter[{$key}]=" . urlencode($value);
			}
			$url .= implode('&', $filters_arr);
			redirect( $url );
		}

		$this->template_data->set('filters', $this->input->get('filter'));

		$bank_accounts = new $this->Accounting_charts_model;
		$bank_accounts->setReconcile(1,true);
		$bank_accounts->set_order('type', 'ASC');
		$bank_accounts->set_order('number', 'ASC');
		$bank_accounts->set_where('type LIKE "001_BS_ASS_CSB"');
		$bank_accounts_data = $bank_accounts->populate();
		$this->template_data->set('bank_accounts', $bank_accounts_data);

		$this->template_data->set('output', $output);
		$this->load->view('accounting/deposits/deposits_filters', $this->template_data->get_data());
	}


	public function pr1nt() {

		$filters = $this->input->get('filter');
		$this->template_data->set('filters', $filters);

		$filters_uri = '';
		$filters_arr = array();
		if( $filters ) {
			foreach($filters as $key=>$value) {
				$filters_arr[] = "filter[{$key}]=" . urlencode($value);
			}
		}
		$filters_uri = implode("&", $filters_arr);
		$this->template_data->set('filters_uri', $filters_uri);

		$deposits = new $this->Accounting_deposits_model;
		$deposits->set_select('*');
		$deposits->set_select('(SELECT ac.title FROM accounting_charts ac WHERE ac.id=accounting_deposits.bank_id) as bank_name');
		$deposits->set_select('(SELECT COUNT(*) FROM accounting_bankrecon_items abi JOIN accounting_entries ae ON ae.id = abi.entry_id WHERE ae.chart_id=accounting_deposits.bank_id AND ae.trn_id=accounting_deposits.trn_id) as recon');
		$deposits->set_select('(SELECT abi.recon_id FROM accounting_bankrecon_items abi JOIN accounting_entries ae ON ae.id = abi.entry_id WHERE ae.chart_id=accounting_deposits.bank_id AND ae.trn_id=accounting_deposits.trn_id LIMIT 1) as recon_id');
		$deposits->set_select('(SELECT SUM(ae.debit) FROM `accounting_deposits_items` adi JOIN accounting_entries ae ON adi.entry_id=ae.id WHERE adi.deposit_id=accounting_deposits.id) as total_entries');
		$deposits->set_order('date_deposited', 'DESC');
		$deposits->set_limit(0);
		
		if( isset($filters['bank_account']) && ($filters['bank_account'] != '') ) {
			$deposits->setBankId($filters['bank_account'], true);
		}
		if( isset($filters['date_range_beg']) && ($filters['date_range_beg'] != '') ) {
			$deposits->set_where("date_deposited >=", date("Y-m-d", strtotime($filters['date_range_beg'])));
		}
		if( isset($filters['date_range_end']) && ($filters['date_range_end'] != '') ) {
			$deposits->set_where("date_deposited <=", date("Y-m-d", strtotime($filters['date_range_end'])));
		}

		$deposits_data = $deposits->populate();

		if( isset($filters['show_items']) && ($filters['show_items'] == 1) ) {
			foreach($deposits_data as $key=>$deposit_data) {
				$deposit_items = new $this->Accounting_deposits_items_model('adi');
				$deposit_items->setDepositId($deposit_data->id, true);
				$deposit_items->set_join('accounting_entries ae', 'ae.id=adi.entry_id');
				$deposit_items->set_join('accounting_transactions at', 'ae.trn_id=at.id');
				$deposit_items->set_select("at.number");
				$deposit_items->set_select("at.date");
				$deposit_items->set_select("ae.item_type");
				$deposit_items->set_select("ae.memo");
				$deposit_items->set_select("ae.debit");
				$deposit_items->set_group_by('at.id');
				$deposit_items->set_order('at.number', 'DESC');
				$deposits_data[$key]->items = $deposit_items->populate();
			}
		}

		$this->template_data->set('deposits', $deposits_data);

		$this->load->view('accounting/deposits/deposits_print', $this->template_data->get_data());
	}

	public function save($report_id=NULL) {
		if( $this->input->get('filter') ) {
			$filters = $this->input->get('filter');
			$saved = new $this->Reports_saved_model;
			if( $report_id ) {
				$saved->setId($report_id, true);
			}
			$saved->setName( $filters['title'] );
			$saved->setType('accounting_deposits');
			$saved->setFilters( json_encode($filters) );

			if( $report_id && $saved->nonEmpty() ) {
				$saved->update();
				redirect( site_url("accounting_deposits/view/{$report_id}")  );
			} else {
				if( $saved->insert() ) {
					redirect( site_url("accounting_deposits/view/{$saved->getId()}")  );
				}
			}

			$filters_uri = '';
			$filters_arr = array();
			if( $this->input->get('filter') ) {
				foreach($filters as $key=>$value) {
					$filters_arr[] = "filter[{$key}]=" . urlencode($value);
				}
				$filters_uri = implode("&", $filters_arr);
			}
			redirect( site_url("accounting_deposits") . "?" . $filters_uri );
		}
	}

	public function view($report_id, $start=0) {

		$report = new $this->Reports_saved_model;
		$report->setId($report_id, true);
		$report_filters = $report->get();
		$this->template_data->set('saved_filters', $report_filters);

		$filters = ($this->input->get('filter')) ? $this->input->get('filter') : (array) json_decode( $report_filters->filters );

		$this->_index($start, $filters);
	}

	public function delete($report_id) {
			$saved = new $this->Reports_saved_model;
			$saved->setId($report_id, true);
			$saved->delete();
			redirect("accounting_deposits");
	}

}
