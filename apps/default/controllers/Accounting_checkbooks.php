<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Accounting_checkbooks extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->template_data->set('page_title', 'COOP - Checkbooks');
		$this->template_data->set('current_page', 'Checkbooks');
		$this->template_data->set('current_uri', 'accounting_checkbooks');
		
		$this->_isAuth('system', 'settings', 'view');
		$this->_isAuth('accounting', 'checkbooks', 'view');
		
		$this->template_data->set('dept_sect', unserialize(COOP_DEPT_SECT));
		$this->template_data->set('check_item_types', unserialize(CHECK_ITEM_TYPES));
		$this->template_data->set('receipt_item_types', unserialize(RECEIPT_ITEM_TYPES));

		$this->load->model('Accounting_charts_model');
		$this->load->model('Accounting_checkbooks_model');
		$this->load->model('Accounting_checkbooks_checks_model');
	}

	public function index($start=0) {

		$bank_accounts = new $this->Accounting_charts_model;
		$bank_accounts->setReconcile(1,true);
		$bank_accounts->set_order('type', 'ASC');
		$bank_accounts->set_order('number', 'ASC');
		$bank_accounts->set_where('type LIKE "001_BS_ASS_CSB"');
		$bank_accounts_data = $bank_accounts->populate();
		$this->template_data->set('bank_accounts', $bank_accounts_data);

		if(( $this->input->post('output') == 'body_wrapper' )||( $this->input->post('output') == 'inner_page' )) {

		$books = new $this->Accounting_checkbooks_model('c');
		$books->set_join('accounting_charts ac', 'ac.id=c.chart_id');
		$books->set_select('c.*');
		$books->set_select('ac.title as account_title');
		$books->set_start($start);
		$this->template_data->set('books', $books->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'base_url' => base_url($this->config->item('index_page') . "/accounting_checkbooks/index"),
			'total_rows' => $books->count_all_results(),
			'per_page' => $books->get_limit(),
			'ajax' => true,
		)));

		}

		$this->load->view('accounting/checkbooks/checkbooks', $this->template_data->get_data());
	}

	public function checks($book_id, $start=0) {

		$bank_accounts = new $this->Accounting_charts_model;
		$bank_accounts->setReconcile(1,true);
		$bank_accounts->set_order('type', 'ASC');
		$bank_accounts->set_order('number', 'ASC');
		$bank_accounts->set_where('type LIKE "001_BS_ASS_CSB"');
		$bank_accounts_data = $bank_accounts->populate();
		$this->template_data->set('bank_accounts', $bank_accounts_data);

		if(( $this->input->post('output') == 'body_wrapper' )||( $this->input->post('output') == 'inner_page' )) {

			$checks = new $this->Accounting_checkbooks_checks_model('c');
			$checks->set_select('c.*');
			$checks->set_join('accounting_checkbooks ac', 'ac.id=c.checkbook_id');
			$checks->set_join('accounting_charts ac2', 'ac2.id=ac.chart_id');
			$checks->set_select('ac2.title as account_title');
			$checks->set_select('(SELECT ac.id FROM accounting_checks ac WHERE ac.check_number=c.check_number) as used');
			$checks->set_start($start);
			$checks->setCheckbookId($book_id,true);
			$this->template_data->set('checks', $checks->populate());

			$this->template_data->set('pagination', bootstrap_pagination(array(
				'base_url' => base_url( $this->config->item('index_page') . "/accounting_checkbooks/checks/{$book_id}"),
				'total_rows' => $checks->count_all_results(),
				'per_page' => $checks->get_limit(),
				'ajax' => true,
			)));

		}

		$this->load->view('accounting/checkbooks/checkbooks_checks', $this->template_data->get_data());
	}

	public function add($chart_id, $output="") {
		
		if( $this->input->post() ) {
			$this->form_validation->set_rules('num_start', 'Starting Number', 'numeric|trim|required');
			$this->form_validation->set_rules('num_end', 'Ending Number', 'numeric|trim|required');
			if($this->form_validation->run()) {
				if( $this->input->post('num_start') < $this->input->post('num_end') ) {
					$book = new $this->Accounting_checkbooks_model;
					$book->setChartId( $chart_id );
					$book->setNumStart( $this->input->post('num_start') );
					$book->setNumEnd( $this->input->post('num_end') );
					if( $book->insert() ) {
						for($i=$this->input->post('num_start');$i<=$this->input->post('num_end');$i++) {
							$checks = new $this->Accounting_checkbooks_checks_model;
							$checks->setCheckbookId($book->get_inserted_id(),true,true);
							$checks->setCheckNumber($i,true,true);
							if( !$checks->nonEmpty() ) {
								$checks->insert();
							}
						}
					}
					$this->postNext();
				}
			}
		}

		$this->template_data->set('output', $output);

		$this->load->view('accounting/checkbooks/checkbooks_add', $this->template_data->get_data());
	}

	public function edit($book_id, $output='') {
		
		$book = new $this->Accounting_checkbooks_model;
		$book->setId($book_id,true);
		$book_data = $book->get();

		if( $this->input->post() ) {
			$this->form_validation->set_rules('num_start', 'Starting Number', 'numeric|trim|required');
			$this->form_validation->set_rules('num_end', 'Ending Number', 'numeric|trim|required');
			if($this->form_validation->run()) {
				$book->setNumStart($this->input->post('num_start'),false,true);
				$book->setNumEnd($this->input->post('num_end'),false,true);
				if( $book->update() ) {
						for($i=$this->input->post('num_start');$i<=$this->input->post('num_end');$i++) {
							$checks = new $this->Accounting_checkbooks_checks_model;
							$checks->setCheckbookId($book_data->id,false,true);
							$checks->setChartId($book_data->chart_id,true,true);
							$checks->setCheckNumber($i,true,true);
							if( !$checks->nonEmpty() ) {
								$checks->insert();
							}
						}
					}
				$this->postNext();
			}
		}

		$this->template_data->set('book', $book->get());

		$this->template_data->set('output', $output);

		$this->load->view('accounting/checkbooks/checkbooks_edit', $this->template_data->get_data());
	}

	public function delete($book_id) {
		$book = new $this->Accounting_checkbooks_model;
		$book->setId($book_id,true);
		$book->delete();

		$checks = new $this->Accounting_checkbooks_checks_model;
		$checks->setCheckbookId($book_id,true);
		$checks->delete();

		redirect("accounting_checkbooks");
	}

}
