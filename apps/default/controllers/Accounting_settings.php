<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Accounting_settings extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->template_data->set('page_title', 'COOP - Accounting Settings');
		$this->template_data->set('current_page', 'Configuration');
		$this->template_data->set('current_uri', 'configuration');

		$this->_isAuth('system', 'settings', 'view');
		$this->_isAuth('accounting', 'settings', 'view');

		$this->load->model('Coop_lists_model');
		$this->load->model('Coop_lists_options_model');
		$this->load->model('Coop_settings_model');
		$this->load->model('Accounting_charts_model');
		
	}

	public function index() {
		redirect("system_settings");
	}

	public function configuration() {

		$this->_isAuth('system', 'settings', 'edit');
		$this->_isAuth('accounting', 'settings', 'edit');

		if($this->input->post()) {

			foreach( $this->input->post() as $key=>$value ) {
				$setting = new $this->Coop_settings_model;
				$setting->setDepartment('ACCOUNTING',true);
				$setting->setSection('CONFIG',true);
				$setting->setKey($key,true);
				$setting->setValue($value);
				if($setting->nonEmpty()) {
					$setting->set_exclude('id');
					$setting->update();
				} else {
					$setting->insert();
				}
			}
			if( $this->input->get('next') ) {
				redirect( $this->input->get('next') );
			}
		}

		$settings = new $this->Coop_settings_model;
		$settings->setDepartment('ACCOUNTING',true);
		$settings->setSection('CONFIG',true);
		$this->template_data->set('settings', $settings->populate());

		$main_charts = new $this->Accounting_charts_model;
		$main_charts->set_order('type', 'ASC');
		$main_charts->set_order('number', 'ASC');
		$main_charts->set_select('id,title,type');
		$main_charts->set_limit(0);
		$charts_data = $main_charts->recursive('parent', 0);
		$this->template_data->set('account_titles', $charts_data);

		$this->load->view('accounting/settings/configuration', $this->template_data->get_data());
	}

	public function class_list($start=0) {
		$list = new $this->Coop_lists_model;
		$list->setDept('ACCOUNTING',true);
		$list->setType('CLASS',true);
		$list->set_start( $start );
		$list->set_limit(0);
		$list_data = $list->recursive('parent', 0);
		$this->template_data->set('class_list', $list_data);

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'base_url' => base_url( $this->config->item('index_page') . "/accounting_settings/class_list"),
			'total_rows' => $list->count_all_results(),
			'per_page' => $list->get_limit(),
			'ajax'=>true,
		)));

		$this->template_data->set('current_page', 'Class List');
		$this->template_data->set('current_uri', 'class_list');

		$this->load->view('accounting/settings/class_list', $this->template_data->get_data());
	}

	public function add_class($output='') {

		$this->_isAuth('system', 'settings', 'add');
		$this->_isAuth('accounting', 'settings', 'add');

		if($this->input->post()) {
			$this->form_validation->set_rules('name', 'Class Name', 'trim|required');
			if($this->form_validation->run()) {
				$new = new $this->Coop_lists_model;
				$new->setDept('ACCOUNTING');
				$new->setType('CLASS');
				$new->setActive(1);
				$new->setValue($this->input->post('name'));
				$new->setParent(($this->input->post('parent')) ? $this->input->post('parent') : 0);
				if( $new->insert() ) {
					redirect("accounting_settings/edit_class/{$new->getId()}");
				}
			}
		}

		$list = new $this->Coop_lists_model;
		$list->setDept('ACCOUNTING',true);
		$list->setType('CLASS',true);
		$list->setActive(1,true);
		$list_data = $list->recursive('parent', 0);
		$this->template_data->set('class_list', $list_data);

		$this->template_data->set('current_page', 'Class List');
		$this->template_data->set('current_uri', 'class_list');

		$this->template_data->set('output', $output);

		$this->load->view('accounting/settings/class_list_add', $this->template_data->get_data());
	}

	public function edit_class($id, $output='') {

		$this->_isAuth('system', 'settings', 'edit');
		$this->_isAuth('accounting', 'settings', 'edit');

		$current = new $this->Coop_lists_model;
		$current->setId($id,true);

		if($this->input->post()) {
			$this->form_validation->set_rules('name', 'Class Name', 'trim|required');
			if($this->form_validation->run()) {
				$current->setActive(($this->input->post('active')) ? 1 : 0);
				$current->setValue($this->input->post('name'));
				$current->setParent(($this->input->post('parent')) ? $this->input->post('parent') : 0);
				$current->set_exclude(array('dept','type','id'));
				$current->update();
			}
		} 
		$current_class = $current->get();
		$this->template_data->set('current_class', $current->get());
		
		$list = new $this->Coop_lists_model;
		$list->setDept('ACCOUNTING',true);
		$list->setType('CLASS',true);
		$list->setActive(1,true);
		$list->set_where('id !=', $id);
		$list_data = $list->recursive('parent', 0);
		$this->template_data->set('class_list', $list_data);

		$this->template_data->set('current_page', 'Class List');
		$this->template_data->set('current_uri', 'class_list');

		$this->template_data->set('output', $output);

		$this->load->view('accounting/settings/class_list_edit', $this->template_data->get_data());
	}

	public function item_list($start=0) {
		$items = new $this->Coop_lists_model;
		$items->setDept('ACCOUNTING',true);
		$items->setType('ITEM',true);
		$items->set_start( $start );
		$items_data = $items->recursive('parent', 0);
		$this->template_data->set('item_list', $items_data);

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'base_url' => base_url( $this->config->item('index_page') . '/accounting_settings/item_list/'),
			'total_rows' => $items->count_all_results(),
			'per_page' => $items->get_limit(),
			'ajax' => true,
		)));

		$this->template_data->set('current_page', 'Item List');
		$this->template_data->set('current_uri', 'item_list');

		$this->load->view('accounting/settings/item_list', $this->template_data->get_data());
	}

	public function add_item($output='') {


		$this->_isAuth('system', 'settings', 'add');
		$this->_isAuth('accounting', 'settings', 'add');

		if($this->input->post()) { 
			$this->form_validation->set_rules('name', 'Item Name', 'trim|required');
			$this->form_validation->set_rules('account_title', 'Account Title', 'trim|required');
			if($this->form_validation->run()) {
				$new = new $this->Coop_lists_model;
				$new->setDept('ACCOUNTING');
				$new->setType('ITEM');
				$new->setActive(1);
				$new->setValue( $this->input->post('name') );
				$new->setParent(($this->input->post('parent')) ? $this->input->post('parent') : 0);
				if( $new->insert() ) {
					if( $this->input->post('account_title') ) {
						$option = $this->Coop_lists_options_model;
						$option->setListId($new->getId(),true);
						$option->setKey('account_title',true);
						$option->setValue($this->input->post('account_title'));
						if($option->nonEmpty()) {
							$option->set_exclude(array('id', 'list_id', 'key'));
							$option->update();
						} else {
							$option->insert();
						}
					}
					redirect("accounting_settings/edit_item/{$new->getId()}");
				}
			}
		}

		$main_charts = new $this->Accounting_charts_model;
		$main_charts->set_order('type', 'ASC');
		$main_charts->set_order('number', 'ASC');
		$main_charts->set_select('id,title,type');
		$main_charts->set_limit(0);
		$charts_data = $main_charts->recursive('parent', 0);
		$this->template_data->set('account_titles', $charts_data);

		$items = new $this->Coop_lists_model;
		$items->setDept('ACCOUNTING',true);
		$items->setType('ITEM',true);
		$items->setActive(1,true);
		$items->set_limit(0);
		$items_data = $items->recursive('parent', 0);
		$this->template_data->set('item_list', $items_data);

		$this->template_data->set('output', $output);

		$this->template_data->set('current_page', 'Item List');
		$this->template_data->set('current_uri', 'item_list');

		$this->load->view('accounting/settings/item_list_add', $this->template_data->get_data());
	}

	public function edit_item($id, $output='') {

		$this->_isAuth('system', 'settings', 'edit');
		$this->_isAuth('accounting', 'settings', 'edit');

		$current = new $this->Coop_lists_model;
		$current->setId($id,true);

		if($this->input->post()) { 
			$this->form_validation->set_rules('name', 'Class Name', 'trim|required');
			$this->form_validation->set_rules('account_title', 'Account Title', 'trim|required');
			if($this->form_validation->run()) {
				$current->setActive(($this->input->post('active')) ? 1 : 0);
				$current->setValue($this->input->post('name'));
				$current->setParent(($this->input->post('parent')) ? $this->input->post('parent') : 0);
				$current->set_exclude(array('dept','type','id'));
				$current->update();

				if( $this->input->post('account_title') ) {
					$option = $this->Coop_lists_options_model;
					$option->setListId($id,true);
					$option->setKey('account_title',true);
					$option->setValue($this->input->post('account_title'));
					if($option->nonEmpty()) {
						$option->set_exclude(array('id', 'list_id', 'key'));
						$option->update();
					} else {
						$option->insert();
					}
				}
			}
		} 

		$current->set_select("coop_lists.*");
		$current->set_select("(SELECT clo.value FROM coop_lists_options clo WHERE clo.list_id=coop_lists.id AND clo.key='account_title') as account_title");
		$this->template_data->set( 'current_item', $current->get() );
		
		$main_charts = new $this->Accounting_charts_model;
		$main_charts->set_order('type', 'ASC');
		$main_charts->set_order('number', 'ASC');
		$main_charts->set_select('id,title,type');
		$main_charts->set_limit(0);
		$charts_data = $main_charts->recursive('parent', 0);
		$this->template_data->set('account_titles', $charts_data);

		$list = new $this->Coop_lists_model;
		$list->setDept('ACCOUNTING',true);
		$list->setType('ITEM',true);
		$list->setActive(1,true);
		$list->set_where('id !=', $id);
		$list_data = $list->recursive('parent', 0);
		$this->template_data->set('item_list', $list_data);

		$this->template_data->set('output', $output);

		$this->template_data->set('current_page', 'Item List');
		$this->template_data->set('current_uri', 'item_list');

		$this->load->view('accounting/settings/item_list_edit', $this->template_data->get_data());
	}

}
