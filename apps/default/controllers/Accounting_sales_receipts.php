<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Accounting_sales_receipts extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->template_data->set('page_title', 'COOP - Sales Receipts');
		$this->template_data->set('current_page', 'Sales Receipts');
		$this->template_data->set('current_uri', 'accounting_sales_receipts');

		$this->_isAuth('accounting', 'sales_receipts', 'view');
		
		$this->template_data->set('item_types', unserialize(RECEIPT_ITEM_TYPES));
		
		$this->load->model('Accounting_charts_model');
		$this->load->model('Accounting_transactions_model');
		$this->load->model('Accounting_entries_model');
		$this->load->model('Accounting_receipts_model');
		$this->load->model('Accounting_receipts_items_model');
		$this->load->model('Coop_names_model');
		$this->load->model('Coop_lists_model');
		$this->load->model('Coop_lists_options_model');
		$this->load->model('Coop_settings_model');
	}

	private function _settings() {

		$undeposited = new $this->Coop_settings_model;
		$undeposited->setDepartment('ACCOUNTING',true);
		$undeposited->setSection('CONFIG',true);
		$undeposited->setKey('undeposited',true);
		$udf_setting = $undeposited->get();

		$settings = (object) array(
			'undeposited' => ($udf_setting) ? $udf_setting->value : false,
		);

		foreach($settings as $key=>$value) {
			if(!$value) {
				if( $key=='undeposited') {
					redirect(site_url("accounting_settings/configuration") . "?error_code=202&next=" . uri_string() );
				}
			}
		}

		return $settings;
	}

	public function index($status='all', $start=0) {

		if(( $this->input->post('output') == 'body_wrapper' )||( $this->input->post('output') == 'inner_page' )) {

		$receipts = new $this->Accounting_receipts_model('ar');
		$receipts->set_join('coop_names cn', 'cn.id=ar.name_id');

		$receipts->set_select('ar.*');
		$receipts->set_select('cn.full_name as full_name');
		$receipts->set_select('(SELECT sum(ari.amount) FROM accounting_receipts_items ari WHERE ari.receipt_id=ar.id) as total_items');

		$receipts->set_select('((SELECT ar.amount) - IF((SELECT sum(ari.amount) FROM accounting_receipts_items ari WHERE ari.receipt_id=ar.id),(SELECT sum(ari.amount) FROM accounting_receipts_items ari WHERE ari.receipt_id=ar.id),0)) as balance');

		$receipts->set_select('(SELECT adi.deposit_id FROM accounting_deposits_items adi JOIN accounting_entries ae ON adi.entry_id=ae.id JOIN accounting_transactions at ON ae.trn_id=at.id WHERE ae.trn_id=ar.trn_id LIMIT 1) as deposit_id');

		$receipts->set_select('(SELECT COUNT(*) FROM accounting_transactions at WHERE at.id=ar.trn_id) as trn_exist');

		//$receipts->set_select('(SELECT SUM(ae.debit) FROM accounting_entries ae WHERE ae.trn_id=ar.trn_id) as entries_debit');
		//$receipts->set_select('(SELECT SUM(ae.credit) FROM accounting_entries ae WHERE ae.trn_id=ar.trn_id) as entries_credit');

		$receipts->set_select('ROUND((SELECT SUM(ae.debit) FROM accounting_entries ae WHERE ae.trn_id=ar.trn_id) - ar.amount) as debit_cleared');
		$receipts->set_select('ROUND((SELECT SUM(ae.credit) FROM accounting_entries ae WHERE ae.trn_id=ar.trn_id) - (SELECT(total_items))) as credit_cleared');

		$receipts->set_order('ar.receipt_date', 'DESC'); 
		$receipts->set_order('ar.receipt_number', 'DESC'); 
		$receipts->set_start($start); 

		switch ($status) {
			case 'deposited':
				$this->template_data->set('receipts_page_title', 'Deposited Receipts'); 
				$receipts->set_where('(SELECT COUNT(*) FROM accounting_deposits_items adi JOIN accounting_entries ae ON adi.entry_id=ae.id JOIN accounting_transactions at ON ae.trn_id=at.id WHERE ae.trn_id=ar.trn_id) > 0'); 
				break;

			case 'undeposited':
				$this->template_data->set('receipts_page_title', 'Undeposited Receipts'); 
				$receipts->set_where('(SELECT COUNT(*) FROM accounting_deposits_items adi JOIN accounting_entries ae ON adi.entry_id=ae.id JOIN accounting_transactions at ON ae.trn_id=at.id WHERE ae.trn_id=ar.trn_id) = 0');
				$receipts->set_where('(SELECT COUNT(*) FROM accounting_receipts_items ari WHERE ari.receipt_id=ar.id) > 0');
				break;

			case 'incomplete':
				$this->template_data->set('receipts_page_title', 'Incomplete Receipts');
				$receipts->set_where('(SELECT COUNT(*) FROM accounting_receipts_items ari WHERE ari.receipt_id=ar.id) = 0');
				break;

			default:
				$this->template_data->set('receipts_page_title', 'All Receipts');
				break;
		}

		$this->template_data->set('receipts', $receipts->populate()); 

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'uri_segment' => 4,
			'base_url' => base_url( $this->config->item('index_page') . "/accounting_sales_receipts/index/{$status}"),
			'total_rows' => $receipts->count_all_results(),
			'per_page' => $receipts->get_limit(),
			'ajax'=>true,
		)));

		}
		
		$this->load->view('accounting/sales_receipts/sales_receipts', $this->template_data->get_data());
	}

	public function record_receipt($output='') {

		$this->_isAuth('accounting', 'sales_receipts', 'add');

		if($this->input->post()) {
			$this->form_validation->set_rules('name_id', 'Name', 'trim|required');
			$this->form_validation->set_rules('receipt_number', 'Receipt Number', 'trim|required|is_unique[lending_payments.receipt_number]|is_unique[shares_capital.receipt_number]');
			$this->form_validation->set_rules('receipt_date', 'Receipt Date', 'trim|required');
			$this->form_validation->set_rules('amount', 'Amount', 'trim|required');

			if($this->form_validation->run()) {
					$receipt_date = date( "Y-m-d", strtotime( $this->input->post('receipt_date') ) );
					$amount = str_replace(',', '', $this->input->post('amount') );

					$receipt = new $this->Accounting_receipts_model;
					$receipt->setNameId($this->input->post('name_id'));
					$receipt->setReceiptNumber($this->input->post('receipt_number'));
					$receipt->setReceiptDate($receipt_date);
					$receipt->setAmount($amount);
					$receipt->setMemo($this->input->post('memo')); 
					if( $receipt->insert() ) {
						redirect("accounting_sales_receipts/receipt_items/{$receipt->getId()}");
					}

			}
			$this->postNext();
		}

		$lastreceipt = new $this->Accounting_receipts_model;
		$lastreceipt->set_order('receipt_number', 'DESC');
		$this->template_data->set('lastreceipt', $lastreceipt->get()); 

		$this->template_data->set('output', $output); 
		$this->load->view('accounting/sales_receipts/record_receipt', $this->template_data->get_data());
	}

	public function edit_receipt($id, $output='') {

		$this->_isAuth('accounting', 'sales_receipts', 'edit');

		$receipt = new $this->Accounting_receipts_model;
		$receipt->setId($id,true);
		$receipt->set_select("accounting_receipts.*");
		$receipt->set_select('(SELECT adi.deposit_id FROM accounting_deposits_items adi JOIN accounting_entries ae ON adi.entry_id=ae.id JOIN accounting_transactions at ON ae.trn_id=at.id WHERE ae.trn_id=accounting_receipts.trn_id LIMIT 1) as deposit_id');

		$receipt->set_select('(SELECT sum(ari.amount) FROM accounting_receipts_items ari WHERE ari.receipt_id=accounting_receipts.id) as total_items');

		$receipt->set_select('((SELECT accounting_receipts.amount) - IF((SELECT sum(ari.amount) FROM accounting_receipts_items ari WHERE ari.receipt_id=accounting_receipts.id),(SELECT sum(ari.amount) FROM accounting_receipts_items ari WHERE ari.receipt_id=accounting_receipts.id),0)) as balance');

		$receipt->set_select('(SELECT adi.deposit_id FROM accounting_deposits_items adi JOIN accounting_entries ae ON adi.entry_id=ae.id JOIN accounting_transactions at ON ae.trn_id=at.id WHERE ae.trn_id=accounting_receipts.trn_id LIMIT 1) as deposit_id');

		$receipt->set_select('(SELECT COUNT(*) FROM accounting_transactions at WHERE at.id=accounting_receipts.trn_id) as trn_exist');

		//$receipts->set_select('(SELECT SUM(ae.debit) FROM accounting_entries ae WHERE ae.trn_id=accounting_receipts.trn_id) as entries_debit');
		//$receipts->set_select('(SELECT SUM(ae.credit) FROM accounting_entries ae WHERE ae.trn_id=accounting_receipts.trn_id) as entries_credit');

		$receipt->set_select('ROUND((SELECT SUM(ae.debit) FROM accounting_entries ae WHERE ae.trn_id=accounting_receipts.trn_id) - accounting_receipts.amount) as debit_cleared');

		$receipt->set_select('ROUND((SELECT SUM(ae.credit) FROM accounting_entries ae WHERE ae.trn_id=accounting_receipts.trn_id) - (SELECT(total_items))) as credit_cleared');

		$receipt->set_select('ROUND((SELECT SUM(ae.credit) FROM accounting_receipts_items ari JOIN accounting_entries ae ON ae.id=ari.entry_id WHERE ae.trn_id=accounting_receipts.trn_id) - (SELECT(total_items)) )  as reverse_credit');

		$receipt_data = $receipt->get();

		if($this->input->post()) { 

			if( !$receipt_data->deposit_id ) {
				$this->form_validation->set_rules('name_id', 'Name', 'trim|required');
				$this->form_validation->set_rules('receipt_number', 'Receipt Number', 'trim|required');
				$this->form_validation->set_rules('receipt_date', 'Receipt Date', 'trim|required');
				$this->form_validation->set_rules('amount', 'Amount', 'trim|required');
			}
			$this->form_validation->set_rules('memo', 'Memo', 'trim');
			if($this->form_validation->run()) {
					$receipt_date = date( "Y-m-d", strtotime( $this->input->post('receipt_date') ) );
					$amount = str_replace(',', '', $this->input->post('amount') );

					if( !$receipt_data->deposit_id ) {
						$receipt->setNameId($this->input->post('name_id'), false, true);
						$receipt->setReceiptNumber($this->input->post('receipt_number'), false, true);
						$receipt->setReceiptDate($receipt_date, false, true);
						$receipt->setAmount($amount, false, true);
					}
					$receipt->setMemo($this->input->post('memo'), false, true);
					$receipt->update();

					if( $receipt_data->trn_id ) {
						$trn = new $this->Accounting_transactions_model;
						$trn->setId( $receipt_data->trn_id, true );
						if( !$receipt_data->deposit_id ) {
							$trn->setDate( $receipt_date, false, true );
							$trn->setNameId( $this->input->post('name_id'), false, true );
							$trn->setNumber( $this->input->post('receipt_number'), false, true );
							$trn->setAmount( $amount, false, true );
						}
						$trn->setMemo( $this->input->post('memo'), false, true );
						$trn->update();

						$setting = $this->_settings();

						$debit = new $this->Accounting_entries_model;
						$debit->setTrnId( $receipt_data->trn_id, true );
						$debit->setChartId( $setting->undeposited, true );
						if( !$receipt_data->deposit_id ) {
							$debit->setNameId( $this->input->post('name_id'), false, true );
						}
						$debit->setMemo( $this->input->post('memo'), false, true );
						$debit->update();
					}

					$this->postNext();

			}
		}
		$receipt->set_join('coop_names cn', 'cn.id=accounting_receipts.name_id');

		$receipt->set_select('accounting_receipts.*');
		$receipt->set_select('cn.full_name as full_name');

		$this->template_data->set('receipt', $receipt->get());

		$this->template_data->set('output', $output); 
		$this->load->view('accounting/sales_receipts/edit_receipt', $this->template_data->get_data());
	}

	public function delete_receipt($id) {

		$this->_isAuth('accounting', 'sales_receipts', 'delete');

		$receipt = new $this->Accounting_receipts_model;
		$receipt->setId($id,true);
		$receipt_data = $receipt->get();

		$receipts_items = new $this->Accounting_receipts_items_model;
		$receipts_items->setReceiptId($id,true);

		$trn = new $this->Accounting_transactions_model;
		$trn->setId($receipt_data->trn_id,true);

		$entries = new $this->Accounting_entries_model;
		$entries->setTrnId($receipt_data->trn_id,true);
		
		$trn->delete();
		$entries->delete();
		$receipts_items->delete();
		$receipt->delete();

		redirect("accounting_sales_receipts");
	}

	public function clear_entries($id) {

		$this->_isAuth('accounting', 'sales_receipts', 'edit');

		$receipt = new $this->Accounting_receipts_model;
		$receipt->setId($id,true);
		$receipt_data = $receipt->get();

		$entries = new $this->Accounting_entries_model;
		$entries->setTrnId($receipt_data->trn_id,true);
		
		$entries->delete();

		redirect("accounting_sales_receipts/receipt_items/{$id}");
	}

	public function publish_receipt($id) {

		$this->_isAuth('accounting', 'sales_receipts', 'edit');

		$receipt = new $this->Accounting_receipts_model;
		$receipt->setId($id,true);
		$receipt->set_join('coop_names cn', 'cn.id=accounting_receipts.name_id');
		$receipt->set_select('accounting_receipts.*');
		$receipt->set_select('cn.full_name as full_name');
		
		$receipt->set_select('(SELECT SUM(ari.amount) FROM accounting_receipts_items ari WHERE ari.receipt_id=accounting_receipts.id) as total_items');

		$receipt->set_select('(SELECT adi.deposit_id FROM accounting_deposits_items adi JOIN accounting_entries ae ON adi.entry_id=ae.id JOIN accounting_transactions at ON ae.trn_id=at.id WHERE ae.trn_id=accounting_receipts.trn_id LIMIT 1) as deposit_id');

		$receipt->set_select("( (SELECT accounting_receipts.amount) - (IF( (SELECT SUM(ari.amount) FROM accounting_receipts_items ari WHERE ari.receipt_id=accounting_receipts.id), (SELECT SUM(ari.amount) FROM accounting_receipts_items ari WHERE ari.receipt_id=accounting_receipts.id), 0)) ) as balance");
		
		$receipt->set_select('(SELECT COUNT(*) FROM accounting_transactions at WHERE at.id=accounting_receipts.trn_id) as trn_exist');
		
		$receipt->set_select('ROUND((SELECT SUM(ae.debit) FROM accounting_entries ae WHERE ae.trn_id=accounting_receipts.trn_id) - accounting_receipts.amount) as debit_cleared');
		
		$receipt->set_select('ROUND((SELECT SUM(ae.credit) FROM accounting_entries ae WHERE ae.trn_id=accounting_receipts.trn_id) - (SELECT(total_items))) as credit_cleared');

		$receipt_data = $receipt->get();	
		$receipt_items = new $this->Accounting_receipts_items_model;
		$receipt_items->setReceiptId($id,true);
		$receipt_items->set_limit(0);

		$receipt_items->set_select('accounting_receipts_items.*');
		$receipt_items->set_select('(SELECT clo.value FROM coop_lists_options clo WHERE clo.key="account_title" AND clo.list_id=accounting_receipts_items.item_id) as chart_id');
		$receipt_items->set_select('(SELECT COUNT(*) FROM accounting_entries ae WHERE ae.id=accounting_receipts_items.entry_id) as entry_exist');
		$receipt_items->set_select('ROUND((SELECT ae.credit FROM accounting_entries ae WHERE ae.id=accounting_receipts_items.entry_id)-accounting_receipts_items.amount) as entry_cleared');

		if( round($receipt_data->balance) > 0 ) {
			redirect("accounting_sales_receipts/receipt_items/{$id}");
		}

		$setting = $this->_settings();

		$trn_id = $receipt_data->trn_id;
		if( !$receipt_data->trn_exist ) {

			$trn = new $this->Accounting_transactions_model;
			$trn->setDept('ACCOUNTING');
			$trn->setSect('RECEIVING');
			$trn->setType('RECEIPT');
			$trn->setDate( $receipt_data->receipt_date );
			$trn->setNameId( $receipt_data->name_id );
			$trn->setNumber( $receipt_data->receipt_number );
			$trn->setMemo( $receipt_data->memo );
			$trn->setAmount( $receipt_data->amount );
			
			if( $trn->insert() ) {
				$trn_id = $trn->getId();
				$receipt->setTrnId( $trn->getId(), false, true);
				$receipt->update();
			}
		}

		foreach($receipt_items->populate() as $item) {

			// debit
			$debit = new $this->Accounting_entries_model;
			$debit->setTrnId( $trn_id, true );
			$debit->setChartId( $setting->undeposited, true );
			$debit->setItemNumber(0, true);
			$debit->setItemId($item->id, true);
			if( $item->amount < 0 ) {
				$debit->setCredit( abs($item->amount), false, true );
			} else {
				$debit->setDebit( abs($item->amount), false, true );
			}
			$debit->setClass( $item->class_id, false, true );
			$debit->setNameId( $receipt_data->name_id, false, true );
			$debit->setItemType($item->item_type, false, true);
			
			if( $debit->nonEmpty() ) {
				$debit->update();
			} else {
				$debit->clear_data_fields();
				$debit->insert();
			}
			// credit
			$credit = new $this->Accounting_entries_model;
			$credit->setTrnId( $trn_id, true );
			$credit->setChartId( $item->chart_id, true );
			$credit->setItemNumber(1, true);
			$credit->setItemId($item->id, true);
			if( $item->amount < 0 ) {
				$credit->setDebit( abs($item->amount), false, true );
			} else {
				$credit->setCredit( $item->amount, false, true );
			}
			$credit->setClass( $item->class_id, false, true );
			$credit->setNameId( $item->name_id, false, true );
			$credit->setItemType($item->item_type, false, true);

			if( $credit->nonEmpty() ) {
				$credit_data = $credit->getResults();
				$entry_id = $credit_data->id; 
				$credit->update();
			} else {
				$credit->clear_data_fields();
				if( $credit->insert() ) {
					$entry_id = $credit->getId();
				}
			}
			
			if( $item->entry_exist == 0 ) {
				$rItem = new $this->Accounting_receipts_items_model;
				$rItem->setId($item->id,true);
				$rItem->setEntryId($entry_id,false,true);
				$rItem->update();
			}

		}

		redirect("accounting_sales_receipts/receipt_items/{$id}");

	}

	public function receipt_items($id, $output='') {

		$receipt = new $this->Accounting_receipts_model;
		$receipt->setId($id,true);
		$receipt->set_join('coop_names cn', 'cn.id=accounting_receipts.name_id');
		$receipt->set_select('accounting_receipts.*');
		$receipt->set_select('cn.full_name as full_name');
		
		$receipt->set_select('(SELECT SUM(ari.amount) FROM accounting_receipts_items ari WHERE ari.receipt_id=accounting_receipts.id) as total_items');
		
		$receipt->set_select('((SELECT accounting_receipts.amount) - IF((SELECT sum(ari.amount) FROM accounting_receipts_items ari WHERE ari.receipt_id=accounting_receipts.id),(SELECT sum(ari.amount) FROM accounting_receipts_items ari WHERE ari.receipt_id=accounting_receipts.id),0)) as balance');
		
		$receipt->set_select('(SELECT COUNT(*) FROM accounting_transactions at WHERE at.id=accounting_receipts.trn_id) as trn_exist');
		
		$receipt->set_select('(SELECT adi.deposit_id FROM accounting_deposits_items adi JOIN accounting_entries ae ON adi.entry_id=ae.id JOIN accounting_transactions at ON ae.trn_id=at.id WHERE ae.trn_id=accounting_receipts.trn_id LIMIT 1) as deposit_id');

		//$receipt->set_select('(SELECT SUM(ae.debit) FROM accounting_entries ae WHERE ae.trn_id=accounting_receipts.trn_id) as entries_debit');
		//$receipt->set_select('(SELECT SUM(ae.credit) FROM accounting_entries ae WHERE ae.trn_id=accounting_receipts.trn_id) as entries_credit');

		$receipt->set_select('ROUND((SELECT SUM(ae.debit) FROM accounting_entries ae WHERE ae.trn_id=accounting_receipts.trn_id) - accounting_receipts.amount) as debit_cleared');

		$receipt->set_select('ROUND((SELECT SUM(ae.credit) FROM accounting_entries ae WHERE ae.trn_id=accounting_receipts.trn_id) - (SELECT(total_items))) as credit_cleared');

		$receipt_data = $receipt->get(); 
		$this->template_data->set('receipt', $receipt_data);

		$receipt_items = new $this->Accounting_receipts_items_model;
		$receipt_items->setReceiptId($id,true);
		$receipt_items->set_limit(0);

		$receipt_items->set_join('coop_names cn', 'cn.id=accounting_receipts_items.name_id');
		$receipt_items->set_join('coop_lists cli', 'cli.id=accounting_receipts_items.item_id');
		$receipt_items->set_join('coop_lists clc', 'clc.id=accounting_receipts_items.class_id');

		$receipt_items->set_select('accounting_receipts_items.*');
		$receipt_items->set_select('cn.full_name as full_name');
		$receipt_items->set_select('cli.value as item_name');
		$receipt_items->set_select('clc.value as class_name');
		$receipt_items->set_select('(SELECT COUNT(*) FROM accounting_entries ae WHERE ae.id=accounting_receipts_items.entry_id) as entry_exist');
		$receipt_items->set_select('ROUND((SELECT ae.credit FROM accounting_entries ae WHERE ae.id=accounting_receipts_items.entry_id)-accounting_receipts_items.amount) as entry_cleared');

		$receipt_items->set_select('(SELECT COUNT(*) FROM accounting_deposits_items adi WHERE adi.entry_id=accounting_receipts_items.entry_id) as deposit_exist');

		$this->template_data->set('receipt_items', $receipt_items->populate());
		
		$this->template_data->set('output', $output); 
		$this->load->view('accounting/sales_receipts/receipt_items', $this->template_data->get_data());
	}

	public function add_receipt_item($id, $output='') {

		$this->_isAuth('accounting', 'sales_receipts', 'edit');

		$receipt = new $this->Accounting_receipts_model;
		$receipt->setId($id,true);
		$receipt->set_join('coop_names cn', 'cn.id=accounting_receipts.name_id');
		$receipt->set_select('accounting_receipts.*');
		$receipt->set_select('cn.full_name as full_name');
		$receipt->set_select('(SELECT SUM(ari.amount) FROM accounting_receipts_items ari WHERE ari.receipt_id=accounting_receipts.id) as total_items');

		$receipt->set_select("( (SELECT accounting_receipts.amount) - (IF( (SELECT SUM(ari.amount) FROM accounting_receipts_items ari WHERE ari.receipt_id=accounting_receipts.id), (SELECT SUM(ari.amount) FROM accounting_receipts_items ari WHERE ari.receipt_id=accounting_receipts.id), 0)) ) as balance");

		$receipt->set_select('(SELECT adi.deposit_id FROM accounting_deposits_items adi JOIN accounting_entries ae ON adi.entry_id=ae.id JOIN accounting_transactions at ON ae.trn_id=at.id WHERE ae.trn_id=accounting_receipts.trn_id LIMIT 1) as deposit_id');

		$receipt_data = $receipt->get(); 
		$this->template_data->set('receipt', $receipt_data); 

		if( round($receipt_data->balance) == 0 ) {
			redirect("accounting_sales_receipts/receipt_items/{$id}");
		}

		if( $receipt_data->deposit_id ) {
			redirect("accounting_sales_receipts/receipt_items/{$id}");
		}

		if($this->input->post()) {

			$this->form_validation->set_rules('item_type', 'Item Type', 'trim|required');
			$this->form_validation->set_rules('item_id', 'Item Name', 'trim|required');
			$this->form_validation->set_rules('amount', 'Amount', 'trim|required');

			if($this->form_validation->run()) {
					$amount = str_replace(',', '', $this->input->post('amount') );
					$amount = (round($amount) > round($receipt_data->balance)) ? $receipt_data->balance : $amount;
					$receipt_item = new $this->Accounting_receipts_items_model;
					$receipt_item->setReceiptId($id);
					$receipt_item->setItemType( $this->input->post('item_type') );
					$receipt_item->setItemId($this->input->post('item_id'));
					$receipt_item->setNameId($this->input->post('name_id'));
					$receipt_item->setClassId($this->input->post('class_id'));
					$receipt_item->setAmount($amount);
					$receipt_item->setMemo($this->input->post('memo'));
					if( $receipt_item->insert() ) {

						if( $receipt_data->trn_id ) {
							$setting = $this->_settings();
							// debit
							$debit = new $this->Accounting_entries_model;
							$debit->setTrnId( $receipt_data->trn_id );
							$debit->setChartId( $setting->undeposited );
							if( $amount < 0) {
								$debit->setCredit( abs($amount) );
							} else {
								$debit->setDebit( $amount );
							}
							$debit->setClass( $this->input->post('class_id') );
							$debit->setNameId( $receipt_data->name_id );
							$debit->setMemo( $receipt_data->memo );
							$debit->setItemNumber(0);
							$debit->setItemId($receipt_item->getId());
							$debit->setItemType( $this->input->post('item_type') );
							

							$item = new $this->Coop_lists_options_model;
							$item->setListId($this->input->post('item_id'),true);
							$item->setKey('account_title', true);
							$item_data = $item->get();

							// credit
							$credit = new $this->Accounting_entries_model;
							$credit->setTrnId( $receipt_data->trn_id );
							$credit->setChartId( $item_data->value );
							if( $amount < 0) {
								$credit->setDebit( abs($amount) );
							} else {
								$credit->setCredit( $amount );
							}
							
							$credit->setClass( $this->input->post('class_id') );
							$credit->setNameId( $this->input->post('name_id') );
							$credit->setMemo( $this->input->post('memo') );
							$credit->setItemNumber(1);
							$credit->setItemId($receipt_item->getId());
							$credit->setItemType( $this->input->post('item_type') );

							$debit->insert();
							if( $credit->insert() ) {
								$receipt_item2 = new $this->Accounting_receipts_items_model;
								$receipt_item2->setId($receipt_item->getId(),true);
								$receipt_item2->setEntryId($credit->getId(),false,true);
								$receipt_item2->update();
							}
						}
						redirect("accounting_sales_receipts/receipt_items/{$id}");
					} else {
						redirect("accounting_sales_receipts/add_receipt_item/{$id}");
					}
			}
		}

		$items = new $this->Coop_lists_model;
		$items->setDept('ACCOUNTING',true);
		$items->setType('ITEM',true);
		$items->setActive(1,true);
		$items->set_limit(0);
		$items_data = $items->recursive('parent', 0);
		$this->template_data->set('item_list', $items_data);

		$classes = new $this->Coop_lists_model;
		$classes->setDept('ACCOUNTING',true);
		$classes->setType('CLASS',true);
		$classes->setActive(1,true);
		$classes->set_limit(0);
		$classes_data = $classes->recursive('parent', 0);
		$this->template_data->set('class_list', $classes_data);

		$this->template_data->set('output', $output); 
		$this->load->view('accounting/sales_receipts/add_receipt_item', $this->template_data->get_data());
	}

	public function edit_receipt_item($id, $output='') {		

		$this->_isAuth('accounting', 'sales_receipts', 'edit');

		$receipt_item = new $this->Accounting_receipts_items_model('ari');
		$receipt_item->setId($id,true);
		$receipt_item->set_select("ari.*");
		$receipt_item->set_select("(SELECT COUNT(*) FROM lending_payments lp WHERE lp.entry_id=ari.entry_id) as payments");
		$receipt_item->set_select("(SELECT COUNT(*) FROM shares_capital sc WHERE sc.entry_id=ari.entry_id) as capitals");
		$receipt_item->set_select('(SELECT COUNT(*) FROM accounting_entries ar WHERE ar.id=ari.entry_id) as entry_exist');
		$receipt_item->set_select('ROUND((SELECT ae.credit FROM accounting_entries ae WHERE ae.id=ari.entry_id)-ari.amount) as entry_cleared');
		$receipt_item_data = $receipt_item->get();
		
		$locked = ($receipt_item_data->payments OR $receipt_item_data->capitals) ? true : false;
		$this->template_data->set('locked', $locked);
		
		$receipt = new $this->Accounting_receipts_model;
		$receipt->setId($receipt_item_data->receipt_id,true);
		$receipt->set_join('coop_names cn', 'cn.id=accounting_receipts.name_id');
		$receipt->set_select('accounting_receipts.*');
		$receipt->set_select('cn.full_name as full_name');

		$receipt->set_select('(SELECT SUM(ari.amount) FROM accounting_receipts_items ari WHERE ari.receipt_id=accounting_receipts.id) as total_items');

		$receipt->set_select("( (SELECT accounting_receipts.amount) - (IF( (SELECT SUM(ari.amount) FROM accounting_receipts_items ari WHERE ari.receipt_id=accounting_receipts.id), (SELECT SUM(ari.amount) FROM accounting_receipts_items ari WHERE ari.receipt_id=accounting_receipts.id), 0)) ) as balance");

		$receipt->set_select('(SELECT adi.deposit_id FROM accounting_deposits_items adi JOIN accounting_entries ae ON adi.entry_id=ae.id JOIN accounting_transactions at ON ae.trn_id=at.id WHERE ae.trn_id=accounting_receipts.trn_id LIMIT 1) as deposit_id');
		$receipt_data = $receipt->get(); 

		if($this->input->post()) {

			if( !$locked ) {
				$this->form_validation->set_rules('item_type', 'Item Type', 'trim|required');
				$this->form_validation->set_rules('item_id', 'Item Name', 'trim|required');
				$this->form_validation->set_rules('amount', 'Amount', 'trim|required');
			}
			$this->form_validation->set_rules('memo', 'Memo', 'trim');

			if($this->form_validation->run()) {

					$amount = str_replace(',', '', $this->input->post('amount') );
					$amount = (round($amount) > round($receipt_data->balance + $receipt_item_data->amount)) ? round($receipt_data->balance + $receipt_item_data->amount) : $amount;
					
					if( !$locked ) {
						$receipt_item->setItemType( $this->input->post('item_type'), false, true );
						$receipt_item->setItemId( $this->input->post('item_id'), false, true );
						$receipt_item->setNameId( $this->input->post('name_id'), false, true );
						$receipt_item->setAmount($amount, false, true);
					}
					$receipt_item->setClassId($this->input->post('class_id'), false, true);
					$receipt_item->setMemo($this->input->post('memo'), false, true);

					if( $receipt_item->update() ) { 

						if( $receipt_data->trn_id ) {

							$setting = $this->_settings();

							$debit = new $this->Accounting_entries_model;
							$debit->setItemId($id, true);
							$debit->setTrnId( $receipt_data->trn_id, true );
							$debit->setChartId( $setting->undeposited, true );
							$debit->setNameId( $receipt_data->name_id, true );
							if( !$locked ) {
								if( $amount < 0) {
									$debit->setDebit( 0, false, true );
									$debit->setCredit( abs($amount), false, true );
								} else {
									$debit->setDebit( $amount, false, true );
									$debit->setCredit( 0, false, true );
								}
								$debit->setItemType( $this->input->post('item_type'), false, true );
							}

							$debit->setClass( $this->input->post('class_id'), false, true );
							$debit->setItemNumber(0, true);

							$item = new $this->Coop_lists_options_model;
							$item->setListId($this->input->post('item_id'),true);
							$item->setKey('account_title', true);
							$item_data = $item->get();

							$credit = new $this->Accounting_entries_model;
							$credit->setItemId($id, true);
							$credit->setTrnId( $receipt_data->trn_id, true );
							if( !$locked ) {
								$credit->setChartId( $item_data->value, false, true );
								if( $amount < 0) {
									$credit->setDebit( abs($amount), false, true );
									$credit->setCredit( 0, false, true );
								} else {
									$credit->setDebit( 0, false, true );
									$credit->setCredit( $amount, false, true );
								}
								$credit->setNameId( $this->input->post('name_id'), false, true );
								$credit->setItemType( $this->input->post('item_type'), false, true );
							}
							$credit->setClass( $this->input->post('class_id'), false, true );
							$credit->setMemo( $this->input->post('memo'), false, true );
							$credit->setItemNumber(1, true);
							
							if( $debit->nonEmpty() ) {
								$debit->update();
							} else {
								$debit->clear_data_fields();
								$debit->insert();
							}
							if( $credit->nonEmpty() ) {
								$credit->update();
							} else {
								$credit->clear_data_fields();
								$credit->insert();
							}

							$credit_data = $credit->get(); 
							$receipt_item2 = new $this->Accounting_receipts_items_model();
							$receipt_item2->setId($id,true);
							$receipt_item2->setEntryId($credit_data->id,false,true);
							$receipt_item2->update();

							
						}

						redirect("accounting_sales_receipts/receipt_items/{$receipt_item_data->receipt_id}");
					}
			}
			$this->postNext();
		}

		$receipt_item->set_join('coop_names cn', 'cn.id=ari.name_id');
		$receipt_item->set_select('ari.*');
		$receipt_item->set_select('cn.full_name as full_name');
		$this->template_data->set('receipt_item', $receipt_item->get());

		$items = new $this->Coop_lists_model;
		$items->setDept('ACCOUNTING',true);
		$items->setType('ITEM',true);
		$items->setActive(1,true);
		$items->set_limit(0);
		$items_data = $items->recursive('parent', 0);
		$this->template_data->set('item_list', $items_data);

		$classes = new $this->Coop_lists_model;
		$classes->setDept('ACCOUNTING',true);
		$classes->setType('CLASS',true);
		$classes->setActive(1,true);
		$classes->set_limit(0);
		$classes_data = $classes->recursive('parent', 0);
		$this->template_data->set('class_list', $classes_data);

		$this->template_data->set('output', $output); 
		$this->load->view('accounting/sales_receipts/edit_receipt_item', $this->template_data->get_data());
	}

	public function duplicate_receipt_item($receipt_id, $item_id) {	

		$this->_isAuth('accounting', 'sales_receipts', 'edit');

		$receipt_item = new $this->Accounting_receipts_items_model('ari');
		$receipt_item->setId($item_id,true);
		$receipt_item->setReceiptId($receipt_id,true);
		$receipt_item->set_select("ari.*");
		
		if( $receipt_item->nonEmpty() ) {

			$receipt_item_data = $receipt_item->get();

			$receipt = new $this->Accounting_receipts_model;
			$receipt->setId($receipt_item_data->receipt_id,true);
			$receipt->set_join('coop_names cn', 'cn.id=accounting_receipts.name_id');
			$receipt->set_select('accounting_receipts.*');
			$receipt->set_select('cn.full_name as full_name');
			$receipt->set_select('(SELECT SUM(ari.amount) FROM accounting_receipts_items ari WHERE ari.receipt_id=accounting_receipts.id) as total_items');

			$receipt->set_select("( (SELECT accounting_receipts.amount) - (IF( (SELECT SUM(ari.amount) FROM accounting_receipts_items ari WHERE ari.receipt_id=accounting_receipts.id), (SELECT SUM(ari.amount) FROM accounting_receipts_items ari WHERE ari.receipt_id=accounting_receipts.id), 0)) ) as balance");

			$receipt->set_select('(SELECT adi.deposit_id FROM accounting_deposits_items adi JOIN accounting_entries ae ON adi.entry_id=ae.id JOIN accounting_transactions at ON ae.trn_id=at.id WHERE ae.trn_id=accounting_receipts.trn_id LIMIT 1) as deposit_id');

			$receipt_data = $receipt->get(); 

				if( round($receipt_data->balance) == 0 ) {
					redirect("accounting_sales_receipts/receipt_items/{$id}");
				}

				if( $receipt_data->deposit_id ) {
					redirect("accounting_sales_receipts/receipt_items/{$id}");
				}

				$amount = (round($receipt_item_data->amount) > round($receipt_data->balance)) ? $receipt_data->balance : $receipt_item_data->amount;

				$new_item = new $this->Accounting_receipts_items_model;
				$new_item->setReceiptId($receipt_item_data->receipt_id);
				$new_item->setItemType( $receipt_item_data->item_type );
				$new_item->setItemId($receipt_item_data->item_id);
				$new_item->setNameId($receipt_item_data->name_id);
				$new_item->setClassId($receipt_item_data->class_id);
				$new_item->setAmount($amount);
				$new_item->setMemo($receipt_item_data->memo);
				
				if( $new_item->insert() ) {

					if( $receipt_data->trn_id ) {

						$setting = $this->_settings();
						// debit
						$debit = new $this->Accounting_entries_model;
						$debit->setTrnId( $receipt_data->trn_id );
						$debit->setChartId( $setting->undeposited );
						if( $amount < 0) {
							$debit->setCredit( abs($amount) );
						} else {
							$debit->setDebit( $amount );
						}
						$debit->setClass( $receipt_item_data->class_id );
						$debit->setNameId( $receipt_data->name_id );
						$debit->setMemo( $receipt_data->memo );
						$debit->setItemNumber(0);
						$debit->setItemId($new_item->getId());
						$debit->setItemType( $receipt_item_data->item_type );
						
						$item = new $this->Coop_lists_options_model;
						$item->setListId($receipt_item_data->item_id,true);
						$item->setKey('account_title', true);
						$item_data = $item->get();

						// credit
						$credit = new $this->Accounting_entries_model;
						$credit->setTrnId( $receipt_data->trn_id );
						$credit->setChartId( $item_data->value );
						if( $amount < 0) {
							$credit->setDebit( abs($amount) );
						} else {
							$credit->setCredit( $amount );
						}
						
						$credit->setClass( $receipt_item_data->class_id );
						$credit->setNameId( $receipt_item_data->name_id );
						$credit->setMemo( $receipt_item_data->memo );
						$credit->setItemNumber(1);
						$credit->setItemId($new_item->getId());
						$credit->setItemType( $receipt_item_data->item_type );

						$debit->insert();
						if( $credit->insert() ) {
							$receipt_item2 = new $this->Accounting_receipts_items_model;
							$receipt_item2->setId($new_item->getId(),true);
							$receipt_item2->setEntryId($credit->getId(),false,true);
							$receipt_item2->update();
						}
					}
					redirect("accounting_sales_receipts/receipt_items/{$receipt_item_data->receipt_id}");
				} 
		}
		
	}

	public function delete_receipt_item($id) {		

		$this->_isAuth('accounting', 'sales_receipts', 'edit');

		$receipt_item = new $this->Accounting_receipts_items_model;
		$receipt_item->setId($id,true);
		$receipt_item->set_join('accounting_receipts ar', 'ar.id=accounting_receipts_items.receipt_id');
		$receipt_item_data = $receipt_item->get();
		
		$entries = new $this->Accounting_entries_model;
		$entries->setTrnId($receipt_item_data->trn_id,true);
		$entries->setItemId($id,true);

		$entries->delete();	
		$receipt_item->delete();	

		redirect("accounting_sales_receipts/receipt_items/{$receipt_item_data->receipt_id}");

	}

	public function clear_item_entry($id) {		

		$this->_isAuth('accounting', 'sales_receipts', 'edit');
		
		$receipt_item = new $this->Accounting_receipts_items_model;
		$receipt_item->setId($id,true);
		$receipt_item->set_join('accounting_receipts ar', 'ar.id=accounting_receipts_items.receipt_id');
		$receipt_item_data = $receipt_item->get();

		$entries = new $this->Accounting_entries_model;
		$entries->setId($receipt_item_data->entry_id,true);
		$entries->delete();

		$entries = new $this->Accounting_entries_model;
		$entries->setTrnId($receipt_item_data->trn_id,true);
		$entries->setItemId($id,true);
		$entries->delete();	

		redirect("accounting_sales_receipts/receipt_items/{$receipt_item_data->receipt_id}");

	}

	public function ajax($action='') {
		$results = array();
		switch($action) {
			case 'name':
				$names = new $this->Coop_names_model;
				if( $this->input->get('term') ) {
					$names->set_where('full_name LIKE "%' . $this->input->get('term') . '%"');
				}
				$data = array();
				foreach($names->populate() as $name) {
					$data[] = array(
						'label' => $name->full_name,
						'id' => $name->id,
						'desc' => $name->address . " - " . $name->contact_number,
						);
				}
				$results = $data;
			break;
		}
		$this->output
        ->set_content_type('application/json')
        ->set_output(json_encode( $results ));
	}

}
