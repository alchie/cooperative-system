<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Accounting_charts extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->template_data->set('page_title', 'COOP - Chart of Accounts');
		$this->template_data->set('current_page', 'Chart of Accounts');
		$this->template_data->set('current_uri', 'accounting_charts');
		
		$this->_isAuth('accounting', 'chart_of_accounts', 'view');
		
		$this->template_data->set('coa_types', unserialize(CHART_ACCOUNT_TYPES));

		$this->load->model('Accounting_charts_model');
		$this->load->model('Accounting_entries_model');

	}

	public function index() {

		if(( $this->input->post('output') == 'body_wrapper' )||( $this->input->post('output') == 'inner_page' )) {

		$main_charts = new $this->Accounting_charts_model('ac');
		$main_charts->set_order('type', 'ASC');
		$main_charts->set_order('number', 'ASC');
		$main_charts->set_select('ac.*');
		$main_charts->set_select('(SELECT SUM(ae.debit) FROM accounting_entries ae WHERE ae.chart_id=ac.id) as debit_balance');
		$main_charts->set_select('(SELECT SUM(ae.credit) FROM accounting_entries ae WHERE ae.chart_id=ac.id) as credit_balance');
		$main_charts->set_limit(0);
		$charts_data = $main_charts->recursive('parent', 0);
		$this->template_data->set('accounts', $charts_data);

		}

		$this->load->view('accounting/charts/accounting_charts', $this->template_data->get_data());
	}

	public function add($output='') {

		$this->_isAuth('accounting', 'chart_of_accounts', 'add');

		$this->template_data->set('output', $output);

		if( $this->input->post() ) {
			$this->form_validation->set_rules('title', 'Account Title', 'trim|required');
			$this->form_validation->set_rules('type', 'Account Type', 'trim|required');
			if($this->form_validation->run()) {
				$charts = new $this->Accounting_charts_model;
				$charts->setTitle($this->input->post('title'));
				$charts->setType($this->input->post('type'));
				if( $this->input->post('number') ) {
					$charts->setNumber($this->input->post('number'));
			 	}
			 	$charts->setParent(0);				
			 	$charts->setActive(1);				
			 	$charts->setReconcile(0);				
				if( $charts->insert() ) {
					redirect(site_url("accounting_charts") . "?highlight=" . $charts->getId());
				}
			}
		}

		$charts1 = new $this->Accounting_charts_model;
		$charts1->set_limit(0);
		$charts1->set_order('type', 'ASC');
		$charts1->set_order('number', 'ASC');
		$charts_data = $charts1->recursive('parent', 0);
		$this->template_data->set('main_accounts', $charts_data);

		$this->load->view('accounting/charts/accounting_charts_add', $this->template_data->get_data());
	}

	public function edit($id, $output='') {

		$this->_isAuth('accounting', 'chart_of_accounts', 'edit');

		$this->template_data->set('output', $output);

		$charts = new $this->Accounting_charts_model;
		$charts->setId($id,true);

		if( $this->input->post() ) {

			if( $this->input->post('delete') ) {
				$this->delete($id,false);
				$this->postNext();
			}

			$this->form_validation->set_rules('title', 'Account Title', 'trim|required');
			$this->form_validation->set_rules('type', 'Account Type', 'trim|required');
			if($this->form_validation->run()) {
				$charts->setTitle($this->input->post('title'),false,true);
				if( $this->input->post('number') ) {
					$charts->setNumber($this->input->post('number'),false,true);
				}
				if( $this->input->post('parent') ) {
					$parent_chart = new $this->Accounting_charts_model;
					$parent_chart->setId($this->input->post('parent'),true);
					$parent_data = $parent_chart->get();
					if( $parent_data->type == $this->input->post('type') ) {
						$charts->setParent($this->input->post('parent'),false,true);
					} else {
						$charts->setParent(0,false,true);
					}
				} else {
					$charts->setParent(0,false,true);
				}
				$charts->setActive((($this->input->post('active'))?1:0), false, true);	
			 	$charts->setReconcile((($this->input->post('reconcile'))?1:0), false, true);

				$charts->update();
			}
			$this->postNext();
		}
		$charts->set_select("accounting_charts.*");
		$charts->set_select("(SELECT COUNT(*) FROM accounting_entries WHERE accounting_entries.chart_id=accounting_charts.id) as entries");
		$charts->set_select("(SELECT COUNT(*) FROM accounting_charts ac WHERE ac.parent=accounting_charts.id) as children");
		$chart_data = $charts->get();
		$this->template_data->set('account', $chart_data);

		$charts1 = new $this->Accounting_charts_model;
		$charts1->setType($chart_data->type,true);
		$charts1->set_limit(0);
		$charts1->set_order('type', 'ASC');
		$charts1->set_order('number', 'ASC');
		$main_accounts = $charts1->recursive('parent', 0);
		$this->template_data->set('main_accounts', $main_accounts);

		$this->load->view('accounting/charts/accounting_charts_edit', $this->template_data->get_data());
	}

	public function delete($id, $next='accounting_charts') {

		$this->_isAuth('accounting', 'chart_of_accounts', 'delete');

		$charts = new $this->Accounting_charts_model;
		$charts->setId($id,true);
		$charts->set_select("(SELECT COUNT(*) FROM accounting_charts ac WHERE ac.parent=accounting_charts.id) as children");
		$charts->set_select("(SELECT COUNT(*) FROM accounting_entries WHERE accounting_entries.chart_id=accounting_charts.id) as entries");
		if( $data = $charts->get() ) {
			if( ( intval($data->entries) > 0 ) || ( intval( $data->children ) > 0 ) ) {
				$charts->setActive(0, false, true);
				$charts->update();
			} else {
				$charts->delete();
			}
		}
		if( $next ) {
			redirect($next);
		}
	}

	public function details($id, $start=0) {
		$charts = new $this->Accounting_charts_model;
		$charts->setId($id,true);
		$this->template_data->set('charts_data', $charts->get());

		$entries = new $this->Accounting_entries_model('ae');
		$entries->setChartId($id,true);
		$entries->set_join('accounting_transactions at', 'at.id=ae.trn_id');
		$entries->set_join('accounting_charts ac', 'ac.id=ae.chart_id');
		$entries->set_join('coop_names cn', 'cn.id=ae.name_id');
		$entries->set_select('ae.*');
		$entries->set_select('at.dept, at.sect, at.type, at.date, , at.memo');
		$entries->set_select('cn.full_name');
		$entries->set_select('ac.type as chart_type');
		$entries->set_start($start);
		$entries->set_order('at.date', 'DESC');

		$entries->set_group_by('ae.trn_id');
		$entries->set_select('SUM(ae.debit) as total_debit');
		$entries->set_select('SUM(ae.credit) as total_credit');

		$entries_data = $entries->populate(); 
		$this->template_data->set('entries_data', $entries_data);

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'uri_segment' => 4,
			'base_url' => base_url($this->config->item('index_page') . "/accounting_charts/details/{$id}/"),
			'total_rows' => $entries->count_all_results(),
			'per_page' => $entries->get_limit(),
			'ajax'=>true,
		)));

		$this->load->view('accounting/charts/accounting_charts_details', $this->template_data->get_data());
	}

}
