<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Accounting_write_checks extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->template_data->set('page_title', 'COOP - Write Checks');
		$this->template_data->set('current_page', 'Write Checks');
		$this->template_data->set('current_uri', 'accounting_write_checks');

		$this->_isAuth('accounting', 'write_checks', 'view');
		
		$this->template_data->set('coa_types', unserialize(CHART_ACCOUNT_TYPES));
		$this->template_data->set('item_types', unserialize(CHECK_ITEM_TYPES));
		
		$this->load->model('Accounting_charts_model');
		$this->load->model('Accounting_checks_model');
		$this->load->model('Accounting_checks_items_model');
		$this->load->model('Accounting_transactions_model');
		$this->load->model('Accounting_entries_model');
		$this->load->model('Coop_lists_model');
		$this->load->model('Coop_names_model');
		$this->load->model('Accounting_checkbooks_checks_model');
	}

	public function index($bank_id=0, $start=0) {

		$bank_accounts = new $this->Accounting_charts_model('ac');
		$bank_accounts->setReconcile(1,true);
		$bank_accounts->set_order('type', 'ASC');
		$bank_accounts->set_order('number', 'ASC');
		$bank_accounts->set_where('type LIKE "001_BS_ASS_CSB"');
		$bank_accounts->set_select('ac.*');
		$bank_accounts_data = $bank_accounts->populate();
		$this->template_data->set('bank_accounts', $bank_accounts_data);

		$checkbooks = new $this->Accounting_charts_model('ac');
		$checkbooks->setReconcile(1,true);
		$checkbooks->set_order('type', 'ASC');
		$checkbooks->set_order('number', 'ASC');
		$checkbooks->set_where('type LIKE "001_BS_ASS_CSB"');
		$checkbooks->set_select('ac.*');
		$checkbooks->set_select('(SELECT COUNT(*) FROM accounting_checkbooks_checks acbc JOIN accounting_checkbooks acb ON acbc.checkbook_id=acb.id WHERE acb.chart_id=ac.id AND ((SELECT ach.id FROM accounting_checks ach WHERE ach.check_number=acbc.check_number) IS NULL)) as count1');
		$checkbooks->set_where('((SELECT COUNT(*) FROM accounting_checkbooks_checks acbc JOIN accounting_checkbooks acb ON acbc.checkbook_id=acb.id WHERE acb.chart_id=ac.id AND ((SELECT ach.id FROM accounting_checks ach WHERE ach.check_number=acbc.check_number) IS NULL)) > 0)');
		$checkbooks_data = $checkbooks->populate();
		$this->template_data->set('checkbooks', $checkbooks_data);

		if(( $this->input->post('output') == 'body_wrapper' )||( $this->input->post('output') == 'inner_page' )) {

			$checks = new $this->Accounting_checks_model('ak');
			$checks->set_join('coop_names cn', 'cn.id=ak.name_id');

			$checks->set_select('ak.*');
			$checks->set_select('cn.full_name as full_name');
			
			if( ($bank_id != 0) && ($bank_id != '') ) {
				$current_bank = new $this->Accounting_charts_model;
				$current_bank->setId($bank_id,true);
				$this->template_data->set( 'current_bank', $current_bank->get() );
				$checks->set_where('ak.chart_id', $bank_id);
			}

			$checks->set_order('check_date', 'DESC');
			$checks->set_order('check_number', 'DESC');
			$checks->set_start($start);
			$checks->set_select("( (SELECT ak.amount) - (IF( (SELECT SUM(ari.amount) FROM accounting_checks_items ari WHERE ari.check_id=ak.id), (SELECT SUM(ari.amount) FROM accounting_checks_items ari WHERE ari.check_id=ak.id), 0)) ) as balance");
			$checks->set_select('(SELECT COUNT(*) FROM accounting_transactions at WHERE at.id=ak.trn_id) as trn_exist');
			$checks->set_select('((SELECT SUM(ae.credit) FROM accounting_entries ae WHERE ae.trn_id=ak.trn_id) - (SELECT SUM(ae.debit) FROM accounting_entries ae WHERE ae.trn_id=ak.trn_id)) as entries_cleared');
			
			$this->template_data->set('checks', $checks->populate());

			$this->template_data->set('pagination', bootstrap_pagination(array(
				'base_url' => base_url( $this->config->item('index_page') . "/accounting_write_checks/index/{$bank_id}"),
				'total_rows' => $checks->count_all_results(),
				'per_page' => $checks->get_limit(),
				'ajax'=>true,
			)));

		}

		$this->load->view('accounting/write_checks/write_checks_written', $this->template_data->get_data());
	}

	public function write_check($bank_id=0, $output='') {

		$this->_isAuth('accounting', 'write_checks', 'add');

		if( ($bank_id == 0) || ($bank_id == '') ) {
			redirect("accounting_write_checks");
		}

		$current_bank = new $this->Accounting_charts_model;
		$current_bank->setId($bank_id,true);
		$current_bank_data = $current_bank->get();
		$this->template_data->set( 'current_bank', $current_bank_data );

		if( $this->input->post() ) {

			$this->form_validation->set_rules('name_id', 'Name', 'trim|required');
			$this->form_validation->set_rules('check_number', 'Check Number', 'trim|required|is_unique[accounting_checks.check_number]');
			$this->form_validation->set_rules('check_date', 'Check Date', 'trim|required');
			$this->form_validation->set_rules('amount', 'Amount', 'trim|required');
			if($this->form_validation->run()) {
				
				$check_date = date( "Y-m-d", strtotime( $this->input->post('check_date') ) );
				$amount = str_replace(',', '', $this->input->post('amount') );

					$check = new $this->Accounting_checks_model;
					$check->setChartId($bank_id);
					$check->setCheckNumber($this->input->post('check_number'));
					$check->setCheckDate($check_date);
					$check->setNameId($this->input->post('name_id'));
					$check->setAmount($amount);
					$check->setMemo( $this->input->post('memo') );
					$check->setCancelled(0);

					if( $check->insert() ) {
						redirect('accounting_write_checks/check_items/' . $check->getId() );
					}

			}
		}

		$lastcheck = new $this->Accounting_checks_model;
		$lastcheck->setChartId($bank_id, true);
		$lastcheck->set_order('check_date', 'DESC');
		$this->template_data->set( 'lastcheck', $lastcheck->get() );

		$checks = new $this->Accounting_checkbooks_checks_model('c');
		$checks->set_select('c.*');
		$checks->set_join('accounting_checkbooks ac', 'ac.id=c.checkbook_id');
		$checks->set_where('((SELECT ac.id FROM accounting_checks ac WHERE ac.check_number=c.check_number) IS NULL)');
		$checks->set_limit(0);
		$checks->set_where("ac.chart_id={$bank_id}");
		$this->template_data->set('checks', $checks->populate());

		$this->template_data->set( 'output', $output );
		$this->load->view('accounting/write_checks/write_checks', $this->template_data->get_data());
		
	}

	public function edit_check($check_id, $output='') {

		$this->_isAuth('accounting', 'write_checks', 'edit');

		$check = new $this->Accounting_checks_model('ak');
		$check->setId($check_id,true);
		$check_data = $check->get();

		if( $this->input->post() ) {

			$this->form_validation->set_rules('name_id', 'Name', 'trim|required');
			$this->form_validation->set_rules('check_number', 'Check Number', 'trim|required');
			$this->form_validation->set_rules('check_date', 'Check Date', 'trim|required');
			$this->form_validation->set_rules('amount', 'Amount', 'trim|required');
			if($this->form_validation->run()) {
				
					$check_date = date( "Y-m-d", strtotime( $this->input->post('check_date') ) );
					$amount = str_replace(',', '', $this->input->post('amount') );

					$check->setCheckNumber($this->input->post('check_number'));
					$check->setCheckDate($check_date);
					$check->setNameId( $this->input->post('name_id') );
					$check->setAmount( $amount );
					$check->setMemo( $this->input->post('memo') );
					$check->setCancelled( ($this->input->post('cancelled')) ? 1 : 0 );
					$check->set_exclude(array('chart_id','trn_id'));
					$check->update();

					if( $check_data->trn_id ) {

						$trn = new $this->Accounting_transactions_model;
						$trn->setId($check_data->trn_id,true);
						$trn->setDate( $check_date );
						$trn->setMemo($this->input->post('memo'));
						$trn->setNumber($this->input->post('check_number'));
						$trn->setNameId( $this->input->post('name_id') );
						$trn->setAmount( $amount );
						$trn->set_exclude( array('dept', 'sect', 'type', 'lines', 'id') );
						$trn->update();

						$credit = new $this->Accounting_entries_model;
						$credit->setTrnId( $check_data->trn_id, true );
						$credit->setChartId( $check_data->chart_id, true );
						$credit->setNameId( $this->input->post('name_id'), false, true );
						$credit->setMemo( $this->input->post('memo'), false, true );
						$credit->update();

					}



			}
			$this->postNext();
		}

		$check->set_join('accounting_charts ac', 'ac.id=ak.chart_id');
		$check->set_join('coop_names cn', 'cn.id=ak.name_id');

		$check->set_select('ak.*');
		$check->set_select('ac.title as chart_title');
		$check->set_select('cn.full_name as full_name');

		$check->set_select('(SELECT COUNT(*) FROM accounting_transactions at WHERE at.id=ak.trn_id) as trn_exist');

		$check->set_select('(SELECT SUM(ae.debit) FROM accounting_entries ae WHERE ae.trn_id=ak.trn_id) as entries_debit');
		$check->set_select('(SELECT SUM(ae.credit) FROM accounting_entries ae WHERE ae.trn_id=ak.trn_id) as entries_credit');

		$check->set_select('((SELECT SUM(ae.credit) FROM accounting_entries ae WHERE ae.trn_id=ak.trn_id) - (SELECT SUM(ae.debit) FROM accounting_entries ae WHERE ae.trn_id=ak.trn_id)) as entries_cleared');

		$check->set_select('(ak.amount - (SELECT(entries_debit))) as debit_cleared');
		$check->set_select('(ak.amount - (SELECT(entries_credit))) as credit_cleared');

		$check->set_select('(SELECT COUNT(*) FROM accounting_transactions at WHERE at.id=ak.trn_id) as trn_exist');

		$check->set_select('(SELECT ll.id FROM accounting_entries ae JOIN lending_loans ll ON ll.entry_id=ae.id WHERE ae.trn_id=ak.trn_id LIMIT 1) as loan_exists');

		$check->set_select('(SELECT sw.member_id FROM accounting_entries ae JOIN shares_withdrawal sw ON sw.entry_id=ae.id WHERE ae.trn_id=ak.trn_id LIMIT 1) as withdrawal_exists');

		$check_data = $check->get(); 
		$this->template_data->set('check_data', $check_data);

		$current_bank = new $this->Accounting_charts_model;
		$current_bank->setId($check_data->chart_id,true);
		$current_bank_data = $current_bank->get();
		$this->template_data->set( 'current_bank', $current_bank_data );

		$this->template_data->set('output', $output);
		$this->load->view('accounting/write_checks/edit_check', $this->template_data->get_data());
	}

	public function delete_check($check_id) {

		$this->_isAuth('accounting', 'write_checks', 'delete');

		$check = new $this->Accounting_checks_model;
		$check->setId($check_id,true);
		$check->set_select('accounting_checks.*');
		
		$check->set_select('(SELECT ll.id FROM accounting_entries ae JOIN lending_loans ll ON ll.entry_id=ae.id WHERE ae.trn_id=accounting_checks.trn_id LIMIT 1) as loan_exists');

		$check->set_select('(SELECT sw.member_id FROM accounting_entries ae JOIN shares_withdrawal sw ON sw.entry_id=ae.id WHERE ae.trn_id=accounting_checks.trn_id LIMIT 1) as withdrawal_exists');

		$check_data = $check->get();
		
		if( $check_data->loan_exists ) {

			redirect( "services_lending/schedule/" . $check_data->loan_exists );

		} elseif( $check_data->withdrawal_exists ) {

			redirect( "services_shares/withdrawals/" . $check_data->withdrawal_exists );
		
		} else {

			$entries = new $this->Accounting_entries_model;
			$entries->setTrnId($check_data->trn_id, true);
			$entries->delete();

			$check->delete();

		}

		redirect("accounting_write_checks/edit_check/{$check_id}");
	}

	public function delete_entries($check_id) {

		$this->_isAuth('accounting', 'write_checks', 'edit');

		$check = new $this->Accounting_checks_model;
		$check->setId($check_id,true);
		$check->set_select('accounting_checks.*');
		$check->set_select('(SELECT ll.id FROM accounting_entries ae JOIN lending_loans ll ON ll.entry_id=ae.id WHERE ae.trn_id=accounting_checks.trn_id LIMIT 1) as loan_exists');
		$check_data = $check->get();
		
		if( $check_data->loan_exists ) {

			redirect( "services_lending/schedule/" . $check_data->loan_exists );
		
		} else {

			$entries = new $this->Accounting_entries_model;
			$entries->setTrnId($check_data->trn_id, true);
			$entries->delete();
		}

		redirect("accounting_write_checks/edit_check/{$check_id}");
	}

	public function check_items($check_id) { 

		$this->_isAuth('accounting', 'write_checks', 'edit');

		$check = new $this->Accounting_checks_model('ak');
		$check->setId($check_id,true);
		$check->set_join('accounting_charts ac', 'ac.id=ak.chart_id');
		$check->set_join('coop_names cn', 'cn.id=ak.name_id');

		$check->set_select('ak.*');
		$check->set_select('ac.title as chart_title');
		$check->set_select('cn.full_name as full_name');
		$check->set_select("( (SELECT ak.amount) - (IF( (SELECT SUM(ari.amount) FROM accounting_checks_items ari WHERE ari.check_id=ak.id), (SELECT SUM(ari.amount) FROM accounting_checks_items ari WHERE ari.check_id=ak.id), 0)) ) as balance");

		$check->set_select('(SELECT COUNT(*) FROM accounting_transactions at WHERE at.id=ak.trn_id) as trn_exist');

		//$check->set_select('(SELECT SUM(ae.debit) FROM accounting_entries ae WHERE ae.trn_id=ak.trn_id) as entries_debit');
		//$check->set_select('(SELECT SUM(ae.credit) FROM accounting_entries ae WHERE ae.trn_id=ak.trn_id) as entries_credit');

		$check->set_select('((SELECT SUM(ae.credit) FROM accounting_entries ae WHERE ae.trn_id=ak.trn_id) - (SELECT SUM(ae.debit) FROM accounting_entries ae WHERE ae.trn_id=ak.trn_id)) as entries_cleared');

		$check_data = $check->get(); 
		$this->template_data->set('check_data', $check_data);

		$items = new $this->Accounting_checks_items_model('aci');
		$items->setCheckId($check_id,true);
		$items->set_join('accounting_charts ac', 'ac.id=aci.chart_id');
		$items->set_join('coop_names cn', 'cn.id=aci.name_id');
		$items->set_join('coop_lists cl', 'cl.id=aci.class_id');

		$items->set_select('aci.*');
		$items->set_select('ac.title as chart_title');
		$items->set_select('cn.full_name as full_name');
		$items->set_select('cl.value as class_name');
		$items->set_limit(0);
		$this->template_data->set('check_items', $items->populate());

		$this->load->view('accounting/write_checks/write_check_items', $this->template_data->get_data());
	}

	public function add_check_item($check_id, $output="") {

		$this->_isAuth('accounting', 'write_checks', 'edit');

		$check = new $this->Accounting_checks_model('ak');
		$check->setId($check_id,true);
		$check->set_join('accounting_charts ac', 'ac.id=ak.chart_id');
		$check->set_join('coop_names cn', 'cn.id=ak.name_id');
		$check->set_select('ak.*');
		$check->set_select('ac.title as chart_title');
		$check->set_select('cn.full_name as full_name');
		$check->set_select("( (SELECT ak.amount) - (IF( (SELECT SUM(ari.amount) FROM accounting_checks_items ari WHERE ari.check_id=ak.id), (SELECT SUM(ari.amount) FROM accounting_checks_items ari WHERE ari.check_id=ak.id), 0)) ) as balance");
		$check_data = $check->get();
		$this->template_data->set('check_data', $check_data);

		if( round($check_data->balance) == 0 ) {
			redirect("accounting_write_checks/check_items/{$id}");
		}

		if( $this->input->post()) {
			$this->form_validation->set_rules('item_type', 'Item Type', 'trim|required');
			$this->form_validation->set_rules('chart_id', 'Account Title', 'trim|required');
			$this->form_validation->set_rules('amount', 'Amount', 'trim|required');
			if($this->form_validation->run()) {

				$amount = str_replace(',', '', $this->input->post('amount') );
				$amount = (round($amount) > round($check_data->balance)) ? $check_data->balance : $amount;

				$item = new $this->Accounting_checks_items_model;
				$item->setCheckId($check_id);
				$item->setItemType( $this->input->post('item_type') );
				$item->setChartId( $this->input->post('chart_id') );
				$item->setNameId( $this->input->post('name_id') );
				$item->setClassId( $this->input->post('class_id') );
				$item->setAmount( $amount );
				$item->setMemo( $this->input->post('memo') );
				
				if( $item->insert() ) {

					if( $check_data->trn_id ) {

						// credit
						$credit = new $this->Accounting_entries_model;
						$credit->setTrnId( $check_data->trn_id );
						$credit->setChartId( $check_data->chart_id );
						$credit->setCredit( $amount );
						$credit->setClass( $this->input->post('class_id') );
						$credit->setNameId( $check_data->name_id );
						$credit->setItemNumber(0);
						$credit->setItemId( $item->getId() );
						$credit->setItemType( $this->input->post('item_type') );
						$credit->insert();

						// debit
						$debit = new $this->Accounting_entries_model;
						$debit->setTrnId( $check_data->trn_id );
						$debit->setChartId( $this->input->post('chart_id') );
						$debit->setDebit( $amount );
						$debit->setClass( $this->input->post('class_id') );
						$debit->setNameId( $this->input->post('name_id') );
						$debit->setItemNumber(1);
						$debit->setItemId( $item->getId() );
						$debit->setItemType( $this->input->post('item_type') );
						$debit->insert();

					}

				}

			}
			redirect("accounting_write_checks/check_items/{$check_id}");
		}

		$main_charts = new $this->Accounting_charts_model;
		$main_charts->set_order('type', 'ASC');
		$main_charts->set_order('number', 'ASC');
		$main_charts->set_select('id,title,type');
		$main_charts->set_limit(0);
		$charts_data = $main_charts->recursive('parent', 0);
		$this->template_data->set('account_titles', $charts_data);

		$classes = new $this->Coop_lists_model;
		$classes->setDept('ACCOUNTING',true);
		$classes->setType('CLASS',true);
		$classes->setActive(1,true);
		$classes->set_limit(0);
		$classes_data = $classes->recursive('parent', 0);
		$this->template_data->set('class_list', $classes_data);

		$this->template_data->set('output', $output);
		$this->load->view('accounting/write_checks/add_check_item', $this->template_data->get_data());
	}

	public function edit_check_item($item_id, $output="") {

		$this->_isAuth('accounting', 'write_checks', 'edit');

		$item = new $this->Accounting_checks_items_model('aci');
		$item->setId($item_id,true);
		$item->set_select('aci.*');
		$item->set_select("(SELECT COUNT(*) FROM lending_loans ll WHERE ll.entry_id=aci.entry_id) as loan_id");
		$item->set_select("(SELECT COUNT(*) FROM shares_withdrawal sw WHERE sw.entry_id=aci.entry_id) as withdrawal_id");
		
		$item_data = $item->get();
		
		$locked = ($item_data->loan_id OR $item_data->withdrawal_id) ? true : false;
		$this->template_data->set('locked', $locked);

		$check = new $this->Accounting_checks_model('ak');
		$check->setId($item_data->check_id,true);
		$check->set_join('accounting_charts ac', 'ac.id=ak.chart_id');
		$check->set_join('coop_names cn', 'cn.id=ak.name_id');
		$check->set_select('ak.*');
		$check->set_select('ac.title as chart_title');
		$check->set_select('cn.full_name as full_name');
		$check->set_select("( (SELECT ak.amount) - (IF( (SELECT SUM(ari.amount) FROM accounting_checks_items ari WHERE ari.check_id=ak.id), (SELECT SUM(ari.amount) FROM accounting_checks_items ari WHERE ari.check_id=ak.id), 0)) ) as balance");
		$check_data = $check->get();
		$this->template_data->set('check_data', $check_data);

		if( $this->input->post()) { 

			if( $locked===false ) {
				$this->form_validation->set_rules('item_type', 'Item Type', 'trim|required');
				$this->form_validation->set_rules('chart_id', 'Account Title', 'trim|required');
				$this->form_validation->set_rules('amount', 'Amount', 'trim|required');
			}
				$this->form_validation->set_rules('memo', 'Memo', 'trim');
			
			if($this->form_validation->run()) {

				if( $locked===false ) {
					$amount = str_replace(',', '', $this->input->post('amount') );
					$amount = (round($amount) > round($check_data->balance + $item_data->amount)) ? round($check_data->balance + $item_data->amount) : $amount;

					$item->setItemType( $this->input->post('item_type'), false, true );
					$item->setChartId( $this->input->post('chart_id'), false, true );
					$item->setNameId( $this->input->post('name_id'), false, true );
					$item->setClassId( $this->input->post('class_id'), false, true );
					$item->setAmount( $amount, false, true );
				}

				$item->setMemo( $this->input->post('memo'), false, true );
				$item->set_exclude(array('id', 'check_id'));
				$item->update();

				if( $check_data->trn_id ) {

					if( $locked===false ) {
						// credit
						$credit = new $this->Accounting_entries_model;
						$credit->setTrnId( $check_data->trn_id, true );
						$credit->setChartId( $check_data->chart_id, true );
						if( $amount < 0) {
							$credit->setDebit( abs($amount), false, true );
							$credit->setCredit( 0, false, true );
						} else {
							$credit->setDebit( 0, false, true );
							$credit->setCredit( $amount, false, true );
						}
						$credit->setClass( $this->input->post('class_id'), false, true );
						$credit->setItemId( $item_id, true );
						$credit->setItemType( $this->input->post('item_type'), false, true );
						$credit->update();
					
					}
						// debit
						$debit = new $this->Accounting_entries_model;
						$debit->setTrnId( $check_data->trn_id, true );

					if( $locked===false ) {
						$debit->setChartId( $this->input->post('chart_id'), false, true );
						if( $amount < 0) {
							$debit->setDebit( 0, false, true );
							$debit->setCredit( abs($amount), false, true );
						} else {
							$debit->setDebit( $amount, false, true );
							$debit->setCredit( 0, false, true );
						}
						$debit->setClass( $this->input->post('class_id'), false, true );
						$debit->setNameId( $this->input->post('name_id'), false, true );
						$debit->setItemType( $this->input->post('item_type'), false, true );
					}
						if( $this->input->post('memo') != '') {
							$debit->setMemo( $this->input->post('memo'), false, true );
						}
						$debit->setItemNumber(1, true);
						$debit->setItemId( $item_id, true );
						
						$debit->update();

				}

			}
			redirect("accounting_write_checks/check_items/{$item_data->check_id}");
		}

		$item->set_join('coop_names cn', 'cn.id=aci.name_id');
		$item->set_select('cn.full_name as full_name');
		$this->template_data->set('item_data', $item->get());

		$main_charts = new $this->Accounting_charts_model;
		$main_charts->set_order('type', 'ASC');
		$main_charts->set_order('number', 'ASC');
		$main_charts->set_select('id,title,type');
		$main_charts->set_limit(0);
		$charts_data = $main_charts->recursive('parent', 0);
		$this->template_data->set('account_titles', $charts_data);

		$classes = new $this->Coop_lists_model;
		$classes->setDept('ACCOUNTING',true);
		$classes->setType('CLASS',true);
		$classes->setActive(1,true);
		$classes->set_limit(0);
		$classes_data = $classes->recursive('parent', 0);
		$this->template_data->set('class_list', $classes_data);

		$this->template_data->set('output', $output);
		$this->load->view('accounting/write_checks/edit_check_item', $this->template_data->get_data());
	}

	public function delete_check_item($id) {		

		$this->_isAuth('accounting', 'write_checks', 'edit');

		$check_item = new $this->Accounting_checks_items_model;
		$check_item->setId($id,true);
		$check_item->set_join('accounting_checks ak', 'ak.id=accounting_checks_items.check_id');
		$check_item_data = $check_item->get();
		
		$entries = new $this->Accounting_entries_model;
		$entries->setTrnId($check_item_data->trn_id,true);
		$entries->setItemId($id,true);

		$entries->delete();	
		$check_item->delete();	

		redirect("accounting_write_checks/check_items/{$check_item_data->check_id}");

	}


	public function publish_check($id) {

		$this->_isAuth('accounting', 'write_checks', 'edit');

		$check = new $this->Accounting_checks_model('ak');
		$check->setId($id,true);
		$check->set_join('accounting_charts ac', 'ac.id=ak.chart_id');
		$check->set_join('coop_names cn', 'cn.id=ak.name_id');
		$check->set_select('ak.*');
		$check->set_select('ac.title as chart_title');
		$check->set_select('cn.full_name as full_name');
		$check->set_select("( (SELECT ak.amount) - (IF( (SELECT SUM(ari.amount) FROM accounting_checks_items ari WHERE ari.check_id=ak.id), (SELECT SUM(ari.amount) FROM accounting_checks_items ari WHERE ari.check_id=ak.id), 0)) ) as balance");
		$check->set_select('(SELECT COUNT(*) FROM accounting_transactions at WHERE at.id=ak.trn_id) as trn_exist');
		$check->set_select('((SELECT SUM(ae.credit) FROM accounting_entries ae WHERE ae.trn_id=ak.trn_id) - (SELECT SUM(ae.debit) FROM accounting_entries ae WHERE ae.trn_id=ak.trn_id)) as entries_cleared');
		$check_data = $check->get();

		$check_items = new $this->Accounting_checks_items_model;
		$check_items->setCheckId($id,true);
		$check_items->set_limit(0);
		
		$trn_id = $check_data->trn_id;
		if( !$check_data->trn_exist ) {

			$trn = new $this->Accounting_transactions_model;
			$trn->setDept('ACCOUNTING');
			$trn->setSect('DISBURSING');
			$trn->setType('CHECK');
			$trn->setDate( $check_data->check_date );
			$trn->setNameId( $check_data->name_id );
			$trn->setNumber( $check_data->check_number );
			$trn->setAmount( $check_data->amount );
			$trn->setMemo( $check_data->memo );
			
			if( $trn->insert() ) {
				$trn_id = $trn->getId();
				$check->setTrnId( $trn->getId(), false, true);
				$check->update();
			}
		}

		foreach($check_items->populate() as $item) {

			// credit
			$credit = new $this->Accounting_entries_model;
			$credit->setTrnId( $trn_id );
			$credit->setChartId( $check_data->chart_id );
			if( $item->amount < 0) {
				$credit->setDebit( abs($item->amount) );
			} else {
				$credit->setCredit( $item->amount );
			}
			$credit->setClass( $item->class_id );
			$credit->setNameId( $check_data->name_id );
			$credit->setItemNumber(0);
			$credit->setItemId($item->id);
			$credit->setItemType( $item->item_type );
			$credit->setMemo( $check_data->memo );
			$credit->insert();

			// debit
			$debit = new $this->Accounting_entries_model;
			$debit->setTrnId( $trn_id );
			$debit->setChartId( $item->chart_id );
			if( $item->amount < 0) {
				$debit->setCredit( abs($item->amount) );
			} else {
				$debit->setDebit( $item->amount );
			}
			$debit->setClass( $item->class_id );
			$debit->setNameId( $item->name_id );
			$debit->setItemNumber(1);
			$debit->setItemId( $item->id );
			$debit->setItemType( $item->item_type );
			$debit->setMemo( ($item->memo) ? $item->memo : $check_data->memo );

			if( $debit->insert() ) {
				$check_item = new $this->Accounting_checks_items_model;
				$check_item->setId($item->id,true);
				$check_item->setEntryId($debit->getId(), false, true);
				$check_item->update();
			}

		}

		redirect("accounting_write_checks/check_items/{$id}");

	}

	public function ajax($action='') {
		$results = array();
		switch($action) {
			case 'name':
				$names = new $this->Coop_names_model;
				if( $this->input->get('term') ) {
					$names->set_where('full_name LIKE "%' . $this->input->get('term') . '%"');
				}
				$data = array();
				foreach($names->populate() as $name) {
					$data[] = array(
						'label' => $name->full_name,
						'id' => $name->id,
						'desc' => $name->address . " - " . $name->contact_number,
						);
				}
				$results = $data;
			break;
		}
		$this->output
        ->set_content_type('application/json')
        ->set_output(json_encode( $results ));
	}

}
