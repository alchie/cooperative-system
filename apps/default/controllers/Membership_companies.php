<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Membership_companies extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->template_data->set('page_title', 'COOP - Companies');
		$this->template_data->set('current_page', 'Companies');
		$this->template_data->set('current_uri', 'membership_companies');
		$this->template_data->set('current_sub_uri', 'membership_companies');
		$this->template_data->set('enable_search', false);

		$this->_isAuth('membership', 'companies', 'view');
		
		$this->load->model('Companies_model');
		$this->load->model('Companies_members_model');
		$this->load->model('Members_model');
		$this->load->model('Coop_names_model');
		$this->load->model('Coop_settings_model');
		$this->load->model('Accounting_transactions_model');
		$this->load->model('Accounting_entries_model');
		$this->load->model('Accounting_receipts_model');
		$this->load->model('Lending_payments_model');
		$this->load->model('Lending_invoices_model');
		$this->load->model('Lending_payment_applied_model');
	}

	private function _settings() {
		$class_name = new $this->Coop_settings_model;
		$class_name->setDepartment('SERVICES',true);
		$class_name->setSection('LENDING',true);
		$class_name->setKey('class_name',true);
		$cls_setting = $class_name->get();

		$receivable = new $this->Coop_settings_model;
		$receivable->setDepartment('SERVICES',true);
		$receivable->setSection('LENDING',true);
		$receivable->setKey('receivable',true);
		$at_setting = $receivable->get();

		$interest_income = new $this->Coop_settings_model;
		$interest_income->setDepartment('SERVICES',true);
		$interest_income->setSection('LENDING',true);
		$interest_income->setKey('interest_income',true);
		$ii_setting = $interest_income->get();

		$undeposited = new $this->Coop_settings_model;
		$undeposited->setDepartment('ACCOUNTING',true);
		$undeposited->setSection('CONFIG',true);
		$undeposited->setKey('undeposited',true);
		$udf_setting = $undeposited->get();

		$releaser = new $this->Coop_settings_model;
		$releaser->setDepartment('SERVICES',true);
		$releaser->setSection('LENDING',true);
		$releaser->setKey('releaser',true);
		$r_setting = $releaser->get();

		$settings = (object) array(
			'class_name' => ($cls_setting) ? $cls_setting->value : false,
			'receivable' => ($at_setting) ? $at_setting->value : false,
			'interest_income' => ($ii_setting) ? $ii_setting->value : false,
			'undeposited' => ($udf_setting) ? $udf_setting->value : false,
			'releaser' => ($r_setting) ? $r_setting->value : false,
		);

		foreach($settings as $key=>$value) {
			if(!$value) {
				if( $key=='undeposited') {
					redirect(site_url("accounting_settings/configuration") . "?error_code=102&next=" . uri_string() );
				} else {
					redirect(site_url("services_lending/configuration") . "?error_code=101&next=" . uri_string() );
				}
			}
		}

		return $settings;
	}

	private function _searchRedirect() {
		if( $this->input->get('q') ) {
			redirect(site_url("membership_companies?q=" . $this->input->get('q') ));
		}
	}
	
	public function index($start=0) {
		
		if(( $this->input->post('output') == 'body_wrapper' )||( $this->input->post('output') == 'inner_page' )) {

		$companies = new $this->Companies_model;
		if( $this->input->get('q') ) {
			$companies->set_where('name LIKE "%' . $this->input->get('q') . '%"');
		}
		$companies->set_select('companies.*');
		$companies->set_select('(SELECT count(*) FROM companies_members m WHERE m.company_id=companies.id) as members');
		$companies->set_start($start);
		$this->template_data->set('companies', $companies->populate());
		$this->template_data->set('companies_count', $companies->count_all_results());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'base_url' => base_url( $this->config->item('index_page') . "/membership_companies/index"),
			'total_rows' => $companies->count_all_results(),
			'per_page' => $companies->get_limit(),
			'ajax'=>true,
		)));

		}
		
		$this->template_data->set('enable_search', true);
		$this->load->view('membership/companies/companies', $this->template_data->get_data());

	}

	public function add($name_id) {

		$this->_isAuth('membership', 'companies', 'add');

		$name = new $this->Coop_names_model;
		$name->setId($name_id,true);
		$name_data = $name->get();

		if(!$name_data) {
			redirect("membership_companies/search_name");
		}

		$this->template_data->set( 'name', $name_data );

		if( $this->input->post() ) {
			$this->form_validation->set_rules('name', 'Company Name', 'trim|required');
			if($this->form_validation->run()) {
				$companies = new $this->Companies_model;
				$companies->setId($name_id);
				$companies->setName($this->input->post('name'));
				if( $companies->insert() ) {
					redirect("membership_companies/employees/" . $name_id);
				}
			}
		}
		$this->load->view('membership/companies/companies_add', $this->template_data->get_data());
	}

	public function edit($id) {

		$this->_isAuth('membership', 'companies', 'edit');

		$this->_searchRedirect();
		
		$companies = new $this->Companies_model;
		$companies->setId($id,true);
		if( $this->input->post() ) {
			$this->form_validation->set_rules('name', 'Company Name', 'trim|required');
			if($this->form_validation->run()) {
				$companies->setName($this->input->post('name'),false,true);
				$companies->setAddress($this->input->post('address'),false,true);
				$companies->setContactNumbers($this->input->post('contact_numbers'),false,true);
				$companies->setEmail($this->input->post('email'),false,true);
				$companies->update();
			}
		}
		redirect("membership_companies/info/" . $id);
	}

	public function delete($id) {

		$this->_isAuth('membership', 'companies', 'delete');

		$companies = new $this->Companies_model;
		$companies->setId($id,true);
		$companies->delete();
		redirect("membership_companies");
	}
	
	public function info($id) {

		$this->_searchRedirect();
		
		$this->template_data->set('current_sub_uri', 'membership_companies_info');

		$companies = new $this->Companies_model;
		$companies->setId($id,true);
		$this->template_data->set('company', $companies->get());

		$cMem = new $this->Companies_members_model('c');
		$cMem->setCompanyId($id,true);
		$cMem->set_join('members m', 'c.member_id=m.id');
		$this->template_data->set('members', $cMem->populate());

		$this->load->view('membership/companies/companies_info', $this->template_data->get_data());
	}

	public function employees($id, $start=0) {

		$this->_searchRedirect();
		
		$this->template_data->set('current_sub_uri', 'membership_companies_employees');

		$companies = new $this->Companies_model;
		$companies->setId($id,true);
		$this->template_data->set('company', $companies->get());

		if(( $this->input->post('output') == 'body_wrapper' )||( $this->input->post('output') == 'inner_page' )) {

		$cMem = new $this->Companies_members_model;
		$cMem->setCompanyId($id,true);
		$cMem->set_join('members m', 'companies_members.member_id=m.id');
		$cMem->set_start( $start );

		$this->template_data->set('members', $cMem->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'uri_segment' => 4,
			'base_url' => base_url( $this->config->item('index_page') . '/membership_companies/employees/' . $id),
			'total_rows' => $cMem->count_all_results(),
			'per_page' => $cMem->get_limit(),
			'ajax' => true,
		)));

		}

		$this->load->view('membership/companies/companies_employees', $this->template_data->get_data());
	}

	public function add_employee($id) {

		$this->_isAuth('membership', 'companies', 'edit');

		$this->template_data->set('current_sub_uri', 'membership_companies_employees');
		
		if( $this->input->post('member_id') ) {
			foreach( $this->input->post('member_id') as $member_id ) {
				$cMem = new $this->Companies_members_model;
				$cMem->setCompanyId($id);
				$cMem->setMemberId($member_id,true);
				if( $cMem->nonEmpty() == false) {
					$cMem->insert();
				}
			}
			redirect("membership_companies/employees/" . $id);
		}

		$companies = new $this->Companies_model;
		$companies->setId($id,true);
		$this->template_data->set('company', $companies->get());

		$members = new $this->Members_model;
		$members->set_select('members.*');
		$members->set_select('(SELECT company_id FROM companies_members WHERE member_id=members.id) as company_id');
		if( $this->input->get('q') ) {
			$members->set_where('lastname LIKE "%' . $this->input->get('q') . '%"');
			$members->set_where_or('firstname LIKE "%' . $this->input->get('q') . '%"');
			$members->set_where_or('middlename LIKE "%' . $this->input->get('q') . '%"');
		}
		$members->set_having('company_id IS NULL');
		$this->template_data->set('members', $members->populate());

		$this->load->view('membership/companies/companies_add_employee', $this->template_data->get_data());
	}

	public function delete_member($cid, $mid) {

		$this->_isAuth('membership', 'companies', 'edit');

		$cMem = new $this->Companies_members_model;
		$cMem->setCompanyId($cid,true);
		$cMem->setMemberId($mid,true);
		$cMem->delete();
		redirect("membership_companies/employees/" . $cid);
	}

	public function search_name() {

		$names = new $this->Coop_names_model('c');
		$names->set_select('c.*');
		$names->set_join('members m', 'c.id=m.id');
		$names->set_where('c.full_name LIKE "%' . $this->input->get('q') . '%"');
		$names->set_where('m.id IS NULL');
		$this->template_data->set('names', $names->populate());

		$this->load->view('membership/companies/companies_search_name', $this->template_data->get_data());
	}

	public function open_invoices($id, $start=0) {
		$this->_searchRedirect();
		
		$this->template_data->set('current_sub_uri', 'membership_companies_open_invoices');

		$companies = new $this->Companies_model;
		$companies->setId($id,true);
		$companies->set_select("companies.*");
		$companies->set_select("(SELECT COUNT(*) FROM lending_invoices li JOIN lending_loans ll ON li.loan_id=ll.id JOIN companies_members cm ON cm.member_id=ll.member_id WHERE cm.company_id=companies.id) as total_invoices");
		$this->template_data->set('company', $companies->get());

		if(( $this->input->post('output') == 'body_wrapper' )||( $this->input->post('output') == 'inner_page' )) {

		$invoices = new $this->Lending_invoices_model('li');
		$invoices->set_join('lending_loans ll', 'll.id=li.loan_id');
		$invoices->set_join('companies_members cm', 'll.member_id=cm.member_id');
		$invoices->set_join('members m', 'm.id=ll.member_id');

		$invoices->set_select('li.*');
		$invoices->set_select('m.lastname, m.firstname, m.middlename');
		$invoices->set_select('(SELECT sum(lpa.amount) FROM `lending_payment_applied` lpa WHERE lpa.invoice_id=li.id) as total_applied');
		$invoices->set_select('(li.principal_due + li.interest_due) as gross_amount');
		$invoices->set_select('(SELECT ((IF(gross_amount IS NULL,0,gross_amount)) - (IF(total_applied IS NULL,0,total_applied)) )) as balance');

		$invoices->set_where('cm.company_id', $id);

		$invoices->set_where("ROUND( (SELECT ((IF((li.principal_due + li.interest_due) IS NULL,0,(li.principal_due + li.interest_due))) - (IF((SELECT sum(lpa.amount) FROM `lending_payment_applied` lpa WHERE lpa.invoice_id=li.id) IS NULL,0,(SELECT sum(lpa.amount) FROM `lending_payment_applied` lpa WHERE lpa.invoice_id=li.id))) )) ) > 0");
		$invoices->set_order('li.due_date', 'ASC');
		$invoices->set_limit(10);
		$invoices->set_start( $start );

		$open_invoices = $invoices->populate();
		$this->template_data->set('open_invoices', $open_invoices);

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'uri_segment' => 4,
			'base_url' => base_url( $this->config->item('index_page') . '/membership_companies/open_invoices/' . $id),
			'total_rows' => $invoices->count_all_results(),
			'per_page' => $invoices->get_limit(),
			'ajax' => true
		)));

		}

		$this->load->view('membership/companies/companies_open_invoices', $this->template_data->get_data());
	}

	public function payments($id, $start=0) {
		$this->_searchRedirect();
		
		$this->template_data->set('current_sub_uri', 'membership_companies_payments');

		$companies = new $this->Companies_model;
		$companies->setId($id,true);
		$this->template_data->set('company', $companies->get());

		if(( $this->input->post('output') == 'body_wrapper' )||( $this->input->post('output') == 'inner_page' )) {

		$receipts = new $this->Accounting_receipts_model('ar');
		$receipts->setNameId($id,true);
		$receipts->set_order('receipt_number', 'DESC');
		$receipts->set_order('receipt_date', 'DESC');
		$receipts->set_limit(10);
		$receipts->set_start( $start );
		$receipts->set_select('ar.*');
		$receipts->set_select('(SELECT sum(ari.amount) FROM accounting_receipts_items ari WHERE ari.receipt_id=ar.id) as total_items');

		$receipts->set_select('((SELECT ar.amount) - IF((SELECT sum(ari.amount) FROM accounting_receipts_items ari WHERE ari.receipt_id=ar.id),(SELECT sum(ari.amount) FROM accounting_receipts_items ari WHERE ari.receipt_id=ar.id),0)) as balance');

		$receipts->set_select('(SELECT adi.deposit_id FROM accounting_deposits_items adi JOIN accounting_entries ae ON adi.entry_id=ae.id JOIN accounting_transactions at ON ae.trn_id=at.id WHERE ae.trn_id=ar.trn_id LIMIT 1) as deposit_id');

		$this->template_data->set('receipts', $receipts->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'uri_segment' => 4,
			'base_url' => base_url( $this->config->item('index_page') . '/membership_companies/payments/' . $id),
			'total_rows' => $receipts->count_all_results(),
			'per_page' => $receipts->get_limit(),
			'ajax' => true
		)));

		}

		$this->load->view('membership/companies/companies_payments', $this->template_data->get_data());
	}

	public function ajax($action='') {
		$results = array();
		switch($action) {
			case 'add_company':
				$names = new $this->Coop_names_model;
				if( $this->input->get('term') ) {
					$names->set_where('full_name LIKE "%' . $this->input->get('term') . '%"');
				}
				$names->set_where("(SELECT COUNT(*) FROM members WHERE members.id=coop_names.id) = 0");
				$names->set_where("(SELECT COUNT(*) FROM companies WHERE companies.id=coop_names.id) = 0");
				$data = array();
				foreach($names->populate() as $name) {
					$data[] = array(
						'label' => $name->full_name,
						'id' => $name->id,
						'redirect'=> site_url("membership_companies/add/" . $name->id),
						);
				}
				$results = $data;
			break;
			case 'company_data':
				$companies = new $this->Companies_model;
				if( $this->input->get('term') ) {
					$companies->set_where('name LIKE "%' . $this->input->get('term') . '%"');
				}
				$companies->set_select('companies.*');
				$companies->set_select('(SELECT count(*) FROM companies_members m WHERE m.company_id=companies.id) as members');
				$data = array();
				foreach($companies->populate() as $company) {
					$data[] = array(
						'label' => $company->name,
						'id' => $company->id,
						'redirect'=> site_url("membership_companies/info/{$company->id}"),
						);
				}
				$results = $data;
			break;
			case 'change_member':
				$companies = new $this->Companies_model;
				if( $this->input->get('term') ) {
					$companies->set_where('name LIKE "%' . $this->input->get('term') . '%"');
				}
				$companies->set_order('name', 'ASC');
				$data = array();
				switch( $this->input->get('sub_uri') ) {
					case 'membership_companies_payments':
						$redirect_uri = "membership_companies/payments/";
					break;
					case 'membership_companies_open_invoices':
						$redirect_uri = "membership_companies/open_invoices/";
					break;
					case 'membership_companies_employees':
						$redirect_uri = "membership_companies/employees/";
					break;
					case 'membership_companies_info':
					default:
						$redirect_uri = "membership_companies/info/";
					break;
				}
				foreach($companies->populate() as $company) {
					$data[] = array(
						'label' => $company->name,
						'id' => $company->id,
						'redirect'=> site_url($redirect_uri . $company->id),
						);
				}
				$results = $data;
			break;
		}
		$this->output
        ->set_content_type('application/json')
        ->set_output(json_encode( $results ));
	}

}
