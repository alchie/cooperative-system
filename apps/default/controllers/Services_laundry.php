<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Services_laundry extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->template_data->set('page_title', 'COOP - Laundry');
		$this->template_data->set('current_page', 'Laundry');
		$this->template_data->set('current_uri', 'services_laundry');
	}

	public function index() {
		$this->load->view('services/laundry/laundry', $this->template_data->get_data());
	}

	public function member($mid) {
		
		$members = new $this->Members_model;
		$members->setId($mid,true);
		$this->template_data->set('member', $members->get());

		$this->load->view('services/laundry/laundry_member', $this->template_data->get_data());
	}
	
}
