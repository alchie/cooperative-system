<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

        var $view_template = 'default';
        public function __construct()
        {
                parent::__construct();
                
                if( ! $this->session->loggedIn || ! isset($this->session->loggedIn) ) {
                	$this->session->sess_destroy();
                	redirect( site_url( 'login' ) . "?next=" . urlencode( uri_string()) );
                }

                $this->template_data->set('session_auth', $this->session->session_auth);
                $this->template_data->set('page_title', 'COOP');
                $this->template_data->set('output', '');

                $this->template_data->set('inner_page', false);
                if( $this->input->post('output') == 'inner_page') {
                    $this->template_data->set('inner_page', true);
                }

                $this->template_data->set('body_wrapper', false);
                if( $this->input->post('output') == 'body_wrapper') {
                    $this->template_data->set('body_wrapper', true);
                }

                // default,cerulean,cosmo,cyborg,darkly,flatly,journal,lumen,paper,readable,sandstone,simplex,slate,spacelab,superhero,united,yeti
                $bootstrap_theme = ( isset($this->session->user_settings['theme']) && $this->session->user_settings['theme'] ) ? $this->session->user_settings['theme'] : 'cerulean';
                $this->template_data->set('bootstrap_theme', $bootstrap_theme);

                $user_settings = $this->session->userdata('user_settings');
                if( isset( $user_settings['template'] ) ) {
                    $this->view_template = $user_settings['template'];
                }
                $this->template_data->set('view_template', $this->view_template);

                $this->load->add_package_path(FCPATH . 'apps' . DIRECTORY_SEPARATOR . $this->view_template, false);

                if( $this->view_template == 'adminlte') {
                    //$this->_open_invoices();
                    //$this->_new_payments();
                }
                
                $this->reset_page();
        }

        public function _isAuth($dept, $sect=NULL, $action='view', $uri=false, $return=false) {
        	
            $auth = false;
            if( isset( $this->session->session_auth ) ) {
            	if( isset($this->session->session_auth[$dept] ) ) {
                    if( isset($this->session->session_auth[$dept][$sect]) ) {
                		if( isset($this->session->session_auth[$dept][$sect][$action]) ) {
                			$auth = (bool) $this->session->session_auth[$dept][$sect][$action];
                		}
                    }
            	} 
            }

        	if( !$auth && !$return ) {
                        if( $uri == '') {
                                if ( $this->session->referrer_uri != '' )
                                {
                                    $uri = $this->session->referrer_uri;
                                } else {
                                    $uri = 'welcome';
                                }
                                if( uri_string() == $uri ) {
                                    $uri = 'welcome';
                                }
                        }

        		  redirect( site_url( $uri ) . "?error_code=999" );

        	}

            if( $auth ) {
                $this->session->set_userdata( 'referrer_uri', uri_string() );
            }

            if( $return ) {
                $this->session->set_userdata( 'referrer_uri', uri_string() );
                return $auth;
            }

        }

        public function postNext($query_string=null, $output='') {
            if( $this->input->post() ) {
                    if( $this->input->get('next') ) {
                            $url = site_url($this->input->get('next'));
                            if( $query_string ) {
                                    $url .= "?" . $query_string;
                            }
                            redirect( $url );
                    } else {
                        if($output=='ajax') {
                                redirect( "/" );
                        }
                    }
            }
        }

        protected function _isLoggedIn($uri='welcome') {
            if( isset($this->session->loggedIn) && $this->session->loggedIn ) {
                redirect($uri);
            }
        }

        protected function _isNotLoggedIn($uri='account/login') {
            if( ! $this->session->loggedIn || ! isset($this->session->loggedIn) ) {
                $this->session->sess_destroy();
                redirect($uri);
           }
        }

        private function _open_invoices() {

                $this->load->model('Lending_invoices_model');
                $invoices = new $this->Lending_invoices_model('li');
                //$invoices->cache_off();
                $invoices->set_limit(10);

                $invoices->set_join('lending_loans ll', 'll.id=li.loan_id');
                $invoices->set_join('members m', 'm.id=ll.member_id');
                
                $invoices->set_select('li.*');
                $invoices->set_select('m.id as member_id, m.lastname, m.firstname, m.middlename');
                $invoices->set_select('(li.principal_due + li.interest_due) as gross_amount');

                $invoices->set_select('(SELECT SUM(amount) FROM lending_payment_applied lpa WHERE lpa.invoice_id=li.id) as total_paid');

                $invoices->set_select('(SELECT (gross_amount - (IF((total_paid IS NULL),0,total_paid) )) ) as balance');

                $invoices->set_order('li.due_date', 'DESC');
                
                $invoices->set_where( 'li.due_date <=', date('Y-m-d') );
                $invoices->set_where( 'ROUND((SELECT ((li.principal_due + li.interest_due) - (IF(((SELECT SUM(amount) FROM lending_payment_applied lpa WHERE lpa.invoice_id=li.id) IS NULL),0,(SELECT SUM(amount) FROM lending_payment_applied lpa WHERE lpa.invoice_id=li.id)) )) )) > 0' );

                $this->template_data->set('tasks_open_invoices', $invoices->populate());
                $this->template_data->set('tasks_open_invoices_count', $invoices->count_all_results());
        }

        private function _new_payments() {

                $this->load->model('Accounting_entries_model');
                $this->load->model('Lending_payments_model');
                $this->load->model('Coop_settings_model');

                $receivable = new $this->Coop_settings_model;
                $receivable->setDepartment('SERVICES',true);
                $receivable->setSection('LENDING',true);
                $receivable->setKey('receivable',true);
                $at_setting = $receivable->get();

                $new_payments = new $this->Accounting_entries_model('ae');
                //$new_payments->cache_off();
                $new_payments->setChartId($at_setting->value, true);
                $new_payments->set_join('accounting_transactions at', 'at.id=ae.trn_id');
                $new_payments->set_join('accounting_receipts ar', 'ar.trn_id=ae.trn_id');
                $new_payments->set_select('ae.*');
                $new_payments->set_select('at.*');
                $new_payments->set_select('ar.*');
                $new_payments->set_select('ae.id as ae_id');
                $new_payments->set_select('ae.name_id as ae_name_id');
                $new_payments->set_select('(SELECT full_name FROM `coop_names` WHERE id=ae.name_id) as full_name');
                $new_payments->set_where('((SELECT entry_id FROM  `lending_payments` WHERE entry_id=ae.id) IS NULL)');
                $new_payments->set_where('((SELECT id FROM `members` WHERE id=ae.name_id) IS NOT NULL)');
                $new_payments->set_where_in('ae.item_type', array('LOANPY', 'LOANADJ'));
                $new_payments->set_limit(0);
                $this->template_data->set('tasks_new_payments', $new_payments->populate());

                $payments = new $this->Lending_payments_model('lp');
                //$payments->cache_off();
                $payments->set_order('receipt_number', 'DESC');
                $payments->set_order('payment_date', 'DESC');
                $payments->set_select('lp.*');
                $payments->set_select('(SELECT SUM(amount) FROM lending_payment_applied lpa WHERE lpa.payment_id=lp.id) as total_applied');
                $payments->set_select('(SELECT full_name FROM `coop_names` WHERE id=lp.member_id) as full_name');
                $payments->set_where('(SELECT SUM(lpa.amount) FROM lending_payment_applied lpa WHERE lpa.payment_id=lp.id) IS NULL');
                $payments->set_order('receipt_number', 'ASC');
                $payments->set_order('payment_date', 'ASC');
                $payments->set_limit(0);
                $this->template_data->set('tasks_unapplied_payments', $payments->populate());  
        }

        private function reset_page() {
            if( $this->input->get('reset_page') && ( $this->encrypt->decode( $this->input->get('reset_page') ) == uri_string() ) ) {
                $this->db->cache_delete( $this->uri->segment(1), $this->uri->segment(2, 'index') );
                redirect( uri_string() );
            }
        }

}