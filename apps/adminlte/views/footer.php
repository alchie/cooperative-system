<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php if( ( ! $inner_page ) && ( ! $body_wrapper ) ): ?>

</div> <!-- #bodyWrapper -->

<div class="container hide-print">
	<div class="row">
		<div class="col-md-12">
			<center><small>
				<p>
				<br> 
				</p>
			</small></center>
		</div>
	</div> 
</div>

<!-- #blankModal Modal -->
<div class="modal fade" id="ajaxModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"></h4>
      </div>
      <form method="post" action="" id="ajaxModalForm">
      <div class="modal-body">
        <p class="loader text-center">
        	<img src="<?php echo base_url("assets/images/loader4.gif"); ?>">
        </p>
        <div class="output"></div>
      </div>
       <div class="modal-footer" style="display: none;">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>
    </div>
  </div>
</div>


</section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
    <strong>M:</strong> <?php echo $this->benchmark->memory_usage();?> <strong>T:</strong> <?php echo $this->benchmark->elapsed_time();?> &middot;
      <strong>Developed by:</strong> 
        <a href="http://www.chesteralan.com/" target="footer_credits">Chester Alan Tagudin</a>
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; <?php echo date("Y"); ?> <a href="http://www.trokis.com/" target="footer_credits">Trokis Philippines</a>.</strong> All rights reserved. 
    <strong><a class="btn btn-xs btn-danger" href="<?php echo site_url(uri_string()) . "?reset_page=" . urlencode( $this->encrypt->encode(uri_string()) ); ?>"><i class="fa fa-refresh"></i> Reset Page Cache</a></strong>
  </footer>

</div>
<!-- ./wrapper -->

<script src="<?php echo base_url('assets/js/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/jqueryui/jquery-ui.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/numeral.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap-select/js/bootstrap-select.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tag-it/js/tag-it.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/adminlte/js/app.js'); ?>"></script>
<script src="<?php echo base_url('assets/adminlte/coop.js'); ?>"></script>

</body>
</html>

<?php endif; // inner_page ?>