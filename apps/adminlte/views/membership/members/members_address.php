<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('membership/membership_navbar'); ?>

<?php $this->load->view('membership/members/members_navbar'); ?>

<div class="container">
<div class="row">

	<div class="col-md-6 col-md-offset-3">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
	    		<h3 class="panel-title">Edit Member</h3>
	    	</div>

<form method="post">

	    	<div class="panel-body">


<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

<?php endif; ?>

<div class="row">
    <div class="col-md-6">
	    <div class="form-group">
            <label class="control-label">Unit/Room No. &amp; Floor</label>
            <input type="text" class="form-control" name="unit" value="<?php echo $member->unit; ?>">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">Building Name</label>
            <input type="text" class="form-control" name="building" value="<?php echo $member->building; ?>">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">Lot No. &amp; Block No. &amp; Phase No.</label>
            <input type="text" class="form-control" name="lot_block" value="<?php echo $member->lot_block; ?>">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">Street</label>
            <input type="text" class="form-control" name="street" value="<?php echo $member->street; ?>">
        </div>
    </div>
</div> 
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">Subdivision</label>
            <input type="text" class="form-control" name="subdivision" value="<?php echo $member->subdivision; ?>">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">Barangay</label>
            <input type="text" class="form-control" name="barangay" value="<?php echo $member->barangay; ?>">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">Municipality / City</label>
            <input type="text" class="form-control" name="city" value="<?php echo $member->city; ?>">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">Province</label>
            <input type="text" class="form-control" name="province" value="<?php echo $member->province; ?>">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">Zip Code</label>
            <input type="text" class="form-control" name="zip" value="<?php echo $member->zip; ?>">
        </div>
    </div>
</div>

  <?php if( isset($output) && ($output!='ajax') ) : ?>

</div>

	    	</div>
	    	<div class="panel-footer">
	    		<button type="submit" class="btn btn-success">Submit</button>
	    		<a href="<?php echo site_url("membership_members"); ?>" class="btn btn-warning">Back</a>
	    	</div>


        </form>


	    </div>
    </div>
</div>
</div>

<?php $this->load->view('footer'); ?>

<?php endif; ?>