<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('membership/membership_navbar'); ?>

<?php $this->load->view('membership/members/members_navbar'); ?>

<div class="container">
<div class="row">

	<div class="col-md-6 col-md-offset-3">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
	    		<h3 class="panel-title">Delete Member</h3>
	    	</div>

<form method="post">

	    	<div class="panel-body">

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

<?php endif; ?>
  <input type="hidden" name="hash" value="<?php echo sha1( $member->id ); ?>" />
	    	<div class="form-group">
            <label>Full Name</label>
            <div class="form-control"><?php echo $member->lastname; ?>, <?php echo $member->firstname; ?> <?php echo $member->middlename; ?></div>
          </div>

<div class="form-group">
            <label>Status</label>

            <table class="table table-default table-condensed table-hover">
              <thead>
                <tr class="success">
                  <th>Services</th>
                  <th class="text-right">Count</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Loans</td>
                  <td class="text-right"><a href="<?php echo site_url("services_lending/loans/{$member->id}"); ?>"><?php echo $member->loans; ?></a></td>
                </tr>
                 <tr>
                  <td>Payments</td>
                  <td class="text-right"><a href="<?php echo site_url("services_lending/payments/{$member->id}"); ?>"><?php echo $member->payments; ?></a></td>
                </tr>
                 <tr>
                  <td>Capital</td>
                  <td class="text-right"><a href="<?php echo site_url("services_shares/share_capital/{$member->id}"); ?>"><?php echo $member->capital; ?></a></td>
                </tr>
                 <tr>
                  <td>Dividends</td>
                  <td class="text-right"><a href="<?php echo site_url("services_shares/dividends/{$member->id}"); ?>"><?php echo $member->dividends; ?></a></td>
                </tr>
                 <tr>
                  <td>Withdrawal</td>
                  <td class="text-right"><a href="<?php echo site_url("services_shares/withdrawals/{$member->id}"); ?>"><?php echo $member->withdrawal; ?></a></td>
                </tr>
              </tbody>
            </table>
</div>

<?php if( $member->loans == 0) { ?>
<div class="well">
<div class="row">
  <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">Confirm Code</label>
            <input type="hidden" name="time" value="<?php echo time(); ?>">
           <div class="form-control text-center bold"><?php echo strtoupper(substr(sha1(time()),0,4)); ?></div>
        </div>
  </div>
  <div class="col-md-6">
        <div class="form-group">
             <label class="control-label">Enter Code Here</label>
           <input type="text" class="form-control text-center" name="confirm_code" value="">
        </div>
  </div>
</div>
</div>
<?php } else { ?>
  <p class="danger text-center">Delete Disabled</p>
<?php } ?>
  <?php if( isset($output) && ($output!='ajax') ) : ?>

	    	</div>
	    	<div class="panel-footer">
	    		<button type="submit" class="btn btn-success">Submit</button>
	    		<a href="<?php echo site_url("membership_members"); ?>" class="btn btn-warning">Back</a>
	    	</div>


	    	</form>


	    </div>
    </div>
</div>
</div>

<?php $this->load->view('footer'); ?>

<?php endif; ?>