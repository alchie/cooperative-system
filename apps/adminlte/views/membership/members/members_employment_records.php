<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>

<?php $this->load->view('membership/members/members_navbar'); ?>

<?php $this->load->view('membership/members/members_navbar2'); ?>

<div class="container">
<div class="row">
	<div class="col-md-12">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
<?php if( hasAccess('membership', 'members', 'edit') ) { ?>
	    		<a data-toggle="modal" data-target="#searchName" class="btn btn-success btn-xs pull-right">Add Record</a>
<?php } ?>
	    		<h3 class="panel-title">Employment Records</h3>
	    	</div>
	    	<div class="panel-body">

	    	</div>
	    </div>
    </div>
    </div>
</div>

<?php $this->load->view('footer'); ?>