<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php if(isset($member) && ($member)) { ?>
<div class="container">
  <nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#member-navbar-collapse" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <div class="navbar-brand"><?php echo $member->lastname; ?>, <?php echo $member->firstname; ?> <?php echo $member->middlename; ?> 
      <?php if($member->company_id) { ?>
      <sup class="badge hidden-xs"><a class="body_wrapper" href="<?php echo site_url('membership_companies/info/' . $member->company_id); ?>"><?php echo $member->company_name; ?></a></sup>
      <?php } ?>
      </div>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="member-navbar-collapse">
<ul class="nav navbar-nav hidden-xs">
<li class="dropdown">
  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="caret"></span></a>
  <ul class="dropdown-menu">
    <li>
<div class="navbar-form">
    <input name="q" type="text" class="form-control autocomplete-member_change" data-source="<?php echo site_url("{$current_uri}/ajax/change_member"); ?>" data-current_sub_uri="<?php echo $current_sub_uri; ?>" placeholder="Search Member" value="<?php echo $this->input->get('q'); ?>">
        </div>
    </li>
  </ul>
</li>
</ul>

      <ul class="nav navbar-nav navbar-right">
<?php 
$url['membership_members'] = array('uri' => 'membership_members/member_data/' . $member->id, 'title'=>'Member\'s Data', 'access'=>hasAccess('membership', 'members', 'view'));
$url['services_shares'] = array('uri' => 'services_shares/overview/' . $member->id, 'title'=>'Share Capital', 'access'=>hasAccess('services', 'shares', 'view'));
$url['services_lending'] = array('uri' => 'services_lending/loans/' . $member->id, 'title'=>'Lending', 'access'=>hasAccess('services', 'lending', 'view'));
foreach($url as $k=>$v) {
  if( $v['access'] ) {
?>
  <li class="<?php echo ((isset($current_sub_uri)) && ($k==$current_uri)) ? 'active' : ''; ?>"><a class="body_wrapper" href="<?php echo site_url($v['uri']); ?>"><?php echo $v['title']; ?></a></li>
<?php } 
} ?>         
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
</div>
<?php } ?>