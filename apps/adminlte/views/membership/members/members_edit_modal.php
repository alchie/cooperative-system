<?php defined('BASEPATH') OR exit('No direct script access allowed'); 
$marital_statuses = array(
                'single'=>'Single/Unmarried',
                'married'=>'Married',
                'widower' => 'Widow /   Widower',
                'legally_separated' => 'Legally Separated',
                'annulled' => 'Annulled'
              );
?>
<!-- #updatePersonalInfo Modal -->
<div class="modal fade" id="updatePersonalInfo" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalLabel">Update Personal Information</h4>
      </div>
<form method="post">
<input type="hidden" name="action" value="personal_info">
      <div class="modal-body">
        <div class="form-group">
            <label class="control-label">Last Name</label>
            <input type="text" class="form-control" name="lastname" value="<?php echo $member->lastname; ?>" required="required">
        </div>
        <div class="form-group">
            <label class="control-label">First Name</label>
            <input type="text" class="form-control" name="firstname" value="<?php echo $member->firstname; ?>" required="required">
        </div>
        <div class="form-group">
            <label class="control-label">Middle Name</label>
            <input type="text" class="form-control" name="middlename" value="<?php echo $member->middlename; ?>" required="required">
        </div>

<div class="row">
  <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">Birthday</label>
           <input type="text" class="form-control datepicker" name="birthdate" value="<?php echo date('m/d/Y', strtotime($member->birthdate)); ?>">
        </div>
  </div>
  <div class="col-md-6">
        <div class="form-group">
             <label class="control-label">Birth Place</label>
           <input type="text" class="form-control" name="birthplace" value="<?php echo $member->birthplace; ?>">
        </div>
  </div>
</div>

<div class="row">
  <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">Gender</label>
            <select class="form-control" name="gender">
              <option value="male" <?php echo ($member->gender=='male') ? "selected" : ""; ?>>Male</option>
              <option value="female" <?php echo ($member->gender=='female') ? "selected" : ""; ?>>Female</option>
            </select>
        </div>
  </div>
  <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">Marital Status</label>
            <select class="form-control" name="marital_status">
            <?php foreach($marital_statuses as $key=>$value) { ?>
              <option value="<?php echo $key; ?>" <?php echo ($member->marital_status==$key) ? "selected" : ""; ?>><?php echo $value; ?></option>
              <?php } ?>
            </select>
        </div>
  </div>
</div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
</form>
    </div>
  </div>
</div>