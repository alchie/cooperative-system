<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header');  ?>

<?php $this->load->view('membership/companies/companies_navbar'); ?>

<div class="container">
<div class="row">

	<div class="col-md-8 col-md-offset-2">

	    <div class="panel panel-success">
	    	<div class="panel-heading">
          <a href="<?php echo site_url("membership_companies/payments/" . $company->id); ?>" class="btn btn-warning pull-right btn-xs">Back</a>
	    		<h3 class="panel-title">Payment Details</h3>
	    	</div>
	    	<form method="post">
	    	<div class="panel-body">

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label class="control-label">Payment Date</label>
            <div class="form-control text-center"><?php echo date('m/d/Y', strtotime($payment->payment_date)); ?></div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="control-label">Receipt Number</label>
            <div class="form-control text-center"><?php echo $payment->receipt_number; ?></div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="control-label">Amount Paid</label>
            <div class="form-control text-right"><?php echo number_format($payment->amount,2); ?></div>
        </div>
    </div>

</div>

</div>
</div>

<?php if( $open_invoices ) { ?>

<form method="post">
      <div class="panel panel-default">
        <div class="panel-heading">
          <button type="submit" class="btn btn-success pull-right btn-xs">Submit</button>

          <!--<div class="btn-group pull-right" role="group" style="margin-right: 10px;">
          <button type="button" class="btn btn-default btn-xs payment_autoapply" data-max_amount="<?php echo round($payment->amount,2); ?>" data-input="amount-input">Auto Apply</button>
          <button type="button" class="btn btn-default btn-xs payment_unapply" data-input="amount-input">Un-Apply</button>
</div>-->

          <h3 class="panel-title">Open Invoices</h3>
        </div>

        <div class="panel-body">

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>



<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
<?php foreach($names_only as $name) { ?>
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="heading<?php echo $name->member_id; ?>">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $name->member_id; ?>" aria-expanded="true" aria-controls="collapse<?php echo $name->member_id; ?>">
          <?php echo $name->lastname ?>, <?php echo $name->firstname ?> <?php echo $name->middlename ?>
        </a>
      </h4>
    </div>
    <div id="collapse<?php echo $name->member_id; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $name->member_id; ?>">
      <div class="panel-body">
        

<table class="table table-condensed table-hover">
          <thead>
            <tr>
              <th class="text-left">Employee Name</th>
              <th class="text-center">Due Date</th>
              <th class="text-right" >Principal</th>
               <th class="text-right">Interest</th>
               <th class="text-right">Amount Due</th>
               <th class="text-right" width="20%">Amount to Apply</th>
            </tr>
          </thead>
          <tbody>
<?php 
$principal_due = 0;
$interest_due = 0;
$due_increment = 0;
$total_applied = 0;
$n=0;
foreach($open_invoices as $invoice) { 
    
    if( $invoice->member_id != $name->member_id) {
      continue;
    }

    $amount_due = ((($invoice->principal_due + $invoice->interest_due) - $invoice->total_applied) + $invoice->applied);

    $total_applied += $invoice->applied;
    $principal_due += $invoice->principal_due;
    $interest_due += $invoice->interest_due;
    $due_increment += $amount_due;

?>
<tr class="<?php 
$due_already = '';            
if( time() > strtotime($invoice->due_date) ) {
  $due_already = 'danger'; 
}

$class_name = (round($total_applied) >= round($due_increment)) ? 'success' : $due_already; 

if( $invoice->applied ) {
  $class_name = 'info';
}

echo $class_name;
?>">
              <td class="text-left"><?php echo $invoice->lastname ?>, <?php echo $invoice->firstname ?> <?php echo $invoice->middlename ?></td>
              <td class="text-center"><?php echo date('m/d/Y', strtotime($invoice->due_date)); ?></td>
              <td class="bold text-right"><?php echo number_format($invoice->principal_due,2); ?></td>
              <td class="bold text-right"><?php echo number_format($invoice->interest_due,2); ?></td>
              <td class="bold text-right">
              <input name="amount_due[<?php echo $invoice->id; ?>]" type="hidden" value="<?php echo $invoice->amount_due; ?>">
              <?php echo number_format($invoice->amount_due,2); ?></td>
              <td class="bold text-right"><input name="apply_invoice[<?php echo $invoice->id; ?>]" type="text" class="form-control text-right amount-input" value="<?php 
 echo number_format((($invoice->applied) ? $invoice->applied : 0.00),2, ".", ""); ?>" data-limit="<?php echo number_format($invoice->amount_due,2,".",""); 
 ?>"></td>
               </tr>
<?php $n++;
 } ?>
<?php if( $n>1) { ?>
<tr>
  <td class="text-left allcaps bold" colspan="2">Total</td>
  <td class="text-right allcaps bold"><?php echo number_format($principal_due,2); ?></td>
  <td class="text-right allcaps bold"><?php echo number_format($interest_due,2); ?></td>
  <td class="text-right allcaps bold"><?php echo number_format($due_increment,2); ?></td>
  <td class="bold text-right"><span class="form-control" id="autosum-output"><?php echo number_format($total_applied,2); ?></span></td>
</tr>
<?php } ?>
          </tbody>
        </table>


      </div>
    </div>
  </div>
<?php } ?>
</div>

	
	    	</div>
	    	<div class="panel-footer">
	    		<button type="submit" class="btn btn-success">Submit</button>
	    		<a href="<?php echo site_url("membership_companies/payments/" . $company->id); ?>" class="btn btn-warning">Back</a>
	    	</div>

	    </div>
</form>

<?php } else { ?>
  <div class="panel panel-default">
    <div class="panel-body">
      <p class="text-center">No Invoices Found!</p>
    </div>
  </div>
<?php } ?>


    </div>
</div>
</div>

<?php $this->load->view('footer'); ?>