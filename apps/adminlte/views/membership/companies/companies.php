<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
<?php if( ! $inner_page ): ?>
<?php $this->load->view('membership/membership_navbar'); ?>

<div class="container">
<div class="row">
	<div class="col-md-12">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
	    	<?php if( hasAccess('membership', 'companies', 'add') ) { ?>
	    		<a data-toggle="modal" data-target="#searchName" class="btn btn-success btn-xs pull-right">Add Company</a>
	    	<?php } ?>
	    		<h3 class="panel-title"><span class="bold allcaps">Companies</span></h3>
	    	</div>
	    	<div class="panel-body" id="ajaxBodyInnerPage" data-url="<?php echo site_url(uri_string()); ?>">
<?php endif; ?>

<?php if( isset($companies) ) { ?>
<?php if( $companies ) { ?>
	    		<table class="table table-default hidden-xs">
	    			<thead>
	    				<tr>
	    					<th>Name</th>
	    					<th class="text-right"># of Members</th>
	    					<th width="150px">Action</th>
	    				</tr>
	    			</thead>
	    			<tbody>
	    			<?php foreach($companies as $company) { ?>
	    				<tr>
	    					<td><?php echo $company->name; ?></td>
	    					<td class="text-right"><?php echo $company->members; ?></td>
	    					<td>

<div class="btn-group">
  <a href="<?php echo site_url("membership_companies/info/" . $company->id); ?>" class="btn btn-success btn-xs body_wrapper">Company Info</a>
  <button type="button" class="btn btn-success btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <span class="caret"></span>
    <span class="sr-only">Toggle Dropdown</span>
  </button>
  <ul class="dropdown-menu dropdown-menu-right">
    <li><a href="<?php echo site_url("membership_companies/employees/" . $company->id); ?>" class="body_wrapper">Employees</a></li>
<li><a href="<?php echo site_url("membership_companies/open_invoices/" . $company->id); ?>" class="body_wrapper">Open Invoices</a></li>
<li><a href="<?php echo site_url("membership_companies/payments/" . $company->id); ?>" class="body_wrapper">Payments</a></li>
  </ul>
</div>
	    					</td>
	    				</tr>
	    			<?php } ?>
	    			</tbody>
	    		</table>


<ul class="list-group visible-xs">
	<?php foreach($companies as $company) { ?>
  		<a href="<?php echo site_url("membership_companies/info/" . $company->id); ?>" class="list-group-item">
  			<h4 class="list-group-item-heading"><?php echo $company->name; ?></h4>
  			<p class="list-group-item-text"></p>
  		</a>
  	<?php } ?>
</ul>

<?php if(isset($pagination)) { ?><div class="text-center"><?php echo $pagination; ?></div><?php } ?>

<?php } else { ?>
	<div class="text-center">No Company Found!</div>
<?php } ?>
<?php } else { ?>
	Please wait... loading data...
<?php } ?>
<?php if( ! $inner_page ): ?>
	    	</div>
	    </div>
	    </div>
	</div>
</div>

<!-- #searchMember Modal -->
<div class="modal fade" id="searchName" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalLabel">Add Company</h4>
      </div>
<form method="get" action="<?php echo site_url('membership_companies/search_name'); ?>">
      <div class="modal-body">

        <div class="form-group">
            <label class="control-label">Search Name</label>
            <input type="text" class="form-control autocomplete-member_change" data-source="<?php echo site_url("membership_companies/ajax/add_company"); ?>" data-current_sub_uri="<?php echo $current_sub_uri; ?>" name="q" value="" >
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Search</button>
      </div>
</form>
    </div>
  </div>
</div>
<?php endif; ?>
<?php $this->load->view('footer'); ?>