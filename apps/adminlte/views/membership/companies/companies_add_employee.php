<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); 
$marital_statuses = array(
                'single'=>'Single/Unmarried',
                'married'=>'Married',
                'widower' => 'Widow /   Widower',
                'legally_separated' => 'Legally Separated',
                'annulled' => 'Annulled'
              );
?>

<?php $this->load->view('membership/membership_navbar'); ?>

<?php $this->load->view('membership/companies/companies_navbar'); ?>

<div class="container">
<div class="row">

	<div class="col-md-6 col-md-offset-3">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
	    		<h3 class="panel-title">Add Employee</h3>
	    	</div>
	    	<form method="post">

<?php if( $members ) { ?>
<div class="list-group">
	<?php foreach($members as $member) { ?>
  		<label class="list-group-item">
  			<input type="checkbox" name="member_id[]" value="<?php echo $member->id; ?>">
  			<?php echo $member->lastname; ?>, <?php echo $member->firstname; ?> <?php echo $member->middlename; ?>
  		</label>
  	<?php } ?>
</div>
<?php } else { ?>
	<div class="text-center panel-body">No Member Found!</div>
<?php } ?>

	    	<div class="panel-footer">
	    		<button type="submit" class="btn btn-success">Submit</button>
	    		<a href="<?php echo site_url("membership_companies/employees/{$company->id}"); ?>" class="btn btn-warning">Back</a>
	    	</div>


	    	</form>
	    </div>
    </div>
</div>
</div>

<?php $this->load->view('footer'); ?>