<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); 
$marital_statuses = array(
                'single'=>'Single/Unmarried',
                'married'=>'Married',
                'widower' => 'Widow /   Widower',
                'legally_separated' => 'Legally Separated',
                'annulled' => 'Annulled'
              );
?>

<?php $this->load->view('membership/companies/companies_navbar'); ?>

<div class="container">
<div class="row">

    <div class="col-md-12">

      <div class="panel panel-default">
        <div class="panel-heading">
<?php if( hasAccess('membership', 'companies', 'edit') ) { ?>
          <button type="button" class="btn btn-warning btn-xs pull-right" data-toggle="modal" data-target="#updateCompanyInfo">Update</button>
<?php } ?>
            <h3 class="panel-title">Company Info</h3>
        </div>
        <div class="panel-body">

        <div class="form-group">
            <label class="control-label">Company Name</label>
            <div class="form-control"><?php echo $company->name; ?></div>
        </div>

        <div class="form-group">
            <label class="control-label">Address</label>
            <div class="form-control"><?php echo $company->address; ?></div>
        </div>

        <div class="form-group">
            <label class="control-label">Contact Numbers</label>
            <div class="form-control"><?php echo $company->contact_numbers; ?></div>
        </div>

        <div class="form-group">
            <label class="control-label">Email Address</label>
            <div class="form-control"><?php echo $company->email; ?></div>
        </div>

        </div>
      </div>

    </div>
</div>
</div>

<!-- #updateCompanyInfo Modal -->
<div class="modal fade" id="updateCompanyInfo" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalLabel">Update Company Information</h4>
      </div>
<form method="post" action="<?php echo site_url('membership_companies/edit/' . $company->id); ?>">
<input type="hidden" name="action" value="company_info">
      <div class="modal-body">

        <div class="form-group">
            <label class="control-label">Company Name</label>
            <input type="text" class="form-control" name="name" value="<?php echo $company->name; ?>" required="required">
        </div>

        <div class="form-group">
            <label class="control-label">Address</label>
            <input type="text" class="form-control" name="address" value="<?php echo $company->address; ?>">
        </div>

        <div class="form-group">
            <label class="control-label">Contact Numbers</label>
            <input type="text" class="form-control" name="contact_numbers" value="<?php echo $company->contact_numbers; ?>">
        </div>

        <div class="form-group">
            <label class="control-label">Email Address</label>
            <input type="email" class="form-control" name="email" value="<?php echo $company->email; ?>">
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
</form>
    </div>
  </div>
</div>

<?php $this->load->view('footer'); ?>