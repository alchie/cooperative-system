<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); 
$marital_statuses = array(
                'single'=>'Single/Unmarried',
                'married'=>'Married',
                'widower' => 'Widow /   Widower',
                'legally_separated' => 'Legally Separated',
                'annulled' => 'Annulled'
              );
?>

<?php $this->load->view('membership/membership_navbar'); ?>

<div class="container">
<div class="row">

	<div class="col-md-6 col-md-offset-3">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
	    		<button type="button" class="btn btn-success btn-xs pull-right ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Add Name" data-url="<?php echo site_url("system_names/add/ajax") . "?next=membership_companies/add/"; ?>$new_id">Add Name</button>
	    		<h3 class="panel-title">Search Name</h3>
	    	</div>
	    	<form method="post">

<?php if( $names ) { ?>
<div class="list-group">
	<?php foreach($names as $name) { ?>
  		<a href="<?php echo site_url('membership_companies/add/' .$name->id ); ?>" class="list-group-item">
  			<?php echo $name->full_name; ?>
  		</a>
  	<?php } ?>
</div>
<?php } else { ?>
	<div class="text-center panel-body">No Name Found!</div>
<?php } ?>

	    	<div class="panel-footer">
	    		<button type="submit" class="btn btn-success">Submit</button>
	    		<a href="<?php echo site_url("membership_companies/employees"); ?>" class="btn btn-warning">Back</a>
	    	</div>


	    	</form>
	    </div>
    </div>
</div>
</div>

<?php $this->load->view('footer'); ?>