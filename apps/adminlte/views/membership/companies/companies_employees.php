<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>

<?php if( ! $inner_page ): ?>
<?php $this->load->view('membership/companies/companies_navbar'); ?>

<div class="container">
<div class="row">

    <div class="col-md-12">

      <div class="panel panel-default">
        <div class="panel-heading">
<?php if( hasAccess('membership', 'companies', 'edit') ) { ?>
          <button type="button" class="btn btn-success btn-xs pull-right" data-toggle="modal" data-target="#searchMember">Add Employee</button>
<?php } ?>
            <h3 class="panel-title">Employees</h3>
        </div>
        <div class="panel-body" id="ajaxBodyInnerPage" data-url="<?php echo site_url(uri_string()); ?>">
<?php endif; ?>
<?php if( isset($members) ) { ?>
<?php if( $members ) { ?>
          <table class="table table-condensed table-hover hidden-xs">
            <thead>
              <tr>
                <th>Member Name</th>
                <th width="150px" class="text-center">Action</th>
              </tr>
            </thead>
            <tbody>
            <?php foreach($members as $member) { ?>
              <tr>
                <td><?php echo $member->lastname; ?>, <?php echo $member->firstname; ?> <?php echo $member->middlename; ?></td>
                <td class="text-right">
<div class="btn-group" role="group" aria-label="Actions">
<?php if( hasAccess('membership', 'members', 'view') ) { ?>
  <a href="<?php echo site_url("membership_members/member_data/" . $member->id); ?>" class="btn btn-primary btn-xs body_wrapper">View Info</a>
<?php } ?>
<?php if( hasAccess('membership', 'companies', 'edit') ) { ?>
  <a href="<?php echo site_url("membership_companies/delete_member/" . $company->id . "/" . $member->id); ?>" class="btn btn-danger btn-xs confirm">Delete</a>
<?php } ?>
</div>
</td>
              </tr>
            <?php } ?>
            </tbody>
          </table>


          <ul class="list-group visible-xs">
 <?php foreach($members as $member) { ?>
      <a href="<?php echo site_url("membership_members/member_data/" . $member->id); ?>" class="list-group-item">
        <h4 class="list-group-item-heading"><?php echo $member->lastname; ?>, <?php echo $member->firstname; ?> <?php echo $member->middlename; ?></h4>
      </a>
    <?php } ?>
          </ul>

          <?php echo ($pagination!='') ? '<center>' . $pagination . '</center>' : ''; ?>
          
<?php } else { ?>
  <div class="text-center">No Member Found!</div>
<?php } ?>
<?php } else { ?>
  Please wait...
<?php } ?>

<?php if( ! $inner_page ): ?>

        </div>
      </div>

    </div>
</div>
</div>

<!-- #searchMember Modal -->
<div class="modal fade" id="searchMember" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalLabel">Add Employee</h4>
      </div>
<form method="get" action="<?php echo site_url('membership_companies/add_employee/' . $company->id); ?>">
      <div class="modal-body">

        <div class="form-group">
            <label class="control-label">Search Name</label>
            <input type="text" class="form-control" name="q" value="">
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Search</button>
      </div>
</form>
    </div>
  </div>
</div>
<?php endif; ?>
<?php $this->load->view('footer'); ?>