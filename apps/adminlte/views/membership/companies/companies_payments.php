<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>

<?php if( ! $inner_page ): ?>

<?php $this->load->view('membership/companies/companies_navbar'); ?>

<div class="container">
<div class="row">

    <div class="col-md-12">

      <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Company Payments</h3>
        </div>
        <div class="panel-body" id="ajaxBodyInnerPage" data-url="<?php echo site_url(uri_string()); ?>">

<?php endif; ?>

<?php if( isset($receipts) ) { ?>
<?php if( $receipts ) { ?>

 <table class="table table-default table-condensed table-hover hidden-xs">
          <thead>
            <tr>
              <th class="text-center" width="1px"></th>
              <th class="text-center" width="10%">Receipt #</th>
              <th class="text-center">Payment Date</th>
              <th class="text-right">Amount Paid</th>
              <th class="text-right" width="5px"></th>
            </tr>
          </thead>
          <tbody>
<?php 
$total_receipts = 0;
$total_applied = 0;
foreach($receipts as $receipt) { 
  ?>
            <tr class="<?php echo ($receipt->trn_id||$receipt->total_items) ? '' : 'danger'; ?>">
              <td class="text-center">
<?php if( $receipt->deposit_id ) { ?>
    <a href="<?php echo site_url("accounting_deposits/edit_deposit/{$receipt->deposit_id}"); ?>?highlight=<?php echo $receipt->trn_id; ?>" class="item body_wrapper"><span class="fa fa-bank"></span></a>
<?php } ?>
              </td>
              <td class="text-center"><?php echo $receipt->receipt_number; ?></td>
              <td class="text-center"><?php echo date("F d, Y", strtotime($receipt->receipt_date)); ?></td>
              <td class="text-right"><?php echo number_format($receipt->amount,2); $total_receipts+=$receipt->amount; ?></td>
              <td class="text-right"><a href="<?php echo site_url("accounting_sales_receipts/receipt_items/{$receipt->id}"); ?>"><span class="fa fa-eye"></span></a></td>
            </tr>
<?php 
} ?>

          </tbody>
          </table>

<div class="panel-group visible-xs" id="accordion" role="tablist" aria-multiselectable="true">
  <?php foreach($receipts as $receipt) { ?>
 <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="heading-<?php echo $receipt->id ?>">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-<?php echo $receipt->id ?>" aria-expanded="false" aria-controls="collapse-<?php echo $receipt->id ?>">
          <?php echo date("F d, Y", strtotime($receipt->receipt_date)); ?> <strong class="pull-right"><?php echo number_format($receipt->amount,2); ?></strong>
        </a>
      </h4>
    </div>
    <div id="collapse-<?php echo $receipt->id ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-<?php echo $receipt->id ?>">
      <div class="panel-body">
       

<div class="list-group">
  <div class="list-group-item">Payment Date: <strong class="pull-right"><?php echo date("m/d/Y", strtotime($receipt->receipt_date)); ?></strong></div>
  <div class="list-group-item">Receipt Number: <strong class="pull-right"><?php echo $receipt->receipt_number; ?></strong></div>
  <div class="list-group-item">Amount: <strong class="pull-right"><?php echo number_format($receipt->amount,2); ?></strong></div>
  <div class="list-group-item">Applied: <strong class="pull-right"><?php echo number_format($receipt->total_applied,2); ?></strong></div>
  
  <a class="list-group-item text-center active" href="<?php echo site_url("membership_companies/receipt/{$receipt->id}"); ?>"">View Payment</a>
</div>

      </div>
    </div>
  </div>

    <?php } ?>
</div>

<?php echo ($pagination!='') ? '<center>' . $pagination . '</center>' : ''; ?>

<?php } else { ?>
  <div class="text-center">No Payment Yet!</div>
<?php } ?>
<?php } else { ?>
  Please wait...
<?php } ?>

<?php if( ! $inner_page ): ?>
        </div>
      </div>

    </div>
</div>
</div>
<?php endif; ?>
<?php $this->load->view('footer'); ?>