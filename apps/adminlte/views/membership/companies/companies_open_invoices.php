<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>

<?php if( ! $inner_page ): ?>

<?php $this->load->view('membership/companies/companies_navbar'); ?>

<div class="container">
<div class="row">

    <div class="col-md-12">

      <div class="panel panel-default">
        <div class="panel-heading">
        <a href="" target="_blank" class="btn btn-xs btn-primary pull-right">Print Statement</a>
          <h3 class="panel-title">Open Invoices</h3>
        </div>

        <div class="panel-body" id="ajaxBodyInnerPage" data-url="<?php echo site_url(uri_string()); ?>">

<?php endif; ?>

<?php if( isset($open_invoices) ) { ?>
<?php if( $open_invoices ) { ?>

<table class="table table-condensed table-hover hidden-xs">
          <thead>
            <tr>
              <th class="text-left">Employee Name</th>
              <th class="text-center">Due Date</th>
              <th class="text-right" >Principal</th>
               <th class="text-right">Interest</th>
               <th class="text-right">Amount Due</th>
               <th class="text-right">Paid</th>
               <th class="text-right">Balance</th>
            </tr>
          </thead>
          <tbody>
<?php 
$principal_due = 0;
$interest_due = 0;
foreach($open_invoices as $invoice) { 
    
    $principal_due += $invoice->principal_due;
    $interest_due += $invoice->interest_due;

?>
<tr class="<?php 
$due_already = '';            
if( time() > strtotime($invoice->due_date) ) {
  $due_already = 'danger'; 
}
echo $due_already; 
?>">
              <td class="text-left">
<a class="body_wrapper" href="<?php echo site_url("services_lending/schedule/{$invoice->loan_id}"); ?>?highlight=<?php echo $invoice->id ?>">
              <?php echo $invoice->lastname ?>, <?php echo $invoice->firstname ?>, <?php echo $invoice->middlename ?>
</a>
              </td>
              <td class="text-center"><?php echo date('m/d/Y', strtotime($invoice->due_date)); ?></td>
              <td class="bold text-right"><?php echo number_format($invoice->principal_due,2); ?></td>
              <td class="bold text-right"><?php echo number_format($invoice->interest_due,2); ?></td>
              <td class="bold text-right"><?php echo number_format($invoice->gross_amount,2); ?></td>
              <td class="bold text-right"><?php echo number_format($invoice->total_applied,2); ?></td>
              <td class="bold text-right"><?php echo number_format($invoice->balance,2); ?></td>
               </tr>
<?php 
 } ?>
          </tbody>
        </table>
  
<div class="panel-group visible-xs" id="accordion" role="tablist" aria-multiselectable="true">

<?php 
$principal_due = 0;
$interest_due = 0;
foreach($open_invoices as $invoice) { 
    
    $principal_due += $invoice->principal_due;
    $interest_due += $invoice->interest_due;

?>
 <div class="panel panel-<?php 
$due_already = 'default';            
if( time() > strtotime($invoice->due_date) ) {
  $due_already = 'warning'; 
}
echo $due_already; 
?>">
    <div class="panel-heading" role="tab" id="heading-<?php echo $invoice->id ?>">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-<?php echo $invoice->id ?>" aria-expanded="false" aria-controls="collapse-<?php echo $invoice->id ?>">
          <?php echo $invoice->lastname ?>, <?php echo $invoice->firstname ?>, <?php echo $invoice->middlename ?>
        </a>
      </h4>
    </div>
    <div id="collapse-<?php echo $invoice->id ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-<?php echo $invoice->id ?>">
      <div class="panel-body">
       

<div class="list-group">
  <div class="list-group-item">Due Date: <strong class="pull-right"><?php echo date('m/d/Y', strtotime($invoice->due_date)); ?></strong></div>
  <div class="list-group-item">Principal Due: <strong class="pull-right"><?php echo number_format($invoice->principal_due,2); ?></strong></div>

  <div class="list-group-item">Interest Due: <strong class="pull-right"><?php echo number_format($invoice->interest_due,2); ?></strong></div>
   <div class="list-group-item">Gross Amount: <strong class="pull-right"><?php echo number_format($invoice->gross_amount,2); ?></strong></div>
  <div class="list-group-item">Total Applied: <strong class="pull-right"><?php echo number_format($invoice->total_applied,2); ?></strong></div>
  <div class="list-group-item">Balance: <strong class="pull-right"><?php echo number_format($invoice->balance,2); ?></strong></div>
  <a class="list-group-item text-center active" href="<?php echo site_url("services_lending/schedule/{$invoice->loan_id}"); ?>?highlight=<?php echo $invoice->id ?>">View Schedule</a>
</div>




      </div>
    </div>
  </div>

<?php } ?>

</div>

<?php echo ($pagination!='') ? '<center>' . $pagination . '</center>' : ''; ?>

        </div>

      </div>


<?php } else { ?>
  <div class="panel panel-default">
    <div class="panel-body">
      <p class="text-center">No Invoices Found!</p>
    </div>
  </div>
<?php } ?>
<?php } else { ?>
  Please wait...
<?php } ?>

<?php if( ! $inner_page ): ?>

    </div>
</div>
</div>

<?php endif; ?>

<?php $this->load->view('footer'); ?>