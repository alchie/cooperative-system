<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php if( ! $inner_page ): ?> 
  <?php if( ! $body_wrapper ): ?> 

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title><?php echo (isset($page_title)) ? $page_title : 'COOP'; ?></title>
    <link href="<?php echo base_url('assets/themes/' . ((isset($bootstrap_theme)) ? $bootstrap_theme : 'default' ) . '/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/js/jqueryui/jquery-ui.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/icons/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/icons/material-icons/material-icons.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/icons/dashicons/css/dashicons.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/icons/ionicons/css/ionicons.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/icons/octicons/octicons.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/icons/genericons/genericons.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/icons/devicons/css/devicons.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/js/bootstrap-select/css/bootstrap-select.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/js/tag-it/css/jquery.tagit.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/adminlte/css/AdminLTE.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/adminlte/css/skins/skin-blue.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/adminlte/styles.css'); ?>" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-collapse sidebar-mini bootswatch-<?php echo ((isset($bootstrap_theme)) ? $bootstrap_theme : 'default' ); ?>">

<div class="wrapper">

  <!-- Main Header -->
  <header class="main-header">

    <!-- Logo -->
    <a href="<?php echo base_url(); ?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>CBH</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>CBH</b>COOP</span>
    </a>

<?php $this->load->view('navbar'); ?>

  </header>

<?php $this->load->view('sidebar_nav'); ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">


    <!-- Main content -->
    <section class="content">

<div id="bodyWrapper">

<?php endif; // static_nav ?>

<?php 
if( $this->input->get('error_code') ) { 
  $errData = (isset($errorData)) ? $errorData : NULL;
  showError( $this->input->get('error_code'), $errData); 
}
?>

<?php endif; // inner_page ?>