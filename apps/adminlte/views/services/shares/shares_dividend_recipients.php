<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( !isset($output) || ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('services/services_navbar'); ?>

<?php $this->load->view('services/shares/shares_navbar'); ?>

<div class="container">
<div class="row">

	<div class="col-md-12">
	    <div class="panel panel-<?php echo ($dividend->trn_id) ? 'default' : 'danger'; ?>">
	    	<div class="panel-heading">
<?php if( $dividend->total_recipients < $dividend->amount ) { ?>
            <button type="button" class="btn btn-success btn-xs pull-right ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Add Recipient" data-url="<?php echo site_url("services_shares/add_recipient/{$dividend->id}/ajax") . "?next=" . uri_string(); ?>">Add Recipient</button>
<?php } else { ?>
        <?php if( ( !$dividend->trn_id ) && ( $dividend->total_recipients == $dividend->amount ) ) { ?>
            <a type="button" class="btn btn-primary btn-xs pull-right confirm" href="<?php echo site_url("services_shares/declare_dividend/{$dividend->id}"); ?>">Declare Dividends</a>
        <?php } ?>
<?php } ?>
	    		<h3 class="panel-title">Dividend Recipients : <strong>Res#<?php echo $dividend->res_number; ?> &middot; <em><?php echo number_format($dividend->amount,2); ?></em></strong>
<a href="#update-resolution" class="ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Update Dividends" data-url="<?php echo site_url("services_shares/edit_dividend/{$dividend->id}/ajax") . "?next=" . uri_string(); ?>"><span class="glyphicon glyphicon-pencil"></span></a>
          </h3>
	    	</div>
	    	<div class="panel-body">

<?php endif; ?>

<?php if( $dividend_items ) { ?>

 <table class="table table-condensed hidden-xs">
          <thead>
            <tr>
              <th class="text-left">Name</th>
              <th class="text-right">Amount</th>
              <th class="text-right" width="100px;"></th>
            </tr>
          </thead>
          <tbody>
<?php 
$total_dividend_item = 0;
$n=0;
foreach($dividend_items as $dividend_item) { ?>
            <tr class="<?php echo ($dividend_item->trn_id) ? '' : 'danger'; ?>">
              <td class="text-left"><a href="<?php echo site_url("services_shares/dividends/{$dividend_item->member_id}"); ?>"><?php echo $dividend_item->full_name; ?></a></td>
              <td class="text-right"><?php echo number_format($dividend_item->amount,2); $total_dividend_item+=$dividend_item->amount; ?></td>
              <td class="text-right">
                  <button class="btn btn-warning btn-xs ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Update Recipient" data-url="<?php echo site_url("services_shares/edit_recipient/{$dividend_item->id}/ajax") . "?next=" . uri_string(); ?>">Update</button>
              </td>
            </tr>
<?php 
$n++;
}
if( $n > 1 ) { 
?>
 <tr>
              <td class="text-left bold">Total</td>
              <td class="text-right bold"><?php echo number_format($total_dividend_item,2); ?></td>
              <td class="text-right bold"></td>
            </tr>
<?php } ?>

          </tbody>
          </table>

<ul class="list-group visible-xs">
  <?php foreach($dividend_items as $dividend_item) { ?>
      <a href="<?php echo site_url("services_shares/edit_recipient/{$dividend_item->id}"); ?>" class="list-group-item">
        <?php echo $dividend_item->full_name; ?> <strong class="pull-right"><?php echo number_format($dividend_item->amount,2); ?></strong>
      </a>
    <?php } ?>
</ul>

<?php } else { ?>
  <div class="text-center">No Recipient Yet!</div>
<?php } ?>

<?php if( !isset($output) || ($output!='ajax') ) : ?>
	    	</div>
	    </div>
    </div>
</div>
</div>

<?php $this->load->view('footer'); ?>

<?php endif; ?>