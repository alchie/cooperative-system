<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('services/services_navbar'); ?>

<?php $this->load->view('membership/members/members_navbar'); ?>

<div class="container">
<div class="row">

<div class="col-md-6 col-md-offset-3">

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

<?php endif; ?>


<?php if( $new_capitals ) { ?>
<ul class="list-group">
    <?php foreach($new_capitals as $new_capital) { ?>
   <a class="list-group-item ajax-modal-inner" href="<?php echo site_url("services_shares/add_capital/{$new_capital->ae_name_id}/{$new_capital->ae_id}/ajax") . "?next=" . $this->input->get('next'); ?>">
    <span class="badge"><?php echo number_format($new_capital->credit,2); ?></span>
    <h4 class="list-group-item-heading"><?php echo $new_capital->lastname; ?>, <?php echo $new_capital->firstname; ?> <?php echo $new_capital->middlename; ?></h4>
    <?php echo $new_capital->entry_type; ?> &middot;
    <?php echo date('F d, Y', strtotime($new_capital->share_date)); ?> &middot;
    <?php echo $new_capital->share_number; ?>
  </a>
  <?php } ?>
</ul>

<?php echo ($pagination!='') ? '<center>' . $pagination . '</center>' : ''; ?>

<?php } else { ?>

<p class="text-center">No New Contributed Capital!</p>

<?php } ?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

    </div>

</div>
</div>

<?php $this->load->view('footer'); ?>

<?php endif; ?>