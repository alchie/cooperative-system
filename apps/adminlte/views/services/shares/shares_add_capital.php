<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( !isset($output) || ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('membership/members/members_navbar'); ?>

<?php $this->load->view('services/shares/shares_navbar');  ?>

<div class="container">
<div class="row">

	<div class="col-md-8 col-md-offset-2">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
	    		<h3 class="panel-title">Add Capital</h3>
	    	</div>
	    	<form method="post">
	    	<div class="panel-body">

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

<?php endif; ?>

<?php if( isset($new_capital) && ($new_capital) ) { ?>
    <input type="hidden" name="hashcode" value="<?php echo sha1($new_capital->trn_id); ?>" ?>
<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label class="control-label">Payment Date</label>
            <div class="form-control text-center"><?php echo date('m/d/Y', strtotime($new_capital->receipt_date)); ?></div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="control-label">Receipt Number</label>
            <div class="form-control text-center"><?php echo $new_capital->receipt_number; ?></div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="control-label">Amount</label>
            <div class="form-control text-right"><?php echo number_format($new_capital->credit,2); ?></div>
        </div>
    </div>

</div>

<?php } ?>

<?php if( !isset($output) || ($output!='ajax') ) : ?>

	    	</div>
	    	<div class="panel-footer">

	    		<button type="submit" class="btn btn-success">Submit</button>
	    		<a href="<?php echo site_url("services_shares/share_capital/" . $member->id); ?>" class="btn btn-warning">Back</a>
	    	</div>

	    	</form>
	    </div>
    </div>
</div>
</div>

<?php $this->load->view('footer'); ?>

<?php endif; ?>