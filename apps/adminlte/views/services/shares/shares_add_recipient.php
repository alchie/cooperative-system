<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( !isset($output) || ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('services/services_navbar'); ?>

<?php $this->load->view('services/shares/shares_navbar'); ?>

<div class="container">
<div class="row">

	<div class="col-md-8 col-md-offset-2">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
	    		<h3 class="panel-title">Add Dividend</h3>
	    	</div>
	    	<form method="post">
	    	<div class="panel-body">

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>
<?php endif; ?>

        <div class="form-group">
            <label class="control-label">Member</label>
            <input id="add_recipients_id" name="member_id" type="hidden">
            <input class="form-control autocomplete-name_select" data-source="<?php echo site_url("services_shares/ajax/add_recipient"); ?>" data-name_id="add_recipients_id" type="text" required>
        </div>
<div class="row">
	<div class="col-md-6">
        <div class="form-group">
            <label class="control-label">Available Balance</label>
            <div class="form-control text-center bold"><?php echo number_format($dividend->balance,2); ?></div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">Amount</label>
            <input class="form-control text-right" type="text" name="amount" value="" required>
        </div>
    </div>
</div>

<?php if( !isset($output) || ($output!='ajax') ) : ?>

	    	</div>
	    	<div class="panel-footer">

	    		<button type="submit" class="btn btn-success">Submit</button>
	    		<a href="<?php echo site_url("services_shares/dividends"); ?>" class="btn btn-warning">Back</a>
	    	</div>

	    	</form>
	    </div>
    </div>
</div>
</div>

<?php $this->load->view('footer'); ?>

<?php endif; ?>