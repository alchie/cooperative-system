<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); 
  $s = array();
  if($settings) {
    foreach($settings as $set) {
      $s[$set->key] = $set->value;
    }
  }

?>

<?php $this->load->view('services/services_navbar'); ?>

<div class="container">
<div class="row">

  <div class="col-md-6 col-md-offset-3">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Configurations</h3>
        </div>
        <form method="post">
        <div class="panel-body">

          <div class="form-group">
            <label>Members Capital</label>
            <select name="capital" class="form-control" title="- - Select Account - -">
<?php 
function display_list($setting, $main_accounts , $pre="", $set='receivable', $filter_type=NULL) {
                 foreach( $main_accounts as $main_account) { 
                    if( ( $filter_type ) && ( $main_account->type != $filter_type ) ) {
                      continue;
                    }
                  ?>
                  <option value="<?php echo $main_account->id; ?>" <?php echo (isset($setting[$set])&&($setting[$set]==$main_account->id)) ? 'selected' : ''; ?> title="<?php echo $main_account->title; ?>"><?php echo $pre; ?><?php echo $main_account->title; ?></option>
          <?php 
          if( $main_account->children ) {
            display_list( $setting, $main_account->children, $pre . "- - - - ", $set, $filter_type );
          }
          ?>
                  <?php } ?> 
<?php } ?>
<?php display_list( $s, $account_titles, '', 'capital', '090_BS_EQU_CAP' ); ?>
              </select>
          </div>
          
          <div class="form-group">
            <label>Class Name</label>
            <select name="class_name" class="form-control" title="- - Select Class - -">
<?php 
function display_class($setting, $class_list , $pre="", $set='class_name') {
                 foreach( $class_list as $class) { 
                  ?>
                  <option value="<?php echo $class->id; ?>" <?php echo (isset($setting[$set])&&($setting[$set]==$class->id)) ? 'selected' : ''; ?> title="<?php echo $class->value; ?>"><?php echo $pre; ?><?php echo $class->value; ?></option>
          <?php 
          if( $class->children ) {
            display_class( $setting, $class->children, $pre . "- - - - ", $set);
          }
          ?>
                  <?php } ?> 
<?php } ?>
<?php display_class( $s, $class_list ); ?>        
              </select>
          </div>

        </div>
        <div class="panel-footer">
          <button type="submit" class="btn btn-success">Submit</button>
          <a href="<?php echo site_url("system_settings"); ?>" class="btn btn-warning">Back</a>
        </div>


        </form>
      </div>
    </div>
</div>
</div>


<?php $this->load->view('footer'); ?>