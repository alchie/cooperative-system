<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>

<?php if( ! $inner_page ): ?>

<?php 
  if( isset( $member ) ) { 
      $this->load->view('membership/members/members_navbar'); 
    } else {
      $this->load->view('services/services_navbar');
   }

?>

<?php $this->load->view('services/shares/shares_navbar'); ?>

<div class="container">
<div class="row">
  <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">
<?php if( hasAccess('services', 'shares', 'add') ) { ?>
        <?php if( isset( $new_capitals ) && ($new_capitals) ) { ?>

          <button type="button" class="btn btn-success btn-xs pull-right ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Add Capital" data-url="<?php echo site_url("services_shares/new_capitals/".((isset($member))?$member->id:0)."/ajax") . "?next=" . uri_string(); ?>">Add Capital</button>

        <?php } ?>   
<?php } ?>
          <h3 class="panel-title">Share Capital</h3>

        </div>
        <div class="panel-body" id="ajaxBodyInnerPage" data-url="<?php echo site_url(uri_string()); ?>">

<?php endif; ?>

<?php if( isset($shares_capital) ) { ?>
<?php if( $shares_capital ) { ?>

 <table class="table table-condensed hidden-xs">
          <thead>
            <tr>
<?php if(!isset($member)) { ?>
              <th class="text-left">Name</th>
<?php } ?>
              <th class="text-left">Date Contributed</th>
              <th class="text-center">Receipt Number</th>
              <th class="text-right">Amount</th>
<?php if(isset($member)) { ?>
<?php if( hasAccess('services', 'shares', 'delete') ) { ?>
              <th class="text-right" width="10px"></th>
<?php } ?>
<?php } ?>
            </tr>
          </thead>
          <tbody>
<?php 

$total_capital = 0;

foreach($shares_capital as $capital) { ?>
            <tr>
<?php if(!isset($member)) { ?>
              <td class="text-left"><a class="body_wrapper" href="<?php echo site_url("services_shares/share_capital/{$capital->member_id}"); ?>"><?php echo $capital->full_name; ?></a></td>
<?php } ?>
              <td class="text-left"><?php echo date("F d, Y", strtotime($capital->payment_date)); ?></td>
              <td class="text-center"><?php echo $capital->receipt_number; ?></td>
              <td class="text-right"><?php echo number_format($capital->amount,2); $total_capital+=$capital->amount; ?></td>
<?php if(isset($member)) { ?>
<?php if( hasAccess('services', 'shares', 'delete') ) { ?>
              <td class="text-right">
              <a class="confirm" href="<?php echo site_url("services_shares/delete_capital/{$capital->id}"); ?>"><span class="glyphicon glyphicon-trash"></span></a>
              </td>
<?php } ?>
<?php } ?>
            </tr>
<?php 
} 
?>
          </tbody>
          </table>

<ul class="list-group visible-xs">
  <?php foreach($shares_capital as $capital) { ?>
      <<?php echo (!isset($member)) ? 'a' : 'span'; ?> href="<?php echo site_url("services_shares/share_capital/{$capital->member_id}"); ?>" class="list-group-item <?php echo (!isset($member)) ? 'body_wrapper' : ''; ?>">
        <?php echo date("F d, Y", strtotime($capital->payment_date)); ?> <strong class="pull-right"><?php echo number_format($capital->amount,2); ?></strong>
      </<?php echo (!isset($member)) ? 'a' : 'span'; ?>>
    <?php } ?>
</ul>

<?php echo ($pagination!='') ? '<center>' . $pagination . '</center>' : ''; ?>

<?php } else { ?>
  <div class="text-center">No Capital Yet!</div>
<?php } ?>
<?php } else { ?>
  Please wait...
<?php } ?>

<?php if( ! $inner_page ): ?>

        </div>
      </div>
    </div>
</div>
</div>

<?php endif; ?>

<?php $this->load->view('footer'); ?>