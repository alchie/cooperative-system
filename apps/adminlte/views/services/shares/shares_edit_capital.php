<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( !isset($output) || ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('membership/members/members_navbar'); ?>

<?php $this->load->view('services/shares/shares_navbar'); ?>

<div class="container">
<div class="row">

	<div class="col-md-8 col-md-offset-2">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
                <a href="<?php echo site_url("services_shares/delete_capital/{$capital->id}/{$capital->member_id}"); ?>" class="btn btn-xs btn-danger pull-right confirm">Delete</a>
	    		<h3 class="panel-title">Edit Capital</h3>
	    	</div>
	    	<form method="post">
	    	<div class="panel-body">

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

<?php endif; ?>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label class="control-label">Payment Date</label>
            <input class="form-control datepicker text-center" type="text" name="date" value="<?php echo date('m/d/Y', strtotime($capital->payment_date)); ?>">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="control-label">Receipt Number</label>
            <input class="form-control text-center" type="text" name="receipt_number" value="<?php echo $capital->receipt_number; ?>">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="control-label">Amount</label>
            <input class="form-control text-right" type="text" name="amount" value="<?php echo number_format($capital->amount,2,'.',''); ?>">
        </div>
    </div>
</div>

<?php if( isset($output) && ($output=='ajax') ) { ?>
    <?php if( ! $no_delete ) { ?>
        <a href="<?php echo site_url("services_shares/delete_capital/{$capital->id}/{$capital->member_id}"); ?>" class="btn btn-xs btn-danger confirm">Delete Capital</a>
    <?php } ?>
<?php } ?>

<?php if( !isset($output) || ($output!='ajax') ) : ?>

	    	</div>
	    	<div class="panel-footer">

	    		<button type="submit" class="btn btn-success">Submit</button>
	    		<a href="<?php echo site_url( ($this->input->get('next')) ? $this->input->get('next') : "services_shares/share_capital/" . $member->id); ?>" class="btn btn-warning">Back</a>
	    	</div>

	    	</form>
	    </div>
    </div>
</div>
</div>

<?php $this->load->view('footer'); ?>

<?php endif; ?>