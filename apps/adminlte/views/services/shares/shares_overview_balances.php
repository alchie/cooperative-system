<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
<?php 
  if( isset( $member ) ) { 
      $this->load->view('membership/members/members_navbar'); 
    } else {
      $this->load->view('services/services_navbar');
   }

?>

<?php $this->load->view('services/shares/shares_navbar'); ?>

<div class="container">
<div class="row">
  <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">
     
          <h3 class="panel-title">Overview Balances</h3>

        </div>
        <div class="panel-body">


<?php if( $balances ) { ?>

 <table class="table table-condensed hidden-xs">
          <thead>
            <tr>
              <th class="text-left">Name</th>
              <th class="text-right">Amount</th>
            </tr>
          </thead>
          <tbody>
<?php 
$total_capital = 0;
foreach($balances as $balance) { ?>
            <tr>

              <td class="text-left"><a href="<?php echo site_url("services_shares/overview/{$balance->id}"); ?>"><?php echo $balance->lastname; ?>, <?php echo $balance->firstname; ?> <?php echo $balance->middlename; ?></a></td>
              <td class="text-right"><?php echo number_format($balance->balance,2); ?></td>
            </tr>
<?php 
} 
?>
          </tbody>
          </table>

<ul class="list-group visible-xs">
  <?php foreach($balances as $balance) { ?>

    <?php } ?>
</ul>

<?php echo ($pagination!='') ? '<center>' . $pagination . '</center>' : ''; ?>

<?php } else { ?>
  <div class="text-center">Nothing to display!</div>
<?php } ?>

        </div>
      </div>
    </div>
</div>
</div>



<?php $this->load->view('footer'); ?>