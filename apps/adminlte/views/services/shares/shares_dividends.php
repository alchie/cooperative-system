<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>

<?php if( ! $inner_page ): ?>

<?php 
  if( isset( $member ) ) { 
      $this->load->view('membership/members/members_navbar'); 
    } else {
      $this->load->view('services/services_navbar');
   }
?>

<?php $this->load->view('services/shares/shares_navbar'); ?>

<div class="container">
<div class="row">
  <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">
<?php if( hasAccess('services', 'shares', 'edit') ) { ?>
        <?php if( !isset( $member ) ) { ?>
          <button type="button" class="btn btn-success btn-xs pull-right ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Add Dividend" data-url="<?php echo site_url("services_shares/add_dividend/ajax") . "?next=" . uri_string(); ?>">Add Dividend</button>
        <?php } ?>        
<?php } ?>        
          <h3 class="panel-title">Dividends</h3>

        </div>
        <div class="panel-body" id="ajaxBodyInnerPage" data-url="<?php echo site_url(uri_string()); ?>">

<?php endif; ?>

<?php if( isset($dividends) ) { ?>
<?php if( $dividends ) { ?>

 <table class="table table-condensed hidden-xs">
          <thead>
            <tr>
              <th class="text-left">Resolution Date</th>
               <th class="text-center">Resolution #</th>
              <th class="text-right">Amount</th>
              
<?php if( !isset($member) ) { ?>
              <th class="text-right">Recipients</th>
              <th class="text-right" width="200px"></th>
<?php } ?>
            </tr>
          </thead>
          <tbody>
<?php 

foreach($dividends as $dividend) { ?>
  <tr class="<?php echo ($dividend->trn_id) ? '' : 'danger'; ?>">
              <td class="text-left"><?php echo date("F d, Y", strtotime($dividend->res_date)); ?></td>
              <td class="text-center">
<a href="<?php echo site_url("services_shares/dividend_recipients/{$dividend->id}"); ?>">
              <?php echo $dividend->res_number; ?>
</a>
</td>
              <td class="text-right"><?php echo number_format($dividend->amount,2);  ?></td>
              

<?php if( !isset($member) ) { ?>
              <td class="text-right"><?php echo number_format($dividend->total_recipients,2);  ?></td>
              <td class="text-right">
              <button class="btn btn-warning btn-xs ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Update Dividend" data-url="<?php echo site_url("services_shares/edit_dividend/{$dividend->id}/ajax") . "?next=" . uri_string(); ?>">Update</button>
              <a class="btn btn-primary btn-xs" href="<?php echo site_url("services_shares/dividend_recipients/{$dividend->id}"); ?>">Recipients</a>
              </td>
<?php } ?>

            </tr>
<?php 
} ?>
          </tbody>
          </table>

<ul class="list-group visible-xs">
  <?php foreach($dividends as $dividend) { ?>
      <a href="<?php echo site_url("services_shares/dividend_recipients/{$dividend->id}"); ?>" class="list-group-item">
        <?php echo date("F d, Y", strtotime($dividend->res_date)); ?> <strong class="pull-right"><?php echo number_format($dividend->amount,2); ?></strong>
      </a>
    <?php } ?>
</ul>

<?php echo ($pagination!='') ? '<center>' . $pagination . '</center>' : ''; ?>

<?php } else { ?>
  <div class="text-center">No Dividends Yet!</div>
<?php } ?>
<?php } else { ?>
  Please wait...
<?php } ?>

<?php if( ! $inner_page ): ?>

        </div>
      </div>
    </div>
</div>
</div>


<?php $this->load->view('footer'); ?>

<?php endif; ?>