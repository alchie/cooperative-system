<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('services/services_navbar'); ?>

<div class="container">
<div class="row">

	<div class="col-md-6 col-md-offset-3">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
	    		<h3 class="panel-title">Search Name</h3>
	    	</div>
	    	<form method="post">

<?php if( $names ) { ?>
<div class="list-group">
	<?php foreach($names as $name) { ?>
  		<a href="<?php echo site_url('services_lending/add/' .$name->id ); ?>" class="list-group-item">
  			<?php echo $name->lastname; ?>, <?php echo $name->firstname; ?> <?php echo $name->middlename; ?>
  		</a>
  	<?php } ?>
</div>
<?php } else { ?>
	<div class="text-center panel-body">No Name Found!</div>
<?php } ?>

	    	<div class="panel-footer">
	    		<button type="submit" class="btn btn-success">Submit</button>
	    		<a href="<?php echo site_url("membership_members"); ?>" class="btn btn-warning">Back</a>
	    	</div>


	    	</form>
	    </div>
    </div>
</div>
</div>

<?php $this->load->view('footer'); ?>