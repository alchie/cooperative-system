<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('services/services_navbar'); ?>

<?php $this->load->view('membership/members/members_navbar'); ?>

<div class="container">
<div class="row">

<div class="col-md-6 col-md-offset-3">

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

<?php endif; ?>


<?php if( $new_payments ) { ?>
<ul class="list-group">
    <?php foreach($new_payments as $new_payment) { ?>
   <a class="list-group-item ajax-modal-inner" href="<?php echo site_url("services_lending/pay/{$new_payment->ae_name_id}/{$new_payment->ae_id}/ajax") . "?next=" . $this->input->get('next'); ?>">
    <span class="badge"><?php echo number_format($new_payment->credit,2); ?></span>
    <h4 class="list-group-item-heading"><?php echo $new_payment->lastname; ?>, <?php echo $new_payment->firstname; ?> <?php echo $new_payment->middlename; ?></h4>
    <?php echo $new_payment->trn_number; ?> &middot;
    <?php echo date('F d, Y', strtotime($new_payment->trn_date)); ?>
  </a>
  <?php } ?>
</ul>

<?php echo ($pagination!='') ? '<center>' . $pagination . '</center>' : ''; ?>

<?php } else { ?>
<p class="text-center">No New Loan Payments!</p>
<?php } ?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

    </div>

</div>
</div>

<?php $this->load->view('footer'); ?>

<?php endif; ?>