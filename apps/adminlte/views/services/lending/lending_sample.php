<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<html>
<head>
  <title><?php echo $member->lastname; ?>, <?php echo $member->firstname; ?> <?php echo $member->middlename; ?> Sample Computation</title>
  <style>
    <!--
    body {
      font-family: Arial;
      padding:0;
      margin:0;
      font-size: 11px;
    }
    .container {
      width: 100%;
    }
    .wrapper {
      border: 1px solid #000;
      padding:10px;
      margin-bottom: 10px;
    }
    .allcaps {
      text-transform: uppercase;
    }
    .label {
      color:#000;
      text-transform: uppercase;
      font-size: 10px;
      text-decoration: underline;
    }
    .detail {
      font-size: 12px;
      margin-left: 10px;
      margin-top: 10px;
      font-weight: bold;
    }
    table td {
      vertical-align: middle;
      padding: 3px;
    }
    .border-bottom td {
      border-bottom: 1px solid #000;
    }
    .text-center {
      text-align: center;
    }
    th {
      font-size: 12px;
    }
    td {
      font-size: 12px;
    }
    .signature {
      border-top:1px solid #000;
      text-align: center;
      margin: 40px auto 0;
      font-size: 12px;
      font-weight: bold;
    }
    .header-label {
      margin: 0 0 20px 0;
      border-bottom: 1px solid #000;
      padding: 2px;
      background-color: #CCC;
    }
    .total {
      font-size: 12px;
      font-weight: bold;
    }
    -->
  </style>
</head>
<body>
<div class="wrapper">
<h3 class="text-center allcaps header-label">Lenders Info</h3>
<table width="100%" class="border-bottom">
    <tr>
      <td width="33.33%">
        <span class="label">Last Name</span>
        <div class="detail"><?php echo $member->lastname; ?></div>
      </td>
      <td width="33.33%">
        <span class="label">First Name</span>
        <div class="detail"><?php echo $member->firstname; ?></div>
      </td>
      <td width="33.33%">
        <span class="label">Middle Name</span>
        <div class="detail"><?php echo $member->middlename; ?></div>
      </td>
    </tr>
  </table>

<table width="100%" class="border-bottom">
    <tr>
      <td width="25%">
        <span class="label">Birthday</span>
        <div class="detail"><?php echo date('F d, Y', strtotime($member->birthdate)); ?></div>
      </td>
      <td width="25%">
        <span class="label">Birth Place</span>
        <div class="detail"><?php echo $member->birthplace; ?></div>
      </td>
      <td width="25%">
        <span class="label">Gender</span>
        <div class="detail"><?php echo ucwords($member->gender); ?></div>
      </td>
       <td width="25%">
        <span class="label">Marital Status</span>
        <div class="detail"><?php echo ucwords($member->marital_status); ?></div>
      </td>
    </tr>
  </table>
  <table width="100%" class="border-bottom">
    <tr>
      <td>
        <span class="label">Address</span>
        <div class="detail"><?php echo ($member->unit) ? $member->unit . "," : ""; ?> <?php echo ($member->building) ? $member->building. "," : ""; ?> <?php echo ($member->lot_block) ? $member->lot_block. "," : ""; ?> <?php echo ($member->street) ? $member->street. "," : ""; ?> <?php echo ($member->subdivision) ? $member->subdivision. "," : ""; ?> <?php echo ($member->barangay) ? $member->barangay. "," : ""; ?> <?php echo ($member->city) ? $member->city. "," : ""; ?> <?php echo ($member->province) ? $member->province. "," : ""; ?> <?php echo $member->zip; ?></div>
      </td>
    </tr>
  </table>
  <table width="100%" class="border-bottom">
    <tr>
      <td width="25%">
        <span class="label">Mobile Phone Number</span>
        <div class="detail"><?php echo $member->phone_mobile; ?></div>
      </td>
      <td width="25%">
        <span class="label">Home Phone Number</span>
        <div class="detail"><?php echo $member->phone_home; ?></div>
      </td>
       <td width="25%">
        <span class="label">Email Address</span>
        <div class="detail"><?php echo $member->email; ?></div>
      </td>
    </tr>
  </table>

  </div>
<div class="wrapper">
<?php 

$installments = ceil((30 / $current_loan->skip_days) * $current_loan->months);
$payment_date = date('m/d/Y', strtotime($current_loan->payment_start));
$principal = $current_loan->principal;
$principal_diminishing = $current_loan->principal;
$principal_cancel = $current_loan->principal;
$principal_installment = ($current_loan->principal/$installments);
$principal_total = 0;
$principal_decrement = $current_loan->principal;
$interest_total = 0;
$due_increment = 0;

if( $current_loan->interest_type == 'diminishing' ) {
  $principal_interest = $current_loan->principal + ((($current_loan->principal * ($current_loan->interest_rate / 100) ) / 2 ) * ($installments + 1) * (1/ceil((30 / $current_loan->skip_days))));
  $total_interest = ((($current_loan->principal * ($current_loan->interest_rate / 100) ) / 2 ) * ($installments + 1) * (1/ceil((30 / $current_loan->skip_days))));
} else {
  $interest = (($current_loan->principal * $current_loan->interest_rate) / 100) / ceil(30 / $current_loan->skip_days);
  $total_interest = ((($current_loan->principal * $current_loan->interest_rate) / 100) / ceil(30 / $current_loan->skip_days) * $installments);
  $principal_interest = $current_loan->principal + ($interest * $installments);
}

?>
<h3 class="text-center allcaps header-label">Loan Details</h3>
<table width="100%" >
    <tr>
      <td width="20%">
        <span class="label">Date of Loan</span>
        <div class="detail"><?php echo date('F d, Y', strtotime($current_loan->loan_date)); ?></div>
      </td>
      <td width="20%">
        <span class="label">Principal Amount</span>
        <div class="detail"><?php echo number_format($principal,2); ?></div>
      </td>
      <td width="20%">
        <span class="label">Interest Rate</span>
        <div class="detail"><?php echo round($current_loan->interest_rate,2); ?>% Monthly</div>
      </td>
      <td width="20%">
        <span class="label"># of Months</span>
        <div class="detail"><?php echo $current_loan->months; ?> Months</div>
      </td>
      <td width="20%">
        <span class="label">Total Addon Interest</span>
        <div class="detail"><?php echo number_format($total_interest,2); ?></div>
      </td>
    </tr>
  </table>
</div>
<div class="wrapper">
<h3 class="text-center allcaps header-label">Schedule of Fees</h3>
        <table class="table table-condensed" width="100%">
          <thead>
            <tr>
              <th class="text-center" width="1%">#</th>
              <th class="text-center">Date</th>
              <th class="text-center" colspan="3">Principal</th>
               <th class="text-right">Interest</th>
               <th class="text-center" colspan="2">Amount Due</th>
               <th class="text-center">Buy Back</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td class="text-center"></td>
              <td></td>
              <td></td>
              <td></td>
              <td class="text-center"><?php echo number_format($principal,2); ?></td>
              <td></td>
              <td></td>
              <td class="text-center"><?php echo number_format($principal_interest,2); ?></td>
              <td class="text-center"></td>
            </tr>
<?php 
$nn=0;
foreach($invoices as $invoice) { 
    $principal_total += $invoice->principal_due;
    $principal_decrement -= $invoice->principal_due;
    $principal_interest -= ($invoice->principal_due + $invoice->interest_due);
    $due_increment += ($invoice->principal_due + $invoice->interest_due);
    $interest_total += $invoice->interest_due;

    $nn++;
?>
            <tr>
              <td class="text-center"><?php echo $nn; ?></td>
              <td class="text-center"><?php echo date('m/d/Y', strtotime($invoice->due_date)); ?></td>
              <td class="text-center"><?php echo number_format($principal_cancel,2); ?></td>
              <td class="bold text-center"><?php echo number_format($invoice->principal_due,2); ?></td>
              <td class="text-center"><?php echo number_format($principal_decrement,2); ?></td>
              <td class="bold text-center"><?php echo number_format($invoice->interest_due,2); ?></td>
              <td class="bold text-center"><?php echo number_format($invoice->principal_due + $invoice->interest_due,2); ?></td>
              <!--<td class="text-center"><?php echo number_format($due_increment,2); ?></td>-->
              <td class="text-center"><?php echo number_format($principal_interest,2); ?></td>
              <td class="text-center"><?php echo number_format($principal_cancel + $invoice->interest_due,2); ?></td>
            </tr>
<?php 
 $principal_cancel -= $invoice->principal_due;
} ?>
            <tr>
              <td class="text-center"></td>
              <td></td>
              <td></td>
              <td class="bold text-center total"><?php echo number_format($principal_total,2); ?></td>
              <td></td>
              <td class="bold text-center total"><?php echo number_format($interest_total,2); ?></td>
              <td class="bold text-center total"><?php echo number_format($due_increment,2); ?></td>
              <td></td>
              
            </tr>
          </tbody>
        </table>


</div>
<div class="wrapper">
<table width="100%">
    <tr>
       
      <td width="25%" class="text-center">
        <div class="signature allcaps"><?php echo strtoupper($member->lastname); ?>, <?php echo strtoupper($member->firstname); ?> <?php echo strtoupper(substr($member->middlename, 0, 1)); ?>.</div>
        <div class="allcaps">Lender</div>
      </td>
      <td width="25%">
       </td>
       <td width="25%">
       </td>
      <td width="25%" class="text-center">
        <div class="signature allcaps"><?php echo $releaser->name; ?></div>
        <div class="allcaps">Released By</div>
      </td>

  </tr>
  </table>
    
</div> 
</body>
</html>