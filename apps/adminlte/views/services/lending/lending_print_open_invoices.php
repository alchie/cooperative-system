<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<html>
<head>
  <title>Open Invoices</title>
  <style>
    <!--
    body {
      font-family: Arial;
      padding:0;
      margin:0;
      font-size: 11px;
    }
    .container {
      width: 100%;
    }
    .wrapper {
      border: 1px solid #000;
      padding:10px;
      margin-bottom: 10px;
    }
    .allcaps {
      text-transform: uppercase;
    }
    .label {
      color:#000;
      text-transform: uppercase;
      font-size: 10px;
      text-decoration: underline;
    }
    .detail {
      font-size: 12px;
      margin-left: 10px;
      margin-top: 10px;
      font-weight: bold;
    }
    table td {
      vertical-align: middle;
      padding: 3px;
    }
    .border-bottom td {
      border-bottom: 1px solid #000;
    }
    .text-left {
      text-align: left;
    }
    .text-center {
      text-align: center;
    }
    .text-right {
      text-align: right;
    }
    th {
      font-size: 14px;
    }
    td {
      font-size: 14px;
    }
    .signature {
      border-top:1px solid #000;
      text-align: center;
      margin: 40px auto 0;
      font-size: 12px;
      font-weight: bold;
    }
    .header-label {
      margin: 0 0 20px 0;
      border-bottom: 1px solid #000;
      padding: 2px;
      background-color: #CCC;
    }
    .total {
      font-size: 12px;
      font-weight: bold;
    }
    td {
      border: 1px solid #000;
      height: 50px;
    }
    .pull-right {
      float: right;
    }
    -->
  </style>
</head>
<body>
<div class="wrapper">

        <table class="table table-condensed" width="100%">
          <thead>
          <tr>
            <th colspan="7">
<h3 class="text-left allcaps header-label">
<span class="pull-right">as of <?php echo date("F d, Y"); ?></span>
Open Invoices</h3>
            </th>
          </tr>
            <tr>
              <th class="text-left" width="20%">Name</th>
              <th class="text-center" width="10%">Total Principal</th>
              <th class="text-center" width="10%">Total Interest</th>
              <th class="text-center" width="10%">Total Amount Due</th>
               <th class="text-center" width="10%">Collection Date</th>
               <th class="text-center" width="10%">Receipt Number</th>
               <th class="text-center" width="10%">Amount</th>
            </tr>
          </thead>
          <tbody>
<?php 
$n=1;
foreach($invoices as $invoice) { ?><?php //print_r($invoice); ?>
   <tr>
      <td><strong><?php echo $n++; ?>. <?php echo $invoice->lastname; ?>, <?php echo $invoice->firstname; ?> <?php echo $invoice->middlename; ?></strong>
        <br><small><?php echo $invoice->phone_mobile; ?> &middot; <?php echo $invoice->phone_home; ?></small>
      </td>
      <td class="text-center"><?php echo number_format($invoice->total_principal_due,2); ?></td>
      <td class="text-center"><?php echo number_format($invoice->total_interest_due,2); ?></td>
      <td class="text-center"><?php echo number_format(($invoice->total_principal_due + $invoice->total_interest_due),2); ?></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
<?php } ?>
          </tbody>
        </table>

</div> 
</body>
</html>