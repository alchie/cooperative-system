<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('services/services_navbar'); ?>

<?php $this->load->view('membership/members/members_navbar'); ?>

<div class="container">
<div class="row">

<div class="col-md-6 col-md-offset-3">

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

<?php endif; ?>


<?php if( $payments ) { ?>
<ul class="list-group">
    <?php foreach($payments as $payment) { ?>
   <a class="list-group-item" href="<?php echo site_url("services_lending/payment_apply/{$payment->id}") . "?next=" . $this->input->get('next'); ?>">
    <span class="badge"><?php echo number_format($payment->amount,2); ?></span>
    <h4 class="list-group-item-heading"><?php echo $payment->lastname; ?>, <?php echo $payment->firstname; ?> <?php echo $payment->middlename; ?></h4>
    <?php echo $payment->receipt_number; ?> &middot;
    <?php echo date('F d, Y', strtotime($payment->payment_date)); ?>
  </a>
  <?php } ?>
</ul>

<?php echo ($pagination!='') ? '<center>' . $pagination . '</center>' : ''; ?>

<?php } else { ?>
<p class="text-center">No New Loan Payments!</p>
<?php } ?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

    </div>

</div>
</div>

<?php $this->load->view('footer'); ?>

<?php endif; ?>