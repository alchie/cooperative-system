<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( !isset($output) || ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('services/services_navbar'); ?>

<?php $this->load->view('membership/members/members_navbar'); ?>

<div class="container">
<div class="row">

	<div class="col-md-8 col-md-offset-2">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
	    		<h3 class="panel-title">Add Payment</h3>
	    	</div>

	    	<form method="post">
	    	<div class="panel-body">


<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

<?php endif; ?>

<?php if( isset($new_payment) && ($new_payment) ) { ?>


<?php if( isset( $member ) ) { ?>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label">Received From</label>
                <div class="form-control bold"><?php echo $member->lastname; ?>, <?php echo $member->firstname; ?> <?php echo $member->middlename; ?></div>
            </div>
        </div>
    </div>
<?php } ?>
    <input type="hidden" name="hashcode" value="<?php echo sha1($new_payment->trn_id); ?>" ?>
<div class="row">
    <div class="col-md-4">
	    <div class="form-group">
            <label class="control-label">Payment Date</label>
            <div class="form-control text-center"><?php echo date('m/d/Y', strtotime($new_payment->trn_date)); ?></div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="control-label">Receipt Number</label>
            <div class="form-control text-center"><?php echo $new_payment->trn_number; ?></div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="control-label">Amount</label>
            <div class="form-control text-right"><?php echo number_format($new_payment->credit,2); ?></div>
        </div>
    </div>

</div>

<?php } ?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

	    	</div>
	    	<div class="panel-footer">
	    		<button type="submit" class="btn btn-success">Submit</button>
<?php if( isset($current_loan) ) { ?>
	    		<a href="<?php echo site_url("services_lending/loan_payments/{$current_loan->id}" ); ?>" class="btn btn-warning">Back</a>
<?php } else { ?>
            <a href="<?php echo site_url("services_lending/payments/{$member->id}" ); ?>" class="btn btn-warning">Back</a>
<?php }  ?>
	    	</div>

	    	</form>

	    </div>
    </div>
</div>
</div>

<?php $this->load->view('footer'); ?>

<?php endif; ?>