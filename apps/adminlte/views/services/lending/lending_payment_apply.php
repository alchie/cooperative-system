<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( !isset($output) || ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('membership/members/members_navbar'); ?>

<?php $this->load->view('services/lending/lending_navbar'); ?>

<div class="container">
<div class="row">

	<div class="col-md-8 col-md-offset-2">

<?php endif; ?>

	    <div class="panel panel-success">
	    	<div class="panel-heading">
	    		<h3 class="panel-title">Payment Details</h3>
	    	</div>

	    	<div class="panel-body">

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label class="control-label">Payment Date</label>
            <div class="form-control text-center"><?php echo date('m/d/Y', strtotime($payment->payment_date)); ?></div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="control-label">Receipt Number</label>
            <div class="form-control text-center"><?php echo $payment->receipt_number; ?></div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="control-label">Amount Paid</label>
            <div class="form-control text-right"><?php echo number_format($payment->amount,2,".",","); ?></div>
        </div>
    </div>

</div>

</div>
</div>

<?php if( $open_invoices ) { ?>

<form method="post">
      <div class="panel panel-default">
        <div class="panel-heading">

        <?php if( !isset($output) || ($output!='ajax') ) : ?>

          <button type="submit" class="btn btn-success pull-right btn-xs">Submit</button>
<div class="btn-group pull-right" role="group" style="margin-right: 10px;">
          <button type="button" class="btn btn-default btn-xs payment_autoapply" data-max_amount="<?php echo round($payment->amount,2); ?>" data-input="amount-input">Auto Apply</button>
          <button type="button" class="btn btn-default btn-xs payment_unapply" data-input="amount-input">Un-Apply</button>
</div>

<?php endif; ?>

          <h3 class="panel-title">Open Invoices
<?php if( $payment->total_applied ) { ?>
<a class="confirm" href="<?php echo site_url("services_lending/reset_payment_apply/{$payment->id}"); ?>"><span class="glyphicon glyphicon-refresh"></span></a>
<?php } else { ?>
  <a class="confirm" href="<?php echo site_url("services_lending/payment_delete/{$payment->id}/{$payment->member_id}"); ?>"><span class="glyphicon glyphicon-trash"></span></a>
<?php } ?>

          </h3>
        </div>

        <div class="panel-body">

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>



<table class="table table-condensed table-hover">
          <thead>
            <tr>
              <th class="text-center">Due Date</th>
              <th class="text-right hidden-xs" >Principal</th>
               <th class="text-right hidden-xs">Interest</th>
               <th class="text-right">Amount Due</th>
               <th class="text-right">Amount to Apply</th>
            </tr>
          </thead>
          <tbody>
<?php 
$principal_due = 0;
$interest_due = 0;
$due_increment = 0;
$total_applied = 0;
$n=0;
foreach($open_invoices as $invoice) { 
    
    $amount_due = ((($invoice->principal_due + $invoice->interest_due) - $invoice->total_applied) + $invoice->applied);

    if( round($amount_due,2) <= 0) {
      continue;
    }
    $total_applied += $invoice->applied;
    $principal_due += $invoice->principal_due;
    $interest_due += $invoice->interest_due;
    $due_increment += $amount_due;

?>
            <tr class="<?php 
$due_already = '';            
if( time() > strtotime($invoice->due_date) ) {
  $due_already = 'danger'; 
}
$class_name = (round($total_applied) >= round($due_increment)) ? 'success' : $due_already; 

if( $invoice->applied ) {
  $class_name = 'info';
}

echo $class_name;
?>">
              <td class="text-center"><?php echo date('m/d/Y', strtotime($invoice->due_date)); ?></td>
              <td class="bold text-right hidden-xs"><?php echo number_format($invoice->principal_due,2); ?></td>
              <td class="bold text-right hidden-xs"><?php echo number_format($invoice->interest_due,2); ?></td>
              <td class="bold text-right">
              <input name="amount_due[<?php echo $invoice->id; ?>]" type="hidden" value="<?php echo $invoice->amount_due; ?>">
              <?php echo number_format($invoice->amount_due,2); ?></td>
              <td class="bold text-right"><input name="apply_invoice[<?php echo $invoice->id; ?>]" type="text" class="form-control text-right amount-input" value="<?php 
echo number_format((($invoice->applied) ? $invoice->applied : 0.00),2,".",""); ?>" data-limit="<?php echo number_format($invoice->amount_due,2,".",""); 
?>"></td>
               </tr>
<?php 
$n++;
} ?>
<?php if( $n>1) { ?>
<tr>
  <td class="text-left allcaps bold">Total</td>
  <td class="text-right allcaps bold hidden-xs"><?php echo number_format($principal_due,2); ?></td>
  <td class="text-right allcaps bold hidden-xs"><?php echo number_format($interest_due,2); ?></td>
  <td class="text-right allcaps bold"><?php echo number_format($due_increment,2); ?></td>
  <td class="bold text-right"><span class="form-control" id="autosum-output"><?php echo number_format($total_applied,2); ?></span></td>
</tr>
<?php } ?>
          </tbody>
        </table>

<?php if( !isset($output) || ($output!='ajax') ) : ?>
		
	    	</div>
	    	<div class="panel-footer">
	    		<button type="submit" class="btn btn-success">Submit</button>
	    		<a href="<?php echo site_url( ($this->input->get('next')) ? $this->input->get('next') : "services_lending/payments/" . $member->id); ?>" class="btn btn-warning body_wrapper">Back</a>
	    	</div>

<?php endif; ?>

	    </div>

<?php if( !isset($output) || ($output!='ajax') ) : ?>

</form>

<?php endif; ?>

<?php } else { ?>

    <div class="panel panel-default">
    <div class="panel-body">
      <p class="text-center">No Invoices Found!</p>
    </div>
  </div>
<?php } ?>

<?php if( !isset($output) || ($output!='ajax') ) : ?>

    </div>
</div>
</div>

<?php $this->load->view('footer'); ?>

<?php endif; ?>