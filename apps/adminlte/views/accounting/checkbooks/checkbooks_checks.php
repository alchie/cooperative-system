<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
<?php if( ! $inner_page ): ?>
<?php $this->load->view('accounting/settings/settings_navbar'); ?>

<div class="container">
<div class="row">
	<div class="col-md-12">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
	    		<h3 class="panel-title">Checks</h3>
	    	</div>
	    	<div class="panel-body" id="ajaxBodyInnerPage" data-url="<?php echo site_url(uri_string()); ?>">
<?php endif; ?>

<?php if( isset($checks) ) { ?>
<?php if( $checks ) { ?>
	    		<table class="table table-default table-hover table-condensed">
	    			<thead>
	    				<tr>
	    					<th>Bank Account</th>
	    					<th class="text-right">Check Number</th>
	    				</tr>
	    			</thead>
	    			<tbody>
	    			<?php foreach($checks as $check) { ?>
	    				<tr class="<?php echo ($check->used) ? "success" : ""; ?>">
	    					<td><?php echo $check->account_title; ?></td>
	    					<td class="text-right">
	    					<?php if($check->used) { ?>
	    					<a href="<?php echo site_url("accounting_write_checks/check_items/{$check->used}"); ?>" class="body_wrapper">
	    					<?php } ?>
	    					<?php echo $check->check_number; ?>
	    						<?php if($check->used) { ?>
	    						</a>
	    					<?php } ?>
	    					</td>
	    				</tr>
	    			<?php } ?>
	    			</tbody>
	    		</table>

	    		<?php echo ($pagination!='') ? '<center>' . $pagination . '</center>' : ''; ?>
	    		
<?php } else { ?>
	<div class="text-center">No Journal Found!</div>
<?php } ?>
<?php } else { ?>
	Please wait...
<?php } ?>

<?php if( ! $inner_page ): ?>
	    	</div>
	    </div>
    </div>
</div>
</div>
<?php endif; ?>
<?php $this->load->view('footer'); ?>