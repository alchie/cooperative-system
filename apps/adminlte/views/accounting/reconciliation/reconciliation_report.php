<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>

<?php $this->load->view('accounting/accounting_navbar'); ?>

<div class="container">
<div class="row">
	<div class="col-md-12">
	    <div class="panel panel-default">
	    	<div class="panel-heading">

        <a target="print_summary" href="<?php echo site_url("accounting_reconciliation/print_summary/{$recon->id}"); ?>" class="pull-right btn btn-warning btn-xs">Print Summary</a>

	    		<h3 class="panel-title">Reconciliation Summary - <span class="badge"><?php echo date('F Y', strtotime($recon->date_end)); ?></span>
<?php if( hasAccess('accounting', 'reconciliation', 'edit') ) { ?>
<a href="#" class="ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Edit Recon" data-url="<?php echo site_url("accounting_reconciliation/edit_recon/{$recon->id}/ajax") . "?next=" . uri_string(); ?>"><span class="glyphicon glyphicon-pencil"></span></a>
<?php } ?>
          </h3>
	    	</div>
	    	        
        <div class="panel-body">
 
<table class="table table-default table-hover table-condensed">
  <tr class="success">
    <th colspan="2">Beginning Balance</th>
    <td class="text-right"></td>
    <td class="text-right"></td>
    <td class="text-right bold"><?php echo ($last_recon) ? number_format( $last_recon->balance_end, 2) : '0.00'; ?></td>
  </tr> 
  <tr class="warning">
    <th colspan="2">ADD: Deposits</th>
     <td class="text-right"></td>
    <td class="text-right bold"><?php echo number_format( $recon->total_deposit, 2); ?></td>
    <td class="text-right"></td>
  </tr>
<?php foreach($deposits as $deposit) {  ?>
  <tr class="<?php echo ($deposit->trn_id==$this->input->get('highlight')) ? 'info' : ''; ?>">
    <th></th>
    <th>
      <?php 
    $data_ar = array();
    $data_ar[] = date('m/d/Y', strtotime($deposit->date));
    $data_ar[] = $deposit->trn_type;
    if( $deposit->number ) {
      $data_ar[] = $deposit->number;
    }
    if( $deposit->memo ) {
      $data_ar[] = $deposit->memo;
    }
    echo implode($data_ar, " &middot; "); ?>
    </th>
    <td class="text-right"><?php echo number_format( $deposit->total_amount, 2); ?></td>
    <td class="text-right"></td>
    <td class="text-right"></td>
  </tr>
<?php } ?>
    <tr class="warning">
    <th colspan="2">LESS: Disbursements</th>
     <td class="text-right"></td>
    <td class="text-right bold"><?php echo number_format( $recon->total_disburse, 2); ?></td>
    <td class="text-right"></td>
  </tr>
<?php foreach($disbursements as $disbursement) { ?>
  <tr class="<?php echo ($disbursement->trn_id==$this->input->get('highlight')) ? 'info' : ''; ?>">
    <th></th>
    <th>
      <?php 
       $data_ar = array();
       $data_ar[] = date('m/d/Y', strtotime($disbursement->date));
       $data_ar[] = $disbursement->trn_type;
       if( $disbursement->number ) {
        $data_ar[] = $disbursement->number; 
      }
       if( $disbursement->memo ) {
        $data_ar[] = $disbursement->memo; 
        }
        echo implode($data_ar, " &middot; ");
   ?>
    </th>
    <td class="text-right"><?php echo number_format( $disbursement->total_amount, 2); ?></td>
    <td class="text-right"></td>
    <td class="text-right"></td>
  </tr>
<?php } ?>
  <tr class="success">
    <th colspan="2">Ending Balance</th>
    <td class="text-right"></td>
    <td class="text-right"></td>
    <td class="text-right bold"><?php echo number_format( $recon->balance_end, 2); ?></td>
  </tr>  
</table>

        </div>

	    </div>
    </div>
</div>
</div>

<?php $this->load->view('footer'); ?>