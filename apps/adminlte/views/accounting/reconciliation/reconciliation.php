<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
<?php if( ! $inner_page ): ?>
<?php $this->load->view('accounting/accounting_navbar'); ?>

<div class="container">
<div class="row">
	<div class="col-md-12">
	    <div class="panel panel-default">
	    	<div class="panel-heading">

<?php if( hasAccess('accounting', 'reconciliation', 'add') ) { ?>
<div class="btn-group pull-right hidden-print">
  <button type="button" class="btn btn-success btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Add Recon <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
  <?php if( $bank_accounts ) { ?>
    <?php foreach($bank_accounts as $acct) { ?>
      <li> 
      <a class="ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Add Recon : <?php echo $acct->title; ?>" data-url="<?php echo site_url("accounting_reconciliation/add_recon/{$acct->id}/ajax") . "?next=" . uri_string(); ?>"><?php echo $acct->title; ?></a>
      </li>
    <?php } ?>
  <?php } ?>
  </ul>
</div>
<?php } ?>

	    		<h3 class="panel-title">Bank Reconciliation</h3>
	    	</div>
	    	<div class="panel-body" id="ajaxBodyInnerPage"  data-url="<?php echo site_url(uri_string()); ?>">
<?php endif; ?>
<?php if( isset($recons) ) { ?>
<?php if( $recons ) { ?>
          <table class="table table-default table-hover table-condensed">
            <thead>
              <tr>
                <th width="15%">Bank Account</th>
                <th class="text-left">Month</th>
                <th class="text-left">Ending Date</th>

                <th class="text-right">Ending Balance</th>
                <th class="text-right hidden-print" width="100px">Actions</th>
              </tr>
            </thead>
            <tbody>
<?php foreach($recons as $recon) { ?>
	<tr class="<?php echo ($recon->recon_items) ? "" : "danger"; ?>"> 
		<td><?php echo $recon->bank_name; ?></td>
		<td class="text-left"><span class="badge"><?php echo date('F Y', strtotime($recon->date_end)); ?></span></td>
		<td class="text-left"><?php echo date('m/d/Y', strtotime($recon->date_end)); ?></td>

		<td class="text-right"><?php echo number_format($recon->balance_end,2); ?></td>
		<td class="text-right hidden-print">
<?php if($recon->recon_items) { ?>
	<a href="<?php echo site_url("accounting_reconciliation/recon_items/" . $recon->id); ?>" class="btn btn-primary btn-xs body_wrapper">Report</a>
<?php } else { ?>
  <?php if( hasAccess('accounting', 'reconciliation', 'add') ) { ?>
	<a href="<?php echo site_url("accounting_reconciliation/recon_items/" . $recon->id); ?>" class="btn btn-danger btn-xs body_wrapper">Reconcile</a>
  <?php } ?>
<?php } ?>
		</td>
	</tr>
<?php } ?>
            </tbody>
          </table>

<?php echo ($pagination!='') ? '<center>' . $pagination . '</center>' : ''; ?>

<?php } else { ?>
  <p class="text-center">No Reconciliation Found!</p>
<?php } ?>
<?php } else { ?>
	Please wait... loading data...
<?php } ?>

<?php if( ! $inner_page ): ?>
	    	</div>
	    </div>
    </div>
</div>
</div>
<?php endif; ?>
<?php $this->load->view('footer'); ?>