<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<html>
<head>
  <title>Bank Reconciliation Print</title>
  <style>
    <!--
    body {
      font-family: Arial;
      padding:0;
      margin:0;
      font-size: 12px;
    }
    .container {
      widtd: 100%;
    }
    .wrapper {
      border: 1px solid #000;
      padding:10px;
      margin-bottom: 10px;
    }
    .allcaps {
      text-transform: uppercase;
    }
    .label {
      color:#000;
      text-transform: uppercase;
      text-decoration: underline;
    }
    .detail {
      font-size: 12px;
      margin-left: 10px;
      margin-top: 10px;
      font-weight: bold;
    }
    table td {
      vertical-align: middle;
      padding: 3px;
    }
    .border-bottom td {
      border-bottom: 1px solid #000;
    }
    .text-center {
      text-align: center;
    }
    td {
      font-size: 12px;
    }
    td {
      font-size: 12px;
    }
    .signature {
      border-top:1px solid #000;
      text-align: center;
      margin: 40px auto 0;
      font-size: 12px;
      font-weight: bold;
    }
    .header-label {
      margin: 0 0 20px 0;
      border-bottom: 1px solid #000;
      padding: 10px;
      background-color: #EEE;
    }
    .header-label small {
      font-weight: normal;
    }
    .total {
      font-size: 12px;
      font-weight: bold;
    }
    .text-right {
      text-align: right;
    }
    table td {
      border-bottom: 1px solid #CCC;
    }
    tr.overalltotal {
      background-color: #EEE;
    }
    tr.overalltotal td {
      padding: 5px;
      font-size: 14px;
    }
    .bold {
      font-weight: bold;
    }
    -->
  </style>
</head>
<body>
<div class="wrapper">

<h3 class="text-center allcaps header-label">Bank Reconciliation Summary
<br><small><?php echo date('F Y', strtotime($recon->date_end)); ?></small>
</h3>

<table width="100%">
  <tr class="success">
    <td colspan="2">Beginning Balance</td>
    <td class="text-right"></td>
    <td class="text-right"></td>
    <td class="text-right bold"><?php echo ($last_recon) ? number_format( $last_recon->balance_end, 2) : '0.00'; ?></td>
  </tr> 
  <tr class="warning">
    <td colspan="2">ADD: Deposits</td>
     <td class="text-right"></td>
    <td class="text-right bold"><?php echo number_format( $recon->total_deposit, 2); ?></td>
    <td class="text-right"></td>
  </tr>
<?php foreach($deposits as $deposit) {  ?>
  <tr class="<?php echo ($deposit->trn_id==$this->input->get('highlight')) ? 'info' : ''; ?>">
    <td></td>
    <td><?php 
    $data_ar = array();
    $data_ar[] = date('m/d/Y', strtotime($deposit->date));
    $data_ar[] = $deposit->trn_type;
    if( $deposit->number ) {
      $data_ar[] = $deposit->number;
    }
    if( $deposit->memo ) {
      $data_ar[] = $deposit->memo;
    }
    echo implode($data_ar, " &middot; "); ?></td>
    <td class="text-right"><?php echo number_format( $deposit->total_amount, 2); ?></td>
    <td class="text-right"></td>
    <td class="text-right"></td>
  </tr>
<?php } ?>
    <tr class="warning">
    <td colspan="2">LESS: Disbursements</td>
     <td class="text-right"></td>
    <td class="text-right bold"><?php echo number_format( $recon->total_disburse, 2); ?></td>
    <td class="text-right"></td>
  </tr>
<?php foreach($disbursements as $disbursement) { ?>
  <tr class="<?php echo ($disbursement->trn_id==$this->input->get('highlight')) ? 'info' : ''; ?>">
    <td></td>
    <td><?php 
       $data_ar = array();
       $data_ar[] = date('m/d/Y', strtotime($disbursement->date));
       $data_ar[] = $disbursement->trn_type;
       if( $disbursement->number ) {
        $data_ar[] = $disbursement->number; 
      }
       if( $disbursement->memo ) {
        $data_ar[] = $disbursement->memo; 
        }
        echo implode($data_ar, " &middot; ");
   ?></td>
    <td class="text-right"><?php echo number_format( $disbursement->total_amount, 2); ?></td>
    <td class="text-right"></td>
    <td class="text-right"></td>
  </tr>
<?php } ?>
  <tr class="success">
    <td colspan="2">Ending Balance</td>
    <td class="text-right"></td>
    <td class="text-right"></td>
    <td class="text-right bold"><?php echo number_format( $recon->balance_end, 2); ?></td>
  </tr>  
</table>


</div> 
</body>
</html>