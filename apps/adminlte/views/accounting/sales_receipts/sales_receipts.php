<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>

<?php if( ! $inner_page ): ?>
<?php $this->load->view('accounting/accounting_navbar'); ?>

<div class="container">
<div class="row">
	<div class="col-md-12">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
<?php if(hasAccess('accounting', 'sales_receipts', 'add')) { ?>
                <button type="button" class="btn btn-success btn-xs pull-right ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Record Receipt" data-url="<?php echo site_url("accounting_sales_receipts/record_receipt/ajax") . "?next=" . uri_string(); ?>">Record Receipt</button>
<?php } ?>
	    		<h3 class="panel-title">Sales Receipts
<div class="btn-group">
  <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <?php echo (isset($receipts_page_title)) ? $receipts_page_title : 'All Receipts'; ?> <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
    <li class="<?php echo (isset($receipts_page_title)&&($receipts_page_title=='All Receipts')) ? 'active' : ''; ?>"><a class="ajaxPage" href="<?php echo site_url("accounting_sales_receipts/index/all"); ?>">All Receipts</a></li>
    <li class="<?php echo (isset($receipts_page_title)&&($receipts_page_title=='Deposited Receipts')) ? 'active' : ''; ?>"><a class="ajaxPage" href="<?php echo site_url("accounting_sales_receipts/index/deposited"); ?>">Deposited Receipts</a></li>
    <li class="<?php echo (isset($receipts_page_title)&&($receipts_page_title=='Undeposited Receipts')) ? 'active' : ''; ?>"><a class="ajaxPage" href="<?php echo site_url("accounting_sales_receipts/index/undeposited"); ?>">Undeposited Receipts</a></li>
    <li class="<?php echo (isset($receipts_page_title)&&($receipts_page_title=='Incomplete Receipts')) ? 'active' : ''; ?>"><a class="ajaxPage" href="<?php echo site_url("accounting_sales_receipts/index/incomplete"); ?>">Incomplete Receipts</a></li>
  </ul>
</div>
                </h3>
	    	</div>
	    	<div class="panel-body" id="ajaxBodyInnerPage" data-url="<?php echo site_url(uri_string()); ?>">
<?php endif; // inner_page ?>

<?php if( isset($receipts) ) { ?>
<?php if( $receipts ) { ?>
          <table class="table table-default table-hover table-condensed">
            <thead>
              <tr>
                <th width="10px"></th>
                <th>Date</th>
                <th>Receipt Number</th>
                <th>Name</th>
                <th class="text-right">Amount</th>
                <th class="text-right" width="150px">Actions</th>
              </tr>
            </thead>
            <tbody>
<?php foreach($receipts as $receipt) { ?>
<tr class="<?php echo ((round($receipt->balance) > 0)||($receipt->trn_id=='')||($receipt->trn_exist==0)||($receipt->debit_cleared!=0)||($receipt->credit_cleared!=0)) ? 'danger' : ''; ?>">
    <td class="text-center">
<?php if( $receipt->deposit_id ) { ?>
    <a href="<?php echo site_url("accounting_deposits/edit_deposit/{$receipt->deposit_id}"); ?>?highlight=<?php echo $receipt->trn_id; ?>" class="item body_wrapper"><span class="fa fa-bank"></span></a>
<?php } ?>
    </td>
    <td><?php echo date('F d, Y', strtotime($receipt->receipt_date)); ?></td>
    <td><?php echo $receipt->receipt_number; ?></td>
    <td><?php echo $receipt->full_name; ?></td>

    <td class="text-right"><?php echo number_format($receipt->amount,2); ?></td>
    <td class="text-right">
    <?php if(hasAccess('accounting', 'sales_receipts', 'edit')) { ?>
    <button type="button" class="btn btn-warning btn-xs ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Edit Receipt" data-url="<?php echo site_url("accounting_sales_receipts/edit_receipt/{$receipt->id}/ajax") . "?next=" . uri_string(); ?>">Edit</button>
    <?php } ?>
    <a href="<?php echo site_url("accounting_sales_receipts/receipt_items/" . $receipt->id); ?>" class="btn btn-primary btn-xs">Items</a>
    </td>
 </tr>
<?php } ?>
            </tbody>
          </table>

<?php echo ($pagination!='') ? '<center>' . $pagination . '</center>' : ''; ?>


<?php } else { ?>
  <p class="text-center">No Receipt Found!</p>
<?php } ?>

<?php } else { ?>
  Please wait... loading data...
<?php } ?>

<?php if( ! $inner_page ): ?>
	    	</div>
	    </div>
    </div>
</div>
</div>
<?php endif; // inner_page ?>
<?php $this->load->view('footer'); ?>