<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>

<?php $this->load->view('accounting/accounting_navbar'); ?>

<div class="container">
<div class="row">
	<div class="col-md-12">

<form method="get" action="<?php echo site_url("accounting_deposits/add_deposit/{$bank->id}"); ?>">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
	    		<a href="<?php echo site_url('accounting_deposits'); ?>" class="btn btn-warning btn-xs pull-right body_wrapper">Back</a>

	    		<h3 class="panel-title">Undeposited Funds to <strong><?php echo $bank->title; ?></strong> <em class="deposits_receipts_selected" style="display:none;">&middot; TOTAL: <span class="bold total_selected_deposits"></span></em> <button type="submit" class="btn btn-success btn-xs deposits_receipts_selected" style="margin-left:10px;display:none;">Deposit Now</button> </h3>
	    	</div>
	    	<div class="panel-body">
<?php if( $undeposited ) { 
$total_undeposited = 0;
	?>

<?php if( isset($entries_data) && ($entries_data) ) { ?>
	    		<table class="table table-condensed table-hover">
	    			<thead>
	    				<tr>
	    					<th><input type="checkbox" id="deposits_select_all"></th>
	    					<th>Number</th>
	    					<th>Date</th>
	    					<th>Name</th>
	    					<th>Class</th>
	    					<th>Memo</th>
	    					<th class="text-right">Amount</th>
	    					<th class="text-right"></th>
	    				</tr>
	    			</thead>
	    			<tbody>
<?php 
$total = 0;
foreach($entries_data as $entry) { 
?>
<tr class="select_sales_receipt" id="sales-receipt-<?php echo $entry->trn_id; ?>" data-id="<?php echo $entry->trn_id; ?>">
	<td class=""><input type="checkbox" class="deposit_item" id="deposit_item-<?php echo $entry->trn_id; ?>" name="receipts[]" value="<?php echo $entry->trn_id; ?>" data-amount="<?php echo $entry->total_amount; ?>"></td>
	<td class="clickable"><?php echo $entry->number; ?></td>
	<td class="clickable"><?php echo date('m/d/Y', strtotime($entry->date)); ?></td>
	<td class="clickable"><?php echo $entry->full_name; ?></td>
	<td class="clickable"><?php echo $entry->class_name; ?></td>
	<td class="clickable"><?php echo $entry->memo; ?></td>
	<td class="clickable text-right"><?php echo number_format($entry->total_amount,2); $total_undeposited += $entry->total_amount; ?></td>
	<td class="text-right">
<a href="javascript:void(0);" class="item  ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="View Details" data-url="<?php echo site_url("report_transactions/redirect/{$entry->trn_id}/ajax/1") . "?next=" . uri_string(); ?>" data-hide_footer='1'><span class="fa fa-eye"></span></a>
</td>
</tr>
<?php } ?>

<tr>
	<td></td>
	<td colspan="5" class="bold">Total Undeposited Funds</td>
	<td class="text-right bold"><?php echo number_format($total_undeposited,2); ?></td>
	<td></td>
</tr>
	    			</tbody>
	    		</table>
<?php } else { ?>
	<p class="text-center">Nothing to deposit!</p>
<?php } ?>

<?php } else { ?>
	You have not set undeposited fund account. <a href="<?php echo site_url('accounting_settings/configuration'); ?>">Settings</a>
<?php } ?>
	    	</div>
	    	<div class="panel-footer deposits_receipts_selected" style="display: none;">
	    		TOTAL: <span class="bold total_selected_deposits"></span>
	    		<button type="submit" class="pull-right btn btn-success btn-xs">Deposit Now</button>
	    		<span class="clearfix"></span>
	    	</div>
	    </div>
</form>
    </div>
</div>
</div>

<?php $this->load->view('footer'); ?>