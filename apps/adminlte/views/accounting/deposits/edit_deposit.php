<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( !isset($output) || ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('accounting/accounting_navbar'); ?>

<div class="container">
<div class="row">
	<div class="col-md-6 col-md-offset-3">

<form method="post" action="">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
<?php if(!$deposit->recon_id) { ?>
	    		<a href="<?php echo site_url("accounting_deposits/delete_deposit/{$deposit->id}"); ?>" class="btn btn-danger btn-xs pull-right confirm">Delete</a>
<?php } else { ?>
	<a href="<?php echo site_url("accounting_reconciliation/recon_items/{$deposit->recon_id}") . "?highlight=" . $deposit->trn_id; ?>" class="item pull-right">
		<span class="glyphicon glyphicon-check"></span>
	</a>
<?php } ?>
	    		<h3 class="panel-title">Deposited to <strong><?php echo $deposit->bank_name; ?></strong></h3>
	    	</div>
	    	<div class="panel-body">

<?php endif; ?>

<div class="row">
	<div class="col-md-6">
          <div class="form-group">
            <label>Date Deposited</label>
            <input name="deposit_date" type="text" class="form-control text-center datepicker" value="<?php echo ($deposit->date_deposited) ? date('m/d/Y', strtotime($deposit->date_deposited)) : date('m/d/Y'); ?>" <?php echo ($deposit->recon) ? "disabled" : ""; ?>>
          </div>
	</div>
	<div class="col-md-6">
          <div class="form-group">
            <label>Deposited Amount</label>
            <input type="hidden" name="deposit_amount" value="<?php echo $deposit->amount; ?>">
            <span class="form-control bold text-right"><?php echo number_format( $deposit->amount, 2); ?></span>
          </div>
	</div>
</div>

<div class="form-group">
            <label>Memo</label>
            <textarea name="memo" class="form-control"><?php echo $deposit->memo; ?></textarea>
          </div>


<?php if( isset($output) && ($output=='ajax') ) : ?>
   <?php if( $deposit->recon ) { ?>
   <a href="<?php echo site_url("accounting_reconciliation/recon_items/{$deposit->recon_id}"); ?>?highlight=<?php echo $deposit->trn_id; ?>" class="btn btn-primary btn-xs">View Recon</a>
   <?php } else { ?>
 <a href="<?php echo site_url("accounting_deposits/delete_deposit/{$deposit->id}"); ?>" class="btn btn-danger btn-xs confirm">Delete this deposit</a>
    <?php } ?>
<?php endif; ?>

<?php if( !isset($output) || ($output!='ajax') ) : ?>

	    	</div>
	    <div class="panel-footer">
          <button type="submit" class="btn btn-success">Submit</button>
          <a href="<?php echo site_url("accounting_deposits"); ?>" class="btn btn-warning body_wrapper">Back</a>
        </div>



	    </div>
</form>
    </div>
</div>

<div class="row">
	<div class="col-md-12">

	    <div class="panel panel-default">
	    	<div class="panel-body">
<?php if( $undeposited ) { ?>

<?php if( $entries_data ) { ?>
	    		<table class="table table-condensed table-hover">
	    			<thead>
	    				<tr>
	    					<th>Number</th>
	    					<th>Date</th>
	    					<th>Name</th>
	    					<th>Class</th>
	    					<th>Memo</th>
	    					<th class="text-right">Amount</th>
	    				</tr>
	    			</thead>
	    			<tbody>
<?php 
$total = 0; 
foreach($entries_data as $entry) { 
?>
<tr class="<?php echo (($this->input->get('highlight')) && ($entry->trn_id==$this->input->get('highlight'))) ? 'info' : ''; ?>">
	<td><?php echo $entry->number; ?></td>
	<td><?php echo date('m/d/Y', strtotime($entry->date)); ?></td>
	<td><?php echo $entry->full_name; ?></td>
	<td><?php echo $entry->class_name; ?></td>
	<td><?php echo $entry->memo; ?></td>
	<td class="text-right"><?php echo number_format($entry->total_amount,2); $total+=$entry->total_amount; ?></td>
</tr>
<?php } ?>
<tr class="success">
	<td colspan="5"><strong>TOTAL DEPOSIT</strong></td>
	<td class="text-right bold"><?php echo number_format($total,2); ?></td>
</tr>
	    			</tbody>
	    		</table>

<?php } ?>


<?php } ?>
	    	</div>

	    </div>

    </div>
</div>

</div>

<?php $this->load->view('footer'); ?>

<?php endif; ?>