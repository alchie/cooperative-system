<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<html>
<head>
  <title><?php echo (isset($filters['title'])) ? $filters['title'] : "Deposits"; ?></title>
  <link href="<?php echo base_url('assets/css/print.css'); ?>" rel="stylesheet">
</head>

<?php if( isset($output) && ($output!='iframe') ) : ?>

<body>
<div class="wrapper">

<?php else: ?>

  <body style="padding:10px">

<?php endif; ?>

<?php if( $deposits ) { ?>

         <table width="100%" cellpadding="0" cellspacing="0">
          <thead>
          <tr>
            <th colspan="4">
              
<h3 class="text-center allcaps header-label">
<?php echo (isset($filters['title'])) ? $filters['title'] : "Deposits"; ?>
<br><small><?php echo date('F d, Y', strtotime($filters['date_range_beg'])); ?> - <?php echo date('F d, Y', strtotime($filters['date_range_end'])); ?></small>
</h3>
              
            </th>
          </tr>
	<tr>
		<th class="text-left">Date</th>
		<th class="text-left">Bank</th>
		<th class="text-left">Memo</th>
		<th class="text-right">Amount</th>
	</tr>
          </thead>
          <tbody>
<?php 
$total_payments = 0;
$total_applied = 0;
$total_principal = 0;
$total_interest = 0;
$n=0;
foreach($deposits as $deposit) {  $n++;  ?>
	<tr>
		<td class="text-left"><?php echo date('m/d/Y', strtotime($deposit->date_deposited)); ?></td>  
		<td class="text-left"><?php echo $deposit->bank_name; ?></td>  
		<td class="text-left"><?php echo $deposit->memo; ?></td>  
		<td class="text-right"><?php echo number_format($deposit->amount,2); ?></td>  
	</tr>
<?php if (isset($filters['show_items']) && ($filters['show_items']==1)) { ?>
<?php if( $deposit->items ) { ?>
	<tr>
		<td colspan="4">
			 <table width="100%" cellpadding="0" cellspacing="0" style="border:1px solid #000;padding:10px;margin-bottom:10px;">
			 	<tr>
					<td class="text-left">Number</td>
					<td class="text-left">Date</td>
					<td class="text-left">Item Type</td>
					<td class="text-left">Memo</td>
					<td class="text-right">Amount</td>
				</tr>
<?php foreach($deposit->items as $item) { ?>
				<tr>
					<td class="text-left"><?php echo $item->number; ?></td>  
					<td class="text-left"><?php echo date('m/d/Y', strtotime($item->date)); ?></td>  
					<td class="text-left"><?php echo $item->item_type; ?></td>  
					<td class="text-left"><?php echo $item->memo; ?></td>  
					<td class="text-right"><?php echo number_format($item->debit,2); ?></td>  
				</tr>
<?php } ?>
			 </table>
		</td>
	</tr>
<?php } ?>
<?php } ?>
<?php } ?>

          </tbody>
          </table>

<?php } else { ?>
  <div class="wrapper text-center allcaps bold">
  Empty Result
  </div>
 <?php } ?>
 

<?php if( isset($output) && ($output!='iframe') ) : ?>

</div>

<?php endif; ?>

</body>
</html>