<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( !isset($output) || ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('accounting/accounting_navbar'); ?>

<div class="container">

<div class="row">
	<div class="col-md-12">

	    <div class="panel panel-default">
	    <div class="panel-heading">
	    
	    <a href="<?php echo site_url("accounting_deposits"); ?>" class="btn btn-warning btn-xs pull-right body_wrapper">Back</a>

	    	<div class="panel-title">Deposit Items</div>
	    </div>
	    	<div class="panel-body">
<?php if( $undeposited ) { ?>

<?php if( $entries_data ) { ?>
	    		<table class="table table-condensed table-hover">
	    			<thead>
	    				<tr>
	    					<th>Number</th>
	    					<th>Date</th>
	    					<th>Name</th>
	    					<th>Class</th>
	    					<th>Memo</th>
	    					<th class="text-right">Amount</th>
	    				</tr>
	    			</thead>
	    			<tbody>
<?php 
$total = 0; 
foreach($entries_data as $entry) { 
?>
<tr class="<?php echo (($this->input->get('highlight')) && ($entry->trn_id==$this->input->get('highlight'))) ? 'info' : ''; ?>">
	<td><?php echo $entry->number; ?></td>
	<td><?php echo date('m/d/Y', strtotime($entry->date)); ?></td>
	<td><?php echo $entry->full_name; ?></td>
	<td><?php echo $entry->class_name; ?></td>
	<td><?php echo $entry->memo; ?></td>
	<td class="text-right"><?php echo number_format($entry->total_amount,2); $total+=$entry->total_amount; ?></td>
</tr>
<?php } ?>
<tr class="success">
	<td colspan="5"><strong>TOTAL DEPOSIT</strong></td>
	<td class="text-right bold"><?php echo number_format($total,2); ?></td>
</tr>
	    			</tbody>
	    		</table>

<?php } ?>


<?php } ?>
	    	</div>

	    </div>

    </div>
</div>

</div>

<?php $this->load->view('footer'); ?>

<?php endif; ?>