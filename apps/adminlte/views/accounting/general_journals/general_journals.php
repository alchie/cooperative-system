<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
<?php if( ! $inner_page ): ?>
<?php $this->load->view('accounting/accounting_navbar'); ?>

<div class="container">
<div class="row">
	<div class="col-md-12">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
<?php if( hasAccess('accounting', 'general_journals', 'add') ) { ?>
	<button type="button" class="btn btn-success btn-xs pull-right ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Edit Journal" data-url="<?php echo site_url("accounting_general_journals/add/ajax") . "?next=" . uri_string(); ?>">Add Journal</button>
<?php } ?>
	    		<h3 class="panel-title">General Journals</h3>
	    	</div>
	    	<div class="panel-body" id="ajaxBodyInnerPage" data-url="<?php echo site_url(uri_string()); ?>">
<?php endif; ?>

<?php if( isset($journals) ) { ?>
<?php if( $journals ) { ?>
	    		<table class="table table-default table-hover table-condensed">
	    			<thead>
	    				<tr>
	    					<th width="5%">#</th>
	    					<th width="15%">Date</th>
	    					<th>Memo</th>
	    					<th class="text-right">Amount</th>
	    					<?php if( hasAccess('accounting', 'general_journals', 'edit') ) { ?>
	    						<th width="110px">Action</th>
	    					<?php } ?>
	    				</tr>
	    			</thead>
	    			<tbody>
	    			<?php foreach($journals as $journal) { ?>
	    				<tr>
	    					<td><?php echo $journal->number; ?></td>
	    					<td><?php echo date('m/d/Y', strtotime($journal->date)); ?></td>
	    					<td><?php echo $journal->memo; ?></td>
	    					<td class="text-right"><?php echo number_format($journal->amount,2); ?></td>
	    					<?php if( hasAccess('accounting', 'general_journals', 'edit') ) { ?>
		    					<td>
		    					<button type="button" class="btn btn-warning btn-xs ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Edit Journal" data-url="<?php echo site_url("accounting_general_journals/edit/{$journal->id}/ajax") . "?next=" . uri_string(); ?>">Edit</button>
		    					<a href="<?php echo site_url("accounting_general_journals/entries/" . $journal->id); ?>" class="btn btn-primary btn-xs body_wrapper">Entries</a>
		    					</td>
	    					<?php } ?>
	    				</tr>
	    			<?php } ?>
	    			</tbody>
	    		</table>

	    		<?php echo ($pagination!='') ? '<center>' . $pagination . '</center>' : ''; ?>
	    		
<?php } else { ?>
	<div class="text-center">No Journal Found!</div>
<?php } ?>
<?php } else { ?>
	Please wait...
<?php } ?>

<?php if( ! $inner_page ): ?>
	    	</div>
	    </div>
    </div>
</div>
</div>
<?php endif; ?>
<?php $this->load->view('footer'); ?>