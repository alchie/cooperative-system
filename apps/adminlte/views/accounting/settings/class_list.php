<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>

<?php if( ! $inner_page ): ?>

<?php $this->load->view('accounting/settings/settings_navbar'); ?>

<div class="container">
<div class="row">
	<div class="col-md-12">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
<?php if( hasAccess('accounting', 'settings', 'add') ) { ?>
	    		 <button type="button" class="btn btn-success btn-xs pull-right ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Add Class" data-url="<?php echo site_url("accounting_settings/add_class/ajax") . "?next=" . uri_string(); ?>">Add Class</button>
<?php } ?>
	    		<h3 class="panel-title">Class List</h3>
	    	</div>

	    	<div class="panel-body" id="ajaxBodyInnerPage">

<?php endif; ?>

<?php if( $class_list ) { ?>
	    		<table class="table table-default table-hover">
	    			<thead>
	    				<tr>
	    					<th>Class Name</th>
<?php if( hasAccess('accounting', 'settings', 'edit') || hasAccess('accounting', 'settings', 'delete') ) { ?>
	    					<th width="110px">Action</th>
<?php } ?>
	    				</tr>
	    			</thead>
	    			<tbody>
<?php 
function display_list($classList , $pre="") {
foreach($classList as $class) { 
?>
	    				<tr class="<?php echo ($class->active) ? '' : 'danger'; ?>">
	    					<td><?php echo $pre; ?><?php echo $class->value; ?></td>
	    					<td class="text-right">
<?php if( hasAccess('accounting', 'settings', 'edit') ) { ?>
	    					<button type="button" class="btn btn-warning btn-xs ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Edit Class" data-url="<?php echo site_url("accounting_settings/edit_class/{$class->id}/ajax") . "?next=" . uri_string(); ?>">Edit</button>
<?php } ?>
<?php if( hasAccess('accounting', 'settings', 'delete') ) { ?>
	    					<a href="<?php echo site_url("accounting_settings/delete_class/" . $class->id); ?>" class="btn btn-danger btn-xs confirm">Delete</a>
<?php } ?>
	    					</td>
	    				</tr>
<?php
	if( $class->children ) {
		display_list( $class->children, $pre . "- - - - " );
	}
}
}
display_list($class_list);
?>
	    			</tbody>
	    		</table>

	    		<?php echo ($pagination!='') ? '<center>' . $pagination . '</center>' : ''; ?>

<?php } else { ?>
	<div class="text-center">No Class Found!</div>
<?php } ?>

<?php if( ! $inner_page ): ?>

	    	</div>
	    </div>
    </div>
</div>
</div>
<?php endif; ?>

<?php $this->load->view('footer'); ?>