<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); 
	$setting = array();
	if($settings) {
		foreach($settings as $set) {
			$setting[$set->key] = $set->value;
		}
	}
?>

<?php $this->load->view('accounting/settings/settings_navbar'); ?>


<div class="container">
<div class="row">

  <div class="col-md-6 col-md-offset-3">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Configurations</h3>
        </div>
        <form method="post">
        <div class="panel-body">


<div class="row">
  <div class="col-md-6">
      <div class="form-group">
            <label>Year Start</label>
            <input class="form-control datepicker text-center" name="year_start" value="<?php echo (isset($setting['year_start'])) ? $setting['year_start'] : ''; ?>">
      </div>
  </div>
  <div class="col-md-6">
      <div class="form-group">
            <label>Year End</label>
            <input class="form-control datepicker text-center" name="year_end" value="<?php echo (isset($setting['year_end'])) ? $setting['year_end'] : ''; ?>">
      </div>
  </div>
</div>

          <div class="form-group">
            <label>Undeposited Fund Account</label>
            <select name="undeposited" class="form-control"  title="Choose an account...">
<?php 
function display_list($setting, $main_accounts , $pre="", $set='undeposited', $type_filter=false) {
                 foreach( $main_accounts as $main_account) { 
                  if( ($type_filter) && ( $type_filter != $main_account->type ) ) {
                     continue;
                  }
                  ?>
                  <option value="<?php echo $main_account->id; ?>" <?php echo (isset($setting[$set])&&($setting[$set]==$main_account->id)) ? 'selected' : ''; ?> title="<?php echo $main_account->title; ?>"><?php echo $pre; ?><?php echo $main_account->title; ?></option>
          <?php 
          if( $main_account->children ) {
            display_list( $setting, $main_account->children, $pre . "- - - - ", $set, $type_filter );
          }
          ?>
                  <?php } ?> 
<?php } 
display_list( $setting, $account_titles, '', 'undeposited', '000_BS_ASS_CSH' );
?>        
              </select>
          </div>
          
<div class="form-group">
            <label>Funds Surplus Account</label>
            <select name="fund_surplus" class="form-control" title="Choose an account...">
<?php 
display_list( $setting, $account_titles, '', 'fund_surplus', '090_BS_EQU_CAP' );
?>        
              </select>
          </div>


        </div>
        <div class="panel-footer">
          <button type="submit" class="btn btn-success">Submit</button>
          <a href="<?php echo site_url("system_settings"); ?>" class="btn btn-warning">Back</a>
        </div>


        </form>
      </div>
    </div>
</div>
</div>

<?php $this->load->view('footer'); ?>