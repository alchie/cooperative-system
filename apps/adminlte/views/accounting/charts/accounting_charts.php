<?php defined('BASEPATH') OR exit('No direct script access allowed'); 
$types = array();
foreach($coa_types as $type) {
	foreach($type as $k=>$t) {
		$types[$k] = $t;
	}
}
?>

<?php $this->load->view('header'); ?>

<?php if( ! $inner_page ): ?>
<style>
<!-- 
a.item, a.item:hover {
	color:#555555;
	text-decoration: none;
}
-->
</style>
<?php $this->load->view('accounting/accounting_navbar'); ?>

<div class="container">
<div class="row">
	<div class="col-md-12">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
	    		<!--<a href="<?php echo site_url("accounting_charts/add"); ?>" class="btn btn-success btn-xs pull-right">Add Account</a>-->
<?php if( hasAccess('accounting', 'chart_of_accounts', 'add') ) { ?>
	    		<button type="button" class="btn btn-success btn-xs pull-right ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Add Account" data-url="<?php echo site_url("accounting_charts/add/ajax") . "?next=" . uri_string(); ?>">Add Account</button>
<?php } ?>
	    		<h3 class="panel-title"><strong>Chart of Accounts</strong>
<?php if( $this->input->get('show_inactive') ) { ?>
<a href="<?php echo site_url(uri_string()) . "?show_inactive=0"; ?>" class="small">Hide Inactive</a>
<?php } else { ?>
<a href="<?php echo site_url(uri_string()) . "?show_inactive=1"; ?>" class="small">Show Inactive</a>
<?php } ?>

	    		</h3>
	    	</div>
	    	<div class="panel-body" id="ajaxBodyInnerPage" data-url="<?php echo site_url(uri_string()) . "?show_inactive=" . $this->input->get('show_inactive'); ?>">

<?php endif; // inner_page ?>

<?php if( isset($accounts) ) { ?>
<?php if( $accounts ) { ?>
	    		<table class="table table-default table-hover table-condensed">
	    			<thead>
	    				<tr>
	    				<?php if( hasAccess('accounting', 'chart_of_accounts', 'edit') ) { ?>
	    					<th width="1%"></th>
	    				<?php } ?>
	    					<th>#</th>
	    					<th>Account Title</th>
	    					<th>Type</th>
	    					<th class="text-right">Balance</th>
	    					<th width="10px"></th>
	    				</tr>
	    			</thead>
	    			<tbody>
<?php 
function display_list($types, $account, $pre="", $level=0, $parent_active=true, $highlight=NULL, $show_inactive=false) {
			if( $level == 0) {
				$parent_active = true;
			}

			$balance = ''; 
			if( (strpos($account->type, '_BS_') ) ) {
				if( (strpos($account->type, '_BS_ASS_') ) ) {
					$balance = number_format(($account->debit_balance - $account->credit_balance),2);
				} else {
					$balance = number_format(($account->credit_balance - $account->debit_balance),2);
				}
			}

			$parent_active = ($parent_active==false) ? false : $account->active;

			if( $parent_active==false && $show_inactive==false) {
				return;
			}

			?>
	    				<tr class="<?php echo ($parent_active) ? (($account->id==$highlight) ? 'info' : '') : 'danger'; ?>">
	    				<?php if( hasAccess('accounting', 'chart_of_accounts', 'edit') ) { ?>
	    					<td>
	    						<a href="#edit-account" class="item ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Edit Account" data-url="<?php echo site_url("accounting_charts/edit/{$account->id}/ajax") . "?next=" . uri_string(); ?>">
	    					<span class="glyphicon glyphicon-pencil"></span>
	    					</a>
	    					</td>
	    				<?php } ?>
	    					<td><?php echo $account->number; ?></td>
	    					<td><?php echo $pre; ?>
	    					
	    					<?php echo $account->title; ?>
	    					
	    					</td>
	    					<td><?php echo $types[$account->type]; ?></td>
	    					<td class="text-right">
	    					<a class="item ajaxPage" href="<?php echo site_url("accounting_charts/details/" . $account->id); ?>">
	    					<?php echo $balance; ?>
	    						</a>
	    					</td>
	    					<td><a href="<?php echo site_url("accounting_charts/details/" . $account->id); ?>" class="item body_wrapper"><span class="glyphicon glyphicon-th-list"></span></a></td>
	    				</tr>
	    		<?php 

					if( $account->children ) {
						foreach($account->children as $child) {
							display_list( $types, $child, $pre . "- - - - ", ($level+1), $parent_active, $highlight, $show_inactive );
						}
					}
	
}

foreach($accounts as $account) { 
	display_list($types, $account, "", 0, true, $this->input->get('highlight'), $this->input->get('show_inactive'));
}
?>
	    			</tbody>
	    		</table>
<?php } else { ?>
	<div class="text-center">No Account Found!</div>
<?php } ?>
<?php } else { ?>
	Please wait... loading data...
<?php } ?>
<?php if( ! $inner_page ): ?>
	
	    	</div>
	    </div>
    </div>
</div>
</div>

<?php endif; // inner_page ?>

<?php $this->load->view('footer'); ?>