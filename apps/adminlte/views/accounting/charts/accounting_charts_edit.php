<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( !isset($output) || ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('accounting/accounting_navbar'); ?>

<div class="container">
<div class="row">

	<div class="col-md-6 col-md-offset-3">
	    <div class="panel panel-default">
	    	<div class="panel-heading">

<?php if( hasAccess('accounting', 'chart_of_accounts', 'delete') ) { ?>
<?php if( (intval($account->entries) == 0) && (intval($account->children) == 0) ) { ?>
         <a href="<?php echo site_url("accounting_charts/delete/" . $account->id); ?>" class="btn btn-danger btn-xs pull-right confirm">Delete</a>
<?php } ?>
<?php } ?>

	    		<h3 class="panel-title">Edit Account</h3>
	    	</div>
	    	<form method="post">
	    	<div class="panel-body">

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

<?php endif; ?>

          <div class="form-group">
            <label>Account Title</label>
            <input name="title" type="text" class="form-control" value="<?php echo $account->title; ?>">
          </div>

<div class="row">
<div class="col-md-6">

          <div class="form-group">
            <label>Account Type</label>
<?php if( intval($account->children) == 0 ) { ?>
            <select name="type" class="form-control">
              <?php foreach( $coa_types as $label=>$types) { ?>
                 <optgroup label="<?php echo $label; ?>">
                 <?php foreach( $types as $key=>$value) { ?>
                  <option value="<?php echo $key; ?>" <?php echo ($account->type==$key) ? 'selected' : ''; ?>><?php echo $value; ?></option>
                  <?php } ?>
                </optgroup>
              <?php } ?>
            </select>
<?php } else { ?> 
  <input type="hidden" name="type" value="<?php echo $account->type; ?>">
  <div class="form-control"><?php echo (isset($coa_types['Balance Sheet'][$account->type])) ? $coa_types['Balance Sheet'][$account->type] : $coa_types['Income Statement'][$account->type]; ?></div>
<?php } ?> 
          </div>
    </div>
    <div class="col-md-6">
              <div class="form-group">
                <label>Account Number</label>
                <input name="number" type="text" class="form-control text-center" value="<?php echo $account->number; ?>">
              </div>
    </div>
</div>
          <div class="form-group">
            <label>Parent Account</label>

            <select name="parent" class="form-control" title="Select Parent Account">
<?php 
function display_list($account, $main_accounts , $pre="") {
                 foreach( $main_accounts as $main_account) { 
                  if( $main_account->id==$account->id ) {
                    continue;
                  }

          $coa_types = unserialize(CHART_ACCOUNT_TYPES);
          $charts = array_merge($coa_types['Balance Sheet'], $coa_types['Income Statement']);

                  ?>
                  <option data-subtext="<?php echo $charts[$main_account->type]; ?>" value="<?php echo $main_account->id; ?>" <?php echo ($main_account->id==$account->parent) ? 'selected' : ''; ?>><?php echo $pre; ?><?php echo $main_account->title; ?></option>
<?php 
          if( $main_account->children ) {
            display_list( $account, $main_account->children, $pre . "- - - - " );
          }
          ?>
                  <?php } ?> 
<?php } 
display_list( $account, $main_accounts );
?>        
            </select>
          </div>

          <div class="form-group">
              <label><input name="active" type="checkbox" value="1" <?php echo ($account->active==1) ? 'checked' : ''; ?>> Active</label>
          </div>

<?php if( $account->type == '001_BS_ASS_CSB' ) { ?>
          <div class="form-group">
              <label><input name="reconcile" type="checkbox" value="1" <?php echo ($account->reconcile==1) ? 'checked' : ''; ?>> Reconcile</label>
          </div>
<?php } ?>

<?php if( hasAccess('accounting', 'chart_of_accounts', 'delete') ) { ?>
<?php if( isset($output) && ($output=='ajax') && (intval($account->entries) == 0) && (intval($account->children) == 0) ) { ?>
          <div class="form-group">
              <label><input name="delete" type="checkbox" value="1" class="danger"> Delete this account</label>
          </div>
<?php } else {?>
  <div class="form-group">
    <strong>Delete Disabled!</strong> Move or delete child accounts.
  </div>
<?php } ?>
<?php } ?>

<?php if( !isset($output) || ($output!='ajax') ) : ?>

	    	</div>

	    	<div class="panel-footer">
	    		<button type="submit" class="btn btn-success">Submit</button>
	    		<a href="<?php echo site_url("accounting_charts"); ?>" class="btn btn-warning">Back</a>
	    	</div>


	    	</form>
	    </div>
    </div>
</div>
</div>

<?php $this->load->view('footer'); ?>

<?php endif; ?>