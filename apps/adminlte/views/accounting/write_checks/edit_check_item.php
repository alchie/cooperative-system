<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php 

function display_list($main_accounts, $pre="", $selected=NULL) {
          $coa_types = unserialize(CHART_ACCOUNT_TYPES);
          $charts = array_merge($coa_types['Balance Sheet'], $coa_types['Income Statement']);

          foreach( $main_accounts as $main_account) { 
            ?>
                  <option data-subtext="<?php echo $charts[$main_account->type]; ?>" value="<?php echo $main_account->id; ?>" <?php echo ($selected==$main_account->id) ? 'selected' : ''; ?> title="<?php echo $main_account->title; ?>"><?php echo $pre; ?><?php echo $main_account->title; ?></option>
          <?php 
            if( $main_account->children ) {
              display_list( $main_account->children, $pre . "- - - - ", $selected );
            }
          }  
  } 

function display_class($class_list , $pre="", $selected=NULL ) {
    foreach($class_list as $class) { 
      echo "<option value=\"{$class->id}\"";
        if($selected==$class->id) {
          echo " SELECTED";
        }
      echo ">{$pre}{$class->value}</option>";
      if( $class->children ) {
        display_items( $class->children, $pre . "- - - - ", $selected);
      }
    }
  } 

?>
<?php if( !isset($output) || ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('accounting/accounting_navbar'); ?>

<div class="container">
<div class="row">

<form method="post">

  <div class="col-md-6 col-md-offset-3">


<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>



        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Add Check Item</h3>
            </div>

            <div class="panel-body">

<?php endif; ?>

          <div class="form-group">
            <label>Item Type</label>
            <select name="item_type" class="form-control" <?php echo ($locked) ? 'disabled' : ''; ?>>
              <?php foreach( $item_types as $key=>$value) { ?>
                  <option value="<?php echo $key; ?>" <?php echo ($item_data->item_type==$key) ? 'selected' : ''; ?>><?php echo $value; ?></option>
              <?php } ?>
            </select>
          </div>

<div class="row">
  <div class="col-md-8">
        <div class="form-group ">
            <label class="control-label">Account Title</label>
       <select type="text" class="form-control selectpicker" name="chart_id" title="Select an Account..."  <?php echo ($locked) ? 'disabled' : ''; ?>>

<?php 
display_list( $account_titles, '', $item_data->chart_id );
?>        
              </select>
        </div>
  </div>
  <div class="col-md-4">
        <div class="form-group ">
            <label class="control-label">Amount</label>
            <input class="form-control text-right" type="text" name="amount" value="<?php echo number_format($item_data->amount,2,".",""); ?>" <?php echo ($locked) ? 'disabled' : 'required'; ?>>
        </div>
  </div>
  </div>
  <div class="row">
  <div class="col-md-8">
        <div class="form-group ">
            <label class="control-label">Name</label>
             <input id="add_check_item_name_id" name="name_id" type="hidden" value="<?php echo $item_data->name_id; ?>">
            <input class="form-control autocomplete-name_select autocomplete-name_select-name-input-<?php echo time(); ?>" data-source="<?php echo site_url("accounting_write_checks/ajax/name"); ?>" data-name_id="add_check_item_name_id" type="text" <?php echo ( $item_data->name_id ) ? 'style="display: none;"' : ''; ?>  <?php echo ($locked) ? 'disabled' : ''; ?>>
<?php if( $item_data->name_id ) { ?>
            <div class="form-control autocomplete-name_select-name-display-<?php echo time(); ?>"><a class="badge" id="changeName<?php echo ($locked) ? 'disabled' : ''; ?>" href="#changeName" data-id="<?php echo $item_data->name_id; ?>" data-name_id="add_check_item_name_id" data-timestamp="<?php echo time(); ?>"><?php echo $item_data->full_name; ?></a></div>
<?php } ?>
        </div>

  </div>
  <div class="col-md-4">
        <div class="form-group ">
            <label class="control-label">Class</label>
            <select type="text" class="form-control" name="class_id" title="Select a Class..." <?php echo ($locked) ? 'disabled' : ''; ?>>
<?php display_class( $class_list, "", $item_data->class_id ); ?>
              </select>
        </div>
  </div>
</div>
         <div class="form-group ">
            <label class="control-label">Memo</label>
            <textarea class="form-control" name="memo"><?php echo $item_data->memo; ?></textarea>
        </div>

<?php if( isset($output) && ($output=='ajax') && ($locked===false) ) : ?>
 <a href="<?php echo site_url("accounting_write_checks/delete_check_item/{$item_data->id}"); ?>" class="btn btn-danger btn-xs confirm">Delete this item</a>
<?php endif; ?>

<?php if( !isset($output) || ($output!='ajax') ) : ?>

            </div>

                    <div class="panel-footer">
          <button type="submit" class="btn btn-success">Submit</button>
          <a href="<?php echo site_url("accounting_write_checks"); ?>" class="btn btn-warning">Back</a>
        </div>

        </div>


    </div>

    </form>

</div>
</div>

<?php $this->load->view('footer'); ?>

<?php endif; ?>