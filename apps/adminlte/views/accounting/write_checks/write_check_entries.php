<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php 
$je = array();
$i_count = 0;
if( $journal_entries ) {
  foreach( $journal_entries as $j) {
    $je[ ($j->item_number - count($bank_entries)) ] = $j;
  }
}
function display_list($je, $main_accounts , $pre="", $i=0, $posts=array(), $except=NULL) {
  $selected = ((isset($je[$i])&&($je[$i]->chart_id))) ? $je[$i]->chart_id : ((isset($posts[$i])) ? $posts[$i] : '');
          foreach( $main_accounts as $main_account) { 
              if( ( $except != NULL ) && ($except==$main_account->id) ) {
                continue;
              }
            ?>
                  <option value="<?php echo $main_account->id; ?>" <?php echo ($selected==$main_account->id) ? 'selected' : ''; ?> title="<?php echo $main_account->title; ?>"><?php echo $pre; ?><?php echo $main_account->title; ?></option>
          <?php 
            if( $main_account->children ) {
              display_list( $je, $main_account->children, $pre . "- - - - ", $i, $posts, $except );
            }
          }  
  } 

function display_class($je, $class_list, $pre="", $i=0, $posts=array()) {
  
  $selected = (isset($je[$i])&&($je[$i]->class)) ? $je[$i]->class : ((isset($posts[$i])) ? $posts[$i] : '');
                 foreach( $class_list as $class) { 
                  ?>
                  <option value="<?php echo $class->id; ?>" <?php echo ($selected==$class->id) ? 'selected' : ''; ?> title="<?php echo $class->value; ?>"><?php echo $pre; ?><?php echo $class->value; ?></option>
          <?php 
          if( $class->children ) {
            display_class( $je, $class->children, $pre . "- - - - ", $i, $posts);
          }
     } 
} 

?>
<?php $this->load->view('header');  ?>
<?php $this->load->view('accounting/accounting_navbar'); ?>

<script>
  <!--
  var account_titles = '<?php echo json_encode($account_titles); ?>';
  var class_list = '<?php echo json_encode($class_list); ?>';
  -->
</script>

<div class="container">
<div class="row">
	<div class="col-md-12">
  <form method="post">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
          <button type="submit" class="btn btn-success btn-xs pull-right">Save Entries</button>
	    		<h3 class="panel-title"><a href="<?php echo site_url("accounting_write_checks/edit_check/{$check_data->id}"); ?>"><i class="glyphicon glyphicon-pencil"></i></a> <?php echo date( 'm/d/Y', strtotime($check_data->check_date) ); ?> - <?php echo $check_data->memo; ?> </h3> 
	    	</div>
	    	<div class="panel-body">
	    	<table class="table table-default table-hover">
	    		<thead>
	    			<tr>
	    				<th>Account Title</th>
	    				<th width="10%">Debit</th>
              <th width="10%">Credit</th>
	    				<th width="15%">Class</th>
              <th width="20%">Name</th>
	    				<th width="15%">Memo</th>
	    			</tr>
	    		</thead>
          <tbody>
<?php foreach($bank_entries as $bank_entry) { ?>
<tr id="bank_entry-<?php echo $bank_entry->id; ?>">
              <td><div class="form-control">
              <input id="" name="entry_id[]" type="hidden" value="<?php echo $bank_entry->id; ?>">
              <input id="" name="account_title[]" type="hidden" value="<?php echo $bank_entry->chart_id; ?>">
              <a href="#split" class="pull-right split_bank_entry" title="Split" data-id="bank_entry-<?php echo $bank_entry->id; ?>"><span class="glyphicon glyphicon-resize-full"></span></a>
              <strong><?php echo $bank_entry->chart_title; ?></strong>
              </div></td>
              <td>
              <input id="" name="debit[]" type="hidden" value="">
              <div class="form-control"></div></td>
              <td><input type="text" class="form-control text-right" name="credit[]" value="<?php echo round($bank_entry->credit,2); ?>"></td>
              <td class="class_names"><select type="text" class="form-control class_names" name="class_name[]" title="Select a Class...">
<?php display_class( $je, $class_list, "", 0, $this->input->post('class_name') ); ?>
              </select></td>
              <td>

              <input name="full_name[]" type="hidden" value="">

              <input value="<?php echo $bank_entry->name_id; ?>" name="name_id[]" type="hidden">
<div class="form-control"><span class="badge"><?php echo $bank_entry->full_name; ?></span></div>
              </td>              <td>
              <input id="" name="memo[]" type="hidden" value="<?php echo $bank_entry->memo; ?>">
              <div class="form-control"><?php echo $bank_entry->memo; ?></div></td>
            </tr>
<?php } ?>

<?php 
$account_title = $this->input->post('account_title');
for($i=0;$i<( (count($i_count) > $check_data->lines) ? $i_count : $check_data->lines );$i++) { 
      if( $account_title[$i] == $check_data->chart_id) {
          continue;
      }
  ?>
            <tr id="entries-<?php echo $i; ?>">
              <td>
              <input id="" name="entry_id[]" type="hidden" value="<?php echo (isset($je[$i])) ? $je[$i]->id : ((isset($this->input->post('entry_id')[$i])) ? $this->input->post('entry_id')[$i] : ''); ?>">
              <select type="text" class="form-control select_account_titles selectpicker" name="account_title[]" title="Select an Account Title...">

<?php 
display_list( $je, $account_titles, '', $i, $this->input->post('account_title'), $check_data->chart_id );
?>        
              </select></td>
              <td><input type="text" class="form-control text-right" name="debit[]" value="<?php echo (isset($je[$i]) && (round($je[$i]->debit,2) > 0)) ? round($je[$i]->debit,2) :  ((isset($this->input->post('debit')[$i])) ? $this->input->post('debit')[$i] : ''); ?>"></td>
              <td><input type="text" class="form-control text-right" name="credit[]" value="<?php echo (isset($je[$i]) && (round($je[$i]->credit,2) > 0)) ? round($je[$i]->credit,2) :  ((isset($this->input->post('credit')[$i])) ? $this->input->post('credit')[$i] : ''); ?>"></td>
              <td><select type="text" class="form-control" name="class_name[]" title="Select a Class...">
<?php display_class( $je, $class_list, "", $i, $this->input->post('class_name') ); ?>
              </select></td>
              <td>

              <input id="journal_full_name_<?php echo $i; ?>" name="full_name[]" type="hidden" value="<?php echo (isset($je[$i])) ? $je[$i]->full_name : ((isset($this->input->post('full_name')[$i])) ? $this->input->post('full_name')[$i] : ''); ?>">

              <input id="journal_name_id_<?php echo $i; ?>" name="name_id[]" type="hidden" value="<?php echo (isset($je[$i])) ? $je[$i]->name_id : ((isset($this->input->post('name_id')[$i])) ? $this->input->post('name_id')[$i] : ''); ?>">
           
            <input class="form-control autocomplete-name_select autocomplete-name_select-name-input-<?php echo (isset($je[$i])) ? $je[$i]->name_id : ((isset($this->input->post('name_id')[$i])) ? $this->input->post('name_id')[$i] : ''); ?>" data-source="<?php echo site_url("accounting_general_journals/ajax/name"); ?>" data-name_id="journal_name_id_<?php echo $i; ?>" data-full_name="journal_full_name_<?php echo $i; ?>" type="text" <?php echo ( (isset($je[$i])) && ($je[$i]->name_id) ) ? ' style="display:none;"' : ''; ?>>

<?php if( (isset($je[$i])) && ($je[$i]->name_id) ) { ?>
<div class="form-control autocomplete-name_select-name-display-<?php echo $je[$i]->name_id; ?>"><a class="badge writeCheckChangeName" href="#changeName" data-id="<?php echo $je[$i]->name_id; ?>" data-name_id="journal_name_id_<?php echo $i; ?>" data-timestamp="<?php echo $je[$i]->name_id; ?>"><?php echo $je[$i]->full_name; ?></a></div>
<?php } ?>
            </td>
              <td><input type="text" class="form-control" name="memo[]" value="<?php echo (isset($je[$i])) ? $je[$i]->memo : ((isset($this->input->post('memo')[$i])) ? $this->input->post('memo')[$i] : ''); ?>"></td>
            </tr>
<?php } ?>
          </tbody>
	    	</table>
	    	</div>
	    </div>
</form>
    </div>
</div>
</div>
<?php $this->load->view('footer'); ?>