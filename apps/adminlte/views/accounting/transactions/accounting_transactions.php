<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>

<?php $this->load->view('accounting/accounting_navbar'); ?>

<div class="container">
<div class="row">
	<div class="col-md-12">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
	    		<h3 class="panel-title">Transactions</h3>
	    	</div>
	    	<div class="panel-body">

<?php if( $transactions ) { ?>
	    		<table class="table table-default table-hover">
	    			<thead>
	    				<tr>
	    					<th>Type</th>
	    					<th width="15%">Date</th>
	    					<th>Number</th>
	    					<th>Name</th>
	    					<th>Memo</th>
	    					<th width="20px" class="text-right"></th>
	    				</tr>
	    			</thead>
	    			<tbody>
	    			<?php foreach($transactions as $transaction) { ?>
	    				<tr>
	    					<td>
<span class="hoverPopRight" title="<?php echo $transaction->type; ?>" data-content="<strong>Department:</strong> <?php echo $transaction->dept; ?><br><strong>Section:</strong> <?php echo $transaction->sect; ?>">
                <?php echo $transaction->type; ?>
                  </span>
	    					</td>
	    					<td><?php echo date('m/d/Y', strtotime($transaction->date)); ?></td>
	    					<td><?php echo $transaction->number; ?></td>
	    					<td><?php echo $transaction->full_name; ?></td>
	    					<td><?php echo $transaction->memo; ?></td>
	    					<td>
	    					<a href="<?php echo site_url('accounting_transactions/redirect/' .  $transaction->id ) . "?next=" . uri_string(); ?>" class="item"><span class="fa fa-eye"></span></a>
	    					</td>
	    				</tr>
	    			<?php } ?>
	    			</tbody>
	    		</table>

<?php echo ($pagination!='') ? '<center>' . $pagination . '</center>' : ''; ?>

<?php } else { ?>

	<div class="text-center">No Transaction Found!</div>

<?php } ?>

	    	</div>
	    </div>
    </div>
</div>
</div>

<?php $this->load->view('footer'); ?>