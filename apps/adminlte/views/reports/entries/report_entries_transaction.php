<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>

<?php if( ! $inner_page ): ?>

<?php $this->load->view('reports/reports_navbar'); ?>

<div class="container">
<div class="row">
	<div class="col-md-12">
	    <div class="panel panel-default">
	    	<div class="panel-heading">

	    		<h3 class="panel-title">Entries</h3>
	    	</div>
	    	<div class="panel-body" id="ajaxBodyInnerPage">

<?php endif; ?>

<?php if( $transactions ) { ?>
	    		<table class="table table-condensed table-hover">
	    			<thead>
	    				<tr>
	    					<th>Type</th>
	    					<th width="15%">Date</th>
	    					<th>Number</th>
	    					<th>Name</th>
	    					<th>Memo</th>
	    					<th class="text-right">Debit</th>
	    					<th class="text-right">Credit</th>
	    					<th width="10px" class="text-right"></th>
	    				</tr>
	    			</thead>
	    			<tbody>
	    			<?php foreach($transactions as $transaction) { ?>
	    				<tr>
	    					<td>
<span class="hoverPopRight" title="<?php echo $transaction->type; ?>" data-content="<strong>Department:</strong> <?php echo $transaction->dept; ?><br><strong>Section:</strong> <?php echo $transaction->sect; ?>">
                <?php echo $transaction->type; ?>
                  </span>
	    					</td>
	    					<td><?php echo date('m/d/Y', strtotime($transaction->date)); ?></td>
	    					<td><?php echo $transaction->number; ?></td>
	    					<td><?php echo $transaction->full_name; ?></td>
	    					<td><?php echo $transaction->memo; ?></td>
	    					<td class="text-right"><?php echo (round($transaction->total_debit)) ? number_format($transaction->total_debit,2) : ''; ?></td>
	    					<td class="text-right"><?php echo (round($transaction->total_credit)) ? number_format($transaction->total_credit,2) : ''; ?></td>
	    					<td class="text-right">
	    					<a href="<?php echo site_url('report_transactions/redirect/' .  $transaction->id ) . "?next=" . uri_string(); ?>" class="item"><span class="fa fa-eye"></span></a>
	    					</td>
	    				</tr>
	    			<?php } ?>
	    			</tbody>
	    		</table>

<?php echo ($pagination!='') ? '<center>' . $pagination . '</center>' : ''; ?>

<?php } else { ?>

	<div class="text-center">No Transaction Found!</div>

<?php } ?>

<?php if( ! $inner_page ): ?>

	    	</div>
	    </div>
    </div>
</div>
</div>

<?php endif; ?>

<?php $this->load->view('footer'); ?>