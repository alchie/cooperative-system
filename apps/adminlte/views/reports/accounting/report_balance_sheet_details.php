<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>

<?php $this->load->view('reports/reports_navbar'); ?>

<div class="container">
<div class="row">
	<div class="col-md-12">
	    <div class="panel panel-default">
	    	<div class="panel-heading">
	    		<a href="<?php echo site_url("report_balance_sheet"); ?>" class="pull-right btn btn-xs btn-warning">Back</a>
	    		<h3 class="panel-title"><?php echo $charts_data->title; ?></h3>
	    	</div>
	    	<div class="panel-body">
<?php if( $entries_data ) { ?>
	    		<table class="table table-default table-hover">
	    			<thead>
	    				<tr>
	    					<th>Type</th>
	    					<th>Date</th>
	    					<th>Name</th>
	    					<th>Memo</th>
	    					<th class="text-right">Amount</th>
	    					<th class="text-right" width="10px"></th>
	    				</tr>
	    			</thead>
	    			<tbody>
<?php 
$total = 0;
foreach($entries_data as $entry) { 

 ?>
<tr>
	<td><?php echo $entry->type; ?></td>
	<td><?php echo date('m/d/Y', strtotime($entry->date)); ?></td>
	<td><?php echo $entry->full_name; ?></td>
	<td><?php echo $entry->memo; ?></td>
	<td class="text-right"><?php echo number_format(($entry->debit-$entry->credit),2); ?></td>
	<td class="text-right"><a href="<?php echo site_url('accounting_transactions/redirect/' . $entry->trn_id) . "?next=" . uri_string(); ?>" class="item"><span class="fa fa-eye"></span></a></td>
</tr>
<?php } ?>
	    			</tbody>
	    		</table>

<?php echo ($pagination!='') ? '<center>' . $pagination . '</center>' : ''; ?>

<?php } else { ?>
	<div class="text-center">No Account Found!</div>
<?php } ?>
	    	</div>
	    </div>
    </div>
</div>
</div>

<?php $this->load->view('footer'); ?>