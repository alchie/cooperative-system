<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<html>
<head>
  <title>Applied Payments</title>
  <link href="<?php echo base_url('assets/css/print.css'); ?>" rel="stylesheet">
</head>

<?php if( isset($output) && ($output!='iframe') ) : ?>

<body>
<div class="wrapper">

<?php else: ?>

  <body style="padding:10px">

<?php endif; ?>

<?php if( $payments ) { ?>

         <table width="100%" cellpadding="0" cellspacing="0">
          <thead>
          <tr>
            <th colspan="8">
              
<h3 class="text-center allcaps header-label">
Applied Payments
<br><small><?php echo date('F d, Y', strtotime($filters['date_range_beg'])); ?> - <?php echo date('F d, Y', strtotime($filters['date_range_end'])); ?></small>
</h3>
              
            </th>
          </tr>
            <tr>
              <th class="text-left">Lender</th>
              <th class="text-center">Receipt #</th>
              <th class="text-center">Payment Date</th>
              <th class="text-center">Loan Date</th>
              <th class="text-right">Amount Paid</th>
              <th class="text-right">Amount Applied</th>
              <th class="text-right">Applied Principal</th>
              <th class="text-right">Applied Interest</th>
            </tr>
          </thead>
          <tbody>
<?php 
$total_payments = 0;
$total_applied = 0;
$total_principal = 0;
$total_interest = 0;
$n=1;
foreach($payments as $payment) {  ?>
            <tr>
            <td class="text-left"><?php echo $n++; ?>. <?php echo $payment->lastname; ?>, <?php echo $payment->firstname; ?> <?php echo substr($payment->middlename,0,1); ?></td>
              <td class="text-center"><?php echo $payment->receipt_number; ?></td>
              <td class="text-center"><?php echo date("F d, Y", strtotime($payment->payment_date)); ?></td>
              <td class="text-center"><?php echo date("F d, Y", strtotime($payment->loan_date)); ?></td>
              <td class="text-right"><?php echo number_format($payment->amount,2); $total_payments+=$payment->amount; ?></td>
              <td class="text-right"><?php echo number_format($payment->total_applied,2); $total_applied+=$payment->total_applied; ?></td>
              <td class="text-right"><?php echo number_format($payment->total_applied_principal,2); $total_principal+=$payment->total_applied_principal; ?></td>
              <td class="text-right"><?php echo number_format($payment->total_applied_interest,2); $total_interest+=$payment->total_applied_interest; ?></td>
            </tr>
<?php } ?>
<?php if($n>2) { ?>
<tr class="overalltotal">
              <td class="text-left bold" colspan="4">Total</td>
              <td class="text-right bold"><?php echo number_format($total_payments,2);  ?></td>
              <td class="text-right bold"><?php echo number_format($total_applied,2);  ?></td>
              <td class="text-right bold"><?php echo number_format($total_principal,2);  ?></td>
              <td class="text-right bold"><?php echo number_format($total_interest,2);  ?></td>
            </tr>
<?php } ?>
          </tbody>
          </table>

<?php } else { ?>
  <div class="wrapper text-center allcaps bold">
  Empty Result
  </div>
 <?php } ?>
 

<?php if( isset($output) && ($output!='iframe') ) : ?>

</div>

<?php endif; ?>

</body>
</html>