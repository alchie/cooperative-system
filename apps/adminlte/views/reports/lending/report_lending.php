<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>

<?php $this->load->view('reports/reports_navbar'); ?>

<div class="container">
<div class="row">
	<div class="col-md-12">

	    <div class="panel panel-default">

	    	<div class="panel-heading">

            <div class="btn-group btn-group-xs pull-right">
<?php $report_type1 = (isset($report_type)) ? $report_type : 'outstanding_loans';  
$save_uri = "report_lending/save_report/{$report_type1}"; 
$modify_report_next = '';
  if( isset($current_report) && $current_report ) {
    $save_uri .= '/' . $current_report->id;
    $modify_report_next = "&next=" . uri_string();
  }
?>
<?php if(hasAccess('report', 'lending', 'edit')) { ?>
<?php if( $this->input->get('filter') ) { ?>
  <a type="button" class="btn btn-success btn-xs" href="<?php echo site_url($save_uri) . "?" . $filters_uri; ?>"><span class="fa fa-pencil"></span> Save</a>
<?php } ?>
<?php } ?>

<?php $modify_title = (isset($current_sub_page)) ? $current_sub_page : 'Modify Report'; ?>
 
  <button type="button" class="btn btn-primary btn-xs ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="<?php echo $modify_title; ?>" data-url="<?php echo site_url("report_lending/modify/{$report_type1}/ajax") . "?" . $filters_uri . $modify_report_next ; ?>"><span class="fa fa-pencil"></span> Modify</button>

  <a target="_blank" class="btn btn-warning" href="<?php echo site_url("report_lending/generate_report/{$report_type1}") . "?" . $filters_uri; ?>"><span class="fa fa-print"></span> Print</a>
</div>

	    		<h3 class="panel-title">

<?php if(hasAccess('report', 'lending', 'edit')) { ?>
<?php if( isset($current_report) && $current_report ) { ?>
<a class="confirm" href="<?php echo site_url("report_lending/delete_report/{$current_report->id}"); ?>"><span class="glyphicon glyphicon-trash"></span></a>
<?php } ?>
<?php } ?>


          Lending Reports : <strong><?php echo ((isset($filters['report_title'])) && ($filters['report_title']!='')) ? $filters['report_title'] : $modify_title; ?></strong>

<small><?php echo (isset($filters['date_range_beg'])) ? date('F d, Y', strtotime($filters['date_range_beg'])) : date('F 01, Y'); ?> - <?php echo (isset($filters['date_range_end'])) ? date('F d, Y', strtotime($filters['date_range_end'])) : date('F d, Y'); ?></small>

          </h3>

	    	</div>

            <iframe src="<?php echo $iframe_url; ?>" class="panel-body" width="100%" height="500px"></iframe>

        </div>

    </div>
</div>
</div>

<?php $this->load->view('footer'); ?>