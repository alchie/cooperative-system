<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php 
$filter_columns = (isset($filters['columns'])) ? explode('|', urldecode($filters['columns']) ) : array('lender', 'loan_date', 'principal', 'interests', 'gross_amount', 'amount_paid');
    $columns = array(
      'lender' => array('Lender','text-left'),
      'loan_date' => array('Loan Date','text-left'),
      'principal' => array('Principal','text-right'),
      'interests' => array('Total Interest','text-right'),
      'gross_amount' => array('Gross Amount','text-right'),
      'interest_type' => array('Interest Type','text-center'),
      'months' => array('Months','text-right'),
      'amount_paid' => array('Amount Paid','text-right'),
      'balance' => array('Gross Balance','text-right'),
      'principal_balance' => array('Principal Balance','text-right'),
      'interest_collected' => array('Interest Collected','text-right'),
      ); 
?>
<html>
<head>
  <title>Outstanding Loans</title>
  <link href="<?php echo base_url('assets/css/print.css'); ?>" rel="stylesheet">
</head>

<?php if( isset($output) && ($output!='iframe') ) : ?>

<body>
<div class="wrapper">

<?php else: ?>

  <body style="padding:10px">

<?php endif; ?>

<?php if( $loans ) {  ?>
          <table width="100%" cellpadding="0" cellspacing="0">
            <thead>

<?php if( isset($output) && ($output!='iframe') ) : ?>
            <tr>
              <th colspan="<?php echo count($columns); ?>">
<h3 class="text-center allcaps header-label">
<?php echo ($filters['report_title']) ? $filters['report_title'] : 'Outstanding Loans'; ?>
<br><small><?php echo date('F d, Y', strtotime($filters['date_range_beg'])); ?> - <?php echo date('F d, Y', strtotime($filters['date_range_end'])); ?></small>
</h3>
              </th>
            </tr>
<?php endif; ?>

              <tr>
<?php foreach($columns as $key=>$value) { 
if(!in_array($key, $filter_columns)) {
    continue;
}
  ?>
                <th class="column-<?php echo $key; ?> <?php echo $value[1]; ?>"><?php echo $value[0]; ?></th>
<?php } ?>
              </tr>
            </thead>
            <tbody>
            <?php 

$grand_principal = 0;
$grand_interest = 0;
$grand_gross = 0;
$grand_payments = 0;
$grand_balance = 0;
$principal_balance = 0;
$interest_collected = 0;
$n=1;

foreach($loans as $loan) { 

$interest_rate = ($loan->interest_rate) ? $loan->interest_rate : 0;
$skip = ($loan->skip_days) ? $loan->skip_days : 1;
$installments = ceil((30 / $skip) * $loan->months);
$payment_date = date('m/d/Y', strtotime($loan->payment_start));
$principal = $loan->principal;
$principal_diminishing = $loan->principal;
$principal_total = 0;
$principal_decrement = $loan->principal; 

if( $loan->interest_type == 'diminishing' ) {
  $principal_interest = $loan->principal + ((($loan->principal * ($interest_rate / 100) ) / 2 ) * ($installments + 1) * (1/ceil((30 / $skip))));
  $total_interest = ((($loan->principal * ($interest_rate / 100) ) / 2 ) * ($installments + 1) * (1/ceil((30 / $skip))));
} else {
  $interest = (($loan->principal * $interest_rate) / 100) / ceil(30 / $skip);
  $total_interest = ((($loan->principal * $interest_rate) / 100) / ceil(30 / $skip) * $installments);
  $principal_interest = $loan->principal + ($interest * $installments);
}

?>
              <tr >

<?php 

foreach($columns as $key=>$value) { 
  if(!in_array($key, $filter_columns)) {
    continue;
}

  switch ($key) {
    case 'lender':
      ?>
    <td class="text-left"><?php echo $n++; ?>. <?php echo $loan->lastname; ?>, <?php echo $loan->firstname; ?> <?php echo substr($loan->middlename,0,1); ?></td>
      <?php
      break;
    case 'loan_date':
      ?>
    <td class="text-left"><?php echo date('F d, Y', strtotime($loan->loan_date)); ?></td>
      <?php
      break;
       case 'principal':
      ?>
     <td class="text-right"><?php echo number_format($loan->principal,2); $grand_principal+=$loan->principal; ?></td>
      <?php
      break;
      case 'interests':
      ?>
    <td class="text-right"><?php echo number_format($loan->total_interest,2); $grand_interest+=$loan->total_interest; ?></td>
      <?php
      break;
      case 'gross_amount':
      ?>
    <td class="text-right"><?php echo number_format($loan->gross_amount,2); $grand_gross+=$loan->gross_amount; ?></td>
      <?php
      break;
       case 'interest_type':
      ?>
    <td class="text-center"><?php echo substr(ucwords($loan->interest_type),0,5); ?></td>
      <?php
      break;
       case 'months':
      ?>
     <td class="text-right"><?php echo $loan->months; ?></td>
      <?php
      break;
       case 'amount_paid':
      ?>
    <td class="text-right"><?php echo number_format($loan->payments,2); $grand_payments+=$loan->payments; ?></td>
      <?php
      break;
       case 'balance':
      ?>
   <td class="text-right"><?php echo number_format($loan->balance,2); $grand_balance+=$loan->balance; ?></td>
      <?php
      break;
       case 'principal_balance':
      ?>
    <td class="text-right"><?php 
                $principal_bal = ($loan->principal_balance > 0) ? $loan->principal_balance : 0;
                echo number_format($principal_bal,2); $principal_balance+=$principal_bal; ?></td>
      <?php
      break;
       case 'interest_collected':
      ?>
   <td class="text-right"><?php 
                $interest_coll = ($loan->interest_collected > 0) ? ($loan->interest_collected) : 0;
                echo number_format($interest_coll,2); $interest_collected+=$interest_coll; ?></td>
      <?php
      break;
  }

 } ?>

              </tr>

            <?php } ?>

              <tr class="overalltotal">


<?php foreach($columns as $key=>$value) { 
  if(!in_array($key, $filter_columns)) {
    continue;
}
  switch ($key) {
    case 'lender':
      ?>
   <td class="text-left"><strong>TOTAL</strong></td>
      <?php
      break;
    case 'loan_date':
      ?>
    <td class="text-left"></td>
      <?php
      break;
       case 'principal':
      ?>
    <td class="text-right bold"><?php echo number_format($grand_principal,2); ?></td>
      <?php
      break;
      case 'interests':
      ?>
      <td class="text-right bold"><?php echo number_format($grand_interest,2); ?></td>
      <?php
      break;
      case 'gross_amount':
      ?>
    <td class="text-right bold"><?php echo number_format($grand_gross,2); ?></td>
      <?php
      break;
       case 'interest_type':
      ?>
   <td class="text-left"></td>
      <?php
      break;
       case 'months':
      ?>
     <td class="text-left"></td>
      <?php
      break;
       case 'amount_paid':
      ?>
                <td class="text-right bold"><?php echo number_format($grand_payments,2); ?></td>
      <?php
      break;
       case 'balance':
      ?>

                <td class="text-right bold"><?php echo number_format($grand_balance,2); ?></td>
      <?php
      break;
       case 'principal_balance':
      ?>

                <td class="text-right bold"><?php echo number_format($principal_balance,2); ?></td>
      <?php
      break;
       case 'interest_collected':
      ?>
                <td class="text-right bold"><?php echo number_format($interest_collected,2); ?></td>
      <?php
      break;
  }

 } ?>

              </tr>

            </tbody>
          </table>

<?php } else { ?>
  <div class="wrapper text-center allcaps bold">
  Empty Result
  </div>
 <?php } ?>

<?php if( isset($output) && ($output!='iframe') ) : ?>

</div>

<?php endif; ?>

</body>
</html>