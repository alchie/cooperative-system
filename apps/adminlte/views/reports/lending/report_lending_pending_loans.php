<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<html>
<head>
  <title>Pending Loans</title>
  <link href="<?php echo base_url('assets/css/print.css'); ?>" rel="stylesheet">
</head>
<body>

<?php if( isset($output) && ($output!='iframe') ) : ?>

<div class="wrapper">
<h3 class="text-center allcaps header-label">
Pending Loans
<br><small><?php echo date('F d, Y', strtotime($filters['date_range_beg'])); ?> - <?php echo date('F d, Y', strtotime($filters['date_range_end'])); ?></small>
</h3>

<?php else: ?>

  <body style="padding:10px">

<?php endif; ?>

<?php if( $loans ) { ?>
          <table width="100%" cellpadding="0" cellspacing="0">
            <thead>
              <tr>
                <th class="text-left">Lender</th>
                <th class="text-left">Loan Date</th>
                <th class="text-right" width="10%">Principal</th>
                <th class="text-right" width="10%">Interests</th>
                <th class="text-right" width="10%">Gross Amount</th>
                <th class="text-right" width="10%">Interest Type</th>
                <th class="text-right" width="10%">Months</th>
                <th class="text-right" width="10%">Amount Paid</th>
                <th class="text-right" width="10%">Balance</th>
              </tr>
            </thead>
            <tbody>
            <?php 

$grand_principal = 0;
$grand_interest = 0;
$grand_gross = 0;
$grand_payments = 0;
$grand_balance = 0;
$n=1;
foreach($loans as $loan) { 

$interest_rate = ($loan->interest_rate) ? $loan->interest_rate : 0;
$skip = ($loan->skip_days) ? $loan->skip_days : 1;
$installments = ceil((30 / $skip) * $loan->months);
$payment_date = date('m/d/Y', strtotime($loan->payment_start));
$principal = $loan->principal;
$principal_diminishing = $loan->principal;
$principal_total = 0;
$principal_decrement = $loan->principal; 

if( $loan->interest_type == 'diminishing' ) {
  $principal_interest = $loan->principal + ((($loan->principal * ($interest_rate / 100) ) / 2 ) * ($installments + 1) * (1/ceil((30 / $skip))));
  $total_interest = ((($loan->principal * ($interest_rate / 100) ) / 2 ) * ($installments + 1) * (1/ceil((30 / $skip))));
} else {
  $interest = (($loan->principal * $interest_rate) / 100) / ceil(30 / $skip);
  $total_interest = ((($loan->principal * $interest_rate) / 100) / ceil(30 / $skip) * $installments);
  $principal_interest = $loan->principal + ($interest * $installments);
}

?>
              <tr >
                <td class="text-left"><?php echo $n++; ?>. <?php echo $loan->lastname; ?>, <?php echo $loan->firstname; ?> <?php echo substr($loan->middlename,0,1); ?></td>
                <td class="text-left"><?php echo date('F d, Y', strtotime($loan->loan_date)); ?></td>
                <td class="text-right"><?php echo number_format($loan->principal,2); $grand_principal+=$loan->principal; ?></td>
                <td class="text-right"><?php echo number_format($total_interest,2); $grand_interest+=$total_interest; ?></td>
                <td class="text-right"><?php echo number_format($loan->principal+$total_interest,2); $grand_gross+=($loan->principal+$total_interest); ?></td>
                <td class="text-right"><?php echo substr(ucwords($loan->interest_type),0,5); ?></td>
                <td class="text-right"><?php echo $loan->months; ?></td>
                <td class="text-right"><?php echo number_format($loan->payments,2); $grand_payments+=$loan->payments; ?></td>
                <td class="text-right"><?php echo number_format(($loan->principal + $total_interest) - $loan->payments,2); $grand_balance+=(($loan->principal + $total_interest) - $loan->payments); ?></td>
              </tr>
            <?php } ?>

              <tr class="overalltotal">
                <td class="text-left"><strong>TOTAL</strong></td>
                <td class="text-left"></td>
                <td class="text-right bold"><?php echo number_format($grand_principal,2); ?></td>
                <td class="text-right bold"><?php echo number_format($grand_interest,2); ?></td>
                <td class="text-right bold"><?php echo number_format($grand_gross,2); ?></td>
                <td class="text-center"></td>
                <td class="text-center"></td>
                <td class="text-right bold"><?php echo number_format($grand_payments,2); ?></td>
                <td class="text-right bold"><?php echo number_format($grand_balance,2); ?></td>
              </tr>

            </tbody>
          </table>

<?php } else { ?>
  <div class="wrapper text-center allcaps bold">
  Empty Result
  </div>
 <?php } ?>
 

<?php if( isset($output) && ($output!='iframe') ) : ?>

</div>

<?php endif; ?>

</body>
</html>