<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php 
$columns = array(
                    'id'=>'ID',
                    'dept'=>'Department',
                    'sect'=>'Section',
                    'type'=>'Type',
                    'date'=>'Date',
                    'memo'=>'Memo',
                    'number'=>'Number',
                    'full_name'=>'Name',
                    'debit'=>'Debit',
                    'credit'=>'Credit',
                    'amount'=>'Amount',
                    );
?>
<?php if( isset($output) && ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('reports/reports_navbar'); ?>

<div class="container">
<div class="row">
	<div class="col-md-6 col-md-offset-3">

<form method="post">

	    <div class="panel panel-default">

	    	<div class="panel-heading">

	    		<h3 class="panel-title">Modify Transaction Report</h3>

	    	</div>
	    	<div class="panel-body">

<?php endif; ?>



<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingHeader">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseHeader" aria-expanded="true" aria-controls="collapseHeader">
          Header
        </a>
      </h4>
    </div>
    <div id="collapseHeader" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingHeader">
      <div class="panel-body">
        
            <div class="form-group ">
            <label class="control-label">Report Title</label>
            <input class="form-control" type="text" name="filter[report_title]" value="<?php echo (isset($filters['report_title'])) ? $filters['report_title'] : 'Transactions Report'; ?>">
            </div>

      </div>
    </div>
  </div>


  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingDateRange">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseDateRange" aria-expanded="false" aria-controls="collapseDateRange">
          Date Range
        </a>
      </h4>
    </div>
    <div id="collapseDateRange" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingDateRange">
      <div class="panel-body">
        <div class="row">
    <div class="col-md-6">
        <div class="form-group ">
            <label class="control-label">Beginning</label>
            <input class="form-control datepicker text-center" type="text" name="filter[date_range_beg]" value="<?php echo (isset($filters['date_range_beg'])) ? $filters['date_range_beg'] : date('m/01/Y'); ?>">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group ">
            <label class="control-label">Ending</label>
            <input class="form-control datepicker text-center" type="text" name="filter[date_range_end]" value="<?php echo (isset($filters['date_range_end'])) ? $filters['date_range_end'] : date('m/d/Y'); ?>">
        </div>
    </div>
</div>

      </div>
    </div>
  </div>

  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingSort">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSort" aria-expanded="false" aria-controls="collapseSort">
          Sort
        </a>
      </h4>
    </div>
    <div id="collapseSort" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSort">
      <div class="panel-body">
        
        <div class="row">
    <div class="col-md-6">
        <div class="form-group ">
            <label class="control-label">Order By</label>
                 <select name="filter[order_by]" class="form-control" title="Select Report">

                 <?php 
                 $filter_order_by = ($filters['order_by']) ? $filters['order_by'] : 'date';
                 $filter_columns = ($filters['columns']) ? explode('|', urldecode($filters['columns']) ) : array('type', 'date', 'number', 'full_name', 'memo', 'amount');
                 foreach($columns as $key=>$value) { 
                    if(!in_array($key, $filter_columns)) {
                        continue;
                    }
                    echo "<option value=\"{$key}\"";
                        echo ($filter_order_by==$key) ? " SELECTED" : "";
                    echo ">{$value}</option>";
                 } ?>

                </select>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group ">
            <label class="control-label">Sort Order</label>
            <select name="filter[sort]" class="form-control" title="Select Report">
               <?php 
$filter_sort = ($filters['sort']) ? $filters['sort'] : 'DESC';
               foreach(array(
                    'ASC'=>'Ascending',
                    'DESC'=>'Descending',
                    ) as $key=>$value) { 
                    echo "<option value=\"{$key}\"";
                    echo ($filter_sort==$key) ? " SELECTED" : "";
                    echo ">{$value}</option>";
                 } ?>
            </select>
        </div>
    </div>
</div>

      </div>
    </div>
  </div>

    <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingColumns">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseColumns" aria-expanded="false" aria-controls="collapseColumns">
          Columns
        </a>
      </h4>
    </div>
    <div id="collapseColumns" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingColumns">
      <div class="panel-body">
        <div class="row">
        <?php 
        $filter_columns = (isset($filters['columns'])) ? explode('|', urldecode($filters['columns']) ) : array('type', 'date', 'number', 'full_name', 'memo', 'amount');
        foreach($columns as $key=>$value) { 
            ?>
            <div class="col-md-4"><div class="form-group "><label class="control-label"><input <?php echo (in_array($key, $filter_columns)) ? 'CHECKED' : ''; ?> type="checkbox" name="filter[columns][]" value="<?php echo $key; ?>"> <?php echo $value; ?></label></div></div>
        <?php } ?>
        </div>
      </div>
    </div>
  </div>


  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingdept">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsedept" aria-expanded="false" aria-controls="collapsedept">
          Department Filter
        </a>
      </h4>
    </div>
    <div id="collapsedept" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingdept">
      <div class="panel-body">
        <div class="row">
        <?php 
        $depts = array();
        foreach($departments as $dept) {
            $depts[] = $dept->dept;
        }
        $filter_depts = (isset($filters['departments'])) ? explode('|', urldecode($filters['departments']) ) : $depts; 
        foreach($depts as $value) { 
            ?>
            <div class="col-md-4"><div class="form-group "><label class="control-label"><input <?php echo (in_array($value, $filter_depts)) ? 'CHECKED' : ''; ?> type="checkbox" name="filter[departments][]" value="<?php echo $value; ?>"> <?php echo $value; ?></label></div></div>
        <?php } ?>
        </div>
      </div>
    </div>
  </div>  
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingsect">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsesect" aria-expanded="false" aria-controls="collapsesect">
          Section Filter
        </a>
      </h4>
    </div>
    <div id="collapsesect" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingsect">
      <div class="panel-body">

                <div class="row">
        <?php 
        $sects = array();
        foreach($sections as $sect) {
            $sects[] = $sect->sect;
        }
        $filter_sects = (isset($filters['sections'])) ? explode('|', urldecode($filters['sections']) ) : $sects; 
        foreach($sects as $value) { 
            ?>
            <div class="col-md-4"><div class="form-group "><label class="control-label"><input <?php echo (in_array($value, $filter_sects)) ? 'CHECKED' : ''; ?> type="checkbox" name="filter[sections][]" value="<?php echo $value; ?>"> <?php echo $value; ?></label></div></div>
        <?php } ?>
        </div>

      </div>
    </div>
  </div>  
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingtype">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsetype" aria-expanded="false" aria-controls="collapsetype">
          Type Filter
        </a>
      </h4>
    </div>
    <div id="collapsetype" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingtype">
      <div class="panel-body">

                <div class="row">
        <?php 
        $types_ar = array();
        foreach($types as $type) {
            $types_ar[] = $type->type;
        }
        $filter_types = (isset($filters['types'])) ? explode('|', urldecode($filters['types']) ) : $types_ar; 
        foreach($types_ar as $value) { 
            ?>
            <div class="col-md-4"><div class="form-group "><label class="control-label"><input <?php echo (in_array($value, $filter_types)) ? 'CHECKED' : ''; ?> type="checkbox" name="filter[types][]" value="<?php echo $value; ?>"> <?php echo $value; ?></label></div></div>
        <?php } ?>
        </div>

      </div>
    </div>
  </div>  
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingmemo">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsememo" aria-expanded="false" aria-controls="collapsememo">
          Memo Filter
        </a>
      </h4>
    </div>
    <div id="collapsememo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingmemo">
      <div class="panel-body">
        <input class="form-control" type="text" name="filter[memo]" value="<?php echo (isset($filters['memo'])) ? $filters['memo'] : ""; ?>">
      </div>
    </div>
  </div>  
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingnumber">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsenumber" aria-expanded="false" aria-controls="collapsenumber">
          Number Filter
        </a>
      </h4>
    </div>
    <div id="collapsenumber" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingnumber">
      <div class="panel-body">
        <input class="form-control" type="text" name="filter[number]" value="<?php echo (isset($filters['number'])) ? $filters['number'] : ""; ?>">
      </div>
    </div>
  </div>  
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingname">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsename" aria-expanded="false" aria-controls="collapsename">
          Name Filter
        </a>
      </h4>
    </div>
    <div id="collapsename" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingname">
      <div class="panel-body">
            <input id="report_transactions_name_id" name="filter[name_id][]" type="hidden" value="<?php echo (isset($filter_name)) ? $filter_name->id : ''; ?>">
            <input class="form-control autocomplete-name_select autocomplete-name_select-name-input-<?php echo time(); ?>" data-source="<?php echo site_url("report_transactions/ajax/name"); ?>" data-name_id="report_transactions_name_id" type="text" <?php echo (isset($filter_name)) ? 'style="display:none;"' : ''; ?>>
            <?php if(isset($filter_name)) { ?>
            <div class="form-control autocomplete-name_select-name-display-<?php echo time(); ?>"><a class="badge" id="changeName" href="#changeName" data-id="<?php echo $filter_name->id; ?>" data-name_id="report_transactions_name_id" data-timestamp="<?php echo time(); ?>"><?php echo $filter_name->full_name; ?></a></div>
            <?php } ?>
      </div>
    </div>
  </div>  


  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingFooter">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFooter" aria-expanded="false" aria-controls="collapseFooter">
          Footer
        </a>
      </h4>
    </div>
    <div id="collapseFooter" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFooter">
      <div class="panel-body">
        
      </div>
    </div>
  </div>

</div>

<?php if( isset($output) && ($output!='ajax') ) : ?>
	    	</div>
	    	<div class="panel-footer">
	    		<button type="submit" class="btn btn-success">Generate Report</button>
	    	</div>
	    </div>

</form>

    </div>
</div>
</div>

<?php $this->load->view('footer'); ?>

<?php endif; ?>