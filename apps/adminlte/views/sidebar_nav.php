<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu">
        <li class="header">NAVIGATION</li>
<?php 

$main_menu = array(
  'membership' => array(
      'title' => 'Membership',
      'uri' => 'membership',
      'permission' => 'membership',
      'icon' => 'fa fa-users',
      'sub_menus' => array(
          'membership_members' => array(
            'title' => 'Members',
            'uri' => 'membership_members',
            'permission' => 'members',
          ),
          'membership_companies' => array(
            'title' => 'Companies',
            'uri' => 'membership_companies',
            'permission' => 'companies',
          )
        )
    ),

  'services' => array(
      'title' => 'Services',
      'uri' => 'services',
      'permission' => 'services',
      'icon' => 'fa fa-coffee',
      'sub_menus' => array(
          'services_shares' => array(
            'title' => 'Share Capital',
            'uri' => 'services_shares',
            'permission' => 'shares',
          ),
          'services_lending' => array(
            'title' => 'Lending',
            'uri' => 'services_lending',
            'permission' => 'lending',
          ),
        )
    ),
/*
  'marketing' => array(
      'title' => 'Marketing',
      'uri' => 'marketing',
      'sub_menus' => array(
          'marketing_mlm' => array(
            'title' => 'Multi-level Marketing',
            'uri' => 'marketing_mlm',
          ),
        )
    ),
*/
  'accounting' => array(
      'title' => 'Accounting',
      'uri' => 'accounting',
      'permission' => 'accounting',
      'icon' => 'fa fa-book',
      'sub_menus' => array(
          'accounting_charts' => array(
            'title' => 'Chart of Accounts',
            'uri' => 'accounting_charts',
            'permission' => 'chart_of_accounts',
          ),
          'accounting_sales_receipts' => array(
            'title' => 'Sales Receipts',
            'uri' => 'accounting_sales_receipts',
            'permission' => 'sales_receipts',
          ),
          'accounting_write_checks' => array(
            'title' => 'Write Checks',
            'uri' => 'accounting_write_checks',
            'permission' => 'write_checks',
          ),
          'accounting_deposits' => array(
            'title' => 'Deposits',
            'uri' => 'accounting_deposits',
            'permission' => 'deposits',
          ),
          'accounting_reconciliation' => array(
            'title' => 'Bank Reconciliation',
            'uri' => 'accounting_reconciliation',
            'permission' => 'reconciliation',
          ),
          'accounting_general_journals' => array(
            'title' => 'General Journals',
            'uri' => 'accounting_general_journals',
            'permission' => 'general_journals',
          ),
        )
    ),

  'reports' => array(
    	'title' => 'Reports',
      	'uri' => 'reports',
      	'permission' => 'report',
      	'icon' => 'fa fa-print',
       'sub_menus' => array(
          'report_balance_sheet' => array(
            'title' => 'Balance Sheet',
            'uri' => 'report_balance_sheet',
            'permission' => 'balance_sheet',
          ),
          'report_income_statement' => array(
            'title' => 'Income Statement',
            'uri' => 'report_income_statement',
            'permission' => 'income_statement',
          ),
          'report_cash_flow' => array(
            'title' => 'Cash Flow Statement',
            'uri' => 'report_cash_flow',
            'permission' => 'cash_flow_statement',
          ),
          'report_ar_aging' => array(
            'title' => 'A/R Aging',
            'uri' => 'report_ar_aging',
            'permission' => 'accounts_receivable_aging',
          ),
          'report_transactions' => array(
            'title' => 'Transactions',
            'uri' => 'report_transactions',
            'permission' => 'transactions',
          ),
            'report_entries' => array(
            'title' => 'Entries',
            'uri' => 'report_entries',
            'permission' => 'entries',
          ),
          'report_lending' => array(
            'title' => 'Lending',
            'uri' => 'report_lending',
            'permission' => 'lending',
          ),
        ),
    ),

  'system' => array(
      'title' => 'System',
      'uri' => 'system',
      'permission' => 'system',
      'icon' => 'fa fa-cogs',
      'sub_menus' => array(
          'system_names' => array(
            'title' => 'Names List',
            'uri' => 'system_names',
            'permission' => 'names',
          ),
          'system_users' => array(
            'title' => 'User Accounts',
            'uri' => 'system_users',
            'permission' => 'users',
          ),
          'system_settings' => array(
            'title' => 'Settings',
            'uri' => 'system_settings',
            'permission' => 'settings',
          ),
          'system_backup' => array(
            'title' => 'Database Backup',
            'uri' => 'system_backup',
            'permission' => 'backup',
          ),
        )
    ),
);

foreach($main_menu as $main=>$menu): 
  if( ! isset( $menu['permission'] ) ) {
    continue;
  }
  if( ! isset( $this->session->menu_module[$menu['permission']] ) ) {
    continue;
  }
?>
          <li class="treeview">
              <a href="#<?php echo $menu['uri']; ?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
              <i class="<?php echo (isset($menu['icon'])) ? $menu['icon'] : 'fa fa-arrow-right'; ?>"></i> <span><?php echo $menu['title']; ?></span>
			<span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
              </a>
                  <ul class="treeview-menu">
                  <?php foreach($menu['sub_menus'] as $sub=>$sub_menu): 
                    if( ! isset( $sub_menu['permission'] ) ) {
                      continue;
                    }
                    if( ($sub_menu['permission']) && (! in_array($sub_menu['permission'], $this->session->menu_module[$menu['permission']] ) ) ) {
                      continue;
                    }
                  ?>
                    <?php if( isset($sub_menu['header']) && ($sub_menu['header']) ) { ?>
                        <?php if( isset($sub_menu['separator']) && ($sub_menu['separator']) ) { ?>
                          <li role="separator" class="divider"></li>
                        <?php } ?>
                        <?php if( isset($sub_menu['title']) && ($sub_menu['title'] != '') ) { ?>
                          <span class="dropdown-header"><?php echo $sub_menu['title']; ?></span>
                        <?php } ?>
                    <?php } else { ?>
                    <li><a class="body_wrapper" href="<?php echo site_url($sub_menu['uri']); ?>"><?php echo $sub_menu['title']; ?></a></li>
                    <?php } ?>
                  <?php endforeach; ?>
                  </ul>
          </li>
<?php endforeach; ?>

      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>