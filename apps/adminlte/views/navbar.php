<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">

          <li class="dropdown tasks-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning">0</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">Due invoices</li>
              <li>
                <ul class="menu">
                  <li>
                  
                  </li>
                </ul>
              </li>
              <li class="footer"><a class="body_wrapper" href="<?php echo site_url("services_lending/index/open_invoices"); ?>">View all</a></li>
            </ul>
          </li>

          <li class="dropdown tasks-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-flag-o"></i>
              <span class="label label-danger">0</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">Tasks</li>
              <li>
                <ul class="menu">
                  <li>
                  
                  </li>
                </ul>
              </li>
              <li class="footer"><a class="body_wrapper" href="<?php echo site_url("services_lending/index/open_invoices"); ?>">View all</a></li>
            </ul>
          </li>


          <!-- User Account Menu -->
          <li class="dropdown user user-menu">
            <!-- Menu Toggle Button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-user"></i>
              <span class="hidden-xs"><?php echo $this->session->name; ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- The user image in the menu -->
              <li class="user-header">
                <p>
                  <?php echo $this->session->name; ?>
                </p>
              </li>
              <!-- Menu Body -->
              <li class="user-body">
                <div class="row">
                  <div class="col-xs-6 text-center">
                    <a href="javascript:void(0);" class="ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Account Settings" data-url="<?php echo site_url("account/settings/ajax") . "?next=" . uri_string(); ?>">Account Settings</a>
                  </div>
                  <div class="col-xs-6 text-center">
                    <a href="javascript:void(0);" class="ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Change Password" data-url="<?php echo site_url("account/change_password/ajax") . "?next=" . uri_string(); ?>">Change Password</a>
                  </div>
                </div>
                <!-- /.row -->
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  
                </div>
                <div class="pull-right">
                  <a href="<?php echo site_url('account/logout') . "?next=" . urlencode( uri_string() ); ?>" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>